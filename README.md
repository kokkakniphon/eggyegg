<p align="center">
  <h1>EggYegg Game</h1>
  <p align="center"><strong>EggYegg is a 2D game project created using DucklingEngine. It showcases the engine's capabilities and provides a practical example of its usage.</strong></p>
  <p align="center"><img src="./Readme/EggYegg-Thumbnail.png" alt="EggYegg Logo"></p>
</p>

[EggYegg Gameplay](https://youtu.be/-IFYGY3M2Kk) 

# DucklingEngine

**DucklingEngine** is a versatile, custom 2D game engine written in C++ and built on top of the Simple DirectMedia Layer (SDL). It provides a strong foundation for developing 2D games and interactive applications. DucklingEngine is licensed under the Apache 2.0 license.

**EggYegg Preview**

![EggYegg Preview](./Readme/DucklingEngine_Preview.png)

**Zuma Preview**

![Zuma Preview](./Readme/DucklingEngine_2_Preview.png)

[Zuma Clone - Git](https://bitbucket.org/kokkakniphon/zuma-clone/src/master/)

## Features

- **Entity-Component-System (ECS):** DucklingEngine utilizes an ECS architecture to manage game entities and components efficiently.

- **Custom 2D Physics Engine:** A custom physics system is integrated to handle 2D physics simulations in your games.

- **Custom Audio System (KHORA):** KHORA is a separate project for managing audio and sound in DucklingEngine, offering flexibility and ease of use. [Check out KHORA](https://bitbucket.org/kokkakniphon/khora/src/master/).

- **Rendering System:** Enjoy efficient 2D rendering capabilities with DucklingEngine, including support for sprite handling and animations.

- **Scene System:** Organize your game content using the scene system for seamless transitions and management.

- **Animation System:** Create dynamic and interactive animations to breathe life into your games.

- **Editor GUI (using ImGui):** A built-in editor GUI is available, powered by ImGui, making game development more accessible.

- **Engine Layer Separation:** Keep your game project organized and maintain a clear separation between the engine and project layers.

### Project Contributors

- **Niphon Prasopphol** - Lead Programmer / Engine Designer
  - LinkedIn: [Niphon Prasopphol's Profile](https://www.linkedin.com/in/kokkak-niphon-48a498204/)

- **Nattamon Chungwatana** - Lead Artist / Programmer
  - LinkedIn: [Nattamon Chungwatana's Profile](https://www.linkedin.com/in/nattamon-chungwatana-2b94b912a/)

- **Jettanat Trivichanan** - Lead Designer
  - LinkedIn: [Jettanat Trivichanan's Profile](https://www.linkedin.com/in/jettanat-trivichanan-9a9714186/)

## Note
- **This game project is created during Year 2 at KMUTT** so the code is **NOT** clean and efficient.
- DucklingsEngine folder contain the source project
- DucklingsEngine-Template contain the visual studio custom template for editor.

## License

DucklingEngine is distributed under the terms of the Apache 2.0 license.

---

We hope you find DucklingEngine and the EggYegg project useful and inspiring for your game development endeavors.