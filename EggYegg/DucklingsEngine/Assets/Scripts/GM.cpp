#include "GM.h"

namespace DucklingsEngine
{

	void GM::UpdateGM()
	{
		musicPlayer.Update();
		playerSfxPlayer.Update();
		uiSfxPlayer.Update();
	}
	void GM::UpdateVolume()
	{
		musicPlayer.SetVolume(MusicVolume * 100 * MasterVolume);
		playerSfxPlayer.SetVolume(SfxVolume * 100 * MasterVolume);
		uiSfxPlayer.SetVolume(SfxVolume * 100 * MasterVolume);
	}
}