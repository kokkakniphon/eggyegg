#pragma once
#include "MonoBehaviour.h"
#include "TextRenderer.h"
#include "Transform.h"
#include "Input.h"

namespace DucklingsEngine
{
	class DuckUI : public MonoBehaviour
	{
	private:
		TextRenderer* bulletText;

		GameObject* duckHealthBar;
		TextRenderer* duckHealthText;
		GameObject* aimCurser;
	public:
		DuckUI() = default;
		~DuckUI() = default;

		inline static DuckUI* instance = nullptr;

		void Start() override
		{
			instance = this;
#ifndef _DEBUG
			SDL_ShowCursor(SDL_DISABLE);
#endif
		}

		void Update() override
		{
			this->aimCurser->transform->SetPosition(Input::GetMousePosition() + Camera::mainCamera->GetTransform()->GetPosition() - Camera::mainCamera->rect/2);
		}

		void Setup(TextRenderer* bullet, GameObject* healthBar, TextRenderer* healthText, GameObject* aimCurser)
		{
			this->bulletText = bullet;
			this->duckHealthBar = healthBar;
			this->duckHealthText = healthText;
			this->aimCurser = aimCurser;
		}

		void UpdateBulletUI(int amount)
		{
			bulletText->text = std::to_string(amount);
			bulletText->RefreshText();
		}
		
		void UpdateHealthUI(int health, int maxhealth)
		{
			float healthRatio = (float)health / (float)maxhealth;
			duckHealthBar->transform->localPosition.x = healthRatio * 128 - 128;
			duckHealthText->text = std::to_string(health) + "/" + std::to_string(maxhealth);
			duckHealthText->RefreshText();
			duckHealthBar->transform->localScale.x = healthRatio;
		}

	};
}

