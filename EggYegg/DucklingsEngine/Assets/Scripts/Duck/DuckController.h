#pragma once
#include "MonoBehaviour.h"

#include "Input.h"
#include "Animator.h"
#include "Scripts/Amadillo/Ball.h"
#include "Camera.h"
#include "Scripts/Core/Character.h"
#include "TextRenderer.h"

namespace DucklingsEngine
{
	class DuckController : public Character
	{
	private:
#pragma region General Settings
		float hit_force = 900.0f;
		float max_charge = 100;
		float current_charge = 100;

		Vector2 move_dir;
		Vector2 aim_dir;

		bool isInRange;
		float hitCoolDown = 0.5f;

		float head_spin_speed = 800;

		// Timer
		float timer_quack = 0.0f;
		float quack_duration = 0.6f;

		float chargeup_speed = 10.0f;

		float currentInvi_time = 0.0f;
		float inviDuration = 1.0f;

		//static float current_health;
		//Bullet setting

		int b_count;
		int b_maxBullet = 50;
		int b_startAmount = 15;

		const float b_speed = 1000.0f;
		const float b_damage = 20.0f;
		float shootCoolDown = 0.25f;

		bool hitted = false;


#pragma endregion


#pragma region Functions

		void GetMovementInput();

		void SpawnBullet();

		void GetActionInput();

		void GetAnimDirection();

		void UpdateChargeVisual();

		void SpinDuckhead(bool isSpinning);

		void OnTriggerEnter();

#pragma endregion


	public:
		DuckController() { }
		~DuckController() = default;

		Animator* anim;
		GameObject* ballPointer;
		Transform* currentTrackTransform;

		void AddBullet(int amount);
		
		void Quack();

		void Start() override;

		void Update() override;

		void FixedUpdate() override;

		void Hitted(int damage);

		void Healed(int amount);

		void ChargeUp(float amount);

		int GetBullet() { return b_count; }

		float speed = 400.0f;

		Ball* ball;
		Rigidbody* rid2D;

		GameObject* quack;
		GameObject* aim_obj;
		GameObject* duckHead;
		GameObject* chargeBar;

	};

}

