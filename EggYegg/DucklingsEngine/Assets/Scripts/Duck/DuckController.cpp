#include "DuckController.h"

#include "Input.h"
#include "Camera.h"
#include "DuckUI.h"
#include "Scripts/Amadillo/Ball.h"
#include "CircleCollider.h"
#include "Scripts/Boss/ProjectileObject.h"
#include "TextRenderer.h"
#include "Scripts/Drop/Drop.h"
#include "Scripts/WinLoseScreen.h"

#include "AnimationController/DuckAnimationController.h"
#include "Scripts/Tutorial/TutorialController.h"

#include "Scripts/GM.h"

namespace DucklingsEngine
{
	void DuckController::Start()
	{
		this->gameObject->tag = "Player";
		anim = &this->gameObject->GetComponent<Animator>();
		anim->SetAnimationController(std::make_shared<DuckAnimationController>());
		b_count = b_startAmount;

		m_Health = 8;
		m_MaxHealth = 8;

		current_charge = max_charge;
		UpdateChargeVisual();
	}

	void DuckController::Update()
	{
		// This will execute every frame in update.
		currentTrackTransform = this->transform;

		ChargeUp(chargeup_speed * DuckTime::GetDeltaTime());

		OnTriggerEnter();

		if (hitCoolDown > 0)
		{
			hitCoolDown -= DuckTime::GetDeltaTime();
		}

		if (shootCoolDown >= 0.0f)
		{
			shootCoolDown -= DuckTime::GetDeltaTime();
		}

		if (currentInvi_time > 0)
			currentInvi_time -= DuckTime::GetDeltaTime();
		else if (hitted == true)
		{
			hitted = false;
			this->gameObject->GetComponent<SpriteRenderer>().color = Color();
			duckHead->GetComponent<SpriteRenderer>().color = Color();
		}


		GetMovementInput();
		GetActionInput();
		GetAnimDirection();
		Quack();

		if (Input::GetMouseButton(SDL_BUTTON_LEFT))
		{
			if (isInRange && hitCoolDown <= 0)
			{
				ball->Hitted(aim_dir, hit_force);
				UpdateChargeVisual();
				hitCoolDown = 0.5f;
			}
			SpinDuckhead(true);
		}
		else
		{
			SpinDuckhead(false);
		}

		anim->SetPlay(move_dir != Vector2());


		if (Input::GetMouseButtonDown(SDL_BUTTON_RIGHT) && ball != nullptr && current_charge >= max_charge/2)
		{
			//ball->Hitted(Vector2(this->transform->GetPosition() - ball->transform->GetPosition()).normalized(), hit_force);
			if (move_dir != Vector2())
			{
				ball->rid2D->velocity = move_dir * ball->dashSpeed;
			}
			else
			{
				ball->rid2D->velocity = Vector2(cos(this->transform->localEulerAngles), sin(this->transform->localEulerAngles)).normalized() * ball->dashSpeed;
			}
			GM::GetInstance().playerSfxPlayer.SetBool("Bounce", false);
			GM::GetInstance().playerSfxPlayer.SetBool("Shooting", false);
			GM::GetInstance().playerSfxPlayer.SetBool("IsQuack", true);
			GM::GetInstance().playerSfxPlayer.SetBool("Hitted", false);
			GM::GetInstance().playerSfxPlayer.Play();

			hitted = false;
			this->gameObject->GetComponent<SpriteRenderer>().color = Color();
			duckHead->GetComponent<SpriteRenderer>().color = Color();
			current_charge -= max_charge/2;
			if (SceneManager::GetInstance().GetLatestActiveScene()->name == "Tutorial" && current_charge < max_charge / 2)
				current_charge += max_charge / 2;
			UpdateChargeVisual();
			timer_quack = quack_duration;
			//Activate ball and deactivate self
			ballPointer->transform->SetPosition(this->transform->GetPosition());
			this->gameObject->SetActive(false);
			aim_obj->SetActive(false);
			ballPointer->SetActive(true);
			currentTrackTransform = ballPointer->transform;
		}
	}

	void DuckController::FixedUpdate()
	{
		aim_obj->transform->SetPosition(this->transform->GetPosition());

		rid2D->velocity = move_dir * speed;
		rid2D->angularVelocity = 0.0f;
		aim_obj->transform->localEulerAngles = (atan2(aim_dir.y, aim_dir.x) * 180 / M_PI) + 90;
		//aim_obj->transform->localEulerAngles = (std::abs(this->transform->localEulerAngles) != 90) ? (atan2(aim_dir.y, aim_dir.x) * 180 / M_PI) + 90 + this->transform->localEulerAngles : (atan2(aim_dir.y, aim_dir.x) * 180 / M_PI) + 90 + this->transform->localEulerAngles + 180;

		if (move_dir != Vector2())
			this->transform->localEulerAngles = (atan2(move_dir.y, move_dir.x) * 180 / M_PI) + 180;
	}

	void DuckController::Hitted(int damage)
	{
		if (currentInvi_time > 0)
			return;
		Character::Hitted(damage);
		GM::GetInstance().playerSfxPlayer.SetBool("Bounce", false);
		GM::GetInstance().playerSfxPlayer.SetBool("IsQuack", false);
		GM::GetInstance().playerSfxPlayer.SetBool("Hitted", true);
		GM::GetInstance().playerSfxPlayer.Play();

		hitted = true;
		currentInvi_time = inviDuration;
		this->gameObject->GetComponent<SpriteRenderer>().color = Color(1.0f, 0.5f, 0.5f);
		duckHead->GetComponent<SpriteRenderer>().color = Color(1.0f, 0.5f, 0.5f);

		DuckUI::instance->UpdateHealthUI(m_Health, m_MaxHealth);

		if (m_Health <= 0)
		{
			WinLoseScreen::IsGoodEnd = false;
			SceneManager::GetInstance().OpenScene("EndingScene");
		}
	}

	void DuckController::Healed(int amount)
	{
		Character::Healed(amount);

		DuckUI::instance->UpdateHealthUI(m_Health, m_MaxHealth);
	}

	void DuckController::ChargeUp(float amount)
	{
		current_charge = Mathf::min(current_charge + amount, max_charge);
		UpdateChargeVisual();
	}

	void DuckController::AddBullet(int amount)
	{
		if (SceneManager::GetInstance().GetLatestActiveScene()->name == "Tutorial" && amount > 0)
			TutorialController::_tutorial_PICKEDUP = true;

		b_count += amount;
		// Set it to not lower than 0 and not greater than max
		b_count = Mathf::clamp(b_count, 0, b_maxBullet);
		DuckUI::instance->UpdateBulletUI(b_count);
	}

	void DuckController::GetMovementInput()
	{
		if (DuckTime::GetInstance().timeScale <= 0)
			return;

		Vector2 input;
		if (Input::GetKey(SDL_SCANCODE_W))
		{
			input.y = -1.0f;
		}
		else if (Input::GetKey(SDL_SCANCODE_S))
		{
			input.y = 1.0f;
		}

		if (Input::GetKey(SDL_SCANCODE_A))
		{
			input.x = -1.0f;
		}
		else if (Input::GetKey(SDL_SCANCODE_D))
		{
			input.x = 1.0f;
		}

		if (input.x != 0 && input.y != 0)
			input = input * 1 / 1.44f;

		move_dir = input;
	}

	void DuckController::SpawnBullet()
	{
		GM::GetInstance().playerSfxPlayer.SetBool("Hitted", false);
		GM::GetInstance().playerSfxPlayer.SetBool("Bounce", false);
		GM::GetInstance().playerSfxPlayer.SetBool("Shooting", true);
		GM::GetInstance().playerSfxPlayer.SetBool("IsQuack", false);
		GM::GetInstance().playerSfxPlayer.Play();

		GameObject* proj = new GameObject("DuckProjectile Clone");
		proj->AddComponent<SpriteRenderer>("blue_energy").order = 5;
		proj->AddComponent<CircleCollider>().radius = 23.65f;
		proj->AddComponent<ProjectileObject>();
		proj->transform->localScale = Vector2(0.5f, 0.5f);
		proj->GetComponent<CircleCollider>().isTrigger = true;

		proj->GetComponent<ProjectileObject>().Setup(aim_dir, duckHead->transform->GetPosition(), this->gameObject->name, "solid_layer", "Chicken", "", b_speed, b_damage);
		proj->GetComponent<ProjectileObject>().SetDuration(2.0f);
	}

	void DuckController::GetActionInput()
	{
		//Shoot bullets
		if (Input::GetMouseButton(SDL_BUTTON_LEFT))
		{
			if (shootCoolDown < 0.0f && b_count > 0)
			{
				AddBullet(-1);
				SpawnBullet();
				shootCoolDown = 0.25f;
			}
		}
	}

	void DuckController::GetAnimDirection()
	{
		Vector2 currentMouse_pos = Camera::mainCamera->WorldToScreenPoint(Input::GetMousePosition());
		Vector2 currentDuck_pos = this->transform->GetPosition();
		aim_dir = currentMouse_pos - currentDuck_pos;
		aim_dir.Normalize();
	}

	void DuckController::UpdateChargeVisual()
	{
		if (chargeBar == nullptr)
			return;

		float chargeRatio = (current_charge / max_charge);
		chargeBar->transform->localPosition.x = chargeRatio * 60 - 60;
		chargeBar->transform->localScale.x = chargeRatio * 3.644f;
	}

	void DuckController::SpinDuckhead(bool isSpinning)
	{
		if (isSpinning == true)
		{
			duckHead->transform->localEulerAngles += DuckTime::GetDeltaTime() * head_spin_speed;
			duckHead->transform->localEulerAngles = (int)duckHead->transform->localEulerAngles % 360;
		}
		else
		{
			if (duckHead->transform->localEulerAngles > 0)
			{
				duckHead->transform->localEulerAngles += DuckTime::GetDeltaTime() * head_spin_speed;
				duckHead->transform->localEulerAngles = std::min(360.0f, duckHead->transform->localEulerAngles);
				duckHead->transform->localEulerAngles = (int)duckHead->transform->localEulerAngles % 360;
			}
		}
	}

	void DuckController::Quack()
	{
		if (timer_quack > 0)
		{
			if (quack->GetActive() == false)
				quack->SetActive(true);
			quack->transform->SetPosition(this->transform->GetPosition());
			timer_quack -= DuckTime::GetDeltaTime();

			if (timer_quack > quack_duration / 2)
			{
				Vector2 fromValue = Vector2();
				Vector2 toValue = Vector2(1.0f, 1.0f);
				quack->transform->localScale = Vector2::Lerp(fromValue, toValue, (1 - ((timer_quack - quack_duration / 2) / (quack_duration / 2))));
			}
			else
			{
				Vector2 toValue = Vector2();
				Vector2 fromValue = Vector2(1.0f, 1.0f);
				quack->transform->localScale = Vector2::Lerp(fromValue, toValue, (1 - (timer_quack / (quack_duration / 2))));
			}
		}
		else
		{
			if (quack->GetActive() == true)
				quack->SetActive(false);
		}
	}

	void DuckController::OnTriggerEnter()
	{

		if (currentInvi_time > 0)
			return;

		std::vector<Collider*> allCollider = this->gameObject->GetComponent<CircleCollider>().collidedColliders;

		for (size_t i = 0; i < allCollider.size(); i++)
		{
			if (allCollider[i]->gameObject->tag == "Chicken")
			{
				Hitted(1);
			}
			else if (allCollider[i]->gameObject->name == "Chicken Boss")
			{
				Hitted(2);
			}
		}
		
	}
}