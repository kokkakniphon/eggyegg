#pragma once
#include "MonoBehaviour.h"
#include "GameObject.h"
#include "DuckTime.h"
#include "Mathf.h"
#include "SpriteRenderer.h"
#include "SceneManager.h"

namespace DucklingsEngine
{
	class SplashScreen : public MonoBehaviour
	{
	private:
		bool fadeIn = true;
		float pauseTimer;
		float pauseDuration = 1.0f;

	public:
		SplashScreen() = default;
		~SplashScreen() = default;

		GameObject* logos[3];

		int currentLogo = 0;

		void Start() override
		{
			// This will execute once at the begginning of the scene.
			for (size_t i = 0; i < 3; i++)
			{
				logos[i]->GetComponent<SpriteRenderer>().color.a = 0;
			};
		}
		void Update() override
		{
			// This will execute every frame in update.
			if (fadeIn == true)
			{
				if (FadeIn(logos[currentLogo]))
				{
					fadeIn = false;
					pauseTimer = pauseDuration;
				}
			}
			else if (pauseTimer > 0)
			{
				pauseTimer -= DuckTime::GetDeltaTime();
			}
			else if (FadeOut(logos[currentLogo]))
			{
				currentLogo++;
				fadeIn = true;
				if (currentLogo == 3)
					SceneManager::GetInstance().OpenScene("NewTitleScene");
			}
		}

		bool FadeIn(GameObject* obj)
		{
			//return true uf completely fade in
			SpriteRenderer* pic = &obj->GetComponent<SpriteRenderer>();
			if (pic->color.a < 1.0f)
			{
				pic->color.a += DuckTime::GetDeltaTime() * 0.8f;
				pic->color.a = Mathf::min(1.0f, pic->color.a);
				return false;
			}
			return true;
		}

		bool FadeOut(GameObject* obj)
		{
			//return true if completely fade out
			SpriteRenderer* pic = &obj->GetComponent<SpriteRenderer>();
			if (pic->color.a > 0.0f)
			{
				pic->color.a -= DuckTime::GetDeltaTime() * 0.8f;
				pic->color.a = Mathf::max(0.0f, pic->color.a);
				return false;
			}
			return true;
		}

	};
}

