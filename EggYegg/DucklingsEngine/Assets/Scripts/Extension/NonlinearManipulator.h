#pragma once

namespace DucklingsEngine
{
	class NonlinearManipulator
	{
	public:
		NonlinearManipulator() = default;
		~NonlinearManipulator() = default;

		
		static float SmoothStart2(float x) { return x * x; }
		static float SmoothStart3(float x) { return x * x * x; }

		static float SmoothStop2(float x) { return 1 - ((1 - x) * (1 - x)); }
		static float SmoothStop3(float x) { return 1 - ((1 - x) * (1 - x) * (1 - x)); }

		static float Mix(float(*funcA)(float), float(*funcB)(float), float blend, float t) { return funcA(t) + blend * (funcB(t) - funcA(t)); }
	};
}

