#pragma once
#include "MonoBehaviour.h"
#include "Rigidbody.h"
#include "SpriteRenderer.h"
#include "GameObject.h"
#include "CircleCollider.h"
#include "Animator.h"

namespace DucklingsEngine
{
	enum MinionType
	{
		MELEE,
		RANGE
	};

	class ChickenController : public MonoBehaviour
	{
	private:
		// Range
		int minRangeDistance = 850;
		int maxRangeDistance = 1250;
		bool inRange = false;
		float shootCD = 0.5f;
		float currentShootCD = 0.0f;
		float sideStepCD = 10.0f;
		float currentSideStepCD = 0.0f;
		float currentSideStepTimer;
		float sideStepLimitDuration = 5.0f;
		bool sideSteping = false;
		Vector2 sideStepPos;
		int sideStepDist = 300;
		// Bullet
		const float b_speed = 500.0f;
		const float b_damage = 1.0f;

		// Launch
		float currentLaunchTimer;
		float launchDuration;
		float launchForce;
		float launchSizeMultiplier = 1.5f;
		bool launched = true;

		float speed = 300.0f;
		bool stunted = false;

		Rigidbody* rid;
		SpriteRenderer* renderer;
		float stuntCountDown = 0.0f;
		int chickenHealth = 100;
		Vector2 originalSize;
		bool isDead = false;

		MinionType type;

		Animator* anim;

		void SpawnBullet();

	public:
		static int GLOBAL_CHICKEN_AMOUNT;

		ChickenController() = default;
		~ChickenController() = default;

		GameObject* player;

		void Start() override;
		void Update() override;

		void MoveTowardTarget(const Vector2& targetPos);
		void Setup(MinionType type, GameObject* i_player);
		void Launch(float force, float duration);

		void GotHitted(float damage);

		void Dead();
	};

}

