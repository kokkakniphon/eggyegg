#pragma once
#include "MonoBehaviour.h"

#include <vector>
#include <stdlib.h>

#include "Time.h"
#include "GameObject.h"
#include "Vector2D.h"

#include "ChickenController.h"

namespace DucklingsEngine
{
	class ChickenManager : public MonoBehaviour
	{
	private:
		std::vector<ChickenController*> _chickens;

	public:		
		// General settings
		GameObject* _player;

		unsigned int _spawnAmount = 1;
		std::vector<Vector2> _spawnPositions = 
		{
			Vector2(960, 540),
			Vector2(520, 260),
			Vector2(980, 200),
			Vector2(1630, 570),
			Vector2(1140, 940)
		};

		// Spawn Timer
		float _spawnTimer = 0;
		// unit (seconds)
		float _spawnDelay = 60.0f;

		ChickenManager() = default;
		~ChickenManager() = default;

		void Start() override;
		void Update() override;

		// Spawn chicken randomly at the pool of possible spawn position.
		void SpawnChicken(unsigned int amount, const std::vector<Vector2> spawnPos, MinionType type);
	};
}

