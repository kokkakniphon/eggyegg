#include "ChickenManager.h"

#include <stdlib.h>
#include <time.h>

#include "ChickenController.h"


namespace DucklingsEngine
{
	void ChickenManager::Start() 
	{
		ChickenController::GLOBAL_CHICKEN_AMOUNT = 0;
	}

	void ChickenManager::Update() {}

	void ChickenManager::SpawnChicken(unsigned int amount, const std::vector<Vector2> spawnPos, MinionType type)
	{
		std::vector<Vector2> listOfSpawnPos = spawnPos;

		for (size_t i = 0; i < amount; i++)
		{
			int posIndex = std::rand() % listOfSpawnPos.size();

			if (Vector2::Distance(_player->transform->GetPosition(), listOfSpawnPos[posIndex]) < 250)
			{
				i--;
				continue;
			}

			GameObject* chicken = new GameObject("Chicken");
			chicken->tag = "Chicken";
			chicken->layer = "solid_layer";
			_chickens.push_back(&chicken->AddComponent<ChickenController>());
			chicken->transform->SetParent(this->transform);
			chicken->transform->SetPosition(listOfSpawnPos[posIndex]);
			chicken->GetComponent<ChickenController>().Setup(type, _player);

			// Enable this if you want it NOT to be spawn at the same position.
			listOfSpawnPos.erase(listOfSpawnPos.begin() + posIndex);
		}
	}
}