#include "ChickenController.h"
#include "Scripts/DropManager.h"

#include "Animator.h"
#include "AnimationController/ChickenAnimationController.h"
#include "Scripts/Duck/DuckController.h"
#include "Scripts/Boss/ProjectileObject.h"

#include <stdlib.h>
#include <time.h>

namespace DucklingsEngine
{
	void ChickenController::Start()
	{
		// This will execute once at the begginning of the scene.
		rid = &this->gameObject->GetComponent<Rigidbody>();
		renderer = &this->gameObject->GetComponent<SpriteRenderer>();
	}

	void ChickenController::Update()
	{
		// This will execute every frame in update.
		if (isDead)
			return;
		if (currentLaunchTimer > 0.0f)
		{
			currentLaunchTimer -= DuckTime::GetDeltaTime() * 1.5f;
			if (rid != nullptr)
				rid->velocity = (player->transform->GetPosition() - this->transform->GetPosition()).normalized() * launchForce;
			float t = launchDuration - currentLaunchTimer;
			float interPolate = -(4 / (launchDuration * launchDuration)) * (t - launchDuration / 2) * (t - launchDuration / 2) + 1;
			this->transform->localScale =  (originalSize * (1 - interPolate)) + (originalSize * launchSizeMultiplier * interPolate);
			return;
		}

		if (launched == true)
		{
			launched = false;
			this->transform->localScale = originalSize;
			this->gameObject->GetComponent<CircleCollider>().isTrigger = false;
		}

		if (stuntCountDown > 0.0f)
		{
			stuntCountDown -= DuckTime::GetDeltaTime();
			return;
		}
		
		if (stunted == true)
		{
			stunted = false;
			anim->SetBool("IsHitted", false);
		}
			
		if (type == MinionType::MELEE)
		{
			MoveTowardTarget(player->transform->GetPosition());
			return;
		}

		// Range move toward player if too far
		if (inRange == false && Vector2::Distance(player->transform->GetPosition(), this->transform->GetPosition()) > minRangeDistance)
		{
			anim->controller->SetBool("IsShooting", false);

			MoveTowardTarget(player->transform->GetPosition());
			return;
		}
		
		// Don't move when distance is with-in range
		if (Vector2::Distance(player->transform->GetPosition(), this->transform->GetPosition()) > maxRangeDistance)
		{
			anim->controller->SetBool("IsShooting", false);

			inRange = false;
			return;
		}
			
		inRange = true;

		if (currentSideStepCD > 0)
			currentSideStepCD -= DuckTime::GetDeltaTime();
		else
		{
			if (sideSteping == false)
			{
				anim->controller->SetBool("IsShooting", false);

				sideSteping = true;
				currentSideStepTimer = 0;
				Vector2 dirToTar = player->transform->GetPosition() - this->transform->GetPosition();
				int dir = std::rand() % 2;
				dirToTar = dirToTar.normalized() * sideStepDist;
				// Either move 90 left or right
				sideStepPos = (dir > 0) ? this->transform->GetPosition() + Vector2(dirToTar.y, -dirToTar.x) : this->transform->GetPosition() + Vector2(-dirToTar.y, dirToTar.x);
			}

			if (sideSteping == true)
			{
				if (Vector2::Distance(sideStepPos, this->transform->GetPosition()) > 0.5f && currentSideStepTimer < sideStepLimitDuration)
				{
					currentSideStepTimer += DuckTime::GetDeltaTime();
					MoveTowardTarget(sideStepPos);
					return;
				}

				sideSteping = false;
				currentSideStepCD = sideStepCD;
			}
		}

		Vector2 lookDir = this->transform->GetPosition() - player->transform->GetPosition();
		lookDir.Normalize();
		this->transform->localEulerAngles = (atan2(lookDir.y, lookDir.x) * 180 / M_PI);

		if (currentShootCD > 0)
		{
			currentShootCD -= DuckTime::GetDeltaTime();
			return;
		}

		anim->controller->SetBool("IsShooting", false);

		currentShootCD = shootCD;
		SpawnBullet();

	}

	void ChickenController::SpawnBullet()
	{
		anim->controller->SetBool("IsShooting", true);
		GameObject* proj = new GameObject("DuckProjectile Clone");
		proj->AddComponent<SpriteRenderer>("orange_energy").order = 5;
		proj->AddComponent<CircleCollider>().radius = 23.65f;
		proj->AddComponent<ProjectileObject>();
		proj->transform->localScale = Vector2(0.5f, 0.5f);
		proj->GetComponent<CircleCollider>().isTrigger = true;

		proj->GetComponent<ProjectileObject>().Setup((player->transform->GetPosition() - this->transform->GetPosition()).normalized(), this->transform->GetPosition(), this->gameObject->name, "solid_layer", player->tag, "Chicken", b_speed, b_damage);
		proj->GetComponent<ProjectileObject>().SetDuration(5.0f);
	}

	void ChickenController::MoveTowardTarget(const Vector2 & targetPos)
	{
		Vector2 lookDir = this->transform->GetPosition() - targetPos;
		lookDir.Normalize();
		this->transform->localEulerAngles = (atan2(lookDir.y, lookDir.x) * 180 / M_PI);

		if (rid != nullptr)
			rid->velocity = lookDir * -speed;
	}

	void ChickenController::Setup(MinionType type, GameObject* i_player)
	{
		this->type = type;
		this->gameObject->AddComponent<SpriteRenderer>("Chickens/ChickenSpriteWalk");
		player = i_player;
		this->gameObject->AddComponent<Rigidbody>().gravityScale = 0.0f;
		this->gameObject->AddComponent<CircleCollider>().radius = 30.0f;
		if (type == MinionType::MELEE)
		{
			this->transform->localScale = Vector2(0.5f, 0.5f);
			chickenHealth = 100;
		}
		else
		{
			this->transform->localScale = Vector2(0.4f, 0.4f);
			chickenHealth = 80;
		}

		rid = &this->gameObject->GetComponent<Rigidbody>();
		renderer = &this->gameObject->GetComponent<SpriteRenderer>();
		renderer->order = 2;
		anim = &this->gameObject->AddComponent<Animator>();
		anim->SetAnimationController(std::make_shared<ChickenAnimationController>());
		anim->controller->SetBool("IsRange", type == MinionType::RANGE);
		anim->SetBool("IsDead", false);
		anim->SetBool("IsHitted", false);
		GLOBAL_CHICKEN_AMOUNT++;

		originalSize = this->transform->GetScale();
	}

	void ChickenController::Launch(float force, float duration)
	{
		launchForce = force;
		launchDuration = duration;
		currentLaunchTimer = duration;
	}

	void ChickenController::GotHitted(float damage)
	{
		if (this == nullptr)
			return;
		if (stuntCountDown > 0)
			return;
		stuntCountDown = 0.1f;
		stunted = true;
		anim->SetBool("IsHitted", true);

		player->GetComponent<DuckController>().ChargeUp(5);

		if (chickenHealth > 0)
			chickenHealth -= damage;
		else if (isDead == false)
		{
			Dead();
		}
	}
	void ChickenController::Dead()
	{
		if (isDead == true)
			return;
		isDead = true;
		anim->SetBool("IsDead", true);
		GLOBAL_CHICKEN_AMOUNT--;
		DropManager::instance->SpawnDrop(this->transform->GetPosition(), type);
		renderer->color = Color();
		renderer->order = 1;
		this->gameObject->GetComponent<CircleCollider>().isTrigger = true;
	}
}