#pragma once
#include "MonoBehaviour.h"
#include "Editor.h"
#include "Input.h"
#include "DuckTime.h"
#include "SpriteRenderer.h"

namespace DucklingsEngine
{
	class CutSceneSlideShow : public MonoBehaviour
	{
	public:
		CutSceneSlideShow() = default;
		~CutSceneSlideShow() = default;

		void Start();
		void Update();

		bool FadeOut(GameObject* obj);
		bool FadeIn(GameObject* obj);
		bool Side(GameObject* obj, float move = 700.0f);
		bool Vertical(GameObject* obj, float move = 700.0f);
	
		void Assign(GameObject* obj, int index);

	private:
		const float fadeDir = 0.8f;
		int page = 1;
		bool fadeOut = false;

		const float autoPlayTimer = 20.0f;
		float timer = 0.0f;

		GameObject* pics[3] = {nullptr};
	};
}
