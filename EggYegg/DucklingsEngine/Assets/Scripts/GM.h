#pragma once
#include "GameObject.h"
#include "AudioPlayer.h"
#include "Singleton.h"

namespace DucklingsEngine
{
	class GM : public Singleton<GM>
	{
	private:
		GameObject* player;

	public:
		GM() = default;
		~GM() = default;

		bool IsFullscreen = false;
		float MasterVolume = 1.0f;
		float MusicVolume = 1.0f;
		float SfxVolume = 1.0f;

		KHORA::AudioPlayer musicPlayer;
		KHORA::AudioPlayer uiSfxPlayer;
		KHORA::AudioPlayer playerSfxPlayer;

		void UpdateGM();
		void UpdateVolume();
		void SetPlayer(GameObject* p) { player = p; }
		GameObject* GetPlayerPtr() { return player; }
	};
}

