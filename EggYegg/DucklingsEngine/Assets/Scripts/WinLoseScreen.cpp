#include "WinLoseScreen.h"
#include "SceneManager.h"

#include "Input.h"
#include "GM.h"

namespace DucklingsEngine
{
	bool WinLoseScreen::IsGoodEnd = true;

	void WinLoseScreen::Start()
	{
		if (GM::GetInstance().musicPlayer.GetController() == nullptr)
		{
			GM::GetInstance().musicPlayer.SetAudioControllerPath("MusicController");
			GM::GetInstance().musicPlayer.Play();
		}

		if (the_scene != nullptr)
		{
			if (IsGoodEnd == false)
			{
				//change image to gameover scene
				if (credit != nullptr)
					credit->SetActive(false);
				GM::GetInstance().musicPlayer.Stop();
				the_scene->GetComponent<SpriteRenderer>().SetSprite("Cutscenes/gameover");
			}
			else
			{
				if (credit != nullptr)
					credit->SetActive(true);
				timer = 180.0f;
				GM::GetInstance().musicPlayer.SetBool("IsEnd", true);
				GM::GetInstance().musicPlayer.SetBool("IsWin", true);
				the_scene->GetComponent<SpriteRenderer>().SetSprite("Cutscenes/ending");
			}
		}
	}

	void WinLoseScreen::Update()
	{
		GM::GetInstance().UpdateGM();
		if (the_scene != nullptr)
		{
			if (IsGoodEnd = true)
			{
				credit->transform->Translate(Vector2(0.0f, -30.0f) * DuckTime::GetDeltaTime());
			}

			if (!isDisplay && FadeIn(the_scene))
			{
				isDisplay = true;
			}
			else if (timer > countdown)
			{
				countdown += DuckTime::GetInstance().GetDeltaTime();
			}
			else
			{
				if (FadeOut(the_scene))
				{
					//back to title screen
					SceneManager::GetInstance().OpenScene("NewTitleScene");
				}
			}
		}

		if (Input::GetKeyDown(SDL_SCANCODE_SPACE))
		{
			countdown = timer;
		}
	}

	bool WinLoseScreen::FadeIn(GameObject* obj)
	{
		//return true uf completely fade in
		SpriteRenderer* pic = &obj->GetComponent<SpriteRenderer>();
		if (pic->color.a < 1.0f)
		{
			pic->color.a += DuckTime::GetDeltaTime() * 0.8f;
			pic->color.a = Mathf::min(1.0f, pic->color.a);
			return false;
		}
		return true;
	}

	bool WinLoseScreen::FadeOut(GameObject* obj)
	{
		//return true if completely fade out
		SpriteRenderer* pic = &obj->GetComponent<SpriteRenderer>();
		if (pic->color.a > 0.0f)
		{
			pic->color.a -= DuckTime::GetDeltaTime() * 0.8f;
			pic->color.a = Mathf::max(0.0f, pic->color.a);
			return false;
		}
		return true;
	}
}