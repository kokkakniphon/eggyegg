#pragma once
#include "MonoBehaviour.h"

#include "Input.h"

namespace DucklingsEngine
{
	class SceneLoader : public MonoBehaviour
	{
	public:
		SceneLoader() = default;
		~SceneLoader() = default;

		void Start() override;
		void Update() override;
		
		bool loaded[3] = { false, false, false };
	};

	void SceneLoader::Start()
	{
		// This will execute once at the begginning of the scene.

	}

	void SceneLoader::Update()
	{
		// This will execute every frame in update.
		if (Input::GetKeyDown(SDL_SCANCODE_0)) // Unload all scene
		{
			for (size_t i = 0; i < 3; i++)
			{
				if (loaded[i] == true)
					SceneManager::GetInstance().UnloadScene(i+1);
			}
		}

		if (Input::GetKeyDown(SDL_SCANCODE_1)) // Load scene 1
		{
			loaded[1] = !loaded[1];
			if(loaded[1] == false)
				SceneManager::GetInstance().LoadScene(1);
			else
				SceneManager::GetInstance().UnloadScene(1);

		}

		if (Input::GetKeyDown(SDL_SCANCODE_2)) // Load scene 2
		{
			loaded[2] = !loaded[2];
			if (loaded[2] == false)
				SceneManager::GetInstance().LoadScene(2);
			else
				SceneManager::GetInstance().UnloadScene(2);
		}

		if (Input::GetKeyDown(SDL_SCANCODE_3)) // Load scene 3
		{
			loaded[3] = !loaded[3];
			if (loaded[3] == false)
				SceneManager::GetInstance().LoadScene(3);
			else
				SceneManager::GetInstance().UnloadScene(3);
		}
	}
}

