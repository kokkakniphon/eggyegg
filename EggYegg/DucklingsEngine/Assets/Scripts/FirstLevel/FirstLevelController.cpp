#include "FirstLevelController.h"

#include "Input.h"
#include "Scripts/DropManager.h"
#include "Scripts/GM.h"

namespace DucklingsEngine
{

	void FirstLevelController::SpawnChickenWave()
	{
		if (_waveDatas.front()._waitTillNoMinion == true && ChickenController::GLOBAL_CHICKEN_AMOUNT > 0 && _waveTimer < _limitWaveTimer)
			return;
		if (_waveTimer > _limitWaveTimer)
		{
			ChickenController::GLOBAL_CHICKEN_AMOUNT = 0;
		}
			
		_waveDelayTimer = _waveDatas.front()._delayBeforeNextWave;
		_chickenManager->SpawnChicken(_waveDatas.front()._minionAmount, _spawnPositions, _waveDatas.front()._type);
		_waveDatas.pop();
		_waveTimer = 0;
	}

	void FirstLevelController::SetupWave()
	{
		_waveDatas.push(WaveData(MinionType::MELEE, 3, 10, false));
		_waveDatas.push(WaveData(MinionType::MELEE, 5, 10, false));
		_waveDatas.push(WaveData(MinionType::RANGE, 3, 1.5f, true));
		_waveDatas.push(WaveData(MinionType::MELEE, 3, 10, false));
		_waveDatas.push(WaveData(MinionType::RANGE, 5, 1.5f, true));
		_waveDatas.push(WaveData(MinionType::MELEE, 5, 10, false));
		_waveDatas.push(WaveData(MinionType::RANGE, 8, 1.5f, true));
		_waveDatas.push(WaveData(MinionType::MELEE, 8, 10, false));
	}

	void FirstLevelController::SetupBoss()
	{
		for (size_t i = 0; i < 1; i++)
		{
			int posIndex = std::rand() % _spawnPositions.size();

			if (Vector2::Distance(_player->transform->GetPosition(), _spawnPositions[posIndex]) < 250)
			{
				i--;
				continue;
			}
			_boss->uiHolder->SetActive(true);
			_boss->transform->SetPosition(_spawnPositions[posIndex]);
			_boss->gameObject->SetActive(true);
		}
	}

	void FirstLevelController::Start()
	{
		GM::GetInstance().musicPlayer.SetBool("IsInGame", true);
		GM::GetInstance().musicPlayer.SetBool("IsEnd", false);
		// This will execute once at the begginning of the scene.
		_boss->uiHolder->SetActive(false);
		_boss->gameObject->SetActive(false);
		dropTimer = dropDelay;
		_waveDelayTimer = 5.0f;
		SetupWave();
	}

	void FirstLevelController::Update()
	{
		// This will execute every frame in update.
		GM::GetInstance().UpdateGM();

		if (ChickenController::GLOBAL_CHICKEN_AMOUNT > 0)
		{
			_textRenderer->text = "Remaining chicken " + std::to_string(ChickenController::GLOBAL_CHICKEN_AMOUNT) + "\n"
				+ "Remaining wave " + std::to_string(_waveDatas.size());
		}
		else
		{
			_textRenderer->text = std::to_string((int)(_waveDelayTimer) + 1) + " second till next wave";
		}

		_textRenderer->RefreshText();

		if (dropTimer > 0)
		{
			dropTimer -= DuckTime::GetDeltaTime();
		}
		else
		{
			dropTimer = dropDelay;
			int randIndex = std::rand() % _spawnPositions.size();
			DropManager::instance->SpawnDrop(_spawnPositions[randIndex], MinionType::MELEE);
		}

		if (_waveDelayTimer > 0)
		{
			_waveDelayTimer -= DuckTime::GetDeltaTime();
		}
		else
		{
			if (_setupBoss == false)
				_waveTimer += DuckTime::GetDeltaTime();

			if (_waveDatas.size() > 0)
			{
				SpawnChickenWave();
			}
			else if (ChickenController::GLOBAL_CHICKEN_AMOUNT <= 0 || _waveTimer > _limitWaveTimer)
			{
				if (_setupBoss == false)
				{
					_setupBoss = true;
					dropDelay = 7.0f;
					SetupBoss();
				}
			}
		}

		//{
		//	if (Input::GetKeyDown(SDL_SCANCODE_5)) // Max Player Health
		//	{
		//		GM::GetInstance().GetPlayerPtr()->GetComponent<DuckController>().Healed(10);
		//	}
		//	if (Input::GetKeyDown(SDL_SCANCODE_6)) // Max Boss Health
		//	{
		//		_boss->Healed(9999);
		//	}
		//	if (Input::GetKeyDown(SDL_SCANCODE_7)) // Kill player
		//	{
		//		GM::GetInstance().GetPlayerPtr()->GetComponent<DuckController>().Hitted(10);
		//	}
		//	if (Input::GetKeyDown(SDL_SCANCODE_8)) // Kill boss
		//	{
		//		_boss->Hitted(9999);
		//	}
		//	if (Input::GetKeyDown(SDL_SCANCODE_9)) //Clear all future wave
		//	{
		//		while (!_waveDatas.empty())
		//		{
		//			_waveDatas.pop();
		//		}
		//	}
		//}

		

		// NEXT ROOM ARROW ANIMATION
		//_nextRoomArrow->transform->Translate(Vector2(sin(DuckTime::GetTime() * 5) * _arrowMoveDis, 0));
		// END NEXT ROOM ARROW ANIMATION
	}
}