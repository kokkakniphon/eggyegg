#pragma once
#include <queue>

#include "MonoBehaviour.h"
#include "GameObject.h"
#include "BoxCollider.h"
#include "TextRenderer.h"

#include "Scripts/Core/SceneTransition.h"
#include "Scripts/Chicken/ChickenManager.h"
#include "Scripts/Duck/DuckController.h"
#include "Scripts/Boss/BossController.h"

namespace DucklingsEngine
{
	class FirstLevelController : public MonoBehaviour
	{
	private:
		bool _setupBoss = false;
		bool _roomClear = false;

		float _arrowMoveDis = 0.5f;

		// Drop
		float dropTimer;
		float dropDelay = 10.0f;

		// Scene transition
		bool _transitioning;

		float _waveDelayTimer = 0.0f;
		float _waveTimer = 0.0f;
		float _limitWaveTimer = 45.0f; // ForceSpawn minion if player took too long to kill.

		struct WaveData 
		{
			int _minionAmount;
			MinionType _type;
			float _delayBeforeNextWave = 10.0f;
			bool _waitTillNoMinion = false;

			WaveData() = default;
			WaveData(MinionType type, int amount, float delay, bool waitTill) : _minionAmount(amount), _type(type), _delayBeforeNextWave(delay), _waitTillNoMinion(waitTill) {}
		};

		std::queue<WaveData> _waveDatas;
		std::vector<Vector2> _spawnPositions =
		{
			Vector2(387 , 465),
			Vector2(1126, 262),
			Vector2(1997, 227),
			Vector2(2345, 217),
			Vector2(2694, 318),
			Vector2(3293, 287),
			Vector2(3393, 648),
			Vector2(3358, 941),
			Vector2(3267,1329),
			Vector2(3101,1600),
			Vector2(3098,1881),
			Vector2(2680,1857),
			Vector2(2128,1902),
			Vector2(1535,1870),
			Vector2(949 ,1876),
			Vector2(235 ,1859),
			Vector2(148 ,1504),
			Vector2(184 ,1180),
			Vector2(614 , 702),
			Vector2(2264, 493),
			Vector2(3005,1129),
			Vector2(2864,1571),
			Vector2(2067,1654),
			Vector2(1647,1692),
			Vector2(1070,1581),
			Vector2(1070,1581),
			Vector2(639 ,1175),
		};
		
		void SpawnChickenWave();
		void SetupWave();
		void SetupBoss();
	public:
		GameObject* _nextRoomArrow;
		GameObject* _fenceDoor;

		BossController* _boss;
		DuckController* _player;
		ChickenManager* _chickenManager;
		SceneTransition* _sceneTransition;
		TextRenderer* _textRenderer;

		FirstLevelController() = default;
		~FirstLevelController() = default;

		void Start() override;
		
		void Update() override;

	};
}

