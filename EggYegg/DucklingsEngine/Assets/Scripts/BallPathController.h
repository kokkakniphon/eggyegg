#pragma once
#include "MonoBehaviour.h"

#include "BallController.h"
#include "PathCreator.h"

#include "Input.h"

namespace DucklingsEngine
{
	class BallPathController : public MonoBehaviour
	{
	private:
		float ballRadius;
		std::vector<BallController*> balls;

		float progression = 0.0f;

	public:
		BallPathController() = default;
		~BallPathController() = default;

		void Start() override;
		void Update() override;

		PathCreator* pathCreator;
		bool started = false;

		void AddBall(BallController* ball);

		void SetAllBall();
	};

	void BallPathController::Start()
	{
		// This will execute once at the begginning of the scene.
		if(balls.size() > 0)
			ballRadius = 15 * 2;
	}

	void BallPathController::Update()
	{
		// This will execute every frame in update.
		if (Input::GetKey(SDL_SCANCODE_G) && Input::GetKeyDown(SDL_SCANCODE_S))
		{
			started = !started;
		}

		if (started)
		{
			progression = (progression + Time::GetDeltaTime() * 5);

		}
		SetAllBall();

		
	}

	void BallPathController::AddBall(BallController * ball)
	{
		ball->isInPath = true;
		balls.push_back(ball);
	}

	void BallPathController::SetAllBall()
	{
		std::vector<Vector2> points = pathCreator->GetCalculatedPath();
		float segmentLength = Vector2::Distance(points[0], points[1]);
		float ballLengthInSegment = ballRadius / segmentLength;

		float lastBallProgression = progression;

		for (unsigned int i = 0; i < balls.size(); i++)
		{
			balls[i]->gameObject->enabled = false;
		}

		bool endOfPath = false;
		if (lastBallProgression >= points.size() - 1)
		{
			progression = points.size() - 1;
			endOfPath = true;
		}

		for (unsigned int i = 0; i < balls.size(); i++)
		{

			if (lastBallProgression <= 0)
				break;

			balls[i]->gameObject->enabled = true;

			if (endOfPath == false)
			{
				float currentProgressionInSegment = lastBallProgression - std::floor(lastBallProgression);
				Vector2 newPosition = Vector2::Lerp(points[std::floor(lastBallProgression)], points[std::ceil(lastBallProgression)], currentProgressionInSegment);
				balls[i]->gameObject->GetComponent<BallController>().MoveToDestination(newPosition);

				Vector2 segmentDir = points[std::ceil(lastBallProgression)] - points[std::floor(lastBallProgression)];
				balls[i]->transform->localEulerAngles = (atan2(segmentDir.y, segmentDir.x) * 180 / M_PI) - 90;
			
				std::vector<Collider*> collide = balls[i]->gameObject->GetComponent<CircleCollider>().collidedTriggers;

				if (collide.size() > 0 && balls[i]->gameObject->name == "baBall")
				{
					for (size_t j = 0; j < collide.size(); j++)
					{
						// If collide with shoot ball
						if (collide[j]->gameObject->name == "ShootBall")
						{
							collide[j]->gameObject->name = "baBall";
							collide[j]->gameObject->GetComponent<Rigidbody>().enabled = false;
							collide[j]->gameObject->GetComponent<BallController>().isInPath = true;
							collide[j]->gameObject->GetComponent<CircleCollider>().isTrigger = true;
							collide[j]->gameObject->GetComponent<BallController>().MoveToDestination(newPosition, 0.5f);

							Vector2 newBallToPathBall = collide[j]->transform->GetPosition() - balls[i]->transform->GetPosition();
							newBallToPathBall = newBallToPathBall + (collide[j]->gameObject->GetComponent<Rigidbody>().velocity / collide[j]->gameObject->GetComponent<Rigidbody>().velocity.magnitude()) * collide[j]->gameObject->GetComponent<CircleCollider>().radius;

							float dotOfBall = Vector2::Dot(segmentDir, newBallToPathBall);
							int insertIndex = (dotOfBall > 0) ? i : i + 1;
							insertIndex = insertIndex % (balls.size() - 1);
							balls.insert(balls.begin() + insertIndex, &collide[j]->gameObject->GetComponent<BallController>());
							progression += ballLengthInSegment;

							// Loop thought front balls to smoothly move it to position.
							for (size_t k = 0; k <= i; k++)
							{
								balls[k]->gameObject->GetComponent<BallController>().MoveToDestination(balls[k]->transform->GetPosition(), 0.5f);
							}

							i++;
							break;
						}
					}
				}
			}

			lastBallProgression -= ballLengthInSegment;
		}


	}
}

