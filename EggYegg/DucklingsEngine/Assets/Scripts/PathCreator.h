﻿#pragma once
#include "MonoBehaviour.h"

#include "Path.h"
#include "ColliderManager.h"
#include "Input.h"

#include <fstream>

namespace DucklingsEngine
{
	class PathCreator : public MonoBehaviour
	{
	private:
		std::string defaultPath = "../Assets/Paths/";

		std::unique_ptr<Path> path;

		Transform *selectedBall = nullptr;
		Vector2 offset = Vector2();

		std::vector<Vector2> calculatedPath;

		float resolution = 2;

		bool showTools = true;

		// Genaral settings
		SDL_Color anchorColor = SDL_Color{ 255,255,255,255 };
		SDL_Color controlColor = SDL_Color{ 255,0,0,255 };

	public:

		float spacing = 20.0f;

		PathCreator() = default;
		~PathCreator() = default;

		void Start() override;
		void Update() override;
		void Draw() override;

		std::string currentPathName = "DefaultPath";

		void CreatePath();
		Vector2 EvaluateCubic(Vector2 p1, Vector2 p2, Vector2 p3, Vector2 p4, float t);
		void CalculateEvenlySpacedPoints();

		void DrawPoints();

		std::vector<Vector2> GetCalculatedPath();

		void LoadPathFromFile(std::string fileName);
		void SavePathToFile(std::string fileName);
	};

	void PathCreator::Start()
	{
		// This will execute once at the begginning of the scene.
		if (currentPathName != "")
		{
			LoadPathFromFile(currentPathName);
		}
		else
		{
			CreatePath();
		}
		CalculateEvenlySpacedPoints();
	}

	void PathCreator::Update()
	{
		// This will execute every frame in update.
		
		// Handle of the for the node in the path
		std::vector<Collider*> colliders;
		if (Input::GetMouseButtonDown(SDL_BUTTON_LEFT))
		{
			if (ColliderManager::GetInstance().OverlapPoint(&colliders, Vector2(Input::GetMousePosition().x, Input::GetMousePosition().y)))
			{
				for (unsigned int i = 0; i < colliders.size(); i++)
				{
					if (colliders[i]->gameObject->name == "PathPoint")
					{
						selectedBall = colliders[i]->transform;
						offset = selectedBall->GetPosition() - Vector2(Input::GetMousePosition().x, Input::GetMousePosition().y);
						break;
					}
				}
			}
		}


		if (Input::GetKey(SDL_SCANCODE_LSHIFT) && Input::GetKey(SDL_SCANCODE_P))
		{
			if (Input::GetKeyDown(SDL_SCANCODE_C)) // Toggle close path
			{
				path->ToggleClosed();
				CalculateEvenlySpacedPoints();
			}	

			if (Input::GetKeyDown(SDL_SCANCODE_A)) // Toggle auto set control point
			{
				path->ToggleAutoSetControlPoints();
				CalculateEvenlySpacedPoints();
			}

			if (Input::GetKeyDown(SDL_SCANCODE_S)) // Toggle auto set control point
			{
				currentPathName = "DefaultPath";
				SavePathToFile(currentPathName);
			}

			if (Input::GetKeyDown(SDL_SCANCODE_D)) // Toggle show tool
			{
				showTools = !showTools;
			}

			if (Input::GetKeyDown(SDL_SCANCODE_E)) // Toggle enable path control point.
			{
				for (size_t i = 0; i < path->pointObjs.size(); i++)
				{
					path->pointObjs[i]->GetComponent<CircleCollider>().enabled = !path->pointObjs[i]->GetComponent<CircleCollider>().enabled;
				}
			}
		}

		if (Input::GetMouseButtonDown(SDL_BUTTON_RIGHT))
		{
			Vector2Int mousePos = Input::GetMousePosition();

			if (ColliderManager::GetInstance().OverlapPoint(&colliders, Vector2(mousePos.x, mousePos.y)))
			{
				path->DeleteSegment(colliders[0]->gameObject->GetUniqueID());
				CalculateEvenlySpacedPoints();
			}
			else
			{
				path->AddSegment(Vector2(mousePos.x, mousePos.y));
				CalculateEvenlySpacedPoints();
			}
		}

		if (Input::GetMouseButton(SDL_BUTTON_LEFT) && selectedBall != nullptr)
		{
			path->MovePoint(selectedBall->gameObject->GetUniqueID(), Vector2(Input::GetMousePosition().x, Input::GetMousePosition().y) + offset);
			CalculateEvenlySpacedPoints();
		}

		if ((Input::GetMouseButtonUp(SDL_BUTTON_LEFT)) && selectedBall != nullptr)
		{
			selectedBall = nullptr;
		}

		
	}

	void PathCreator::Draw()
	{
		// Draw path
		if (calculatedPath.size() > 0)
		{
			for (size_t i = 0; i < calculatedPath.size() - 1; i++)
			{
				SDL_SetRenderDrawColor(Engine::GetInstance().m_Renderer, 0, 255, 0, 255);
				SDL_RenderDrawLine(Engine::GetInstance().m_Renderer, calculatedPath[i].x, calculatedPath[i].y, calculatedPath[i + 1].x, calculatedPath[i + 1].y);
			}
		}		

		if (showTools == false)
			return;

		// Draw control points line
		for (size_t i = 0; i < path->NumSegments(); i++)
		{
			std::vector<Vector2> points = path->GetPointsInSegment(i);
			SDL_SetRenderDrawColor(Engine::GetInstance().m_Renderer, 255, 84, 167, 255);
			SDL_RenderDrawLine(Engine::GetInstance().m_Renderer, points[1].x, points[1].y, points[0].x, points[0].y);
			SDL_RenderDrawLine(Engine::GetInstance().m_Renderer, points[2].x, points[2].y, points[3].x, points[3].y);
		}

		// Draw points
		DrawPoints();
	}

	void PathCreator::CreatePath()
	{
		path = std::make_unique<Path>(this->transform->GetPosition());
	}

	Vector2 PathCreator::EvaluateCubic(Vector2 p1, Vector2 p2, Vector2 p3, Vector2 p4, float t)
	{
		return (p1 * std::pow(1 - t, 3)) + (p2 * (3 * std::pow(1 - t, 2) * t))
			+ (p3 * (3 * (1 - t) * std::pow(t, 2))) + (p4 * std::pow(t, 3));;
	}

	void PathCreator::CalculateEvenlySpacedPoints()
	{
		calculatedPath.clear();

		Vector2 previousPoint = path->GetPointsInSegment(0)[0];
		calculatedPath.push_back(previousPoint);
		float dstSinceLastEvenPoint = 0;

		for (size_t i = 0; i < path->NumSegments(); i++)
		{
			std::vector<Vector2> points = path->GetPointsInSegment(i);
			float controlNetLength = Vector2::Distance(points[0], points[1]) + Vector2::Distance(points[1], points[2]) + Vector2::Distance(points[2], points[3]);
			float estimatedCurveLength = Vector2::Distance(points[0], points[3]) + controlNetLength / 2.0f;
			int divisions = std::ceil(estimatedCurveLength * resolution * 10);
			float t = 0;
			while (t <= 1)
			{
				t += 1.0f / divisions;
				Vector2 pointOnCurve = EvaluateCubic(points[0], points[1], points[2], points[3], t);
				dstSinceLastEvenPoint += Vector2::Distance(previousPoint, pointOnCurve);

				while (dstSinceLastEvenPoint >= spacing)
				{
					float overshootDst = dstSinceLastEvenPoint - spacing;
					Vector2 newEvenlySpacedPoint = pointOnCurve + ((previousPoint - pointOnCurve) / (previousPoint - pointOnCurve).magnitude()) * overshootDst;
					calculatedPath.push_back(newEvenlySpacedPoint);
					dstSinceLastEvenPoint = overshootDst;
					previousPoint = newEvenlySpacedPoint;
				}

				previousPoint = pointOnCurve;
			}
		}
	}

	void DrawCircle(Vector2 offset, SDL_Color gizmosColor, float radius)
	{
		int gizmosResolution = 10 * 2;
		for (unsigned int i = 0; i < gizmosResolution; i++)
		{
			Vector2Int drawPoint;
			drawPoint.x = offset.x + radius * cos(i * (360.0f / gizmosResolution) * 3.14f / 180.0f);
			drawPoint.y = offset.y + radius * sin(i * (360.0f / gizmosResolution) * 3.14f / 180.0f);
			SDL_SetRenderDrawColor(Engine::GetInstance().m_Renderer, gizmosColor.r, gizmosColor.g, gizmosColor.b, gizmosColor.a);
			SDL_RenderDrawPoint(Engine::GetInstance().m_Renderer, drawPoint.x, drawPoint.y);
		}
	}

	void PathCreator::DrawPoints()
	{
		for (size_t i = 0; i < path->points.size(); i++)
		{
			if (i % 3 == 0) // is anchor point.
			{
				DrawCircle(path->points[i], anchorColor, path->pointObjs[i]->GetComponent<CircleCollider>().radius);
			}
			else
			{
				DrawCircle(path->points[i], controlColor, path->pointObjs[i]->GetComponent<CircleCollider>().radius);
			}
		}
	}

	std::vector<Vector2> PathCreator::GetCalculatedPath()
	{
		return calculatedPath;
	}

	void PathCreator::LoadPathFromFile(std::string fileName)
	{
		std::ifstream file(defaultPath + fileName + ".path");
		if (!file)
		{
			std::cout << "Error Writing File" << std::endl;
			throw std::exception();
		}
		else
		{
			std::vector<Vector2> points;
			while (file.eof() == false)
			{
				std::string data;
				std::getline(file, data);
				int currentBlock = 0;
				std::string currentValue[2] = {"",""};
				for (int i = 0; i < data.size(); i++)
				{
					if (data[i] == ',')
					{
						currentBlock++;
					}
					else if (data[i] != '(' && data[i] != ')')
					{
						currentValue[currentBlock] += data[i];
					}
					else if (data[i] == ')')
					{
						break;
					}
				}
				if (currentValue[0] != "" && currentValue[1] != "")
				{
					points.push_back(Vector2(std::stof(currentValue[0]), std::stof(currentValue[1])));
				}
			}
			path = std::make_unique<Path>(points);
		}
		file.close();

		currentPathName = fileName;
		
	}

	void PathCreator::SavePathToFile(std::string fileName)
	{
		std::ofstream file(defaultPath + fileName + ".path");
		if (!file)
		{
			std::cout << "Error Reading File" << std::endl;
			throw std::exception();
		}
		else
		{
			for (size_t i = 0; i < path->points.size(); i++)
			{
				file << path->points[i].ToString() << std::endl;
			}
		}
		file.close();
	}

	

	
}

