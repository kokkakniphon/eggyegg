#pragma once
#include "MonoBehaviour.h"

#include "Input.h"
#include "EdgeCollider.h"

namespace DucklingsEngine
{
	class EdgeController : public MonoBehaviour
	{
	public:
		EdgeController() = default;
		~EdgeController() = default;

		void Start() override;
		void Update() override;

		EdgeCollider* edge;

	};

	void EdgeController::Start()
	{
		// This will execute once at the begginning of the scene.
		edge = &this->gameObject->GetComponent<EdgeCollider>();
	}

	void EdgeController::Update()
	{
		// This will execute every frame in update.
		if (Input::GetKey(SDL_SCANCODE_E))
		{
			edge->points[0] = Vector2(Input::GetMousePosition().x, Input::GetMousePosition().y) - this->transform->GetPosition();
		}

		if (Input::GetKey(SDL_SCANCODE_R))
		{
			edge->points[1] = Vector2(Input::GetMousePosition().x, Input::GetMousePosition().y) - this->transform->GetPosition();
		}
	}
}

