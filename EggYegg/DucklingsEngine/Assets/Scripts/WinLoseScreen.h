#pragma once
#include "MonoBehaviour.h"
#include "GameObject.h"
#include "Mathf.h"
#include "SpriteRenderer.h"
#include "DuckTime.h"

namespace DucklingsEngine
{
	class WinLoseScreen : public MonoBehaviour
	{
	public:
		static bool IsGoodEnd;
		GameObject* credit;

		void SetPic(GameObject* p) { the_scene = p; }

		WinLoseScreen() = default;
		~WinLoseScreen() = default;

		void Start();
		void Update();

		bool FadeIn(GameObject* obj);
		bool FadeOut(GameObject* obj);

	private:
		float countdown = 0.0f;
		float timer = 5.0f;
		GameObject* the_scene = nullptr;
		bool isDisplay = false;
	};
}
