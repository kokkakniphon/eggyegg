#pragma once
#include "MonoBehaviour.h"

#include "GameObject.h"
#include "CircleCollider.h"
#include "Time.h"
#include "SpriteRenderer.h"

namespace DucklingsEngine
{
	class ProjectileObject : public MonoBehaviour
	{
	private:
		Vector2 m_Dir;
		float m_Speed = 200.0f;
		unsigned int m_Damage = 10;
		Transform* startTrans;
		Vector2 m_Stearing;

		std::string m_CollideLayer = "";
		std::string m_TargetTag = "";
		std::string m_IgnoreTag = "";
		std::string m_SourceName = "";

		bool m_Launch = false;
		float m_LaunchDelayTimer = 0.0f;
		float m_duration = 5.0f;

		void OnTriggerEnter();
		

	public:
		ProjectileObject() = default;
		~ProjectileObject() = default;

		void SetDuration(float dur) { m_duration = dur;}

		void Setup(const Vector2& dir, const Vector2& startPos, std::string sourceName = "" ,std::string collideLayer = "", std::string targetTag = "", std::string ignoreTag = "", float speed = -1.0f, unsigned int damage = 0, float launchDelay = -1.0f, Transform* startTrans = nullptr, const Vector2& stearing = Vector2());
		

		void Start() override
		{
			// This will execute once at the begginning of the scene.

		}
		void Update() override;
		

	};
}

