#pragma once
#include "MonoBehaviour.h"

#include "Scripts/Chicken/ChickenManager.h"
#include "Scripts/Boss/BossController.h"

namespace DucklingsEngine
{
	class BossLevelController : public MonoBehaviour
	{
	private:
		float _arrowMoveDis = 0.5f;

		// Scene transition
		bool _transitioning;

		void OnTriggerEnter()
		{
			// If player touch next room arrow collider, switch to next room.
			if (_nextRoomArrow->GetActive() == true && _transitioning == false)
			{
				std::vector<Collider*> colliders = _nextRoomArrow->GetComponent<BoxCollider>().collidedTriggers;

				for (size_t i = 0; i < colliders.size(); i++)
				{
					if (colliders[i]->gameObject->name == "Player")
					{
						_transitioning = true;

						_sceneTransition->StartTransition(Camera::mainCamera->GetTransform()->GetPosition() + Vector2(0, 0), 1.0f, "Tutorial");
					}
				}
			}
		}
	public:
		GameObject* _nextRoomArrow;

		BossController* _bossController;
		SceneTransition* _sceneTransition;

		BossLevelController() = default;
		~BossLevelController() = default;

		void Start() override
		{
			// This will execute once at the begginning of the scene.
			_nextRoomArrow->SetActive(false);

		}
		void Update() override
		{
			// This will execute every frame in update.

			if (_bossController->m_Defeated == true)
			{
				if (_nextRoomArrow->GetActive() == false)
				{
					std::cout << "Clear First Level!!" << std::endl;
					_nextRoomArrow->SetActive(true);
				}
				OnTriggerEnter();
			}

			if (Input::GetKeyDown(SDL_SCANCODE_RIGHT))
			{
				_sceneTransition->StartTransition(Camera::mainCamera->GetTransform()->GetPosition() + Vector2(0, 0), 1.0f, "Tutorial");
			}

			// NEXT ROOM ARROW ANIMATION
			_nextRoomArrow->transform->Translate(Vector2(0, sin(DuckTime::GetTime() * 5) * _arrowMoveDis));
			// END NEXT ROOM ARROW ANIMATION
		}

	};
}

