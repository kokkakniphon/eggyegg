#pragma once
#include "MonoBehaviour.h"

#include "GameObject.h"
#include "Rigidbody.h"

#include "Scripts/Chicken/ChickenManager.h"
#include "Scripts/Core/Character.h"
#include "Scripts/Boss/ProjectileObject.h"

namespace DucklingsEngine
{
	class BossController : public Character
	{
	private:
		// Range
		int minRangeDistance = 950;
		int maxRangeDistance = 1250;
		bool inRange = false;
		float shootCD = 0.5f;
		float currentShootCD = 0.0f;
		float sideStepCD = 8.0f;
		float currentSideStepCD = 0.0f;
		float currentSideStepTimer;
		float sideStepLimitDuration = 3.0f;
		bool sideSteping = false;
		Vector2 sideStepPos;
		int sideStepDist = 100;

		int remain_chicken_to_throw = 3;
		int max_chicken_to_throw = 3;

		SpriteRenderer* renderer;
		float stuntCountDown = 0.0f;

		bool m_PhaseOne, m_PhaseTwo, m_PhaseThree;
		Rigidbody* rid;
		Vector2 m_AimDir;
		float m_RotateSpeed = 0.95f;
		float speed = 300.0f;
		bool stunted = false;
		Color originalColor;
		Animator* anim;

		float m_ShootingTimer;
		float m_ShootingCooldown = 0.5f;
		float m_ShootingDelay = 5.0f;

		int m_ProjectileAmount = 3;
		int m_ProjectileDamage = 1;
		float m_ProjectileSpeed = 350.0f;

		int m_ChickenAmount = 3;
		
		void SpawnProjectile(int amount);

		float m_WaveTimer = 0.0f;
		float m_WaveDelay = 15.0f;

		float currentDeadTimer;
		float endDuration = 6.0f;

		void ProjectilePhase();
		void ChickenPhase(int side);
		void MoveTowardTarget(const Vector2 & targetPos);

		void UpdateUI();

	public:

		ChickenManager* _chickenManager;
		std::vector<ChickenController*> chickens;

		GameObject* head;
		GameObject* player;
		GameObject* uiHolder;
		GameObject* healthBar;
		GameObject* innerHealthBar;

		bool m_Defeated;

		BossController() : Character() {}
		~BossController() = default;

		void Hitted(int amount);

		void Start() override;
		void Update() override;

	};
}

