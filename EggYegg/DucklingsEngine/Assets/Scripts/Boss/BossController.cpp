#include "BossController.h"

#include "Scripts/Chicken/ChickenManager.h"
#include "Scripts/Boss/ProjectileObject.h"
#include "AnimationController/BossAnimationController.h"
#include "Scripts/Duck/DuckController.h"
#include "Scripts/WinLoseScreen.h"

namespace DucklingsEngine
{

	void BossController::Start()
	{
		// This will execute once at the begginning of the scene.
		m_MaxHealth = 5000;
		m_Health = m_MaxHealth;

		anim = &this->gameObject->GetComponent<Animator>();
		anim->SetAnimationController(std::make_shared<BossAnimationController>());
		rid = &this->gameObject->GetComponent<Rigidbody>();
		rid->mass = 50.0f;
		rid->drag = 10.0f;
		renderer = &this->gameObject->GetComponent<SpriteRenderer>();
		originalColor = renderer->color;
	}

	void BossController::MoveTowardTarget(const Vector2 & targetPos)
	{
		Vector2 lookDir = this->transform->GetPosition() - targetPos;
		lookDir.Normalize();
		this->transform->localEulerAngles = (atan2(lookDir.y, lookDir.x) * 180 / M_PI) - 90;

		if (rid != nullptr)
			rid->velocity = lookDir * -speed;
	}

	void BossController::UpdateUI()
	{
		healthBar->transform->localScale.x = this->GetHealthPercent();
	}

	void BossController::Update()
	{
		innerHealthBar->transform->localScale = Vector2::Lerp(innerHealthBar->transform->localScale, healthBar->transform->localScale, 2.0f * DuckTime::GetDeltaTime());

		if (m_Health <= 0)
		{
			if (currentDeadTimer > 0)
				currentDeadTimer -= DuckTime::GetDeltaTime();
			else
				SceneManager::GetInstance().OpenScene("EndingScene");

			return;
		}

		if (stuntCountDown > 0.0f)
		{
			stuntCountDown -= DuckTime::GetDeltaTime();
		}
		else if (stunted == true)
		{
			stunted = false;
			if (renderer != nullptr)
				renderer->color = originalColor;
		}

		// This will execute every frame in update.
		// Range move toward player if too far
		if (inRange == false && Vector2::Distance(player->transform->GetPosition(), this->transform->GetPosition()) > minRangeDistance)
		{
			//anim->controller->SetBool("IsShooting", false);

			MoveTowardTarget(player->transform->GetPosition());
			return;
		}

		// Don't move when distance is with-in range
		if (Vector2::Distance(player->transform->GetPosition(), this->transform->GetPosition()) > maxRangeDistance)
		{
			//anim->controller->SetBool("IsShooting", false);

			inRange = false;
			return;
		}

		inRange = true;

		if (currentSideStepCD > 0)
		{
			anim->SetBool("IsLeftAttact", false);
			anim->SetBool("IsRightAttact", false);
			currentSideStepCD -= DuckTime::GetDeltaTime();
		}
		else
		{
			if (sideSteping == false)
			{
				//anim->controller->SetBool("IsShooting", false);				
				
				if (currentSideStepTimer < sideStepLimitDuration / max_chicken_to_throw)
				{
					currentSideStepTimer += DuckTime::GetDeltaTime();
					return;
				}

				Vector2 dirToTar = player->transform->GetPosition() - this->transform->GetPosition();
				int dir = std::rand() % 2;
				dirToTar = dirToTar.normalized() * sideStepDist;
				// Either move 90 left or right
				sideStepPos = (dir > 0) ? this->transform->GetPosition() + Vector2(dirToTar.y, -dirToTar.x) : this->transform->GetPosition() + Vector2(-dirToTar.y, dirToTar.x);
				if (m_Health < m_MaxHealth * 2 / 3)
				{
					ChickenPhase(dir);
					if (--remain_chicken_to_throw == 0) sideSteping = true;
				}
				else
				{
					sideSteping = true;
				}

				currentSideStepTimer = 0;
			}

			if (sideSteping == true)
			{
				if (Vector2::Distance(sideStepPos, this->transform->GetPosition()) > 0.5f && currentSideStepTimer < sideStepLimitDuration)
				{
					currentSideStepTimer += DuckTime::GetDeltaTime();
					MoveTowardTarget(sideStepPos);
					return;
				}

				sideSteping = false;
				remain_chicken_to_throw = max_chicken_to_throw;
				currentSideStepCD = sideStepCD;
				currentSideStepTimer = 0;
				return;
			}
		}

		Vector2 lookDir = this->transform->GetPosition() - player->transform->GetPosition();
		lookDir.Normalize();
		this->transform->localEulerAngles = (atan2(lookDir.y, lookDir.x) * 180 / M_PI) - 90;

		// Boss phases
		{
			// Phase 1
			if (m_Health > m_MaxHealth * 2 / 3)
			{
				if (m_PhaseOne == false)
				{
					m_PhaseOne = true;
					m_ProjectileAmount = 6;
					std::cout << "First Phase" << std::endl;
				}
				ProjectilePhase();
			}
			// Phase 2
			else if (m_Health > m_MaxHealth * 1 / 3)
			{
				if (m_PhaseTwo == false)
				{
					m_PhaseTwo = true;
					m_ProjectileAmount = 6;
					std::cout << "Second Phase" << std::endl;

				}
				ProjectilePhase();
			}
			// Phase 3
			else if (m_Health > 0)
			{
				if (m_PhaseThree == false)
				{
					m_PhaseThree = true;
					m_ProjectileAmount = 8;
					m_ShootingDelay = 3.0f;
					sideStepCD = 6.0f;
					std::cout << "Third Phase" << std::endl;
				}
				ProjectilePhase();
			}
			else
			{
				//_chickenManager
				m_Defeated = true;
			}
			
		}
	}

	void BossController::SpawnProjectile(int amount)
	{

		for (size_t i = 0; i < amount; i++)
		{
			for (size_t j = 0; j < 3; j++)
			{
				GameObject* proj = new GameObject("BossProjectile Clone");
				proj->AddComponent<SpriteRenderer>("orange_energy").order = 3;
				proj->AddComponent<CircleCollider>().radius = 23.65f;
				proj->AddComponent<ProjectileObject>();
				proj->transform->localScale = Vector2(0.8f, 0.8f);
				proj->GetComponent<CircleCollider>().isTrigger = true;

				Vector2 lookDir = player->transform->GetPosition() - this->transform->GetPosition();
				lookDir.Normalize();
				float aimAngle = (atan2(lookDir.y, lookDir.x) * 180 / M_PI);

				Vector2 aimDir = Vector2(cos(aimAngle*M_PI / 180), sin(aimAngle*M_PI / 180));
				if (j == 0)
				{
					aimDir = Vector2(cos((aimAngle - 45)*M_PI / 180), sin((aimAngle - 45)*M_PI / 180));
				}
				else if (j == 2)
				{
					aimDir = Vector2(cos((aimAngle + 45)*M_PI / 180), sin((aimAngle + 45)*M_PI / 180));
				}
				proj->GetComponent<ProjectileObject>().SetDuration(15);
				proj->GetComponent<ProjectileObject>().Setup(aimDir, this->transform->GetPosition(), this->gameObject->name, "solid_layer", player->tag, "Chicken", m_ProjectileSpeed, m_ProjectileDamage, m_ShootingCooldown * i, head->transform, Vector2(aimDir.y, -aimDir.x).normalized() * 0.3f);
			}
		}
	}

	void BossController::ProjectilePhase()
	{
		if (m_ShootingTimer > 0)
		{
			m_ShootingTimer -= DuckTime::GetDeltaTime();
		}
		else
		{
			m_ShootingTimer = m_ShootingDelay;
			SpawnProjectile(m_ProjectileAmount);
		}
	}

	void BossController::ChickenPhase(int side)
	{
		if (side > 0)
			anim->SetBool("IsRightAttact", true);
		else
			anim->SetBool("IsLeftAttact", true);

		GameObject* chicken = new GameObject("Chicken");
		chicken->tag = "Chicken";
		chicken->layer = "solid_layer";
		chickens.push_back(&chicken->AddComponent<ChickenController>());
		chicken->transform->SetPosition(head->transform->GetPosition());
		chicken->GetComponent<ChickenController>().Setup((side > 0) ? MinionType::MELEE : MinionType::RANGE, player);
		chicken->GetComponent<ChickenController>().Launch(200.0f, 1.5f);
	}

	void BossController::Hitted(int amount)
	{
		if (m_Health <= 0)
			return;
		stunted = true;
		stuntCountDown = 0.15f;
		renderer->color = Color(1.0f, 0.8f, 0.8f);
		player->GetComponent<DuckController>().ChargeUp(5);
		Character::Hitted(amount);
		UpdateUI();

		if (m_Health <= 0)
		{
			anim->SetBool("IsDead", true);
			for (size_t i = 0; i < chickens.size(); i++)
			{
				chickens[i]->Dead();
			}
			renderer->color = Color();
			renderer->order = 1;
			rid->velocity = (this->transform->GetPosition() - player->transform->GetPosition()).normalized() * 50.0f;
			this->gameObject->GetComponent<CircleCollider>().isTrigger = true;
			currentDeadTimer = endDuration;
			WinLoseScreen::IsGoodEnd = true;
		}
	}
}