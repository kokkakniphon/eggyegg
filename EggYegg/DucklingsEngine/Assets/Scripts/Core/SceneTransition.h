#pragma once
#include "MonoBehaviour.h"

#include "GameObject.h"
#include "SpriteRenderer.h"
#include "Time.h"
#include "Camera.h"
#include "SceneManager.h"
#include "Scripts/Extension/NonlinearManipulator.h"
#include "Mathf.h"

namespace DucklingsEngine
{
	class SceneTransition : public MonoBehaviour
	{
	private:
		float _transitionTimer = 0;
		// In (sec)
		float _transitionDuration = 2.0f;
		bool _transitionEnded = true;

		Vector2 _oldPos, _newPos;

		std::string _desScene = "";

	public:

		SpriteRenderer* _transitionOverlay;

		SceneTransition() = default;
		~SceneTransition() = default;

		void Start() override
		{
			// This will execute once at the begginning of the scene.
			_transitionOverlay->color.a = 1.0f;
			_transitionTimer = _transitionDuration / 2.0f;
			_oldPos = Camera::mainCamera->GetTransform()->GetPosition();
			_newPos = _oldPos;
		}
		void Update() override
		{
			// This will execute every frame in update.

			if (_transitionTimer > 0)
			{
				_transitionTimer -= DuckTime::GetDeltaTime();

				// There should be at least 1 camera in the scene.
				float timerNormalized = 1 - (_transitionTimer) / (_transitionDuration);

				// This make the transition go from 0 to 1 and down to 0
				_transitionOverlay->color.a = (timerNormalized <= 0.6) ? Mathf::min(NonlinearManipulator::SmoothStop2(timerNormalized * 2), 1.0f) : 1 - Mathf::min(NonlinearManipulator::SmoothStart2((timerNormalized - 0.5f) * 2), 1.0f);

				// Just to check the loading scene transition is working ---- Edit: Yep it work.
				if (_desScene != "" && timerNormalized > 0.5)
				{
					SceneManager::GetInstance().OpenScene(_desScene);
				}


				float valMulti = NonlinearManipulator::Mix(NonlinearManipulator::SmoothStart2, NonlinearManipulator::SmoothStop2, 0.5f, timerNormalized);
				Vector2 difPos = _oldPos - _newPos;
				Vector2 desPos = Vector2( _oldPos.x + difPos.x * valMulti, _oldPos.y + difPos.y * valMulti);
				Camera::mainCamera->GetTransform()->SetPosition(desPos);
			}
			else if (_transitionEnded == false)
			{
				_transitionEnded = true;
				
				_transitionOverlay->color.a = 0;
			}
		}

		void StartTransition(Vector2 transitionDir, float transitionDuration = -1.0f, std::string toScene = "")
		{
			if (transitionDuration != -1.0f) _transitionDuration = transitionDuration;

			_desScene = toScene;

			_transitionTimer = _transitionDuration;
			
			_newPos = transitionDir;
			_oldPos = Camera::mainCamera->GetTransform()->GetPosition();

			_transitionEnded = false;
		}

	};
}

