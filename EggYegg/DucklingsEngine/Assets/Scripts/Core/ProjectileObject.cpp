#include "Scripts/Boss/ProjectileObject.h"
#include "Scripts/Chicken/ChickenController.h"
#include "Scripts/Duck/DuckController.h"

namespace DucklingsEngine
{
	void ProjectileObject::OnTriggerEnter()
	{
		std::vector<Collider*> colliders = this->gameObject->GetComponent<CircleCollider>().collidedTriggers;

		for (size_t i = 0; i < colliders.size(); i++)
		{
			Object* obj = colliders[i]->gameObject;
			if (obj->layer == m_CollideLayer && obj->name != this->gameObject->name && obj->name != m_SourceName && obj->tag != m_IgnoreTag)
			{
				if (obj->tag == m_TargetTag)
				{
					// Try to get the projectile reciever
					ChickenController* character = (obj->HasComponent<ChickenController>()) ? &obj->GetComponent<ChickenController>() : nullptr;

					if (character != nullptr)
					{
						character->GotHitted(m_Damage);
					}
					else
					{
						Character* character = &obj->GetComponent<DuckController>();
						if (character == nullptr) character = &obj->GetComponent<BossController>();

						if (character != nullptr)
						{
							character->Hitted(m_Damage);
						}
					}
				}
				
				this->gameObject->Destroy();
			}
		}
	}
	void ProjectileObject::Setup(const Vector2 & dir, const Vector2 & startPos, std::string sourceName, std::string collideLayer, std::string targetTag, std::string ignoreTag, float speed, unsigned int damage, float launchDelay, Transform* startTrans, const Vector2& stearing)
	{
		if (launchDelay != -1.0f)
		{
			std::cout << "Launch Delayed " << launchDelay << std::endl;
			m_LaunchDelayTimer = launchDelay;
			this->gameObject->GetComponent<CircleCollider>().enabled = false;
			this->gameObject->GetComponent<SpriteRenderer>().enabled = false;
		}

		this->startTrans = startTrans;
		m_Stearing = stearing;

		if (collideLayer != "") m_CollideLayer = collideLayer;
		if (targetTag != "") m_TargetTag = targetTag;
		if (sourceName != "") m_SourceName = sourceName;
		if (ignoreTag != "") m_IgnoreTag = ignoreTag;

		m_Dir = dir;
		m_Dir.Normalize();

		this->transform->SetPosition(startPos);

		if (speed != -1.0f)	m_Speed = speed;
		if (damage != 0) m_Damage = damage;
	}

	void ProjectileObject::Update()
	{
		// This will execute every frame in update.

		// Move the projectile
		{
			if (m_Launch == true)
			{
				this->transform->Translate(m_Dir * DuckTime::GetDeltaTime() * m_Speed);

				m_Dir += m_Stearing * DuckTime::GetDeltaTime();
				m_Dir.Normalize();

				OnTriggerEnter();

				m_duration -= DuckTime::GetDeltaTime();
				if (m_duration <= 0.0f)
					this->gameObject->Destroy();
			}
			else
			{
				if (m_LaunchDelayTimer > 0)
				{
					m_LaunchDelayTimer -= DuckTime::GetDeltaTime();
				}
				else
				{
					m_Launch = true;
					if (startTrans != nullptr)
						this->transform->SetPosition(startTrans->GetPosition());
					this->gameObject->GetComponent<CircleCollider>().enabled = true;
					this->gameObject->GetComponent<SpriteRenderer>().enabled = true;
				}
			}
		}
	}
}