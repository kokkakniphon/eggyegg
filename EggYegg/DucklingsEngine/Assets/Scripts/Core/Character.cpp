#include "Character.h"

#include "Mathf.h"

namespace DucklingsEngine
{
	void Character::Start()
	{
		m_Health = m_MaxHealth;
	}

	void Character::Update()
	{

	}

	void Character::Healed(int amount)
	{
		m_Health = Mathf::min(m_MaxHealth, amount);
	}

	void Character::Hitted(int damage)
	{
		if (m_Health > 0 && m_IsDead == false)
		{
			m_Health = Mathf::max(0, m_Health - damage);
		}
		else
		{
			m_IsDead = true;
		}
	}
}