#pragma once
#include "MonoBehaviour.h"

#include "Mathf.h"

namespace DucklingsEngine
{
	class Character : public MonoBehaviour
	{
	protected:
		int m_Health = 100;
		int m_MaxHealth = 100;

		// For now let just do the int, later will implement the delegate.
		bool m_IsDead = false;

	public:
		Character() = default;
		~Character() = default;

		void Healed(int amount)
		{
			m_Health += amount;
			m_Health = Mathf::min(m_MaxHealth, m_Health);
		}

		virtual void Hitted(int damage) = 0
		{
			if (m_Health > 0 && m_IsDead == false)
			{
				m_Health = Mathf::max(0, m_Health - damage);
			}
			else
			{
				m_IsDead = true;
			}
		}

		int GetHealth()
		{
			return m_Health;
		}

		int GetMaxHealth()
		{
			return m_MaxHealth;
		}

		float GetHealthPercent()
		{
			return (float)m_Health / m_MaxHealth;
		}

		void Start() override
		{
			// This will execute once at the begginning of the scene.

			m_Health = m_MaxHealth;
		}
		void Update() override
		{
			// This will execute every frame in update.


		}

	};
}

