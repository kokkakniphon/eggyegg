#pragma once
#include "MonoBehaviour.h"
#include "GameObject.h"
#include "Scripts/Duck/DuckController.h"
#include "Camera.h"

namespace DucklingsEngine
{
	class CameraController : public MonoBehaviour
	{
	public:
		CameraController() = default;
		~CameraController() = default;
		DuckController* _player;

		// TBLR
		Vector4 _boundary = Vector4(540, 1620, 960, 2880);

		float max_mouseDist = 540;
		float smoothSpeed = 2.0f;

		void Start() override
		{
			// This will execute once at the begginning of the scene.

		}
		void Update() override
		{
			if (_player == nullptr)
				return;
			// This will execute every frame in update.
			if (_player->currentTrackTransform != nullptr)
			{
				Vector2 camPos = Camera::mainCamera->WorldToScreenPoint(Input::GetMousePosition());
				Vector2 dir = (camPos - _player->currentTrackTransform->GetPosition()) / 2.0f;
				Vector2 camDes = (dir.magnitude() > max_mouseDist) ? _player->currentTrackTransform->GetPosition() + dir.normalized() * max_mouseDist : _player->currentTrackTransform->GetPosition() + dir;
				Vector2 targetPos = Vector2::Lerp(_player->currentTrackTransform->GetPosition(), camDes, 0.6f);
				Vector2 desPos = Vector2::Lerp(this->transform->GetPosition(), targetPos, smoothSpeed * DuckTime::GetDeltaTime());

				if (desPos.y < _boundary.x) desPos.y = _boundary.x;
				else if (desPos.y > _boundary.y) desPos.y = _boundary.y;
				if (desPos.x < _boundary.z) desPos.x = _boundary.z;
				else if (desPos.x > _boundary.w) desPos.x = _boundary.w;

				this->transform->SetPosition(desPos);
			}
		}

	};
}

