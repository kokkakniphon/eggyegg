#include "CutsceneSlideShow.h"
#include "Engine.h"
#include "Mathf.h"
#include "SceneManager.h"
#include "GM.h"

namespace DucklingsEngine
{
	bool CutSceneSlideShow::FadeIn(GameObject* obj)
	{
		//return true uf completely fade in
		SpriteRenderer* pic = &obj->GetComponent<SpriteRenderer>();
		if (pic->color.a < 1.0f)
		{
			pic->color.a += DuckTime::GetDeltaTime() * fadeDir;
			pic->color.a = Mathf::min(1.0f, pic->color.a);
			return false;
		}
			return true;
	}

	bool CutSceneSlideShow::FadeOut(GameObject* obj)
	{
		//return true if completely fade out
		SpriteRenderer* pic = &obj->GetComponent<SpriteRenderer>();
		if (pic->color.a > 0.0f)
		{
			pic->color.a -= DuckTime::GetDeltaTime() * fadeDir;
			pic->color.a = Mathf::max(0.0f, pic->color.a);
			return false;
		}
			return true;
	}

	bool CutSceneSlideShow::Side(GameObject* obj, float move)
	{
		//return ture if in place
		Transform* img = &obj->GetComponent<Transform>();
		//find whether its left or right
		//if the picture is on the right of the screen, move left
		if (img->GetPosition().x > 0)  move = -move;

		if (img->GetPosition().x > -10 && img->GetPosition().x < 10)
		{
			return true;
		}
		else
		{
			img->SetPosition(Vector2(img->GetPosition().x + (move * DuckTime::GetDeltaTime()),
				img->GetPosition().y));
			return false;
		}
	}

	bool CutSceneSlideShow::Vertical(GameObject* obj, float move)
	{
		//return ture if in place
		Transform* img = &obj->GetComponent<Transform>();
		//find whether its left or right
		//if the picture is on the right of the screen, move left
		if (img->GetPosition().y > 0)  move = -move;

		if (img->GetPosition().y > -10 && img->GetPosition().y < 10)
		{
			return true;
		}
		else
		{
			img->SetPosition(Vector2(img->GetPosition().x
				, img->GetPosition().y + (move * DuckTime::GetDeltaTime())));
			return false;
		}
	}

	bool SpecialVertical(GameObject* obj, float move)
	{
		//return ture if in place
		Transform* img = &obj->GetComponent<Transform>();
		//find whether its left or right
		move = -move;
		if (img->GetPosition().y < SCREEN_HEIGHT * 0.75f +10.0f && img->GetPosition().y > SCREEN_HEIGHT * 0.75f -10.0f)
		{
			return true;
		}
		else
		{
			img->SetPosition(Vector2(img->GetPosition().x
				, img->GetPosition().y - (move * DuckTime::GetDeltaTime())));
			return false;
		}
	}

	void CutSceneSlideShow::Assign(GameObject* obj, int index)
	{
		pics[index] = obj;
	}

	//8 cutscenes in total
	void CutSceneSlideShow::Start() 
	{
		pics[1]->transform->SetPosition(Vector2(-SCREEN_WIDTH, 0.0f));
		pics[2]->transform->SetPosition(Vector2(0.0f, SCREEN_HEIGHT));
	}

	//use left click to proceed
	void CutSceneSlideShow::Update()
	{
		//mouse event to skip the scene or go to the first level
		if ((Input::GetMouseButtonDown(SDL_BUTTON_LEFT) && page < 8) || timer >= autoPlayTimer)
		{
			fadeOut = true;
			//std::cout << "go to page " << page+1 << std::endl;
		}
		else if (Input::GetMouseButtonDown(SDL_BUTTON_LEFT) && page == 8)
		{
			//change scene
			SceneManager::GetInstance().OpenScene("Tutorial");
		}

		if (fadeOut == true)
		{
			FadeOut(pics[1]);
			FadeOut(pics[2]);

			if (FadeOut(pics[0]))
			{
				fadeOut = false;
				//when the picture is completely up, resest timer
				timer = 0.0f;
				std::cout << "plus page" << std::endl;
				page++;
				if (page == 2)
				{
					pics[0]->GetComponent<SpriteRenderer>().SetSprite("Cutscenes/2-1");
					pics[1]->GetComponent<SpriteRenderer>().SetSprite("Cutscenes/2-2");
					pics[2]->GetComponent<SpriteRenderer>().SetSprite("Cutscenes/2-3");
					//set foreground1 and 2 to be below the screen
					pics[1]->transform->SetPosition(Vector2(0.0f, SCREEN_HEIGHT));
					pics[2]->transform->SetPosition(Vector2(0.0f, SCREEN_HEIGHT*1.2f));
					//set foregroud alpha to 1
					pics[1]->GetComponent<SpriteRenderer>().color.a = 1.0f;
					pics[2]->GetComponent<SpriteRenderer>().color.a = 1.0f;
				}
				else if (page == 3)
				{
					pics[0]->GetComponent<SpriteRenderer>().SetSprite("Cutscenes/3-1");
				}
				else if (page == 4) pics[0]->GetComponent<SpriteRenderer>().SetSprite("Cutscenes/4.1");
				else if (page == 5) pics[0]->GetComponent<SpriteRenderer>().SetSprite("Cutscenes/4.2");
				else if (page == 6) pics[0]->GetComponent<SpriteRenderer>().SetSprite("Cutscenes/4.3");
				else if (page == 7)
				{
					pics[0]->GetComponent<SpriteRenderer>().SetSprite("Cutscenes/4.4-1");
					pics[1]->GetComponent<SpriteRenderer>().SetSprite("Cutscenes/4.4-2");
					pics[1]->transform->SetPosition(Vector2(SCREEN_WIDTH * 0.5f, 0.0f));
					pics[1]->GetComponent<SpriteRenderer>().color.a = 1.0f;
				}
				else if (page == 8)
				{
					pics[0]->GetComponent<SpriteRenderer>().SetSprite("Cutscenes/5-1");
					pics[2]->GetComponent<SpriteRenderer>().SetSprite("Cutscenes/5-light");
					pics[1]->GetComponent<SpriteRenderer>().SetSprite("Cutscenes/5-shadow");

					pics[0]->transform->SetPosition(Vector2(0.0f, -SCREEN_HEIGHT * 0.75f));
					pics[1]->transform->SetPosition(Vector2(0.0f, SCREEN_HEIGHT * 0.75f));
					pics[2]->transform->SetPosition(Vector2(0.0f, -SCREEN_HEIGHT * 0.75f));

					pics[1]->GetComponent<SpriteRenderer>().color.a = 1.0f;
					pics[2]->GetComponent<SpriteRenderer>().color.a = 1.0f;
				}
			}
				
		}
		else
		{
			timer += DuckTime::GetDeltaTime();
			if (page == 1)
			{
				//slide to the right
				Side(pics[1]);
			}
			else if (page == 2)
			{
				//move foreground pics up
				if (FadeIn(pics[0]))
				{
					Vertical(pics[1]);
					Vertical(pics[2]);
				}
			}
			else if (page == 3)
			{
				FadeIn(pics[0]);
			}
			else if (page == 4)
			{
				FadeIn(pics[0]);
			}
			else if (page == 5)
			{
				FadeIn(pics[0]);
			}
			else if (page == 6)
			{
				FadeIn(pics[0]);
			}
			else if (page == 7)
			{
				if (FadeIn(pics[0]))
				{
					Side(pics[1], 1200.0f);
				}
			}
			else if (page == 8)
			{
				FadeIn(pics[2]);
				FadeIn(pics[1]);
				FadeIn(pics[0]);
				SpecialVertical(pics[0], 600);
				SpecialVertical(pics[2], 600);
				if (timer >= autoPlayTimer)
				{
					SceneManager::GetInstance().OpenScene("Tutorial");
				}
			}
		}
	}
}