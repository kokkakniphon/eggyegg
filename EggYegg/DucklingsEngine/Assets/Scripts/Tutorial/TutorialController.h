#pragma once
#include "MonoBehaviour.h"

#include "GameObject.h"

#include "Input.h"
#include "TextRenderer.h"

#include "Scripts/Core/SceneTransition.h"
#include "Scripts/Duck/DuckController.h"
#include "BoxCollider.h"

namespace DucklingsEngine
{
	class TutorialController : public MonoBehaviour
	{
	private:

		// Tutorial step
		unsigned int _tutorialCompletionAmout = 0;
		bool _tutorial_Completed, _updatedMission;

		float _arrowMoveDis = 0.15f;

		// Scene transition
		bool _transitioning;

		void OnTriggerEnter()
		{
			// If player touch next room arrow collider, switch to next room.
			if (_nextRoomArrow->GetActive() == true && _transitioning == false)
			{
				std::vector<Collider*> colliders = _nextRoomArrow->GetComponent<BoxCollider>().collidedTriggers;

				for (size_t i = 0; i < colliders.size(); i++)
				{
					if (colliders[i]->gameObject->name == "Player")
					{
						_transitioning = true;

						_sceneTransition->StartTransition(Camera::mainCamera->GetTransform()->GetPosition() + Vector2(0, 0), 1.0f, "FirstLevel");
					}
				}
			}
		}

	public:

		GameObject* _obstacle;
		GameObject* _nextRoomArrow;
		DuckController* _duck;
		SceneTransition* _sceneTransition;
		TextRenderer* _textRenderer;
		GameObject* _missionObj;

		static bool _tutorial_HITHAY, _tutorial_PICKEDUP, _tutorial_RMOUSE;

		TutorialController() = default;
		~TutorialController() = default;

		void PickedUpCrate() { _tutorial_PICKEDUP = true; }

		void Start() override
		{
			// This will execute once at the begginning of the scene.
			_tutorial_HITHAY = false;
			_tutorial_PICKEDUP = false;
			_tutorial_RMOUSE = false;
			_nextRoomArrow->SetActive(false);
		}
		void Update() override;
		

	};
}

