#include "TutorialController.h"
#include "Scripts/GM.h"
namespace DucklingsEngine
{
	bool TutorialController::_tutorial_HITHAY = false;
	bool TutorialController::_tutorial_PICKEDUP = false;
	bool TutorialController::_tutorial_RMOUSE = false;

	void TutorialController::Update()
	{
		// This will execute every frame in update.
		GM::GetInstance().UpdateGM();

		OnTriggerEnter();

		// TUTORIAL KEY
		//		When player press all of the action key, show next room arrow for player to follow.
		_tutorial_HITHAY |= (_obstacle->name.compare("BulletDroperHay"));
		_tutorial_RMOUSE |= Input::GetMouseButtonDown(SDL_BUTTON_RIGHT);

		if (_updatedMission == false && _tutorial_HITHAY)
		{
			_updatedMission = true;
			_textRenderer->text = "Pick up the crate to complete the tutorial.";
			_textRenderer->RefreshText();
		}

		if (_tutorial_Completed == false && _tutorial_PICKEDUP && _tutorial_HITHAY && _tutorial_RMOUSE)
		{
			_tutorial_Completed = true;
			_missionObj->SetActive(false);
			_nextRoomArrow->SetActive(true);
		}
		// END TUTORIAL KEY

		if (Input::GetKeyDown(SDL_SCANCODE_RIGHT))
		{
			_sceneTransition->StartTransition(Camera::mainCamera->GetTransform()->GetPosition() + Vector2(0, 0), 1.0f, "FirstLevel");
		}

		// NEXT ROOM ARROW ANIMATION
		_nextRoomArrow->transform->Translate(Vector2(0, sin(DuckTime::GetTime() * 3) * _arrowMoveDis));
		// END NEXT ROOM ARROW ANIMATION
	}
}