#pragma once
#include "MonoBehaviour.h"
#include "Input.h"
#include "BoxCollider.h"
#include "CircleCollider.h"
#include "SpriteRenderer.h"

#include "GameObject.h"

namespace DucklingsEngine
{
	class PhysicSceneController : public MonoBehaviour
	{
	public:
		PhysicSceneController() = default;
		~PhysicSceneController() = default;

		bool isGravityEnable = false;

		void Start() override;
		void Update() override;
		void FixedUpdate() override;

		void SpawnCircle(float radius);
		void SpawnSquare();

		void SetPlayerSelection(int i) { playerSelection = i; }
		int GetPlayerSelection() { return playerSelection; }

		int GetCircleMax() { return circleMax; }

		void ChangeRigidbodyState();

	private:
		Vector2 move_dir;

		std::vector<GameObject*> circleVec;
		GameObject *circleParent = new GameObject("CircleParent");
		//GameObject *leftBar = new GameObject("LeftBar");
		//GameObject *rightBar = new GameObject("RightBar");
		//GameObject *spawner = new GameObject("Spawner");

		int playerSelection;
		int circleMax = 8;
		int circleCount = 0;
		
	};

	void PhysicSceneController::Start()
	{
		// This will execute once at the begginning of the scene.

		isGravityEnable = true;

		ChangeRigidbodyState();

	}

	void PhysicSceneController::Update()
	{
		// This will execute every frame in update.

		for (GameObject* circle : circleVec)
		{
			Vector2 objPos = circle->transform->GetPosition();
			if (objPos.x > 1350.0f)
				circle->transform->SetPosition(Vector2(-50.0f, objPos.y));
			if (objPos.x < -50.0f)
				circle->transform->SetPosition(Vector2(1350.0f, objPos.y));

			if (objPos.y > 800.0f)
				circle->transform->SetPosition(Vector2(objPos.x, -50.0f));
			if (objPos.y < -50.0f)
				circle->transform->SetPosition(Vector2(objPos.x, 800.0f));
		}

		if (Input::GetKeyDown(SDL_SCANCODE_F))
		{
			PhysicSceneController::ChangeRigidbodyState();
		}
	}

	void PhysicSceneController::FixedUpdate()
	{
		
	}

	void PhysicSceneController::SpawnCircle(float radius)
	{
		GameObject* circle = new GameObject("Circle" + std::to_string(radius));

		circle->transform->SetParent(circleParent->transform);
		circleVec.push_back(circle);
		circle->AddComponent<SpriteRenderer>("ball");

		circle->transform->localScale = Vector2(0.005f * radius, 0.005f * radius);
		circle->transform->SetPosition(Vector2(SCREEN_WIDTH / 4, 20));
		
		circle->AddComponent<Rigidbody>();
		circle->GetComponent<Rigidbody>().drag = 0.1f;

		circle->AddComponent<CircleCollider>().radius = radius * 1.1f;
	}

	void PhysicSceneController::SpawnSquare()
	{
		GameObject* square = new GameObject("Square");
		square->transform->SetPosition(Vector2(SCREEN_WIDTH / 4, 0));
		square->AddComponent<Rigidbody>();
		square->GetComponent<Rigidbody>().drag = 0.1f;
		square->AddComponent<BoxCollider>().size = Vector2(25, 25);;
	}

	void PhysicSceneController::ChangeRigidbodyState()
	{
		isGravityEnable = !isGravityEnable;

		for (GameObject* circle : circleVec)
		{
			if (isGravityEnable)
			{
				circle->GetComponent<Rigidbody>().gravityScale = GRAVITY;
				circle->GetComponent<SpriteRenderer>().color = Color(1, 0.5f, 0.5f);
			}
			else
			{
				circle->GetComponent<Rigidbody>().gravityScale = 0;
				circle->GetComponent<SpriteRenderer>().color = Color(0.5f, 0.5f, 1);
			}

		}
	}


}

