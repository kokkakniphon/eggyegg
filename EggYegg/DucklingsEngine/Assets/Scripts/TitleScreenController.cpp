#include "TitleScreenController.h"
#include "SceneManager.h"
#include "Mathf.h"
#include "Camera.h"
#include "GM.h"

namespace DucklingsEngine
{
	void TitleScreenController::Start()
	{
		GM::GetInstance().musicPlayer.SetAudioControllerPath("MusicController");
		GM::GetInstance().playerSfxPlayer.SetAudioControllerPath("DuckSoundController");
		GM::GetInstance().uiSfxPlayer.SetAudioControllerPath("UiController");
		GM::GetInstance().playerSfxPlayer.SetBool("Play", true);
		GM::GetInstance().musicPlayer.Play();
		// This will execute once at the begginning of the scene.
		renderer->gameObject->SetActive(true);
		renderer->color.a = 1.0f;
		renderer->order = 30;
		countDown = 1.0f;
		fadeDir = -1.0f;

		menu->SetActive(true);
		setting->SetActive(false);

		if (GM::GetInstance().IsFullscreen)
		{
			SDL_SetWindowFullscreen(Engine::GetInstance().GetWindow(), SDL_WINDOW_FULLSCREEN);
		}
		else
		{
			SDL_SetWindowFullscreen(Engine::GetInstance().GetWindow(), 0);
			SDL_SetWindowResizable(Engine::GetInstance().GetWindow(), SDL_FALSE);
			SDL_SetWindowSize(Engine::GetInstance().GetWindow(), Camera::mainCamera->rect.x, Camera::mainCamera->rect.y);
		}

		fullButton.buttonObj->GetComponent<SpriteRenderer>().enabled = GM::GetInstance().IsFullscreen;
		masterButton.buttonObj->transform->localPosition.x = (GM::GetInstance().MasterVolume) * 660 - 330;
		musicButton.buttonObj->transform->localPosition.x = (GM::GetInstance().MusicVolume) * 660 - 330;
		sfxButton.buttonObj->transform->localPosition.x = (GM::GetInstance().SfxVolume) * 660 - 330;
	}

	void TitleScreenController::Update()
	{
		// This will execute every frame in update.
		GM::GetInstance().UpdateGM();

		if (countDown > 0.0f)
			countDown -= DuckTime::GetDeltaTime();

		if (countDown <= 0)
		{
			if ((renderer->color.a > 0.0f && fadeDir < 0.0f) || (renderer->color.a < 1.0f && fadeDir > 0.0f))
				renderer->color.a += DuckTime::GetDeltaTime() * fadeDir;

			if (renderer->color.a < 0.0f && fadeDir < 0.0f)
			{
				renderer->color.a = 0.0f;
			}

			if (renderer->color.a > 1.0f && fadeDir > 0.0f)
			{
				renderer->color.a = 1.0f;

				SceneManager::GetInstance().OpenScene("StoryCutscene");
			}
		}

		Vector2 mousePos = Input::GetMousePosition();

		if (currentHoldButton == nullptr)
		{
			std::vector<Collider*> allCollider;
			ColliderManager::GetInstance().OverlapPoint(&allCollider, mousePos);

			for (size_t i = 0; i < allCollider.size(); i++)
			{
				if (allCollider[i]->gameObject->tag == "start")
				{
					startButton.isHovering = true;
				}
				else if (allCollider[i]->gameObject->tag == "option")
				{
					optionButton.isHovering = true;
				}
				else if (allCollider[i]->gameObject->tag == "quit")
				{
					quitButton.isHovering = true;
				}
				else if (allCollider[i]->gameObject->tag == "master")
				{
					masterButton.isHovering = true;
				}
				else if (allCollider[i]->gameObject->tag == "music")
				{
					musicButton.isHovering = true;
				}
				else if (allCollider[i]->gameObject->tag == "sfx")
				{
					sfxButton.isHovering = true;
				}
				else if (allCollider[i]->gameObject->tag == "full")
				{
					fullButton.isHovering = true;
				}
				else if (allCollider[i]->gameObject->tag == "back")
				{
					backButton.isHovering = true;
				}
			}
		}

		if (Input::GetMouseButtonDown(SDL_BUTTON_LEFT) && transitioning == false)
		{
			bool clicked = true;
			if (startButton.isHovering)
			{
				transitioning = true;
				renderer->gameObject->SetActive(true);
				fadeDir = 1.0f;
			}
			else if (optionButton.isHovering)
			{
				menu->SetActive(false);
				setting->SetActive(true);
			}
			else if (quitButton.isHovering)
			{
				Engine::GetInstance().Quit();
			}
			else if (masterButton.isHovering)
			{
				currentHoldButton = &masterButton;
			}
			else if (musicButton.isHovering)
			{
				currentHoldButton = &musicButton;
			}
			else if (sfxButton.isHovering)
			{
				currentHoldButton = &sfxButton;
			}
			else if (fullButton.isHovering)
			{
				GM::GetInstance().IsFullscreen = !GM::GetInstance().IsFullscreen;
				if (GM::GetInstance().IsFullscreen)
				{
					SDL_SetWindowFullscreen(Engine::GetInstance().GetWindow(), SDL_WINDOW_FULLSCREEN);
				}
				else
				{
					SDL_SetWindowFullscreen(Engine::GetInstance().GetWindow(), 0);
					SDL_SetWindowResizable(Engine::GetInstance().GetWindow(), SDL_FALSE);
					SDL_SetWindowSize(Engine::GetInstance().GetWindow(), Camera::mainCamera->rect.x, Camera::mainCamera->rect.y);
				}
				fullButton.buttonObj->GetComponent<SpriteRenderer>().enabled = GM::GetInstance().IsFullscreen;
			}
			else if (backButton.isHovering)
			{
				menu->SetActive(true);
				setting->SetActive(false);
			}
			else
			{
				clicked = false;
			}

			if (clicked)
			{
				GM::GetInstance().uiSfxPlayer.SetBool("IsClick", true);
				GM::GetInstance().uiSfxPlayer.Play();
			}
		}
		else if (Input::GetMouseButtonUp(SDL_BUTTON_LEFT))
		{
			if (currentHoldButton != nullptr)
			{
				GM::GetInstance().uiSfxPlayer.SetBool("IsClick", true);
				GM::GetInstance().uiSfxPlayer.Play();
			}
			currentHoldButton = nullptr;
		}
		else if (Input::GetMouseButton(SDL_BUTTON_LEFT) && transitioning == false)
		{
			if (currentHoldButton != nullptr)
			{
				currentHoldButton->buttonObj->transform->localPosition.x = Mathf::min(330.0f, Mathf::max(-330.0f, mousePos.x - 1060));
				if (currentHoldButton->buttonObj->tag == "master")
					GM::GetInstance().MasterVolume = currentHoldButton->buttonObj->transform->localPosition.x / 660 + 0.5f;
				if (currentHoldButton->buttonObj->tag == "music")
					GM::GetInstance().MusicVolume = currentHoldButton->buttonObj->transform->localPosition.x / 660 + 0.5f;
				if (currentHoldButton->buttonObj->tag == "sfx")
					GM::GetInstance().SfxVolume = currentHoldButton->buttonObj->transform->localPosition.x / 660 + 0.5f;
			
				GM::GetInstance().UpdateVolume();
			}
		}

		startButton.Update();
		optionButton.Update();
		masterButton.Update();
		musicButton.Update();
		sfxButton.Update();
		fullButton.Update();
		backButton.Update();
		quitButton.Update();

		startButton.Clear();
		optionButton.Clear();
		masterButton.Clear();
		musicButton.Clear();
		sfxButton.Clear();
		fullButton.Clear();
		backButton.Clear();
		quitButton.Clear();

		for (size_t i = 0; i < layers.size(); i++)
		{
			layers[i].screenObj->transform->SetPosition(this->transform->GetPosition() + ((this->transform->GetPosition() - mousePos) * layers[i].layerDepth * 0.1f));
		}
	}

	void TitleScreenController::Setup(GameObject * startObj, GameObject * optionObj, GameObject* quitObj, GameObject* masterObj, GameObject* musicObj, GameObject* sfxObj, GameObject* fullObj, GameObject* backObj)
	{
		startButton.buttonObj = startObj;
		startButton.originalSize = Vector2(0.583f, 0.583f);
		optionButton.buttonObj = optionObj;
		optionButton.originalSize = Vector2(0.583f, 0.583f);
		quitButton.buttonObj = quitObj;
		quitButton.originalSize = Vector2(0.583f, 0.583f);
		masterButton.buttonObj = masterObj;
		masterButton.originalSize =	Vector2(0.54f, 1.0f);
		musicButton.buttonObj = musicObj;
		musicButton.originalSize = Vector2(0.54f, 1.0f);
		sfxButton.buttonObj = sfxObj;
		sfxButton.originalSize = Vector2(0.54f, 1.0f);
		fullButton.buttonObj = fullObj;
		fullButton.originalSize = Vector2(0.7f, 0.7f);
		backButton.buttonObj = backObj;
		backButton.originalSize = Vector2(1.0f, 1.0f);
	}

	void TitleScreenController::AddScreenLayer(GameObject * obj, float depth)
	{
		layers.push_back(ScreenLayer(obj, depth));
	}
}