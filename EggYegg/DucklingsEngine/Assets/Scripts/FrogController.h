#pragma once
#include "MonoBehaviour.h"

#include "SpriteRenderer.h"
#include "Input.h"
#include "Rigidbody.h"
#include "BallController.h"
#include "BallPathController.h"

namespace DucklingsEngine
{
	class FrogController : public MonoBehaviour
	{
	private:
		float lookAtMouseOffset = -90.0f;
		GameObject* frogTongue;
		BallColor currentBallColor;
		GameObject* currentBall;

		float shootSpeed = 500;

	public:
		FrogController() = default;
		~FrogController() = default;

		void Start() override;
		void Update() override;

		void ShootBall(Vector2 dir);

		BallPathController *path;
	};

	void FrogController::Start()
	{
		// This will execute once at the begginning of the scene.
		this->gameObject->AddComponent<SpriteRenderer>("SMALLFROGonPAD");

		//frogTongue = new GameObject("FROG_tongue");
		//frogTongue->transform->SetParent(*this->transform);
		//frogTongue->transform->localPosition = Vector2(0.0f, 50.0f);
		//frogTongue->AddComponent<SpriteRenderer>("Tongue");
		currentBall = new GameObject("FROG_currentBall");
		currentBall->transform->SetPosition(this->transform->GetPosition());
		//SpriteManager::GetInstance().AddSprite(new Sprite("baBallBlue", true, 1, 47), std::to_string(currentBall->GetUniqueID()));
		//currentBall->AddComponent<SpriteRenderer>(SpriteManager::GetInstance().GetSprite(std::to_string(currentBall->GetUniqueID())));
	}

	void FrogController::Update()
	{
		// This will execute every frame in update.

		// Rotate the frog toward mouse.
		Vector2Int mousePos = Input::GetMousePosition();
		Vector2 frogPos = this->transform->GetPosition();
		Vector2 frogToMouse = Vector2(mousePos.x - frogPos.x, mousePos.y - frogPos.y);
		this->transform->localEulerAngles = (atan2(frogToMouse.y, frogToMouse.x) * 180 / M_PI) + lookAtMouseOffset;

		if (Input::GetMouseButtonDown(SDL_BUTTON_LEFT))
		{
			ShootBall(frogToMouse);
		}
	}

	void FrogController::ShootBall(Vector2 dir)
	{
		GameObject *newBall = new GameObject("ShootBall");
		newBall->transform->SetPosition(this->transform->GetPosition());
		newBall->AddComponent<CircleCollider>().radius = 15.0f;
		newBall->AddComponent<Rigidbody>().velocity = (dir / dir.magnitude())* shootSpeed;
		newBall->GetComponent<Rigidbody>().gravityScale = 0.0f;
		newBall->GetComponent<Rigidbody>().drag = 0.0f;
		newBall->AddComponent<SpriteRenderer>();
		newBall->AddComponent<Animator>();
		newBall->AddComponent<BallController>().ballColor = BallColor::Red;
	}
}

