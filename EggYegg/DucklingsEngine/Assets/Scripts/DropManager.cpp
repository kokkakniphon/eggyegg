#include "DropManager.h"
#include "GM.h"
#include "Duck/DuckController.h"
#include "SpriteRenderer.h"
#include "Chicken/ChickenController.h"

#include <stdlib.h>
#include <time.h>

namespace DucklingsEngine
{
	float DropManager::normalMinionDropRate = 0.33f;
	DropManager* DropManager::instance = nullptr;

	void DropManager::Start()
	{
		instance = this;
	}

	void DropManager::Update() {}

	bool DropManager::CheckPassDrop(MinionType const minionType)
	{
		bool passDropRate = false;
		float rate = ((float)(std::rand() % 100 + 1) / 100);
		if (minionType == MinionType::MELEE || minionType == MinionType::RANGE)
		{
			passDropRate = (rate < normalMinionDropRate);
		}

		return passDropRate;
	}

	DropData const DropManager::GetDrop()
	{
		DuckController* player = &GM::GetInstance().GetPlayerPtr()->GetComponent<DuckController>();

		// Set drop rate depend on player health
		float bulletRate = 0.5f;
		float healthRate = 0.5f;
		if (player->GetHealthPercent() <= 0.0f)
		{
			healthRate = 1.0f;
			bulletRate = 0.0f;
		}
		else if (player->GetHealthPercent() >= 1.0f)
		{
			healthRate = 0.0f;
			bulletRate = 1.0f;
		}
		else if (player->GetHealthPercent() > 0.2f)
		{
			healthRate = 0.2f;
			bulletRate = 1.0f - healthRate;
		}
		else
		{
			healthRate = 0.33f;
			bulletRate = 1.0f - healthRate;
		}

		// Random drop type
		int dropRand = std::rand() % 100 + 1;

		DropData drop;
		drop.type = (dropRand <= healthRate * 100) ? DropData::DropType::Health : DropData::DropType::Bullet;
		drop.amount = (drop.type) ? 2 : 15;

		return drop;
	}

	void DropManager::SpawnDrop(const Vector2& position, MinionType const minionType)
	{
		if (CheckPassDrop(minionType) == false)
			return;

		DropData dropData = GetDrop();
		GameObject* drop = new GameObject("Drop");
		drop->AddComponent<Drop>().Setup(dropData);
		drop->AddComponent<CircleCollider>().isTrigger = true;
		drop->GetComponent<CircleCollider>().radius = 30.0f;
		drop->transform->SetPosition(position);
	}

	void DropManager::ForceSpawnDrop(const Vector2& position, const DropData& dropData)
	{
		GameObject* drop = new GameObject("Drop");
		drop->AddComponent<Drop>().Setup(dropData);
		drop->AddComponent<CircleCollider>().isTrigger = true;
		drop->GetComponent<CircleCollider>().radius = 30.0f;
		drop->transform->SetPosition(position);
	}

	

}