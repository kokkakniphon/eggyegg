#pragma once
#include "MonoBehaviour.h"

#include "Input.h"
#include "FileManager.h"

namespace DucklingsEngine
{
	class YAML_TestScript : public MonoBehaviour
	{
	public:
		YAML_TestScript() = default;
		~YAML_TestScript() = default;

		void Start() override;
		void Update() override;

	};

	void YAML_TestScript::Start()
	{
		// This will execute once at the begginning of the scene.
		std::vector<GameObject*> entities = Engine::GetInstance().GetManager()->GetEntitys();
		for (size_t i = 0; i < entities.size(); i++)
		{
			std::cout << entities[i]->scene << ": " << entities[i]->name << ", " << entities[i]->GetLocalUniqueID() << std::endl;
		}
	}

	void YAML_TestScript::Update()
	{
		// This will execute every frame in update.
		if (Input::GetKeyDown(SDL_SCANCODE_L))
		{
			Scene* currentScene = SceneManager::GetInstance().GetLatestActiveScene();
			std::cout << currentScene->path << std::endl;
			FileManager::LoadYAMLFile(currentScene->path, currentScene->name);
		}

		if (Input::GetKey(SDL_SCANCODE_P))
		{
			Vector2 mousePos = Input::GetMousePosition();
			this->transform->SetPosition(Vector2(mousePos.x, mousePos.y));
		}
	}
}

