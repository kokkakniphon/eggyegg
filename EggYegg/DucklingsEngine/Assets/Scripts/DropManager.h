#pragma once
#include "MonoBehaviour.h"
#include <stdlib.h>
#include <time.h>

#include "Drop/Drop.h"

namespace DucklingsEngine
{
	enum MinionType;

	class DropManager : public MonoBehaviour
	{
	private:
		static float normalMinionDropRate;


	public:
		static DropManager* instance;

		DropManager() = default;
		~DropManager() = default;

		void Start() override;
		void Update() override;

		DropData const GetDrop();
		void SetMinionDropRate(float rate) { normalMinionDropRate = rate; }

		bool CheckPassDrop(MinionType const minionType);

		void SpawnDrop(const Vector2& position, const MinionType minionType);
		void ForceSpawnDrop(const Vector2& position, const DropData& drop);

	};
}

