#pragma once
#include "MonoBehaviour.h"
#include "Mathf.h"
#include "DuckTime.h"

namespace DucklingsEngine
{
	struct DropData
	{
		enum DropType
		{
			Bullet,
			Health
		};

		DropType type;
		int amount;
	};

	class Drop : public MonoBehaviour
	{
	private:
		float currentDuration = 0;
		float bounceDuration = 0.5f;
		Vector2 startSize, newSize;
		
		void OnTriggerEnter();

	public:
		DropData data;

		Drop() = default;
		~Drop() = default;

		void Start() override 
		{
			startSize = Vector2(0.1f, 0.1f);
			currentDuration = bounceDuration;
		}
		void Update() override 
		{
			if (currentDuration > 0)
			{
				currentDuration -= DuckTime::GetDeltaTime();
				currentDuration = Mathf::max(0.0f, currentDuration);
				this->transform->localScale = Vector2::Lerp(startSize, newSize, currentDuration / bounceDuration);
			}
			OnTriggerEnter(); 
		}

		void Setup(const DropData& data);
		DropData& const PickedUp();
	};
}

