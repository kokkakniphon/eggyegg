#include "Drop.h"
#include "GameObject.h"
#include "SpriteRenderer.h"
#include "CircleCollider.h"
#include "Scripts/Duck/DuckController.h"

namespace DucklingsEngine
{
	void Drop::OnTriggerEnter()
	{
		std::vector<Collider*> colliders = this->gameObject->GetComponent<CircleCollider>().collidedTriggers;

		for (size_t i = 0; i < colliders.size(); i++)
		{
			if (colliders[i]->gameObject->tag == "Player" && this->gameObject->IsDestoryed() == false)
			{
				DuckController* duck = &colliders[i]->gameObject->GetComponent<DuckController>();
				if (data.type == DropData::DropType::Bullet)
				{
					duck->AddBullet(data.amount);
				}
				else if (data.type == DropData::DropType::Health)
				{
					duck->Healed(data.amount);
				}

				PickedUp();
			}
		}
	}
	void Drop::Setup(const DropData& data)
	{
		this->data = data;
		GameObject* thisObj = dynamic_cast<GameObject*>(this->gameObject);
		this->transform->localScale = Vector2();
		if (data.type == DropData::DropType::Bullet)
			this->gameObject->AddComponent<SpriteRenderer>("Drops/bullet_crate").order = 3;
		else
			this->gameObject->AddComponent<SpriteRenderer>("Drops/heart_crate").order = 3;

		thisObj->tag = "Drop";
		thisObj->name = "Pickup - drop";
	}
	DropData& const Drop::PickedUp()
	{
		this->gameObject->Destroy();
		return data;
	}
}