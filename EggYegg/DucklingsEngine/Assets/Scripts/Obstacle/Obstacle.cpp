#include "Obstacle.h"
#include "DuckTime.h"
#include "Mathf.h"
#include "SceneManager.h"
#include "Scripts/DropManager.h"

namespace DucklingsEngine
{
	void Obstacle::Start()
	{
		// This will execute once at the begginning of the scene.
		startSize = this->transform->GetScale();
		newSize = startSize * 1.1f;
		this->gameObject->tag = "Obstacle";
	}
	void Obstacle::Update()
	{
		// This will execute every frame in update.
		if (currentDuration > 0)
		{
			currentDuration -= DuckTime::GetDeltaTime();
			currentDuration = Mathf::max(0.0f, currentDuration);
			this->transform->localScale = Vector2::Lerp(startSize, newSize, currentDuration / bounceDuration);
		}
	}
	void Obstacle::Hitted()
	{
		currentDuration = bounceDuration;
		this->transform->localScale = newSize;
	
		if (SceneManager::GetInstance().GetLatestActiveScene()->name == "Tutorial" && this->gameObject->name == "BulletDroperHay")
		{
			DropData drop;
			drop.type = DropData::DropType::Bullet;
			drop.amount = 15;
			DropManager::instance->ForceSpawnDrop(this->transform->GetPosition() - Vector2(150, 0), drop);

			this->gameObject->name = "No Longer Useful, just end this pain";
		}
	}
}