#pragma once
#include "MonoBehaviour.h"

namespace DucklingsEngine
{
	class Obstacle : public MonoBehaviour
	{
	private:
		float currentDuration = 0;
		float bounceDuration = 0.15f;

		Vector2 startSize, newSize;

	public:
		Obstacle() = default;
		~Obstacle() = default;

		void Start() override;
		
		void Update() override;
		

		void Hitted();
		

	};
}

