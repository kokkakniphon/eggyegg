#pragma once
#include "MonoBehaviour.h"

#include "Input.h"
#include "SpriteRenderer.h"

namespace DucklingsEngine
{
	class TitleScreenController : public MonoBehaviour
	{
		struct OnScreenButton
		{
			bool isHovering;
			GameObject* buttonObj = nullptr;
			Vector2 originalSize;

			void Clear()
			{
				isHovering = false;
			}
			void Update()
			{
				buttonObj->transform->localScale = (isHovering) ? originalSize * 1.1f : originalSize;
			}
		};

		struct ScreenLayer
		{
			GameObject* screenObj;
			float layerDepth;

			ScreenLayer(GameObject* obj, float depth) : screenObj(obj), layerDepth(depth) {}
		};

	private:

		std::vector<ScreenLayer> layers;
		float countDown = 2.0f;
		float fadeDir = -0.8f;
		bool transitioning = false;

	public:
		TitleScreenController() = default;
		~TitleScreenController() = default;

		void Start() override;
		void Update() override;

		GameObject* menu, *setting;

		void Setup(GameObject* startObj, GameObject* optionObj, GameObject* quitObj, GameObject* masterObj, GameObject* musicObj, GameObject* sfxObj, GameObject* fullObj, GameObject* backObj);

		OnScreenButton startButton;
		OnScreenButton optionButton;
		OnScreenButton masterButton;
		OnScreenButton musicButton;
		OnScreenButton sfxButton;
		OnScreenButton fullButton;
		OnScreenButton backButton;
		OnScreenButton quitButton;

		OnScreenButton* currentHoldButton;


		SpriteRenderer* renderer = nullptr;

		void AddScreenLayer(GameObject* obj, float depth);

	};

	
}

