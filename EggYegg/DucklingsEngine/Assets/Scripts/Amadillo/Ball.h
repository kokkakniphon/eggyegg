#pragma once
#include "MonoBehaviour.h"

#include "Input.h"
#include "Animator.h"

#include "../Chicken/ChickenController.h"
#include "Scripts/Boss/BossController.h"
#include "Rigidbody.h"

namespace DucklingsEngine
{
	class Ball : public MonoBehaviour
	{
	private:
		Vector2 move_dir;
		float dashTime = 3.0f;
		
		Animator* anim;
	public:
		Ball() = default;
		~Ball() = default;
		GameObject* duckPointer;
		GameObject* aimPointer;
		float dashSpeed = 700.0f;

		void GetMovementInput();

		void Start() override;
		void Update() override;

		Rigidbody* rid2D;


		void Dash();

		void Hitted(Vector2 forceDir, float forceScale);

	};
}

