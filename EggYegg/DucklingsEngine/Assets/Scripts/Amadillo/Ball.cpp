#include "Ball.h"

#include "Scripts/Duck/DuckController.h"
#include "../Chicken/ChickenController.h"
#include "Scripts/Boss/BossController.h"
#include "Transform.h"
#include "Scripts/Obstacle/Obstacle.h"
#include "Scripts/GM.h"

#include "AnimationController/ArmadilloAnimationController.h"

namespace DucklingsEngine
{
	void Ball::Start()
	{
		// This will execute once at the begginning of the scene.
		rid2D = &this->gameObject->GetComponent<Rigidbody>();
		anim = &this->gameObject->GetComponent<Animator>();
		anim->SetAnimationController(std::make_shared<ArmadilloAnimationController>());
		this->gameObject->SetActive(false);
	}

	void Ball::Update()
	{
		// This will execute every frame in update.

		std::vector<Collider*> allCollider = this->gameObject->GetComponent<CircleCollider>().collidedColliders;

		for (size_t i = 0; i < allCollider.size(); i++)
		{
			if (allCollider[i]->gameObject->tag == "Chicken")
			{
				if (allCollider[i]->gameObject->name == "Boss")
					allCollider[i]->gameObject->GetComponent<BossController>().Hitted(100);
				else
					allCollider[i]->gameObject->GetComponent<ChickenController>().GotHitted(100);
				GM::GetInstance().playerSfxPlayer.SetBool("Bounce", true);
				GM::GetInstance().playerSfxPlayer.Play();
			}
			else if (allCollider[i]->gameObject->tag == "Obstacle")
			{
				allCollider[i]->gameObject->GetComponent<Obstacle>().Hitted();
				GM::GetInstance().playerSfxPlayer.SetBool("Bounce", true);
				GM::GetInstance().playerSfxPlayer.Play();
			}
		}

		this->transform->localEulerAngles = (atan2(rid2D->velocity.y, rid2D->velocity.x) * 180 / M_PI);
		duckPointer->transform->SetPosition(this->transform->GetPosition());
		dashTime -= DuckTime::GetDeltaTime();
		if (dashTime < 0.0f)
		{
			//reset dash timer
			dashTime = 3.0f;
			//deactivate self
			this->gameObject->SetActive(false);
			//activate duck
			duckPointer->transform->SetPosition(this->transform->GetPosition());
			aimPointer->transform->SetPosition(this->transform->GetPosition());
			duckPointer->SetActive(true);
			aimPointer->SetActive(true);
		}

		duckPointer->GetComponent<DuckController>().Quack();

		//New Dashing Machanic
		Dash();
	}

	void Ball::Dash() //Dash when cool down is out
	{
		GetMovementInput();
		if (move_dir != Vector2())
		{
			Vector2 moveDirNor = move_dir.normalized();
			Vector2 ridDirNor = rid2D->velocity.normalized();
			Vector2 newDir = (ridDirNor + moveDirNor != Vector2()) ? moveDirNor : Vector2(moveDirNor.y, -moveDirNor.x);
			rid2D->velocity = (ridDirNor + newDir * 5.0f / 100.0f).normalized() * dashSpeed;

		}
		//std::cout << "Dash" << std::endl;
	}

	void Ball::Hitted(Vector2 forceDir, float forceScale)
	{
		rid2D->velocity = Vector2();
		rid2D->AddForce(forceDir * forceScale);
	}

	void Ball::GetMovementInput()
	{
		Vector2 input;
		if (Input::GetKey(SDL_SCANCODE_W))
		{
			input.y = -1.0f;
		}
		else if (Input::GetKey(SDL_SCANCODE_S))
		{
			input.y = 1.0f;
		}

		if (Input::GetKey(SDL_SCANCODE_A))
		{
			input.x = -1.0f;
		}
		else if (Input::GetKey(SDL_SCANCODE_D))
		{
			input.x = 1.0f;
		}

		if (input.x != 0 && input.y != 0)
			input = input * 1 / 1.44f;

		move_dir = input;
	}
}