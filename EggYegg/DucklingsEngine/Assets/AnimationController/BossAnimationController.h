#pragma once
#include "AnimationController.h"
#include "SpriteManager.h"

namespace DucklingsEngine
{
	class BossAnimationController : public AnimationController
	{
	public:
		BossAnimationController() { Init(); }
		~BossAnimationController() = default;

		void Init();

	};

	void BossAnimationController::Init()
	{
		// This will hold the logic for the animation controller.
		{
			Sprite* sprite = SpriteManager::GetInstance().AddSprite(new Sprite("Boss/BossSpriteWalk", true, 18, 18), std::to_string(GetUniqueID()) + "BossWalk");
			std::shared_ptr<Animation> walkAnimation(new Animation("BossWalkAnimation", sprite, 30));
			walkAnimation->destinations["BossLeftAttackAnimation"].push_back(Paramiter("IsLeftAttact", Boolean, true, false, true));
			walkAnimation->destinations["BossRightAttackAnimation"].push_back(Paramiter("IsRightAttact", Boolean, true, false, true));
			walkAnimation->destinations["BossDeadAnimation"].push_back(Paramiter("IsDead", Boolean, true, false, true));
			animations["BossWalkAnimation"] = walkAnimation;
		}

		{
			Sprite* sprite = SpriteManager::GetInstance().AddSprite(new Sprite("Boss/BossSpriteLeftAttack", true, 16, 16), std::to_string(GetUniqueID()) + "BossLeftAttack");
			std::shared_ptr<Animation> leftAttackAnimation(new Animation("BossLeftAttackAnimation", sprite, 30));
			leftAttackAnimation->destinations["BossWalkAnimation"];
			leftAttackAnimation->destinations["BossDeadAnimation"].push_back(Paramiter("IsDead", Boolean, true, false, true));
			animations["BossLeftAttackAnimation"] = leftAttackAnimation;
		}

		{
			Sprite* sprite = SpriteManager::GetInstance().AddSprite(new Sprite("Boss/BossSpriteRightAttack", true, 16, 16), std::to_string(GetUniqueID()) + "BossRightAttack");
			std::shared_ptr<Animation> rightAttackAnimation(new Animation("BossRightAttackAnimation", sprite, 30));
			rightAttackAnimation->destinations["BossWalkAnimation"];
			rightAttackAnimation->destinations["BossDeadAnimation"].push_back(Paramiter("IsDead", Boolean, true, false, true));
			animations["BossRightAttackAnimation"] = rightAttackAnimation;
		}

		{
			Sprite* sprite = SpriteManager::GetInstance().AddSprite(new Sprite("Boss/BossSpriteDead", true, 1, 1), std::to_string(GetUniqueID()) + "BossDead");
			std::shared_ptr<Animation> deadAnimation(new Animation("BossDeadAnimation", sprite, 1));
			deadAnimation->destinations["BossWalkAnimation"].push_back(Paramiter("IsDead", Boolean, true, false, false));
			animations["BossDeadAnimation"] = deadAnimation;
		}
	}

}

