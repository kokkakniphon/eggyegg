#pragma once
#include "AnimationController.h"

#include "SpriteManager.h"

namespace DucklingsEngine
{
	class RedBallAnim : public AnimationController
	{
	public:
		RedBallAnim() { Init(); }
		~RedBallAnim() = default;

		void Init();

	};

	void RedBallAnim::Init()
	{
		// This will hold the logic for the animation controller.
		SpriteManager::GetInstance().AddSprite(new Sprite("baBallRed", true, 1, 50), std::to_string(GetUniqueID()));

		std::shared_ptr<Animation> rollAnimation(new Animation("RollingAnimation", SpriteManager::GetInstance().GetSprite(std::to_string(GetUniqueID())), 50));
		animations["RollingAnimation"] = rollAnimation;
	}

}

