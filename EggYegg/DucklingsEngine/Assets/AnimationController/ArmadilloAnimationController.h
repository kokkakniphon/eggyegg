#pragma once
#include "AnimationController.h"
#include "SpriteManager.h"

namespace DucklingsEngine
{
	class ArmadilloAnimationController : public AnimationController
	{
	public:
		ArmadilloAnimationController() { Init(); }
		~ArmadilloAnimationController() = default;

		void Init();

	};

	void ArmadilloAnimationController::Init()
	{
		// This will hold the logic for the animation controller.
		Sprite* sprite = SpriteManager::GetInstance().AddSprite(new Sprite("Armadillos/ArmadilloSpriteRoll", true, 29, 29), std::to_string(GetUniqueID()));

		std::shared_ptr<Animation> armadilloAnimation(new Animation("ArmadilloRollAnimation", sprite, 29));
		animations["ArmadilloRollAnimation"] = armadilloAnimation;
	}

}

