#pragma once

#include "AnimationController.h"
#include "SpriteManager.h"

namespace DucklingsEngine
{
	class ZumaController : public AnimationController
	{
	public:
		ZumaController() { Init(); }
		~ZumaController() = default;

		void Init();

	};

	void ZumaController::Init()
	{
		// This will hold the logic for the animation controller.
		SpriteManager::GetInstance().AddSprite(new Sprite("IdleSprite", true, 10, 5));
		SpriteManager::GetInstance().AddSprite(new Sprite("WalkSprite", true, 10, 10));

		std::shared_ptr<Animation> idleAnimation(new Animation("idle", SpriteManager::GetInstance().GetSprite("IdleSprite"), 5));
		idleAnimation->destinations["WalkAnimation"].push_back(Paramiter("IsWalking", Boolean, true, false, true));
		idleAnimation->destinations["WalkAnimation"].push_back(Paramiter("IsTryHard", Boolean, true, false, true));
		animations["IdleAnimation"] = idleAnimation;

		std::shared_ptr<Animation> walkAnimation(new Animation("walk", SpriteManager::GetInstance().GetSprite("WalkSprite"), 10));
		walkAnimation->destinations["IdleAnimation"].push_back(Paramiter("IsWalking", Boolean, true, false, false));
		animations["WalkAnimation"] = walkAnimation;
	}

}

