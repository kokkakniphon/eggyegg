#pragma once
#include "AnimationController.h"

#include "SpriteManager.h"

namespace DucklingsEngine
{
	class DuckAnimationController : public AnimationController
	{
	public:
		DuckAnimationController() { Init(); }
		~DuckAnimationController() = default;

		void Init();

	};

	void DuckAnimationController::Init()
	{
		// This will hold the logic for the animation controller.
		Sprite* sprite = SpriteManager::GetInstance().AddSprite(new Sprite("Ducks/DuckSpriteWalk", true, 33, 33), std::to_string(GetUniqueID()));

		std::shared_ptr<Animation> duckAnimation(new Animation("DuckWalkAnimation", sprite, 50));
		animations["DuckWalkAnimation"] = duckAnimation;
	}

}

