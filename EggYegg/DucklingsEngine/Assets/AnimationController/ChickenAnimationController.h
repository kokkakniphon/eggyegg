#pragma once
#include "AnimationController.h"
#include "SpriteManager.h"

namespace DucklingsEngine
{
	class ChickenAnimationController : public AnimationController
	{
	public:
		ChickenAnimationController() { Init(); }
		~ChickenAnimationController() = default;

		void Init();

	};

	void ChickenAnimationController::Init()
	{
		// This will hold the logic for the animation controller.
		{
			Sprite* sprite = SpriteManager::GetInstance().AddSprite(new Sprite("Chickens/ChickenSpriteWalk", true, 13, 13), std::to_string(GetUniqueID()) + "ChickenSpriteWalk");
			std::shared_ptr<Animation> chickenAnimation(new Animation("ChickenWalkAnimation", sprite, 30));
			chickenAnimation->destinations["RangeChickenWalkAnimation"].push_back(Paramiter("IsRange", Boolean, true, false, true));
			chickenAnimation->destinations["ChickenDeadAnimation"].push_back(Paramiter("IsDead", Boolean, true, false, true));
			chickenAnimation->destinations["ChickenHitAnimation"].push_back(Paramiter("IsHitted", Boolean, true, false, true));
			animations["ChickenWalkAnimation"] = chickenAnimation;
		}

		{
			Sprite* sprite = SpriteManager::GetInstance().AddSprite(new Sprite("Chickens/ChickenSpriteDead", true, 1, 1), std::to_string(GetUniqueID()) + "ChickenSpriteDead");
			std::shared_ptr<Animation> chickenAnimationDead(new Animation("ChickenDeadAnimation", sprite, 1));
			chickenAnimationDead->destinations["ChickenWalkAnimation"].push_back(Paramiter("IsDead", Boolean, true, false, false));
			animations["ChickenDeadAnimation"] = chickenAnimationDead;
		}

		{
			Sprite* sprite = SpriteManager::GetInstance().AddSprite(new Sprite("Chickens/ChickenSpriteHit", true, 1, 1), std::to_string(GetUniqueID()) + "ChickenSpriteHit");
			std::shared_ptr<Animation> chickenAnimationHit(new Animation("ChickenHitAnimation", sprite, 1));
			chickenAnimationHit->destinations["ChickenDeadAnimation"].push_back(Paramiter("IsDead", Boolean, true, false, true));
			chickenAnimationHit->destinations["ChickenWalkAnimation"].push_back(Paramiter("IsHitted", Boolean, true, false, false));
			animations["ChickenHitAnimation"] = chickenAnimationHit;
		}

		{
			Sprite* sprite = SpriteManager::GetInstance().AddSprite(new Sprite("Chickens/RangeChickenSpriteWalk", true, 13, 13), std::to_string(GetUniqueID()) + "RangeChickenSpriteWalk");
			std::shared_ptr<Animation> rangeChickenAnimationWalk(new Animation("RangeChickenWalkAnimation", sprite, 30));
			rangeChickenAnimationWalk->destinations["ChickenWalkAnimation"].push_back(Paramiter("IsRange", Boolean, true, false, false));
			rangeChickenAnimationWalk->destinations["RangeChickenShootAnimation"].push_back(Paramiter("IsShooting", Boolean, true, false, true));
			rangeChickenAnimationWalk->destinations["RangeChickenDeadAnimation"].push_back(Paramiter("IsDead", Boolean, true, false, true));
			rangeChickenAnimationWalk->destinations["RangeChickenHitAnimation"].push_back(Paramiter("IsHitted", Boolean, true, false, true));
			animations["RangeChickenWalkAnimation"] = rangeChickenAnimationWalk;
		}

		{
			Sprite* sprite = SpriteManager::GetInstance().AddSprite(new Sprite("Chickens/RangeChickenSpriteShoot", true, 13, 13), std::to_string(GetUniqueID()) + "RangeChickenSpriteShoot");
			std::shared_ptr<Animation> rangeChickenAnimationShoot(new Animation("RangeChickenShootAnimation", sprite, 30));
			rangeChickenAnimationShoot->destinations["RangeChickenWalkAnimation"].push_back(Paramiter("IsShooting", Boolean, true, false, false));
			rangeChickenAnimationShoot->destinations["RangeChickenHitAnimation"].push_back(Paramiter("IsHitted", Boolean, true, false, true));
			rangeChickenAnimationShoot->destinations["RangeChickenDeadAnimation"].push_back(Paramiter("IsDead", Boolean, true, false, true));
			animations["RangeChickenShootAnimation"] = rangeChickenAnimationShoot;
		}

		{
			Sprite* sprite = SpriteManager::GetInstance().AddSprite(new Sprite("Chickens/RangeChickenSpriteDead", true, 1, 1), std::to_string(GetUniqueID()) + "RangeChickenSpriteDead");
			std::shared_ptr<Animation> rangeChickenAnimationDead(new Animation("RangeChickenDeadAnimation", sprite, 1));
			rangeChickenAnimationDead->destinations["RangeChickenWalkAnimation"].push_back(Paramiter("IsDead", Boolean, true, false, false));
			animations["RangeChickenDeadAnimation"] = rangeChickenAnimationDead;
		}

		{
			Sprite* sprite = SpriteManager::GetInstance().AddSprite(new Sprite("Chickens/RangeChickenSpriteHit", true, 1, 1), std::to_string(GetUniqueID()) + "RangeChickenSpriteHit");
			std::shared_ptr<Animation> rangeChickenAnimationHit(new Animation("RangeChickenHitAnimation", sprite, 1));
			rangeChickenAnimationHit->destinations["RangeChickenWalkAnimation"].push_back(Paramiter("IsDead", Boolean, true, false, false));
			rangeChickenAnimationHit->destinations["RangeChickenWalkAnimation"].push_back(Paramiter("IsHitted", Boolean, true, false, false));
			animations["RangeChickenHitAnimation"] = rangeChickenAnimationHit;
		}
	}

}

