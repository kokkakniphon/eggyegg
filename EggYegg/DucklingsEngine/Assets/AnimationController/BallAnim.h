#pragma once
#include "AnimationController.h"

#include "SpriteManager.h"

namespace DucklingsEngine
{
	class BallAnim : public AnimationController
	{
	public:
		BallAnim() { Init(); }
		~BallAnim() = default;

		void Init();

	};

	void BallAnim::Init()
	{
		// This will hold the logic for the animation controller.
		SpriteManager::GetInstance().AddSprite(new Sprite("baBallBlue", true, 1, 47));

		std::shared_ptr<Animation> rollAnimation(new Animation("RollingAnimation", SpriteManager::GetInstance().GetSprite("baBallBlue"), 47));
		animations["RollingAnimation"] = rollAnimation;
	}

}

