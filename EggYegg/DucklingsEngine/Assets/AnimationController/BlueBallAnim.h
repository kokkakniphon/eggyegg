#pragma once
#include "AnimationController.h"

#include "SpriteManager.h"

namespace DucklingsEngine
{
	class BlueBallAnim : public AnimationController
	{
	public:
		BlueBallAnim() { Init(); }
		~BlueBallAnim() = default;

		void Init();

	};

	void BlueBallAnim::Init()
	{
		// This will hold the logic for the animation controller.

		SpriteManager::GetInstance().AddSprite(new Sprite("baBallBlue", true, 1, 47), std::to_string(GetUniqueID()));

		std::shared_ptr<Animation> rollAnimation(new Animation("RollingAnimation", SpriteManager::GetInstance().GetSprite(std::to_string(GetUniqueID())), 47));
		animations["RollingAnimation"] = rollAnimation;
	}

}

