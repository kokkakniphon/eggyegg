#pragma once

#include "Scene.h"
#include "Engine.h"
#include "FileManager.h"

namespace DucklingsEngine
{
	class YAML_TestScene : public Scene
	{
	public:
		YAML_TestScene()
		{ 
			name = "YAML_TestScene"; 
			path = "../Assets/Scenes/YAML_TestScene.scene";
		}
		~YAML_TestScene() = default;

		void Start();

		inline void LoadScene() 
		{ 
			Scene::LoadScene(); 
			std::cout << "Loading Scene: " << name << std::endl;
			Start();
			FileManager::LoadYAMLFile(path, name);
		}

		inline bool UnloadScene() override
		{
			std::cout << "Unload Scene: " << name << std::endl;

			Engine::GetInstance().GetManager()->DestoryAllEntity(name);
			Scene::UnloadScene();

			return true;
		}
	};
}

