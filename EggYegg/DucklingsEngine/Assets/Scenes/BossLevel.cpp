#include "BossLevel.h"

#include "Camera.h"
#include "GameObject.h"
#include "CircleCollider.h"
#include "BoxCollider.h"
#include "Rigidbody.h"
#include "SpriteRenderer.h"
#include "TextRenderer.h"

#include "Scripts/Duck/DuckController.h"
#include "Scripts/Amadillo/Ball.h"
#include "Scripts/Chicken/ChickenController.h"
#include "Scripts/Tutorial/TutorialController.h"
#include "Scripts/Core/SceneTransition.h"
#include "Scripts/Chicken/ChickenManager.h"
#include "Scripts/Boss/BossController.h"
#include "Scripts/Boss/BossLevelController.h"

namespace DucklingsEngine
{
	void BossLevel::Start()
	{
		// This will be execute when loading the scene.

		//----Camera----
		GameObject* mainCamera = new GameObject("Main Camera");
		mainCamera->AddComponent<Camera>();

		//----Create Background----
		GameObject* background = new GameObject("Background");
		background->transform->SetPosition(Vector2(SCREEN_WIDTH / 2, SCREEN_HEIGHT / 2));
		background->transform->localScale = Vector2(1.6f, 1.6f);
		background->AddComponent<SpriteRenderer>("Map");

		//- - - - Set Walls - - - -
		GameObject* topWall = new GameObject("TopWall");
		topWall->transform->SetPosition(Vector2(SCREEN_WIDTH / 2, 0));
		topWall->AddComponent<BoxCollider>().size = Vector2(SCREEN_WIDTH / 2, 50);

		GameObject* bottomWall = new GameObject("TopWall");
		bottomWall->transform->SetPosition(Vector2(SCREEN_WIDTH / 2, SCREEN_HEIGHT));
		bottomWall->AddComponent<BoxCollider>().size = Vector2(SCREEN_WIDTH / 2, 50);

		GameObject* leftWall = new GameObject("LeftWall");
		leftWall->transform->SetPosition(Vector2(0, SCREEN_HEIGHT / 2));
		leftWall->AddComponent<BoxCollider>().size = Vector2(30, SCREEN_HEIGHT / 2);

		GameObject* rightWall = new GameObject("RightWall");
		rightWall->transform->SetPosition(Vector2(SCREEN_WIDTH, SCREEN_HEIGHT / 2));
		rightWall->AddComponent<BoxCollider>().size = Vector2(30, SCREEN_HEIGHT / 2);
		//- - - - - - - - - - - - -

		//-------------------------

		//----Create Duck (Player)----

		GameObject* duck = new GameObject("Player");
		duck->transform->localScale = Vector2(0.07f, 0.07f);
		duck->AddComponent<Rigidbody>().gravityScale = 0.0f;
		duck->AddComponent<SpriteRenderer>("DuckBody");
		duck->AddComponent<CircleCollider>().radius = 30.0f;
		duck->AddComponent<DuckController>().rid2D = &duck->GetComponent<Rigidbody>();
		duck->tag = "Player";

		//- - - - Duck Head - - - -

		GameObject* duckHead = new GameObject("DuckHead");
		duckHead->transform->localScale = Vector2(1.1f, 1.1f);
		duckHead->AddComponent<SpriteRenderer>("DuckHead");
		duckHead->transform->SetParent(duck->transform);
		duckHead->transform->localPosition = Vector2(-15.0f, 0.0f);
		duckHead->AddComponent<CircleCollider>().isTrigger = true;
		duckHead->GetComponent<CircleCollider>().radius = 30.0f;
		//- - - - - - - - - - - - -

		//- - - -Aim Ring - - - -
		GameObject* aim = new GameObject("Aim");
		aim->AddComponent<SpriteRenderer>("Direction");
		aim->transform->localPosition = Vector2(0.0f, 0.0f);
		//- - - - - - - - - - - - 

		//- - - -Quack- - - -
		GameObject* quack = new GameObject("Quack");
		quack->transform->localScale = Vector2(0.6f, 0.6f);
		quack->AddComponent<SpriteRenderer>("quack");
		quack->transform->localPosition = Vector2(0.0f, 0.0f);
		quack->transform->SetPosition(Vector2(SCREEN_WIDTH / 2, SCREEN_HEIGHT / 2));
		quack->transform->localEulerAngles = 45;
		//- - - - - - - - - -

		duck->GetComponent<DuckController>().duckHead = duckHead;
		duck->GetComponent<DuckController>().aim_obj = aim;
		duck->GetComponent<DuckController>().quack = quack;

		//----------------------------

		//----Create Ball----
		GameObject* ball = new GameObject("Ball");
		ball->transform->localScale = Vector2(0.15f, 0.15f);
		ball->transform->SetPosition(Vector2(SCREEN_WIDTH / 4, SCREEN_HEIGHT / 2));
		ball->AddComponent<Rigidbody>().gravityScale = 0.0f;
		ball->GetComponent<Rigidbody>().drag = 0.2f;
		ball->AddComponent<SpriteRenderer>("armadillo");
		ball->AddComponent<CircleCollider>().radius = 35.0f;
		//Pointer to both duck(also aim ring) and ball to enable and disable
		ball->AddComponent<Ball>().duckPointer = duck;
		ball->GetComponent<Ball>().aimPointer = aim;
		duck->GetComponent<DuckController>().ballPointer = ball;
		//-------------------

		duck->GetComponent<DuckController>().ball = &ball->GetComponent<Ball>();

		//----Duck Stamina----
		for (size_t i = 0; i < 3; i++)
		{
			GameObject* stamina = new GameObject("Stamina " + std::to_string(i + 1));
			stamina->AddComponent<SpriteRenderer>("whiteBall");
			stamina->transform->SetParent(aim->transform);
		}
		//--------------------

		//----Scene Transition----
		GameObject* sceneTransition = new GameObject("Scene Transition Controller");
		sceneTransition->AddComponent<SceneTransition>();

		GameObject* transitionOverlay = new GameObject("Transition Overlay");
		transitionOverlay->transform->SetParent(mainCamera->transform);
		transitionOverlay->AddComponent<SpriteRenderer>("whiteBox");

		sceneTransition->GetComponent<SceneTransition>()._transitionOverlay = &transitionOverlay->GetComponent<SpriteRenderer>();
		//------------------------


		//----Chicken----
		GameObject* chickenManager = new GameObject("Chicken Manager");
		chickenManager->AddComponent<ChickenManager>();
		chickenManager->GetComponent<ChickenManager>()._player = duck;

		GameObject* spawnerPosChecker = new GameObject("Spawner Pos");
		//---------------

		//----Level Controller----
		GameObject* levelController = new GameObject("Level Controller");
		levelController->AddComponent<BossLevelController>();

		//------------------------


		//----Boss----
		GameObject* boss = new GameObject("Chicken Boss");
		boss->AddComponent<BossController>();
		boss->AddComponent<SpriteRenderer>("chick");
		boss->AddComponent<CircleCollider>();
		boss->GetComponent<BossController>().player = duck;

		GameObject* boss_shootingHolder = new GameObject("Shooting Holder");
		boss_shootingHolder->transform->SetParent(boss->transform);

		boss->GetComponent<BossController>()._chickenManager = &chickenManager->GetComponent<ChickenManager>();

		//------------

		//----Fence----
		GameObject* obstacle_00 = new GameObject("fence_0");
		obstacle_00->AddComponent<SpriteRenderer>("Obstacles/fence_horizontal");

		GameObject* obstacle_01 = new GameObject("fence_1");
		obstacle_01->AddComponent<SpriteRenderer>("Obstacles/fence_horizontal");

		GameObject* obstacle_02 = new GameObject("fence_2");
		obstacle_02->AddComponent<SpriteRenderer>("Obstacles/fence_verticle");

		GameObject* obstacle_03 = new GameObject("fence_3");
		obstacle_03->AddComponent<SpriteRenderer>("Obstacles/fence_verticle");


		//----Door----
		GameObject* obstacle_04 = new GameObject("door_0");
		obstacle_04->AddComponent<SpriteRenderer>("Obstacles/fence_gate");

		GameObject* obstacle_05 = new GameObject("door_0");
		obstacle_05->AddComponent<SpriteRenderer>("Obstacles/fence_gate");

		//----Fert----
		GameObject* obstacle_06 = new GameObject("fert_0");
		obstacle_06->AddComponent<SpriteRenderer>("Obstacles/fertilizer_group");
		obstacle_06->AddComponent<BoxCollider>();

		GameObject* obstacle_07 = new GameObject("fert_1");
		obstacle_07->AddComponent<SpriteRenderer>("Obstacles/fertilizer_group");
		obstacle_07->AddComponent<BoxCollider>();

		GameObject* obstacle_08 = new GameObject("fert_2");
		obstacle_08->AddComponent<SpriteRenderer>("Obstacles/fertilizer_group");
		obstacle_08->AddComponent<BoxCollider>();

		GameObject* obstacle_09 = new GameObject("fert_3");
		obstacle_09->AddComponent<SpriteRenderer>("Obstacles/fertilizer_group");
		obstacle_09->AddComponent<BoxCollider>();

		//----Carrots----
		GameObject* obstacle_10 = new GameObject("carrots_0");
		obstacle_10->AddComponent<SpriteRenderer>("Obstacles/carrots");
		obstacle_10->AddComponent<BoxCollider>();

		GameObject* obstacle_11 = new GameObject("carrots_1");
		obstacle_11->AddComponent<SpriteRenderer>("Obstacles/carrots");
		obstacle_11->AddComponent<BoxCollider>();

		//---Cart---
		GameObject* obstacle_12 = new GameObject("cart_0");
		obstacle_12->AddComponent<SpriteRenderer>("Obstacles/cart_blue");
		obstacle_12->AddComponent<BoxCollider>();

		GameObject* obstacle_13 = new GameObject("cart_1");
		obstacle_13->AddComponent<SpriteRenderer>("Obstacles/cart");
		obstacle_13->AddComponent<BoxCollider>();

		GameObject* obstacle_14 = new GameObject("stump_0");
		obstacle_14->AddComponent<SpriteRenderer>("Obstacles/tree_stump");
		obstacle_14->AddComponent<CircleCollider>();

		GameObject* obstacle_15 = new GameObject("stump_1");
		obstacle_15->AddComponent<SpriteRenderer>("Obstacles/tree_stump");
		obstacle_15->AddComponent<CircleCollider>();


		//---Log---
		GameObject* obstacle_16 = new GameObject("log_0");
		obstacle_16->AddComponent<SpriteRenderer>("Obstacles/log3");
		obstacle_16->AddComponent<BoxCollider>();

		GameObject* obstacle_17 = new GameObject("log_1");
		obstacle_17->AddComponent<SpriteRenderer>("Obstacles/log3");
		obstacle_17->AddComponent<BoxCollider>();

		//----Arrow Head----
		GameObject* arrowHead = new GameObject("NextRoomArrow");
		arrowHead->AddComponent<SpriteRenderer>("whiteTriangle");
		arrowHead->AddComponent<BoxCollider>();

		levelController->GetComponent<BossLevelController>()._nextRoomArrow = arrowHead;
		levelController->GetComponent<BossLevelController>()._sceneTransition = &sceneTransition->GetComponent<SceneTransition>();

		levelController->GetComponent<BossLevelController>()._bossController = &boss->GetComponent<BossController>();
		//------------------

		////----Create Background----
		//GameObject* grass = new GameObject("Background");
		//grass->transform->SetPosition(Vector2(SCREEN_WIDTH / 2, SCREEN_HEIGHT / 2));
		//grass->transform->localScale = Vector2(1.6f, 1.6f);
		//grass->AddComponent<SpriteRenderer>("Map");
	}
}