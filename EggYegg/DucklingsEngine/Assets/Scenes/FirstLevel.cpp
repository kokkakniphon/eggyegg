#include "FirstLevel.h"

#include "Camera.h"
#include "GameObject.h"
#include "CircleCollider.h"
#include "BoxCollider.h"
#include "Rigidbody.h"
#include "SpriteRenderer.h"
#include "TextRenderer.h"
#include "Animator.h"

#include "Scripts/Duck/DuckController.h"
#include "Scripts/Duck/DuckUI.h"
#include "Scripts/Amadillo/Ball.h"
#include "Scripts/Chicken/ChickenController.h"
#include "Scripts/FirstLevel/FirstLevelController.h"
#include "Scripts/Core/SceneTransition.h"
#include "Scripts/Chicken/ChickenManager.h"
#include "Scripts/Core/CameraController.h"
#include "Scripts/Obstacle/Obstacle.h"

#include "Scripts/GM.h"

namespace DucklingsEngine
{
	int ChickenController::GLOBAL_CHICKEN_AMOUNT = 0;

	void FirstLevel::Start()
	{
		// This will be execute when loading the scene.

		//----Camera----
		GameObject* mainCamera = new GameObject("Main Camera");
		mainCamera->AddComponent<Camera>();
		mainCamera->AddComponent<CameraController>();

		//----Create Background----
		GameObject* background = new GameObject("Background");
		background->transform->SetPosition(Vector2(SCREEN_WIDTH / 2, SCREEN_HEIGHT / 2));
		background->transform->localScale = Vector2(1.6f, 1.6f);
		background->AddComponent<SpriteRenderer>("Map");

		//- - - - Set Walls - - - -
		GameObject* topWall = new GameObject("TopWall");
		topWall->transform->SetPosition(Vector2(SCREEN_WIDTH / 2, 0));
		topWall->AddComponent<BoxCollider>().size = Vector2(SCREEN_WIDTH / 2, 50);

		GameObject* bottomWall = new GameObject("TopWall");
		bottomWall->transform->SetPosition(Vector2(SCREEN_WIDTH / 2, SCREEN_HEIGHT));
		bottomWall->AddComponent<BoxCollider>().size = Vector2(SCREEN_WIDTH / 2, 50);

		GameObject* leftWall = new GameObject("LeftWall");
		leftWall->transform->SetPosition(Vector2(0, SCREEN_HEIGHT / 2));
		leftWall->AddComponent<BoxCollider>().size = Vector2(30, SCREEN_HEIGHT / 2);

		GameObject* rightWall = new GameObject("RightWall");
		rightWall->transform->SetPosition(Vector2(SCREEN_WIDTH, SCREEN_HEIGHT / 2));
		rightWall->AddComponent<BoxCollider>().size = Vector2(30, SCREEN_HEIGHT / 2);
		//- - - - - - - - - - - - -

		//-------------------------

		//----Create Duck (Player)----

		GameObject* duck = new GameObject("Player");
		duck->transform->localScale = Vector2(0.07f, 0.07f);
		duck->AddComponent<Rigidbody>().gravityScale = 0.0f;
		duck->AddComponent<SpriteRenderer>("DuckBody");
		duck->AddComponent<CircleCollider>().radius = 30.0f;
		duck->AddComponent<DuckController>().rid2D = &duck->GetComponent<Rigidbody>();
		duck->tag = "Player";
		mainCamera->GetComponent<CameraController>()._player = &duck->GetComponent<DuckController>();
		//- - - - Duck Head - - - -

		GameObject* duckHead = new GameObject("DuckHead");
		duckHead->transform->localScale = Vector2(1.1f, 1.1f);
		duckHead->AddComponent<SpriteRenderer>("Ducks/DuckSpriteHead");
		duckHead->transform->SetParent(duck->transform);
		duckHead->transform->localPosition = Vector2(-15.0f, 0.0f);
		duckHead->AddComponent<CircleCollider>().isTrigger = true;
		duckHead->GetComponent<CircleCollider>().radius = 30.0f;
		//- - - - - - - - - - - - -

		//- - - -Aim Ring - - - -
		GameObject* aim = new GameObject("Aim");
		aim->AddComponent<SpriteRenderer>("Direction");
		aim->transform->localPosition = Vector2(0.0f, 0.0f);
		//- - - - - - - - - - - - 

		//- - - -Quack- - - -
		GameObject* quack = new GameObject("Quack");
		quack->transform->localScale = Vector2(0.6f, 0.6f);
		quack->AddComponent<SpriteRenderer>("quack");
		quack->transform->localPosition = Vector2(0.0f, 0.0f);
		quack->transform->SetPosition(Vector2(SCREEN_WIDTH / 2, SCREEN_HEIGHT / 2));
		quack->transform->localEulerAngles = 45;
		//- - - - - - - - - -

		duck->GetComponent<DuckController>().duckHead = duckHead;
		duck->GetComponent<DuckController>().aim_obj = aim;
		duck->GetComponent<DuckController>().quack = quack;

		//----------------------------

		//----Create Ball----
		GameObject* ball = new GameObject("Ball");
		ball->transform->localScale = Vector2(0.15f, 0.15f);
		ball->transform->SetPosition(Vector2(SCREEN_WIDTH / 4, SCREEN_HEIGHT / 2));
		ball->AddComponent<Rigidbody>().gravityScale = 0.0f;
		ball->GetComponent<Rigidbody>().drag = 0.2f;
		ball->AddComponent<SpriteRenderer>("armadillo");
		ball->AddComponent<CircleCollider>().radius = 35.0f;
		//Pointer to both duck(also aim ring) and ball to enable and disable
		ball->AddComponent<Ball>().duckPointer = duck;
		ball->GetComponent<Ball>().aimPointer = aim;
		ball->AddComponent<Animator>();
		duck->GetComponent<DuckController>().ballPointer = ball;
		//-------------------

		duck->GetComponent<DuckController>().ball = &ball->GetComponent<Ball>();

		//----Duck Stamina----
		for (size_t i = 0; i < 3; i++)
		{
			GameObject* stamina = new GameObject("Stamina " + std::to_string(i + 1));
			stamina->SetActive(false);
			stamina->AddComponent<SpriteRenderer>("whiteBall");
			stamina->transform->SetParent(aim->transform);
		}
		//--------------------

		//----Scene Transition----
		GameObject* sceneTransition = new GameObject("Scene Transition Controller");
		sceneTransition->AddComponent<SceneTransition>();

		GameObject* transitionOverlay = new GameObject("Transition Overlay");
		transitionOverlay->transform->SetParent(mainCamera->transform);
		transitionOverlay->transform->localScale = Vector2(33, 19);
		transitionOverlay->AddComponent<SpriteRenderer>("whiteBox").order = 30;

		sceneTransition->GetComponent<SceneTransition>()._transitionOverlay = &transitionOverlay->GetComponent<SpriteRenderer>();
		//------------------------


		//----Chicken----
		GameObject* chickenManager = new GameObject("Chicken Manager");
		chickenManager->AddComponent<ChickenManager>();
		chickenManager->GetComponent<ChickenManager>()._player = duck;

		GameObject* spawnerPosChecker = new GameObject("Spawner Pos");
		//---------------

		//----Obstacle----
		
			//Haybale_type1
			GameObject* obstacle_00 = new GameObject("haybale_type1_0");
			obstacle_00->AddComponent<SpriteRenderer>("Obstacles/haybale1");
			obstacle_00->AddComponent<BoxCollider>();
			obstacle_00->AddComponent<Obstacle>();
			GameObject* obstacle_01 = new GameObject("haybale_type1_1");
			obstacle_01->AddComponent<SpriteRenderer>("Obstacles/haybale1");
			obstacle_01->AddComponent<BoxCollider>();
			obstacle_01->AddComponent<Obstacle>();
			GameObject* obstacle_02 = new GameObject("haybale_type1_2");
			obstacle_02->AddComponent<SpriteRenderer>("Obstacles/haybale1");
			obstacle_02->AddComponent<BoxCollider>();
			obstacle_02->AddComponent<Obstacle>();

			//Haybale_type2
			GameObject* obstacle_03 = new GameObject("haybale_type2_0");
			obstacle_03->AddComponent<SpriteRenderer>("Obstacles/haybale2");
			obstacle_03->AddComponent<BoxCollider>();
			obstacle_03->AddComponent<Obstacle>();
			GameObject* obstacle_04 = new GameObject("haybale_type2_1");
			obstacle_04->AddComponent<SpriteRenderer>("Obstacles/haybale2");
			obstacle_04->AddComponent<BoxCollider>();
			obstacle_04->AddComponent<Obstacle>();
			GameObject* obstacle_05 = new GameObject("haybale_type2_2");
			obstacle_05->AddComponent<SpriteRenderer>("Obstacles/haybale2");
			obstacle_05->AddComponent<BoxCollider>();
			obstacle_05->AddComponent<Obstacle>();
			GameObject* obstacle_06 = new GameObject("haybale_type2_3");
			obstacle_06->AddComponent<SpriteRenderer>("Obstacles/haybale2");
			obstacle_06->AddComponent<BoxCollider>();
			obstacle_06->AddComponent<Obstacle>();
		
			//Well
			GameObject* obstacle_07 = new GameObject("well_0");
			obstacle_07->AddComponent<SpriteRenderer>("Obstacles/water1");
			obstacle_07->AddComponent<CircleCollider>();
			obstacle_07->AddComponent<Obstacle>();
			GameObject* obstacle_08 = new GameObject("well_1");
			obstacle_08->AddComponent<SpriteRenderer>("Obstacles/water2");
			obstacle_08->AddComponent<BoxCollider>();
			obstacle_08->AddComponent<Obstacle>();
			
			//Carrot
			GameObject* obstacle_09 = new GameObject("carrot_0");
			obstacle_09->AddComponent<SpriteRenderer>("Obstacles/carrots");
			obstacle_09->AddComponent<BoxCollider>();
			obstacle_09->AddComponent<Obstacle>();
			GameObject* obstacle_10 = new GameObject("carrot_1");
			obstacle_10->AddComponent<SpriteRenderer>("Obstacles/carrots");
			obstacle_10->AddComponent<BoxCollider>();
			obstacle_10->AddComponent<Obstacle>();
			GameObject* obstacle_11 = new GameObject("carrot_2");
			obstacle_11->AddComponent<SpriteRenderer>("Obstacles/carrots");
			obstacle_11->AddComponent<BoxCollider>();
			obstacle_11->AddComponent<Obstacle>();

			//Cabbage
			GameObject* obstacle_12 = new GameObject("cabbage_0");
			obstacle_12->AddComponent<SpriteRenderer>("Obstacles/cabbage");
			obstacle_12->AddComponent<BoxCollider>();
			obstacle_12->AddComponent<Obstacle>();
			GameObject* obstacle_13 = new GameObject("cabbage_1");
			obstacle_13->AddComponent<SpriteRenderer>("Obstacles/cabbage");
			obstacle_13->AddComponent<BoxCollider>();
			obstacle_13->AddComponent<Obstacle>();


			//Well (Addition)
			GameObject* obstacle_14 = new GameObject("well_2");
			obstacle_14->AddComponent<SpriteRenderer>("Obstacles/water1");
			obstacle_14->AddComponent<BoxCollider>();
			obstacle_14->AddComponent<Obstacle>();
			GameObject* obstacle_15 = new GameObject("well_3");
			obstacle_15->AddComponent<SpriteRenderer>("Obstacles/water1");
			obstacle_15->AddComponent<CircleCollider>();
			obstacle_15->AddComponent<Obstacle>();

			//Cart
			GameObject* obstacle_16 = new GameObject("cart_0");
			obstacle_16->AddComponent<SpriteRenderer>("Obstacles/cart");
			obstacle_16->AddComponent<BoxCollider>();
			obstacle_16->AddComponent<Obstacle>();
			GameObject* obstacle_17 = new GameObject("cart_1");
			obstacle_17->AddComponent<SpriteRenderer>("Obstacles/cart");
			obstacle_17->AddComponent<BoxCollider>();
			obstacle_17->AddComponent<Obstacle>();

			//Hose
			GameObject* obstacle_18 = new GameObject("hose_0");
			obstacle_18->AddComponent<SpriteRenderer>("Obstacles/hose");
			obstacle_18->AddComponent<CircleCollider>();
			obstacle_18->AddComponent<Obstacle>();

			//Fences
			GameObject* obstacle_19 = new GameObject("fence_0");
			obstacle_19->AddComponent<SpriteRenderer>("Obstacles/fence_horizontal");
			
			GameObject* obstacle_20 = new GameObject("fence_1");
			obstacle_20->AddComponent<SpriteRenderer>("Obstacles/fence_horizontal");

			GameObject* obstacle_21 = new GameObject("fence_2");
			obstacle_21->AddComponent<SpriteRenderer>("Obstacles/fence_verticle");

			GameObject* obstacle_22 = new GameObject("fence_3");
			obstacle_22->AddComponent<SpriteRenderer>("Obstacles/fence_verticle");

			//Gates
			GameObject* obstacle_23 = new GameObject("gate_0");
			obstacle_23->AddComponent<SpriteRenderer>("Obstacles/fence_gate");
			GameObject* obstacle_24 = new GameObject("gate_1");
			obstacle_24->AddComponent<SpriteRenderer>("Obstacles/fence_gate");
			GameObject* obstacle_25 = new GameObject("gate_2");
			obstacle_25->AddComponent<SpriteRenderer>("Obstacles/fence_gate");


		//---------------


		//----Arrow Head----
		GameObject* levelController = new GameObject("Tutorial Controller");
		levelController->AddComponent<FirstLevelController>()._player = &duck->GetComponent<DuckController>();

		GameObject* arrowHead = new GameObject("NextRoomArrow");
		arrowHead->AddComponent<SpriteRenderer>("whiteTriangle");
		arrowHead->AddComponent<BoxCollider>();

		levelController->GetComponent<FirstLevelController>()._nextRoomArrow = arrowHead;
		levelController->GetComponent<FirstLevelController>()._sceneTransition = &sceneTransition->GetComponent<SceneTransition>();

		levelController->GetComponent<FirstLevelController>()._chickenManager = &chickenManager->GetComponent<ChickenManager>();
		//------------------

		GameObject* mainLevelGate = new GameObject("main gate");
		mainLevelGate->AddComponent<SpriteRenderer>("Obstacles/fence_gate");

		levelController->GetComponent<FirstLevelController>()._fenceDoor = mainLevelGate;
		//---------------------
		GameObject* playerHealth = new GameObject("PlayerHealth");

		for (size_t i = 0; i < 3; i++)
		{
			//----Create Background----
			GameObject* grass = new GameObject("Background");
			grass->transform->SetPosition(Vector2(SCREEN_WIDTH / 2, SCREEN_HEIGHT / 2));
			grass->transform->localScale = Vector2(1.0f, 1.0f);
			grass->AddComponent<SpriteRenderer>("Grounds/Ground_Green");
		}
		
		//----Decorations
		//grasses
		GameObject* grassPatch01 = new GameObject("grass_type1_0");
		grassPatch01->AddComponent<SpriteRenderer>("Decorations/grass1");
		GameObject* grassPatch02 = new GameObject("grass_type2_0");
		grassPatch02->AddComponent<SpriteRenderer>("Decorations/grass2");
		GameObject* grassPatch03 = new GameObject("grass_type3_0");
		grassPatch03->AddComponent<SpriteRenderer>("Decorations/grass3");
		GameObject* grassPatch04 = new GameObject("grass_type1_1");
		grassPatch04->AddComponent<SpriteRenderer>("Decorations/grass1");
		GameObject* grassPatch05 = new GameObject("grass_type2_1");
		grassPatch05->AddComponent<SpriteRenderer>("Decorations/grass2");
		GameObject* grassPatch06 = new GameObject("grass_type3_1");
		grassPatch06->AddComponent<SpriteRenderer>("Decorations/grass3");
		GameObject* grassPatch07 = new GameObject("grass_type1_2");
		grassPatch07->AddComponent<SpriteRenderer>("Decorations/grass1");
		GameObject* grassPatch08 = new GameObject("grass_type3_2");
		grassPatch08->AddComponent<SpriteRenderer>("Decorations/grass3");

		//flowers
		GameObject* flower01 = new GameObject("flower_type1_0");
		flower01->AddComponent<SpriteRenderer>("Decorations/flower1");
		GameObject* flower02 = new GameObject("flower_type1_1");
		flower02->AddComponent<SpriteRenderer>("Decorations/flower1");
		GameObject* flower03 = new GameObject("flower_type1_2");
		flower03->AddComponent<SpriteRenderer>("Decorations/flower1");
		GameObject* flower04 = new GameObject("flower_type2_0");
		flower04->AddComponent<SpriteRenderer>("Decorations/flower2");
		GameObject* flower05 = new GameObject("flower_type2_1");
		flower05->AddComponent<SpriteRenderer>("Decorations/flower2");
		GameObject* flower06 = new GameObject("flower_type2_2");
		flower06->AddComponent<SpriteRenderer>("Decorations/flower2");
		GameObject* flower07 = new GameObject("flower_type2_3");
		flower07->AddComponent<SpriteRenderer>("Decorations/flower2");

		//river
		GameObject* river01 = new GameObject("river_0");
		river01->AddComponent<SpriteRenderer>("Decorations/river");
		GameObject* river02 = new GameObject("river_1");
		river02->AddComponent<SpriteRenderer>("Decorations/river");
		GameObject* river03 = new GameObject("river_2");
		river03->AddComponent<SpriteRenderer>("Decorations/river");

		//rocks
		GameObject* rocks01 = new GameObject("rocks_type1_0");
		rocks01->AddComponent<SpriteRenderer>("Decorations/rocks1");
		GameObject* rocks02 = new GameObject("rocks_type2_0");
		rocks02->AddComponent<SpriteRenderer>("Decorations/rocks2");
		GameObject* rocks03 = new GameObject("rocks_type2_1");
		rocks03->AddComponent<SpriteRenderer>("Decorations/rocks2");
		GameObject* rocks04 = new GameObject("rocks_type3_0");
		rocks04->AddComponent<SpriteRenderer>("Decorations/rocks3");


		//-------All UI Stuff--------

		GameObject* canvas = new GameObject("Canvas");
		canvas->transform->SetParent(mainCamera->transform);
		GameObject* duckUI = new GameObject("DuckUI");
		duckUI->transform->SetParent(canvas->transform);

		GameObject* duckUI_bullet = new GameObject("BulletText");
		duckUI_bullet->transform->SetParent(duckUI->transform);
		duckUI_bullet->AddComponent<TextRenderer>("50", "GeosansLight.ttf");

		GameObject* duckUI_healthBarBG = new GameObject("HealthBarBG");
		duckUI_healthBarBG->transform->SetParent(duckUI->transform);
		duckUI_healthBarBG->AddComponent<SpriteRenderer>("whiteBox");
		GameObject* duckUI_healthBar = new GameObject("HealthBar");
		duckUI_healthBar->transform->SetParent(duckUI_healthBarBG->transform);
		duckUI_healthBar->AddComponent<SpriteRenderer>("whiteBox");
		GameObject* duckUI_healthText = new GameObject("HealthText");
		duckUI_healthText->transform->SetParent(duckUI_healthBarBG->transform);
		duckUI_healthText->AddComponent<TextRenderer>("8/8", "GeosansLight.ttf");

		GameObject* missionHolder = new GameObject("MissionHolder");
		missionHolder->transform->SetParent(canvas->transform);
		GameObject* missionBG = new GameObject("MissionBG");
		missionBG->transform->SetParent(missionHolder->transform);
		missionBG->AddComponent<SpriteRenderer>("Panel_30");

		GameObject* missionText = new GameObject("MissionText");
		missionText->transform->SetParent(missionHolder->transform);
		missionText->AddComponent<TextRenderer>("WASD to walk", "GeosansLight.ttf");

		//---------------------------

		GameObject* gm = new GameObject("GM");
		GM::GetInstance().SetPlayer(duck);

		duck->AddComponent<Animator>();

		// ---- Aim Curser ----
		GameObject* aimCurser = new GameObject("AimCurser");
		aimCurser->transform->SetParent(canvas->transform);
		aimCurser->AddComponent<SpriteRenderer>("UIs/AimCurser");

		duckUI->AddComponent<DuckUI>().Setup(&duckUI_bullet->GetComponent<TextRenderer>(), duckUI_healthBar, &duckUI_healthText->GetComponent<TextRenderer>(), aimCurser);

		// --------------------

		GameObject* duckUI_bulletIcon = new GameObject("BulletIcon");
		duckUI_bulletIcon->transform->SetParent(duckUI->transform);
		duckUI_bulletIcon->AddComponent<SpriteRenderer>("UIs/BulletIcon");

		GameObject* chargeUIHolder = new GameObject("ChargeHolder");
		chargeUIHolder->transform->SetParent(canvas->transform);
		GameObject* chargeFrame = new GameObject("ChargeFrame");
		chargeFrame->transform->SetParent(chargeUIHolder->transform);
		chargeFrame->AddComponent<SpriteRenderer>("UIs/Charge_Frame");
		GameObject* chargeBG = new GameObject("ChargeBG");
		chargeBG->transform->SetParent(chargeUIHolder->transform);
		chargeBG->AddComponent<SpriteRenderer>("whiteBox");
		GameObject* chargeBar = new GameObject("ChargeBar");
		chargeBar->transform->SetParent(chargeUIHolder->transform);
		chargeBar->AddComponent<SpriteRenderer>("whiteBox");

		GameObject* boss = new GameObject("Boss");
		boss->AddComponent<BossController>().player = duck;
		boss->AddComponent<Rigidbody>().gravityScale = 0.0f;
		boss->AddComponent<Animator>();
		boss->AddComponent<SpriteRenderer>("Boss/BossSpriteWalk").order = 6;
		boss->AddComponent<CircleCollider>();

		GameObject* bossHead = new GameObject("BossHead");
		bossHead->transform->SetParent(boss->transform);
		boss->GetComponent<BossController>().head = bossHead;

		GameObject* bossUIHolder = new GameObject("BossUIHolder");
		bossUIHolder->transform->SetParent(canvas->transform);
		GameObject* bossHealthBarBG = new GameObject("BossHealthBarBG");
		bossHealthBarBG->transform->SetParent(bossUIHolder->transform);
		bossHealthBarBG->AddComponent<SpriteRenderer>("whiteBox");
		GameObject* bossHealthBar = new GameObject("BossHealthBar");
		bossHealthBar->transform->SetParent(bossHealthBarBG->transform);
		bossHealthBar->AddComponent<SpriteRenderer>("whiteBox");
		GameObject* bossName = new GameObject("BossNameText");
		bossName->transform->SetParent(bossUIHolder->transform);
		bossName->AddComponent<TextRenderer>("Pok Pok", "GeosansLight.ttf");
		GameObject* bossHealthBarInner = new GameObject("BossHealthBarInner");
		bossHealthBarInner->transform->SetParent(bossHealthBarBG->transform);
		bossHealthBarInner->AddComponent<SpriteRenderer>("whiteBox");

		boss->GetComponent<BossController>().uiHolder = bossUIHolder;
		boss->GetComponent<BossController>().healthBar = bossHealthBar;
		boss->GetComponent<BossController>().innerHealthBar = bossHealthBarInner;

		duck->GetComponent<DuckController>().chargeBar = chargeBar;
		levelController->GetComponent<FirstLevelController>()._textRenderer = &missionText->GetComponent<TextRenderer>();
		levelController->GetComponent<FirstLevelController>()._boss = &boss->GetComponent<BossController>();
	}
}