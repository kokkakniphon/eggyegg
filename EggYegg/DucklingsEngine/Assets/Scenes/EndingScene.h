#pragma once

#include "Scene.h"
#include "Engine.h"
#include "FileManager.h"

namespace DucklingsEngine
{
	class EndingScene : public Scene
	{
	public:
		EndingScene() 
		{ 
			name = "EndingScene"; 
			path = "../Assets/Scenes/EndingScene.scene";
		}
		~EndingScene() = default;

		void Start();

		inline void LoadScene() 
		{ 
			Scene::LoadScene(); 
			std::cout << "Loading Scene: " << name << std::endl;
			Start();
			FileManager::LoadYAMLFile(path, name);
		}

		inline bool UnloadScene() override
		{
			std::cout << "Unload Scene: " << name << std::endl;

			std::vector<GameObject*> gameObjects = Engine::GetInstance().GetManager()->GetEntitysInScene(name);

			for (auto object : gameObjects)
			{
				object->Destroy();
			}

			Scene::UnloadScene();

			return true;
		}
	};
}

