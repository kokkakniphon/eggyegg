#pragma once

#include "Scene.h"
#include "Engine.h"
#include "FileManager.h"

namespace DucklingsEngine
{
	class EngineProof_PhysicTestScene : public Scene
	{
	public:
		EngineProof_PhysicTestScene() 
		{ 
			name = "EngineProof_PhysicTestScene"; 
			path = "../Assets/Scenes/EngineProof_PhysicTestScene.scene";
		}
		~EngineProof_PhysicTestScene() = default;

		void Start();

		inline void LoadScene() 
		{ 
			Scene::LoadScene(); 
			std::cout << "Loading Scene: " << name << std::endl;
			Start();
			FileManager::LoadYAMLFile(path, name);
		}

		inline bool UnloadScene() override
		{
			std::cout << "Unload Scene: " << name << std::endl;

			Engine::GetInstance().GetManager()->DestoryAllEntity(name);
			Scene::UnloadScene();

			return true;
		}
	};
}

