#include "TitleScreenScene.h"

#include "GameObject.h"
#include "SpriteRenderer.h"
#include "BoxCollider.h"

#include "Scripts/TitleScreenController.h"

namespace DucklingsEngine
{
	void TitleScreenScene::Start()
	{
		// This will be execute when loading the scene.
		GameObject* titleScreen = new GameObject("Title Screen");
		titleScreen->AddComponent<TitleScreenController>();
		titleScreen->transform->localPosition = Vector2(SCREEN_WIDTH / 2, SCREEN_HEIGHT / 2);
		titleScreen->transform->localScale = Vector2(SCREEN_WIDTH / 64, SCREEN_HEIGHT / 64);

		TitleScreenController* titleScreenController = &titleScreen->GetComponent<TitleScreenController>();

		Vector2 scaleFactor = Vector2(0.6f, 0.6f);

		GameObject* skyScreen = new GameObject("Sky");
		skyScreen->AddComponent<SpriteRenderer>("title_sky");
		skyScreen->transform->localScale = scaleFactor;
		titleScreenController->AddScreenLayer(skyScreen, 0);
		skyScreen->transform->SetParent(titleScreen->transform);

		GameObject* cloudScreen = new GameObject("Cloud");
		cloudScreen->AddComponent<SpriteRenderer>("title_cloud");
		cloudScreen->transform->localScale = scaleFactor;
		titleScreenController->AddScreenLayer(cloudScreen, 0.1f);
		cloudScreen->transform->SetParent(titleScreen->transform);

		GameObject* penScreen = new GameObject("Pen");
		penScreen->AddComponent<SpriteRenderer>("title_pen");
		penScreen->transform->localScale = scaleFactor;
		titleScreenController->AddScreenLayer(penScreen, 0.2f);
		penScreen->transform->SetParent(titleScreen->transform);

		GameObject* groundScreen = new GameObject("Ground");
		groundScreen->AddComponent<SpriteRenderer>("title_grass");
		groundScreen->transform->localScale = scaleFactor;
		titleScreenController->AddScreenLayer(groundScreen, 0.2f);
		groundScreen->transform->SetParent(titleScreen->transform);

		GameObject* armadilloScreen = new GameObject("Armadillo");
		armadilloScreen->AddComponent<SpriteRenderer>("title_armadillo");
		armadilloScreen->transform->localScale = scaleFactor;
		titleScreenController->AddScreenLayer(armadilloScreen, 0.4f);
		armadilloScreen->transform->SetParent(titleScreen->transform);

		GameObject* henScreen = new GameObject("Hen");
		henScreen->AddComponent<SpriteRenderer>("title_hen");
		henScreen->transform->localScale = scaleFactor;
		titleScreenController->AddScreenLayer(henScreen, 0.6f);
		henScreen->transform->SetParent(titleScreen->transform);

		GameObject* chickenScreen = new GameObject("Chickens");
		chickenScreen->AddComponent<SpriteRenderer>("title_chickens");
		chickenScreen->transform->localScale = scaleFactor;
		titleScreenController->AddScreenLayer(chickenScreen, 0.7f);
		chickenScreen->transform->SetParent(titleScreen->transform);

		GameObject* duckScreen = new GameObject("Duck");
		duckScreen->AddComponent<SpriteRenderer>("title_duck");
		duckScreen->transform->localScale = scaleFactor;
		titleScreenController->AddScreenLayer(duckScreen, 1.0f);
		duckScreen->transform->SetParent(titleScreen->transform);

		GameObject* grassScreen = new GameObject("GrassBlades");
		grassScreen->AddComponent<SpriteRenderer>("title_grassblades");
		grassScreen->transform->localScale = scaleFactor;
		titleScreenController->AddScreenLayer(grassScreen, 1.2f);
		grassScreen->transform->SetParent(titleScreen->transform);

		//----Start Button----
		GameObject* start_button = new GameObject("Start Button");
		start_button->AddComponent<SpriteRenderer>("title_start_button");
		start_button->transform->SetParent(titleScreen->transform);
		start_button->AddComponent<BoxCollider>();

		//Flash
		GameObject* flash = new GameObject("Flash");
		flash->transform->SetParent(titleScreen->transform);
		flash->AddComponent<SpriteRenderer>("whiteBox");
		titleScreen->GetComponent<TitleScreenController>().renderer = &flash->GetComponent<SpriteRenderer>();
	}
}