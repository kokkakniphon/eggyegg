#include "Tutorial.h"

#include "Camera.h"
#include "GameObject.h"
#include "CircleCollider.h"
#include "BoxCollider.h"
#include "Rigidbody.h"
#include "SpriteRenderer.h"
#include "TextRenderer.h"
#include "Animator.h"

#include "Scripts/Duck/DuckController.h"
#include "Scripts/Duck/DuckUI.h"
#include "Scripts/Amadillo/Ball.h"
#include "Scripts/Chicken/ChickenController.h"
#include "Scripts/Tutorial/TutorialController.h"
#include "Scripts/Core/SceneTransition.h"
#include "Scripts/Obstacle/Obstacle.h"

#include "Scripts/DropManager.h"
#include "Scripts/GM.h"

namespace DucklingsEngine
{
	void Tutorial::Start()
	{
		// This will be execute when loading the scene.

		//----Camera----
		GameObject* mainCamera = new GameObject("Main Camera");
		mainCamera->AddComponent<Camera>();

		//----Create Background----
		GameObject* background = new GameObject("Background");
		background->transform->SetPosition(Vector2(SCREEN_WIDTH / 2, SCREEN_HEIGHT / 2));
		background->transform->localScale = Vector2(1.6f, 1.6f);
		background->AddComponent<SpriteRenderer>("Grounds/Ground_Tutorial");

		//- - - - Set Walls - - - -
		GameObject* topWall = new GameObject("TopWall");
		topWall->transform->SetPosition(Vector2(SCREEN_WIDTH / 2, 0));
		topWall->AddComponent<BoxCollider>().size = Vector2(SCREEN_WIDTH / 2, 50);

		GameObject* bottomWall = new GameObject("TopWall");
		bottomWall->transform->SetPosition(Vector2(SCREEN_WIDTH / 2, SCREEN_HEIGHT));
		bottomWall->AddComponent<BoxCollider>().size = Vector2(SCREEN_WIDTH / 2, 50);

		GameObject* leftWall = new GameObject("LeftWall");
		leftWall->transform->SetPosition(Vector2(0, SCREEN_HEIGHT / 2));
		leftWall->AddComponent<BoxCollider>().size = Vector2(30, SCREEN_HEIGHT / 2);

		GameObject* rightWall = new GameObject("RightWall");
		rightWall->transform->SetPosition(Vector2(SCREEN_WIDTH, SCREEN_HEIGHT / 2));
		rightWall->AddComponent<BoxCollider>().size = Vector2(30, SCREEN_HEIGHT / 2);
		//- - - - - - - - - - - - -

		//-------------------------

		//----Create Duck (Player)----

		GameObject* duck = new GameObject("Player");
		duck->transform->localScale = Vector2(0.07f, 0.07f);
		duck->AddComponent<Rigidbody>().gravityScale = 0.0f;
		duck->AddComponent<SpriteRenderer>("DuckBody");
		duck->AddComponent<CircleCollider>().radius = 30.0f;
		duck->AddComponent<DuckController>().rid2D = &duck->GetComponent<Rigidbody>();
		duck->tag = "Player";

		//- - - - Duck Head - - - -

		GameObject* duckHead = new GameObject("DuckHead");
		duckHead->transform->localScale = Vector2(1.1f, 1.1f);
		duckHead->AddComponent<SpriteRenderer>("DuckHead");
		duckHead->transform->SetParent(duck->transform);
		duckHead->transform->localPosition = Vector2(-15.0f, 0.0f);
		duckHead->AddComponent<CircleCollider>().isTrigger = true;
		duckHead->GetComponent<CircleCollider>().radius = 30.0f;
		//- - - - - - - - - - - - -

		//- - - -Aim Ring - - - -
		GameObject* aim = new GameObject("Aim");
		aim->AddComponent<SpriteRenderer>("Direction");
		aim->transform->localPosition = Vector2(0.0f, 0.0f);
		//- - - - - - - - - - - - 

		//- - - -Quack- - - -
		GameObject* quack = new GameObject("Quack");
		quack->transform->localScale = Vector2(0.6f, 0.6f);
		quack->AddComponent<SpriteRenderer>("quack");
		quack->transform->localPosition = Vector2(0.0f, 0.0f);
		quack->transform->SetPosition(Vector2(SCREEN_WIDTH / 2, SCREEN_HEIGHT / 2));
		quack->transform->localEulerAngles = 45;
		//- - - - - - - - - -

		duck->GetComponent<DuckController>().duckHead = duckHead;
		duck->GetComponent<DuckController>().aim_obj = aim;
		duck->GetComponent<DuckController>().quack = quack;

		//----------------------------

		//----Create Ball----
		GameObject* ball = new GameObject("Ball");
		ball->transform->localScale = Vector2(0.15f, 0.15f);
		ball->transform->SetPosition(Vector2(SCREEN_WIDTH / 4, SCREEN_HEIGHT / 2));
		ball->AddComponent<Rigidbody>().gravityScale = 0.0f;
		ball->GetComponent<Rigidbody>().drag = 0.2f;
		ball->AddComponent<SpriteRenderer>("armadillo");
		ball->AddComponent<CircleCollider>().radius = 35.0f;
		ball->AddComponent<Animator>();
		//Pointer to both duck(also aim ring) and ball to enable and disable
		ball->AddComponent<Ball>().duckPointer = duck;
		ball->GetComponent<Ball>().aimPointer = aim;
		duck->GetComponent<DuckController>().ballPointer = ball;
		//-------------------

		duck->GetComponent<DuckController>().ball = &ball->GetComponent<Ball>();

		//----Placing Obstacle----
		GameObject* obstacle = new GameObject("Obstacle");
		obstacle->transform->localScale = Vector2(0.5f, 0.5f);
		obstacle->transform->SetPosition(Vector2(SCREEN_WIDTH / 4, SCREEN_HEIGHT / 3));
		obstacle->transform->localEulerAngles = 30.0f;
		obstacle->AddComponent<SpriteRenderer>("Obstacles/haybale2");
		obstacle->AddComponent<BoxCollider>().size = Vector2(32, 32);
		obstacle->AddComponent<Obstacle>();

		GameObject* obstacle2 = new GameObject("Obstacle");
		obstacle2->transform->localScale = Vector2(0.5f, 0.5f);
		obstacle2->transform->SetPosition(Vector2(3.5*SCREEN_WIDTH / 4, SCREEN_HEIGHT / 3 + 200));
		obstacle2->transform->localEulerAngles = 0.0f;
		obstacle2->AddComponent<SpriteRenderer>("Obstacles/haybale1");
		obstacle2->AddComponent<BoxCollider>().size = Vector2(32, 32);
		obstacle2->AddComponent<Obstacle>();

		GameObject* obstacle3 = new GameObject("Obstacle");
		obstacle3->transform->localScale = Vector2(0.5f, 0.5f);
		obstacle3->transform->SetPosition(Vector2(1.2*SCREEN_WIDTH / 4, SCREEN_HEIGHT / 3 + 350));
		obstacle3->transform->localEulerAngles = 0.0f;
		obstacle3->AddComponent<SpriteRenderer>("Obstacles/water1");
		obstacle3->AddComponent<CircleCollider>().radius = 64;
		obstacle3->AddComponent<Obstacle>();

		//------------------------

		//----Duck Stamina----
		for (size_t i = 0; i < 3; i++)
		{
			GameObject* stamina = new GameObject("Stamina " + std::to_string(i + 1));
			stamina->SetActive(false);
			stamina->AddComponent<SpriteRenderer>("whiteBall");
			stamina->transform->SetParent(aim->transform);
		}
		//--------------------

		//----Tutorial Controller----
		GameObject* tutorialController = new GameObject("Tutorial Controller");
		tutorialController->AddComponent<TutorialController>();
		
		GameObject* arrowHead = new GameObject("NextRoomArrow");
		arrowHead->AddComponent<SpriteRenderer>("whiteTriangle");
		arrowHead->AddComponent<BoxCollider>().isTrigger = true;

		tutorialController->GetComponent<TutorialController>()._nextRoomArrow = arrowHead;
		tutorialController->GetComponent<TutorialController>()._duck = &duck->GetComponent<DuckController>();
		tutorialController->GetComponent<TutorialController>()._obstacle = obstacle;

		GameObject* sceneTransition = new GameObject("Scene Transition Controller");
		sceneTransition->AddComponent<SceneTransition>();

		tutorialController->GetComponent<TutorialController>()._sceneTransition = &sceneTransition->GetComponent<SceneTransition>();
		
		GameObject* transitionOverlay = new GameObject("Transition Overlay");
		transitionOverlay->transform->SetParent(mainCamera->transform);
		transitionOverlay->AddComponent<SpriteRenderer>("whiteBox");

		sceneTransition->GetComponent<SceneTransition>()._transitionOverlay = &transitionOverlay->GetComponent<SpriteRenderer>();
		
		//---------------------------

		//-------All UI Stuff--------

		GameObject* canvas = new GameObject("Canvas");
		canvas->transform->SetParent(mainCamera->transform);
		GameObject* duckUI = new GameObject("DuckUI");
		duckUI->transform->SetParent(canvas->transform);

		GameObject* duckUI_bullet = new GameObject("BulletText");
		duckUI_bullet->transform->SetParent(duckUI->transform);
		duckUI_bullet->AddComponent<TextRenderer>("50", "GeosansLight.ttf");

		GameObject* duckUI_healthBarBG = new GameObject("HealthBarBG");
		duckUI_healthBarBG->transform->SetParent(duckUI->transform);
		duckUI_healthBarBG->AddComponent<SpriteRenderer>("whiteBox");
		GameObject* duckUI_healthBar = new GameObject("HealthBar");
		duckUI_healthBar->transform->SetParent(duckUI_healthBarBG->transform);
		duckUI_healthBar->AddComponent<SpriteRenderer>("whiteBox");
		GameObject* duckUI_healthText = new GameObject("HealthText");
		duckUI_healthText->transform->SetParent(duckUI_healthBarBG->transform);
		duckUI_healthText->AddComponent<TextRenderer>("8/8", "GeosansLight.ttf");

		GameObject* missionHolder = new GameObject("MissionHolder");
		missionHolder->transform->SetParent(canvas->transform);
		GameObject* missionBG = new GameObject("MissionBG");
		missionBG->transform->SetParent(missionHolder->transform);
		missionBG->AddComponent<SpriteRenderer>("Panel_30");

		GameObject* missionText = new GameObject("MissionText");
		missionText->transform->SetParent(missionHolder->transform);
		missionText->AddComponent<TextRenderer>("WASD to walk", "GeosansLight.ttf");

		//---------------------------

		GameObject* gm = new GameObject("GM");
		GM::GetInstance().SetPlayer(duck);

		duck->AddComponent<Animator>();
		// ---- Aim Curser ----
		GameObject* aimCurser = new GameObject("AimCurser");
		aimCurser->transform->SetParent(canvas->transform);
		aimCurser->AddComponent<SpriteRenderer>("UIs/AimCurser");

		duckUI->AddComponent<DuckUI>().Setup(&duckUI_bullet->GetComponent<TextRenderer>(), duckUI_healthBar, &duckUI_healthText->GetComponent<TextRenderer>(), aimCurser);
		tutorialController->GetComponent<TutorialController>()._textRenderer = &missionText->GetComponent<TextRenderer>();
		tutorialController->GetComponent<TutorialController>()._missionObj = missionHolder;
		// --------------------

		GameObject* duckUI_bulletIcon = new GameObject("BulletIcon");
		duckUI_bulletIcon->transform->SetParent(duckUI->transform);
		duckUI_bulletIcon->AddComponent<SpriteRenderer>("UIs/BulletIcon");

		GameObject* chargeUIHolder = new GameObject("ChargeHolder");
		chargeUIHolder->transform->SetParent(canvas->transform);
		GameObject* chargeFrame = new GameObject("ChargeFrame");
		chargeFrame->transform->SetParent(chargeUIHolder->transform);
		chargeFrame->AddComponent<SpriteRenderer>("UIs/Charge_Frame");
		GameObject* chargeBG = new GameObject("ChargeBG");
		chargeBG->transform->SetParent(chargeUIHolder->transform);
		chargeBG->AddComponent<SpriteRenderer>("whiteBox");
		GameObject* chargeBar = new GameObject("ChargeBar");
		chargeBar->transform->SetParent(chargeUIHolder->transform);
		chargeBar->AddComponent<SpriteRenderer>("whiteBox");

		duck->GetComponent<DuckController>().chargeBar = chargeBar;
	}
}