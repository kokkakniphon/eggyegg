#include "EndingScene.h"
#include "Camera.h"
#include "SpriteRenderer.h"
#include "TextRenderer.h"
#include "GameObject.h"
#include "Scripts/WinLoseScreen.h"

namespace DucklingsEngine
{
	void EndingScene::Start()
	{
		// This will be execute when loading the scene.
		//controller script
		GameObject* cutscene_script = new GameObject("Cutscene Script");
		cutscene_script->AddComponent<WinLoseScreen>();
		WinLoseScreen* cutscene_script_controller = &cutscene_script->GetComponent<WinLoseScreen>();

		//----Camera----
		GameObject* mainCamera = new GameObject("Main Camera");
		mainCamera->AddComponent<Camera>();
		mainCamera->transform->SetPosition(Vector2(0.0f, 0.0f));

		//----Create game object to hold cutscene images----
		GameObject* background = new GameObject("Cutscene");
		background->transform->SetPosition(Vector2(0.0f, 0.0f));
		background->transform->localScale = Vector2(2.0f, 2.0f);
		background->AddComponent<SpriteRenderer>("Cutscenes/ending");

		//----Create game object to hold cutscene images----
		GameObject* credit = new GameObject("Credit");
		credit->transform->SetPosition(Vector2(0.0f, 0.0f));
		credit->AddComponent<SpriteRenderer>("Cutscenes/Credit");

		//set alpha to 0
		background->GetComponent<SpriteRenderer>().color.a = 0.0f;
		cutscene_script_controller->SetPic(background);
		cutscene_script_controller->credit = credit;
	}
}