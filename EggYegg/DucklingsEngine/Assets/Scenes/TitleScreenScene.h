#pragma once

#include "Scene.h"
#include "Engine.h"
#include "FileManager.h"

namespace DucklingsEngine
{
	class TitleScreenScene : public Scene
	{
	public:
		TitleScreenScene() 
		{ 
			name = "TitleScreenScene"; 
			path = "../Assets/Scenes/TitleScreenScene.scene";
		}
		~TitleScreenScene() = default;

		void Start();

		inline void LoadScene() 
		{ 
			Scene::LoadScene(); 
			std::cout << "Loading Scene: " << name << std::endl;
			Start();
			FileManager::LoadYAMLFile(path, name);
		}

		inline bool UnloadScene() override
		{
			std::cout << "Unload Scene: " << name << std::endl;

			Engine::GetInstance().GetManager()->DestoryAllEntity(name);
			Scene::UnloadScene();

			return true;
		}
	};
}

