#pragma once

#include "Scene.h"
#include "Engine.h"
#include "FileManager.h"

namespace DucklingsEngine
{
	class EggYegg_EarlyEngineBuild : public Scene
	{
	public:
		EggYegg_EarlyEngineBuild()
		{
			name = "EggYegg_EarlyEngineBuild";
			path = "../Assets/Scenes/EggYegg_EarlyEngineBuild.scene";
		}
		~EggYegg_EarlyEngineBuild() = default;

		void Start();

		inline void LoadScene() 
		{ 
			Scene::LoadScene(); 
			std::cout << "Loading Scene: " << name << std::endl;
			Start();
			FileManager::LoadYAMLFile(path, name);
		}

		inline bool UnloadScene() override
		{
			std::cout << "Unload Scene: " << name << std::endl;

			Engine::GetInstance().GetManager()->DestoryAllEntity(name);
			Scene::UnloadScene();
			
			return true;
		}
	};
}

