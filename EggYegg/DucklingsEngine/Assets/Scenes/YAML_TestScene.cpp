#include "YAML_TestScene.h"

#include "GameObject.h"
#include "SpriteRenderer.h"
#include "Camera.h"

//#include "Scripts/YAML_TestScript.h"
namespace DucklingsEngine
{
	void YAML_TestScene::Start()
	{
		// This will be execute when loading the scene.
		GameObject* bg = new GameObject("Background");
		GameObject* ball = new GameObject("Ball");

		ball->AddComponent<SpriteRenderer>("ball");

		//ball->AddComponent<YAML_TestScript>();

		//----Add Camera----
		GameObject* camera = new GameObject("Main Camera");
		camera->AddComponent<Camera>();
	}
}