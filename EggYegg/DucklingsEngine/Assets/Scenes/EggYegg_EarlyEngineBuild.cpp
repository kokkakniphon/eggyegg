#include "EggYegg_EarlyEngineBuild.h"

#include "Camera.h"
#include "GameObject.h"
#include "CircleCollider.h"
#include "BoxCollider.h"
#include "Rigidbody.h"
#include "SpriteRenderer.h"
#include "TextRenderer.h"

#include "Scripts/Duck/DuckController.h"
#include "Scripts/Amadillo/Ball.h"
#include "Scripts/Chicken/ChickenController.h"

namespace DucklingsEngine
{
	void EggYegg_EarlyEngineBuild::Start()
	{
		// This will be execute when loading the scene.

		//----Create Background----
		GameObject* background = new GameObject("Background");
		background->transform->SetPosition(Vector2(SCREEN_WIDTH / 2, SCREEN_HEIGHT / 2));
		background->transform->localScale = Vector2(1.6f,1.6f);
		background->AddComponent<SpriteRenderer>("Map");

		//- - - - Set Walls - - - -
		GameObject* topWall = new GameObject("TopWall");
		topWall->transform->SetPosition(Vector2(SCREEN_WIDTH / 2, 0));
		topWall->AddComponent<BoxCollider>().size = Vector2(SCREEN_WIDTH / 2, 50);

		GameObject* bottomWall = new GameObject("TopWall");
		bottomWall->transform->SetPosition(Vector2(SCREEN_WIDTH / 2, SCREEN_HEIGHT));
		bottomWall->AddComponent<BoxCollider>().size = Vector2(SCREEN_WIDTH / 2, 50);

		GameObject* leftWall = new GameObject("LeftWall");
		leftWall->transform->SetPosition(Vector2(0, SCREEN_HEIGHT / 2));
		leftWall->AddComponent<BoxCollider>().size = Vector2(30, SCREEN_HEIGHT / 2);

		GameObject* rightWall = new GameObject("RightWall");
		rightWall->transform->SetPosition(Vector2(SCREEN_WIDTH, SCREEN_HEIGHT / 2));
		rightWall->AddComponent<BoxCollider>().size = Vector2(30, SCREEN_HEIGHT / 2);
		//- - - - - - - - - - - - -

		//-------------------------

		//----Create Duck (Player)----

		GameObject* duck = new GameObject("Player");
		duck->transform->localScale = Vector2(0.07f, 0.07f);
		duck->AddComponent<Rigidbody>().gravityScale = 0.0f;
		duck->AddComponent<SpriteRenderer>("DuckBody");
		duck->AddComponent<CircleCollider>().radius = 30.0f;
		duck->AddComponent<DuckController>().rid2D = &duck->GetComponent<Rigidbody>();

		//- - - - Duck Head - - - -

		GameObject* duckHead = new GameObject("DuckHead");
		duckHead->transform->localScale = Vector2(1.1f, 1.1f);
		duckHead->AddComponent<SpriteRenderer>("DuckHead");
		duckHead->transform->SetParent(duck->transform);
		duckHead->transform->localPosition = Vector2(-15.0f, 0.0f);
		duckHead->AddComponent<CircleCollider>().isTrigger = true;
		duckHead->GetComponent<CircleCollider>().radius = 30.0f;
		//- - - - - - - - - - - - -

		//- - - -Aim Ring - - - -
		GameObject* aim = new GameObject("Aim");
		//aim->transform->SetPosition(Vector2(SCREEN_WIDTH/2, SCREEN_HEIGHT / 2));
		aim->transform->localScale = Vector2(1.5f / duck->transform->localScale.x, 1.5f / duck->transform->localScale.y);
		aim->AddComponent<SpriteRenderer>("Direction");
		aim->transform->SetParent(duck->transform);
		aim->transform->localPosition = Vector2(0.0f, 0.0f);
		//- - - - - - - - - - - - 

		//- - - -Quack- - - -
		GameObject* quack = new GameObject("Quack");
		quack->transform->localScale = Vector2(0.6f, 0.6f);
		quack->AddComponent<SpriteRenderer>("quack");
		quack->transform->localPosition = Vector2(0.0f, 0.0f);
		quack->transform->SetPosition(Vector2(SCREEN_WIDTH / 2, SCREEN_HEIGHT / 2));
		quack->transform->localEulerAngles = 45;
		//- - - - - - - - - -

		duck->GetComponent<DuckController>().duckHead = duckHead;
		duck->GetComponent<DuckController>().aim_obj = aim;
		duck->GetComponent<DuckController>().quack = quack;

		//----------------------------

		//----Create Ball----
		GameObject* ball = new GameObject("Ball");
		ball->transform->localScale = Vector2(0.15f, 0.15f);
		ball->transform->SetPosition(Vector2(SCREEN_WIDTH / 4, SCREEN_HEIGHT / 2));
		ball->AddComponent<Rigidbody>().gravityScale = 0.0f;
		ball->GetComponent<Rigidbody>().drag = 0.2f;
		ball->AddComponent<SpriteRenderer>("armadillo");
		ball->AddComponent<CircleCollider>().radius = 35.0f;
		ball->AddComponent<Ball>();
		//-------------------

		duck->GetComponent<DuckController>().ball = &ball->GetComponent<Ball>();

		//----Placing Obstacle----
		GameObject* obstacle = new GameObject("Obstacle");
		obstacle->transform->localScale = Vector2(0.5f, 0.5f);
		obstacle->transform->SetPosition(Vector2(SCREEN_WIDTH / 4, SCREEN_HEIGHT / 3));
		obstacle->transform->localEulerAngles = 30.0f;
		obstacle->AddComponent<SpriteRenderer>("HayBale2");
		obstacle->AddComponent<BoxCollider>().size = Vector2(32, 32);

		obstacle = new GameObject("Obstacle");
		obstacle->transform->localScale = Vector2(0.5f, 0.5f);
		obstacle->transform->SetPosition(Vector2(3.5*SCREEN_WIDTH / 4, SCREEN_HEIGHT / 3 +  200));
		obstacle->transform->localEulerAngles = 0.0f;
		obstacle->AddComponent<SpriteRenderer>("HayBale1");
		obstacle->AddComponent<BoxCollider>().size = Vector2(32, 32);

		obstacle = new GameObject("Obstacle");
		obstacle->transform->localScale = Vector2(0.5f, 0.5f);
		obstacle->transform->SetPosition(Vector2(1.2*SCREEN_WIDTH / 4, SCREEN_HEIGHT / 3 + 350));
		obstacle->transform->localEulerAngles = 0.0f;
		obstacle->AddComponent<SpriteRenderer>("Water");
		obstacle->AddComponent<CircleCollider>().radius = 64;
		//------------------------

		GameObject* blackScreen = new GameObject("Black Screen");
		blackScreen->AddComponent<SpriteRenderer>("whiteBox").color = Color::black();
	
		//----Chicken Enemy-----
		for (size_t i = 0; i < 3; i++)
		{
			GameObject* chicken = new GameObject("Chicken");
			chicken->AddComponent<SpriteRenderer>("chick");
			chicken->AddComponent<ChickenController>().player = duck;
			chicken->AddComponent<Rigidbody>().gravityScale = 0.0f;
			chicken->AddComponent<CircleCollider>();
		}
		
		GameObject* groupChicken = new GameObject("All Chickens");

		//----Camera----
		GameObject* mainCamera = new GameObject("Main Camera");
		mainCamera->AddComponent<Camera>();
		
		//----TextRenderer----
		GameObject* textRender = new GameObject("Text Renderer");
		textRender->AddComponent<TextRenderer>();

		//----9Slice----
		GameObject* nineSlice = new GameObject("9 Slice");
		nineSlice->AddComponent<SpriteRenderer>("Panel_30");
	}
}