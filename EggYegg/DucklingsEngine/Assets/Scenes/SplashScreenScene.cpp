#include "SplashScreenScene.h"
#include "Camera.h"
#include "Scripts/Extension/SplashScreen.h"

namespace DucklingsEngine
{
	void SplashScreenScene::Start()
	{
		// This will be execute when loading the scene.
		GameObject* splashCon = new GameObject("Splash Con");
		splashCon->AddComponent<SplashScreen>();

		//----Camera----
		GameObject* mainCamera = new GameObject("Main Camera");
		mainCamera->AddComponent<Camera>();
		mainCamera->transform->SetPosition(Vector2(0.0f, 0.0f));

		GameObject* background = new GameObject("BG");
		background->transform->SetPosition(Vector2(0.0f, 0.0f));
		background->transform->localScale = Vector2(30.0f, 18.0f);
		background->AddComponent<SpriteRenderer>("whiteBox");

		GameObject* KmuttLogo = new GameObject("KmuttLogo");
		KmuttLogo->transform->SetPosition(Vector2(0.0f, 0.0f));
		KmuttLogo->AddComponent<SpriteRenderer>("KmuttLogo");

		GameObject* TeamLogo = new GameObject("TeamLogo");
		TeamLogo->transform->SetPosition(Vector2(0.0f, 0.0f));
		TeamLogo->AddComponent<SpriteRenderer>("TeamLogo");

		GameObject* dddtlogo = new GameObject("DDDTLogo");
		dddtlogo->transform->SetPosition(Vector2(0.0f, 0.0f));
		dddtlogo->AddComponent<SpriteRenderer>("DDDTLogo");

		splashCon->GetComponent<SplashScreen>().logos[0] = KmuttLogo;
		splashCon->GetComponent<SplashScreen>().logos[1] = dddtlogo;
		splashCon->GetComponent<SplashScreen>().logos[2] = TeamLogo;


	}
}