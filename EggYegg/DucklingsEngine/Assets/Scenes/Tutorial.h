#pragma once

#include "Scene.h"
#include "Engine.h"
#include "FileManager.h"

namespace DucklingsEngine
{
	class Tutorial : public Scene
	{
	public:
		Tutorial() 
		{ 
			name = "Tutorial"; 
			path = "../Assets/Scenes/Tutorial.scene";
		}
		~Tutorial() = default;

		void Start();

		inline void LoadScene() 
		{ 
			Scene::LoadScene(); 
			std::cout << "Loading Scene: " << name << std::endl;
			Start();
			FileManager::LoadYAMLFile(path, name);
		}

		inline bool UnloadScene() override
		{
			std::cout << "Unload Scene: " << name << std::endl;

			Engine::GetInstance().GetManager()->DestoryAllEntity(name);
			Scene::UnloadScene();

			return true;
		}
	};
}

