#include "StoryCutscene.h"
#include "Camera.h"
#include "SpriteRenderer.h"
#include "GameObject.h"
#include "Scripts/CutSceneSlideShow.h"
#include "Engine.h"

namespace DucklingsEngine
{
	void StoryCutscene::Start()
	{
		// This will be execute when loading the scene.
		//----Camera----
		GameObject* mainCamera = new GameObject("Main Camera");
		mainCamera->AddComponent<Camera>();
		mainCamera->transform->SetPosition(Vector2(0.0f, 0.0f));

		//----Create game object to hold cutscene images----
		GameObject* background = new GameObject("Cutscene");
		background->transform->SetPosition(Vector2(0.0f, 0.0f));
		background->transform->localScale = Vector2(2.0f, 2.0f);
		background->AddComponent<SpriteRenderer>("Cutscenes/1-1");

		//foreground 1
		GameObject* foreground1 = new GameObject("foreground 1");
		foreground1->transform->SetPosition(Vector2(-SCREEN_WIDTH, 0.0f));
		foreground1->transform->localScale = Vector2(2.0f, 2.0f);
		foreground1->AddComponent<SpriteRenderer>("Cutscenes/1-2");

		//foreground 2
		GameObject* foreground2 = new GameObject("foreground 2");
		foreground2->transform->SetPosition(Vector2(0.0f, SCREEN_HEIGHT));
		foreground2->transform->localScale = Vector2(2.0f, 2.0f);
		foreground2->AddComponent<SpriteRenderer>("Cutscenes/2-3");

		//controller script
		GameObject* cutscene_script = new GameObject("Cutscene Script");
		cutscene_script->AddComponent<CutSceneSlideShow>();

		CutSceneSlideShow* cutscene_script_controller = &cutscene_script->GetComponent<CutSceneSlideShow>();
		cutscene_script_controller->Assign(background, 0);
		cutscene_script_controller->Assign(foreground1, 1);
		cutscene_script_controller->Assign(foreground2, 2);
	}
}