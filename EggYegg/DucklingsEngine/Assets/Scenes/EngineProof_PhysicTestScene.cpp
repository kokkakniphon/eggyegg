#include "EngineProof_PhysicTestScene.h"

#include "BoxCollider.h"
#include "CircleCollider.h"
#include "Input.h"

#include "Scripts/PhysicSceneController.h"

namespace DucklingsEngine
{
	void EngineProof_PhysicTestScene::Start()
	{
		// This will be execute when loading the scene.

		//PlayerController Object
		GameObject* physicSceneController = new GameObject("PhysicSceneController");
		physicSceneController->AddComponent<PhysicSceneController>();

		//Set up the walls.
		/*GameObject* bottomWall = new GameObject("BottomWall");
		bottomWall->transform->SetPosition(Vector2(SCREEN_WIDTH / 2, SCREEN_HEIGHT));
		bottomWall->AddComponent<BoxCollider>().size = Vector2(SCREEN_WIDTH / 2, 50);*/

		GameObject* leftWall = new GameObject("LeftWall");
		leftWall->transform->SetPosition(Vector2(0, SCREEN_HEIGHT / 4));
		leftWall->AddComponent<BoxCollider>().size = Vector2(20, SCREEN_HEIGHT / 4);

		GameObject* rightWall = new GameObject("RightWall");
		rightWall->transform->SetPosition(Vector2(SCREEN_WIDTH/2, SCREEN_HEIGHT / 4));
		rightWall->AddComponent<BoxCollider>().size = Vector2(20, SCREEN_HEIGHT / 4);

		//Spawn circle
		for (int i = 0; i < physicSceneController->GetComponent<PhysicSceneController>().GetCircleMax() / 2; i++)
		{
			physicSceneController->GetComponent<PhysicSceneController>().SpawnCircle(100.0f);
			physicSceneController->GetComponent<PhysicSceneController>().SpawnCircle(50.0f);
		}
	}
}