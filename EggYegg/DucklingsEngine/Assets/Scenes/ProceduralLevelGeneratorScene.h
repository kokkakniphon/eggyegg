#pragma once

#include "Scene.h"
#include "Engine.h"
#include "FileManager.h"

namespace DucklingsEngine
{
	class ProceduralLevelGeneratorScene : public Scene
	{
	public:
		ProceduralLevelGeneratorScene() 
		{ 
			name = "ProceduralLevelGeneratorScene"; 
			path = "../Assets/Scenes/ProceduralLevelGeneratorScene.scene";
		}
		~ProceduralLevelGeneratorScene() = default;

		void Start();

		inline void LoadScene() 
		{ 
			Scene::LoadScene(); 
			std::cout << "Loading Scene: " << name << std::endl;
			Start();
			FileManager::LoadYAMLFile(path, name);
			Engine::GetInstance().GetManager()->Start();
		}

		inline bool UnloadScene() override
		{
			std::cout << "Unload Scene: " << name << std::endl;

			std::vector<GameObject*> gameObjects = Engine::GetInstance().GetManager()->GetEntitysInScene(name);

			for (auto object : gameObjects)
			{
				object->Destroy();
			}

			Scene::UnloadScene();

			return true;
		}
	};
}

