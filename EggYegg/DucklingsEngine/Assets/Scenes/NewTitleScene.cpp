#include "NewTitleScene.h"
#include "Scripts/Core/CameraController.h"

#include "GameObject.h"
#include "SpriteRenderer.h"
#include "BoxCollider.h"
#include "Scripts/TitleScreenController.h"

namespace DucklingsEngine
{
	void NewTitleScene::Start()
	{
		// This will be execute when loading the scene.
#ifndef _DEBUG
		SDL_ShowCursor(SDL_ENABLE);
#endif
		//----Camera----
		GameObject* mainCamera = new GameObject("Main Camera");
		mainCamera->AddComponent<Camera>();
		mainCamera->AddComponent<CameraController>();

		GameObject* titleScreen = new GameObject("Title Screen");
		titleScreen->AddComponent<TitleScreenController>();
		titleScreen->transform->localPosition = Vector2(SCREEN_WIDTH / 2, SCREEN_HEIGHT / 2);
		titleScreen->transform->localScale = Vector2(SCREEN_WIDTH / 64, SCREEN_HEIGHT / 64);

		TitleScreenController* titleScreenController = &titleScreen->GetComponent<TitleScreenController>();

		Vector2 scaleFactor = Vector2(0.6f, 0.6f);

		GameObject* skyScreen = new GameObject("Sky");
		skyScreen->AddComponent<SpriteRenderer>("MainMenu/sky");
		skyScreen->transform->localScale = scaleFactor;
		titleScreenController->AddScreenLayer(skyScreen, 0);
		skyScreen->transform->SetParent(titleScreen->transform);

		GameObject* cloudScreen = new GameObject("Cloud");
		cloudScreen->AddComponent<SpriteRenderer>("MainMenu/cloud");
		cloudScreen->transform->localScale = scaleFactor;
		titleScreenController->AddScreenLayer(cloudScreen, 0.1f);
		cloudScreen->transform->SetParent(titleScreen->transform);

		GameObject* hill1Screen = new GameObject("Back hill");
		hill1Screen->AddComponent<SpriteRenderer>("MainMenu/grass2");
		hill1Screen->transform->localScale = scaleFactor;
		titleScreenController->AddScreenLayer(hill1Screen, 0.2f);
		hill1Screen->transform->SetParent(titleScreen->transform);

		GameObject* hill2Screen = new GameObject("Front hill");
		hill2Screen->AddComponent<SpriteRenderer>("MainMenu/grass2");
		hill2Screen->transform->localScale = scaleFactor;
		titleScreenController->AddScreenLayer(hill2Screen, 0.25f);
		hill2Screen->transform->SetParent(titleScreen->transform);

		GameObject* groundScreen = new GameObject("Ground");
		groundScreen->AddComponent<SpriteRenderer>("ground");
		groundScreen->transform->localScale = scaleFactor;
		titleScreenController->AddScreenLayer(groundScreen, 0.28f);
		groundScreen->transform->SetParent(titleScreen->transform);

		GameObject* armadilloScreen = new GameObject("Armadillo");
		armadilloScreen->AddComponent<SpriteRenderer>("MainMenu/armadillo");
		armadilloScreen->transform->localScale = scaleFactor;
		titleScreenController->AddScreenLayer(armadilloScreen, 0.43f);
		armadilloScreen->transform->SetParent(titleScreen->transform);

		GameObject* chick1Screen = new GameObject("chick1");
		chick1Screen->AddComponent<SpriteRenderer>("MainMenu/chick1");
		chick1Screen->transform->localScale = scaleFactor;
		titleScreenController->AddScreenLayer(chick1Screen, 0.4f);
		chick1Screen->transform->SetParent(titleScreen->transform);

		GameObject* chick2Screen = new GameObject("chick2");
		chick2Screen->AddComponent<SpriteRenderer>("MainMenu/chick2");
		chick2Screen->transform->localScale = scaleFactor;
		titleScreenController->AddScreenLayer(chick2Screen, 0.43f);
		chick2Screen->transform->SetParent(titleScreen->transform);

		GameObject* chick3Screen = new GameObject("chick3");
		chick3Screen->AddComponent<SpriteRenderer>("MainMenu/chick3");
		chick3Screen->transform->localScale = scaleFactor;
		titleScreenController->AddScreenLayer(chick3Screen, 0.45f);
		chick3Screen->transform->SetParent(titleScreen->transform);

		GameObject* henScreen = new GameObject("Hen");
		henScreen->AddComponent<SpriteRenderer>("MainMenu/hen");
		henScreen->transform->localScale = scaleFactor;
		titleScreenController->AddScreenLayer(henScreen, 0.5f);
		henScreen->transform->SetParent(titleScreen->transform);

		GameObject* duckScreen = new GameObject("Duck");
		duckScreen->AddComponent<SpriteRenderer>("MainMenu/duck");
		duckScreen->transform->localScale = scaleFactor;
		titleScreenController->AddScreenLayer(duckScreen, 0.5f);
		duckScreen->transform->SetParent(titleScreen->transform);

		GameObject* eggScreen = new GameObject("Eggs");
		eggScreen->AddComponent<SpriteRenderer>("MainMenu/eggs");
		eggScreen->transform->localScale = scaleFactor;
		titleScreenController->AddScreenLayer(eggScreen, 0.46f);
		eggScreen->transform->SetParent(titleScreen->transform);

		GameObject* textScreen = new GameObject("egg-yegg");
		textScreen->AddComponent<SpriteRenderer>("MainMenu/title_text");
		textScreen->transform->localScale = scaleFactor;
		titleScreenController->AddScreenLayer(textScreen, 0.0f);
		textScreen->transform->SetParent(titleScreen->transform);

		//----Start Button----
		GameObject* start_button = new GameObject("Start Button");
		start_button->tag = "start";
		start_button->AddComponent<SpriteRenderer>("UIs/start_button");
		start_button->AddComponent<BoxCollider>();

		//Flash
		GameObject* flash = new GameObject("Flash");
		flash->transform->SetParent(titleScreen->transform);
		flash->AddComponent<SpriteRenderer>("whiteBox");
		titleScreen->GetComponent<TitleScreenController>().renderer = &flash->GetComponent<SpriteRenderer>();

		//----Option Button----
		GameObject* option_button = new GameObject("Option Button");
		option_button->tag = "option";
		option_button->AddComponent<SpriteRenderer>("UIs/option_button");
		option_button->AddComponent<BoxCollider>();

		GameObject* menu = new GameObject("Menu");
		menu->transform->SetPosition(Vector2(960, 540));
		start_button->transform->SetParent(menu->transform);
		option_button->transform->SetParent(menu->transform);

		GameObject* setting = new GameObject("Setting");
		setting->transform->SetPosition(Vector2(960, 540));
		
		GameObject* masterSound = new GameObject("Master Sound");
		masterSound->transform->SetParent(setting->transform);
		masterSound->transform->localPosition = Vector2(100.0f, -111.0f);
		masterSound->transform->localScale = Vector2(10.0f, 0.3f);
		masterSound->AddComponent<SpriteRenderer>("UIs/sound_bar");
		GameObject* masterSoundSlider = new GameObject("Master Slider");
		masterSoundSlider->transform->SetParent(masterSound->transform);
		masterSoundSlider->transform->localScale = Vector2(0.04f, 3.0f);
		masterSoundSlider->AddComponent<SpriteRenderer>("UIs/sound_knob");
		masterSoundSlider->AddComponent<BoxCollider>();
		GameObject* masterSoundText = new GameObject("Master Text");
		masterSoundText->transform->SetParent(masterSound->transform);
		masterSoundText->transform->localPosition = Vector2(-478.212f, -5.0f);
		masterSoundText->AddComponent<TextRenderer>("Master Volume:", "GeosansLight.ttf");
		
		GameObject* musicSound = new GameObject("Music Sound");
		musicSound->transform->SetParent(setting->transform);
		musicSound->transform->localPosition = Vector2(100.0f, -32.370f);
		musicSound->transform->localScale = Vector2(10.0f, 0.3f);
		musicSound->AddComponent<SpriteRenderer>("UIs/sound_bar");
		GameObject* musicSoundSlider = new GameObject("Music Slider");
		musicSoundSlider->transform->SetParent(musicSound->transform);
		musicSoundSlider->transform->localScale = Vector2(0.04f, 3.0f);
		musicSoundSlider->AddComponent<SpriteRenderer>("UIs/sound_knob");
		musicSoundSlider->AddComponent<BoxCollider>();
		GameObject* musicSoundText = new GameObject("Music Text");
		musicSoundText->transform->SetParent(musicSound->transform);
		musicSoundText->transform->localPosition = Vector2(-472.045f, -5.0f);
		musicSoundText->AddComponent<TextRenderer>("Music Volume:", "GeosansLight.ttf");
		
		GameObject* sfxSound = new GameObject("Sfx Sound");
		sfxSound->transform->SetParent(setting->transform);
		sfxSound->transform->localPosition = Vector2(100.0f, 47.757f);
		sfxSound->transform->localScale = Vector2(10.0f, 0.3f);
		sfxSound->AddComponent<SpriteRenderer>("UIs/sound_bar");
		GameObject* sfxSoundSlider = new GameObject("Sfx Slider");
		sfxSoundSlider->transform->SetParent(sfxSound->transform);
		sfxSoundSlider->transform->localScale = Vector2(0.04f, 3.0f);
		sfxSoundSlider->AddComponent<SpriteRenderer>("UIs/sound_knob");
		sfxSoundSlider->AddComponent<BoxCollider>();
		GameObject* sfxSoundText = new GameObject("Sfx Text");
		sfxSoundText->transform->SetParent(sfxSound->transform);
		sfxSoundText->transform->localPosition = Vector2(-473.125f, -5.0f);
		sfxSoundText->AddComponent<TextRenderer>("Effect Volume:", "GeosansLight.ttf");

		GameObject* fullscreen = new GameObject("Fullscreen");
		fullscreen->transform->SetParent(setting->transform);
		fullscreen->transform->localPosition = Vector2(10.0f, 120.0f);
		fullscreen->AddComponent<SpriteRenderer>("UIs/check_box");
		GameObject* fullscreenCheck = new GameObject("Fullscreen Check");
		fullscreenCheck->transform->SetParent(fullscreen->transform);
		fullscreenCheck->AddComponent<SpriteRenderer>("UIs/check_mark");
		fullscreenCheck->AddComponent<BoxCollider>();
		GameObject* fullscreenText = new GameObject("Fullscreen Text");
		fullscreenText->transform->SetParent(fullscreen->transform);
		fullscreenText->transform->localPosition = Vector2(-473.125f, -5.0f);
		fullscreenText->AddComponent<TextRenderer>("Fullscreen:", "GeosansLight.ttf");

		GameObject* back = new GameObject("Back");
		back->transform->SetParent(setting->transform);
		back->AddComponent<SpriteRenderer>("UIs/back_button");
		back->AddComponent<BoxCollider>();

		GameObject* quit = new GameObject("Quit");
		quit->transform->SetParent(menu->transform);
		quit->AddComponent<SpriteRenderer>("UIs/quit_button");
		quit->AddComponent<BoxCollider>();

		GameObject* overlay = new GameObject("Overlay");
		overlay->transform->SetParent(setting->transform);
		overlay->AddComponent<SpriteRenderer>("whiteBox");

		titleScreen->GetComponent<TitleScreenController>().menu = menu;
		titleScreen->GetComponent<TitleScreenController>().setting = setting;
		titleScreen->GetComponent<TitleScreenController>().Setup(start_button, option_button, quit, masterSoundSlider, musicSoundSlider, sfxSoundSlider, fullscreenCheck, back);
	}
}