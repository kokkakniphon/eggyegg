#pragma once

#include "SDL.h"
#include "Vector2D.h"

#include "Engine.h"

namespace DucklingsEngine
{
	class Input
	{
	private:
		//
		// Summary:
		//     The current mouse position in pixel coordinates. (Read Only)
		static Vector2Int mousePosition;
		static std::vector<Uint8> m_MouseDatas, m_MouseDownDatas, m_MouseUpDatas;
		static Uint8 m_KeyCurrentState [SDL_NUM_SCANCODES], m_KeyPreviousState[SDL_NUM_SCANCODES];

		static void AddMouseDatas(std::vector<Uint8> *down_up_datas, Uint8 data, bool addNew)
		{
			down_up_datas->emplace_back(data);
			
			if (addNew == true)
			{
				m_MouseDatas.emplace_back(data);
			}
			else
			{
				for (unsigned int i = 0; i < m_MouseDatas.size(); i++)
				{
					if (m_MouseDatas[i] == data)
					{
						m_MouseDatas.erase(m_MouseDatas.begin() + i);
					}
				}
			}
		}

		static bool ContainMouseDatas(std::vector<Uint8> *datas, Uint8 data)
		{
			for (unsigned int i = 0; i < datas->size(); i++)
			{
				if ((*datas)[i] == data)
				{
					return true;
				}
			}
			return false;
		}

	public:

		//
		// Summary:
		//     Return the current mouse position in pixel coordinates. (Read Only)
		static inline Vector2 GetMousePosition() { return Vector2(mousePosition.x, mousePosition.y); }

		//
		// Summary:
		//     Returns whether the given mouse button is held down.
		//
		// Parameters:
		//   button:
		static inline bool GetMouseButton(Uint8 button) { return (ContainMouseDatas(&m_MouseDatas, button)); }

		//
		// Summary:
		//     Returns true during the frame the user pressed the given mouse button.
		//
		// Parameters:
		//   button:
		static inline bool GetMouseButtonDown(Uint8 button) { return (ContainMouseDatas(&m_MouseDownDatas, button)); }

		//
		// Summary:
		//     Returns true during the frame the user releases the given mouse button.
		//
		// Parameters:
		//   button:-
		static inline bool GetMouseButtonUp(Uint8 button) { return (ContainMouseDatas(&m_MouseUpDatas, button)); }

		//
		// Summary:
		//     Returns true while the user holds down the key identified by the key KeyCode
		//     enum parameter.
		//
		// Parameters:
		//   key:
		static inline bool GetKey(SDL_Keycode key) { return (m_KeyCurrentState[key] == 1); }

		//
		// Summary:
		//     Returns true during the frame the user starts pressing down the key identified
		//     by the key KeyCode enum parameter.
		//
		// Parameters:
		//   key:
		static inline bool GetKeyDown(SDL_Scancode key) { return (m_KeyPreviousState[key] == 0 && m_KeyCurrentState[key] == 1); }

		//
		// Summary:
		//     Returns true during the frame the user releases the key identified by the key
		//     KeyCode enum parameter.
		//
		// Parameters:
		//   key:
		static inline bool GetKeyUp(SDL_Scancode key) { return (m_KeyPreviousState[key] == 1 && m_KeyCurrentState[key] == 0); }

		static inline Uint8* GetKeys()
		{
			return m_KeyCurrentState;
		}

		static void Init()
		{
			memset(m_KeyPreviousState, 0, sizeof(Uint8)*SDL_NUM_SCANCODES);
			memcpy(m_KeyCurrentState, SDL_GetKeyboardState(NULL), sizeof(Uint8)*SDL_NUM_SCANCODES);
		}

		static void Listen()
		{
			switch (Engine::event.type)
			{
			case SDL_MOUSEBUTTONDOWN:
				AddMouseDatas(&m_MouseDownDatas, Engine::event.button.button, true);
				break;
			case SDL_MOUSEBUTTONUP:
				AddMouseDatas(&m_MouseUpDatas, Engine::event.button.button, false);
				break;
			case SDL_MOUSEMOTION:
				SDL_GetMouseState(&mousePosition.x, &mousePosition.y);
				break;
			}
		}

		static void Update()
		{
			memcpy(m_KeyPreviousState, m_KeyCurrentState, sizeof(Uint8)*SDL_NUM_SCANCODES);
			memcpy(m_KeyCurrentState, SDL_GetKeyboardState(NULL), sizeof(Uint8)*SDL_NUM_SCANCODES);
		}

		static void Clear()
		{
			m_MouseDownDatas.clear();
			m_MouseUpDatas.clear();
		}
	};
}

