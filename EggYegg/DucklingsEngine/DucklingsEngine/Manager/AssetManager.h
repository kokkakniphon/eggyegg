#pragma once

#include <map>
#include <string>
#include <SDL.h>
#include <SDL_image.h>

#include "Singleton.h"

namespace DucklingsEngine
{

	class AssetManager : public Singleton<AssetManager>
	{
	private:
		std::map<std::string, SDL_Texture*> m_Textures;

		static std::string defaultPath;

	public:
		AssetManager();
		~AssetManager() = default;

		//
		// Summary:
		//     Return SDL_Texture that's been loaded, if it's not loaded try load the texture by id.
		SDL_Texture* GetTexture(std::string id);

		//
		// Summary:
		//     Load a texture by id. Return true if loaded the texture, else false.
		bool LoadTexture(std::string id, std::string path = "");

		void Clean();
	};
}
