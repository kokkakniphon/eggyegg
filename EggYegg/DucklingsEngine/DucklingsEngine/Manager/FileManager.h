#pragma once

#include <string>
#include <fstream>
#include <iostream>
#include <vector>
#include <map>

class GameObject;

#define TAG_MANAGER_PATH = "../ProjectSettings/TagManager.asset"

namespace DucklingsEngine
{
	struct DataBlock
	{
		std::string uniqueID = "";
		std::string local_uniqueID = "";
		std::vector<std::string> data;
	};


	class FileManager
	{
	public:
		static void LoadEditorBuildSettings();
		static void SaveEditorBuildSettings();
		static void LoadYAMLFile(std::string filePath, std::string sceneName = "");
		static void ReadYAMLData(std::vector<DataBlock*>* dataBlocks, DataBlock* dataBlock);
		static void WriteYAMLData(std::string filePath, std::string sceneName = "");
	};


}