#include "SpriteManager.h"

#include "Engine.h"

class SpriteRenderer;

#include "SpriteRenderer.h"

namespace DucklingsEngine
{
	void SpriteManager::Clean()
	{
		m_Sprites.clear();

		Singleton<SpriteManager>::Clean();
	}

	Sprite * SpriteManager::GetSprite(std::string id)
	{
		return (m_Sprites.count(id) > 0) ? m_Sprites[id].get() : nullptr;
	}

	Sprite* SpriteManager::AddSprite(Sprite * sprite, std::string name)
	{
		if (name == "") name = sprite->textureID;
		std::unique_ptr<Sprite> uniquePtr{ sprite };

		uniquePtr->spriteID = name;
		m_Sprites[name] = std::move(uniquePtr);
		return m_Sprites[name].get();
	}


}