#include "RendererManager.h"

namespace DucklingsEngine
{
	bool SortByRenderOrder(Renderer* s1, Renderer* s2)
	{
		return s1->order < s2->order;
	}

	void RendererManager::AddRenderer(Renderer * renderer)
	{
		m_Renderers.push_back(renderer);
	}

	void RendererManager::EraseRenderer(Renderer * renderer)
	{
		for (unsigned int i = 0; i < m_Renderers.size(); i++)
		{
			if (m_Renderers[i]->gameObject->GetUniqueID() == renderer->gameObject->GetUniqueID())
			{
				m_Renderers.erase(m_Renderers.begin() + i);
			}
		}
	}

	void RendererManager::Draw(const Vector2 &center, const Vector2 &rect)
	{
		std::sort(m_Renderers.begin(), m_Renderers.end(), SortByRenderOrder);

		// Render all renderer

		for (size_t i = 0; i < m_Renderers.size(); i++)
		{
			if (m_Renderers[i]->gameObject->IsDestoryed() == true)
			{
				EraseRenderer(m_Renderers[i]);
			}
			else if (m_Renderers[i]->gameObject->GetActiveInHierarchy() == true)
			{
				m_Renderers[i]->Draw(center, rect);
			}
		}
	}
}