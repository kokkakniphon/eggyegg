#include "ColliderManager.h"
#include "SceneWindow.h"
#include "Engine.h"

namespace DucklingsEngine
{
	void ColliderManager::AddCollider(Collider * collider)
	{
		colliders.push_back(collider);
	}

	bool ColliderManager::OverlapPoint(std::vector<Collider*> *colliders, Vector2 point)
	{
		colliders->clear();
		for (auto& collider : this->colliders)
		{
			if (collider->gameObject->GetActiveInHierarchy() == true && collider->OverlapPoint(point) == true)
			{
				colliders->emplace_back(collider);
			}
		}
		return colliders->size() > 0;
	}

	void ColliderManager::EraseCollider(Collider * collider)
	{
		for (unsigned int i = 0; i < colliders.size(); i++)
		{
			if (colliders[i]->gameObject->GetUniqueID() == collider->gameObject->GetUniqueID())
			{
				colliders.erase(colliders.begin() + i);
			}
		}
	}

	bool ColliderManager::OverlapCollider(Collider* source, Collider* target)
	{
		return source->OverlapCollider(target);
	}

	void ColliderManager::StaticResolution(Collider * source, Collider * target)
	{
		if (source->isTrigger == false)
		{
			source->StaticResolution(target);
		}
	}

	void ColliderManager::DynamicResolution(Collider* source, Collider* target)
	{
		if (source->isTrigger == false)
			source->DynamicResolution(target);
	}

	void ColliderManager::FixedUpdate()
	{
		// Segmenting colliders.
#pragma region Segmentation
		//for (std::map<std::string, std::vector<Collider*>>::iterator it = segmentedColliders.begin(); it != segmentedColliders.end(); it++)
		//{
		//	it->second.clear();
		//}
		//segmentedColliders.clear();
		//for (Collider* collider : colliders)
		//{
		//	if (collider->gameObject->enabled == false)
		//		continue;

		//	Vector2 colliderPos = collider->transform->GetPosition();
		//	Vector2Int currentSegment = Vector2Int(std::floor(colliderPos.x / (float)PHYSIC_SUBDIVISION), std::floor(colliderPos.y / (float)PHYSIC_SUBDIVISION));
		//	Vector2 segmentCenter = Vector2(currentSegment.x * PHYSIC_SUBDIVISION + (float)PHYSIC_SUBDIVISION / 2, currentSegment.y * PHYSIC_SUBDIVISION + (float)PHYSIC_SUBDIVISION / 2);
		//	Vector2 colliderClosest = collider->ClosestPoint(segmentCenter);
		//	
		//	Vector2 closestDir = colliderClosest - colliderPos;

		//	// Check opposite
		//	std::vector<Vector2Int> trackAddedPos;
		//	Vector2 checkPos[4] {Vector2(closestDir.x, closestDir.y), Vector2(-closestDir.y, closestDir.x), Vector2(closestDir.y, -closestDir.x), Vector2(-closestDir.x, -closestDir.y) };
		//	for (size_t i = 0; i < 4; i++)
		//	{
		//		checkPos[i] += colliderPos;
		//		Vector2Int closestSegment = Vector2Int(std::floor(checkPos[i].x / (float)PHYSIC_SUBDIVISION), std::floor(checkPos[i].y / (float)PHYSIC_SUBDIVISION));

		//		bool isAddedToSegment = false;
		//		for (size_t j = 0; j < trackAddedPos.size(); j++)
		//		{
		//			if (trackAddedPos[j].x == closestSegment.x, trackAddedPos[j].y == closestSegment.y)
		//			{
		//				isAddedToSegment |= true;
		//			}
		//		}
		//		if (isAddedToSegment == false)
		//		{
		//			trackAddedPos.push_back(closestSegment);
		//			segmentedColliders[closestSegment.ToString()].push_back(collider);
		//		}
		//	}
		//}

		//colliderPairs.clear();

		//for (std::map<std::string, std::vector<Collider*>>::iterator it = segmentedColliders.begin(); it != segmentedColliders.end(); it++)
		//{
		//	for (auto& collider : it->second)
		//	{
		//		if (collider->enabled == false)
		//			continue;
		//		for (auto& target : it->second)
		//		{
		//			if (target->isTrigger == true || target->enabled == false)
		//				continue;

		//			if (collider->gameObject->GetUniqueID() != target->gameObject->GetUniqueID())
		//			{
		//				if (OverlapCollider(collider, target) == true)
		//				{
		//					if (collider->isTrigger == false)
		//					{
		//						collider->collidedColliders.push_back(target);
		//						colliderPairs.push_back(std::make_pair(collider, target));
		//						// Do static resolution
		//						StaticResolution(collider, target);
		//					}
		//					else
		//					{
		//						collider->collidedTriggers.push_back(target);
		//					}
		//				}
		//			}
		//		}

		//	}
		//}
#pragma endregion

#pragma region RawCalculation

		colliderPairs.clear();
		for (auto& collider : colliders)
		{
			if (collider->enabled == false || collider->gameObject->GetActiveInHierarchy() == false)
				continue;
			for (auto& target : colliders)
			{
				if (target->isTrigger == true || target->enabled == false || target->gameObject->GetActiveInHierarchy() == false)
					continue;

				if (collider->gameObject->GetUniqueID() != target->gameObject->GetUniqueID())
				{
					if (OverlapCollider(collider, target) == true)
					{
						if (collider->isTrigger == false)
						{
							collider->collidedColliders.push_back(target);
							colliderPairs.push_back(std::make_pair(collider, target));
							// Do static resolution
							StaticResolution(collider, target);
						}
						else
						{
							collider->collidedTriggers.push_back(target);
						}
					}
				}
			}

		}
#pragma endregion

		for (auto pair : colliderPairs)
		{
			DynamicResolution(pair.first, pair.second);
		}
	}

	void ColliderManager::EndFrameUpdate()
	{
		for (auto& collider : colliders)
		{
			if (collider->gameObject->IsDestoryed() == true)
			{
				EraseCollider(collider);
			}
			else if (collider->gameObject->GetActiveInHierarchy() == true)
			{
				collider->collidedColliders.clear();
				collider->collidedTriggers.clear();
			}
		}
	}

	void ColliderManager::OnDrawGizmos(Vector2 center, Vector2 rect)
	{
		for (auto& collider : colliders)
		{
			if(collider->gameObject->GetActiveInHierarchy() == true)
			{
				collider->OnDrawGizmos(center, rect);
			}
		}
	}
}
