#include "FileManager.h"

#include "GameObject.h"
#include "SpriteRenderer.h"
#include "Rigidbody.h"
#include "Animator.h"
#include "BoxCollider.h"
#include "CircleCollider.h"
#include "Camera.h"
#include "TextRenderer.h"

#include "Engine.h"
#include "Editor.h"
#include "SpriteManager.h"
#include "SceneManager.h"
#include "DefaultScene.h"

using namespace DucklingsEngine;

std::map<std::string, DucklingsEngine::GameObject*> localGameObject;

std::string currentScene = "";

bool FindDataBlockHeader(std::string& data, std::string header, int& readIndex)
{
	std::size_t headerIndex = data.find(header);
	if (headerIndex != std::string::npos)
	{
		readIndex = headerIndex + header.size();
		return true;
	}
	else
	{
		return false;
	}
}

void FileManager::LoadEditorBuildSettings()
{
	std::string EDITOR_BUILD_SETTINGS_PATH = "../ProjectSettings/EditorBuildSettings.asset";

	LoadYAMLFile(EDITOR_BUILD_SETTINGS_PATH);
}

void FileManager::SaveEditorBuildSettings()
{
	std::string EDITOR_BUILD_SETTINGS_PATH = "../ProjectSettings/TagManager.asset";
	
	std::string blockHeader = "--- !u!";

	std::ofstream file(EDITOR_BUILD_SETTINGS_PATH);
	if (!file)
	{
		std::cout << "Error Writing File: " << EDITOR_BUILD_SETTINGS_PATH << std::endl;
		throw std::exception();
	}
	else
	{
		std::vector<Scene*> scenes = SceneManager::GetInstance().GetScenesInBuildSettings();

		file << "%YAML 1.1" << std::endl;
		file << "%TAG !u! tag:ducklingsengine:" << std::endl;

		file << "  m_Scene: ";

		if (scenes.size() <= 0)
		{
			file << "[]" << std::endl;
		}
		else
		{
			file << std::endl;
			for (size_t i = 0; i < scenes.size(); i++)
			{
				file << "  - enabled: " << std::to_string(scenes[i]->isActive) << std::endl;
				file << "    path: " << scenes[i]->path << std::endl;
			}
		}

		file.close();
	}
}

void FileManager::LoadYAMLFile(std::string filePath, std::string sceneName)
{
	if (sceneName != "")
		currentScene = sceneName;

	std::ifstream file(filePath);
	if (!file)
	{
		std::cout << "Error Reading File:" << filePath << std::endl;
		throw std::exception();
	}
	else
	{
		std::vector<DataBlock*> dataBlocks;

		std::string data;
		std::getline(file, data);

		// Setup all data blocks
		while (file.eof() == false)
		{
			std::string uniqueID = "";
			std::string local_uniqueID = "";

			int localReadIndex;

			// Find data block header
			if (FindDataBlockHeader(data, "--- !u!", localReadIndex))
			{
				// Get unique id
				while (data[localReadIndex] != ' ')
				{
					uniqueID += data[localReadIndex];
					localReadIndex++;
				}

				// Get local id
				FindDataBlockHeader(data, "&", localReadIndex);
				while (localReadIndex < data.size() && data[localReadIndex] != ' ' && data[localReadIndex] != '/r')
				{
					local_uniqueID += data[localReadIndex];
					localReadIndex++;
				}

				// Setup data block
				DataBlock* dataBlock = new DataBlock();
				dataBlock->uniqueID = uniqueID;
				dataBlock->local_uniqueID = local_uniqueID;
				while (file.eof() == false)
				{
					std::getline(file, data);

					if (data.find("--- !u!") == std::string::npos)
						dataBlock->data.push_back(data);
					else
						break;
				}

				dataBlocks.push_back(dataBlock);
			}
			else
			{
				std::getline(file, data);
			}
		}


		//Load all GameObject data
		for (DataBlock* data : dataBlocks)
		{
			// Check if it's GameObject data
			if (std::stoi(data->uniqueID) == 1)
			{
				ReadYAMLData(&dataBlocks, data);
			}
		}

		// Load the rest
		for (DataBlock* data : dataBlocks)
		{
			// Check if it isn't GameObject data
			if (std::stoi(data->uniqueID) != 1)
			{
				ReadYAMLData(&dataBlocks, data);
			}
		}

		for (DataBlock* data : dataBlocks)
		{
			delete data;
		}

		dataBlocks.clear();

		file.close();
	}
}

void FileManager::ReadYAMLData(std::vector<DataBlock*>* dataBlocks, DataBlock* dataBlock)
{
	int classID = std::stoi(dataBlock->uniqueID);
	
	if (classID == 1) // GameObject
	{
		GameObject* gameObject = DucklingsEngine::Engine::GetInstance().GetManager()->GetEntityInScene(std::stoi(dataBlock->local_uniqueID), currentScene);
		if (gameObject == nullptr)
		{
			std::cout << "New gameobject in file manager" << std::endl;
			gameObject = new GameObject();
		}

		for (size_t i = 0; i < dataBlock->data.size(); i++)
		{
			int localReadIndex;
			// Check variable name "m_Layer" match
			if (FindDataBlockHeader(dataBlock->data[i], "m_Layer: ", localReadIndex))
			{
				std::string layer = "";

				while (localReadIndex < dataBlock->data[i].size() && dataBlock->data[i][localReadIndex] != '/r')
				{
					layer += dataBlock->data[i][localReadIndex];
					localReadIndex++;
				}

				gameObject->layer = layer;
			}

			// Check variable name "m_Name" match
			if (FindDataBlockHeader(dataBlock->data[i], "m_Name: ", localReadIndex))
			{
				std::string name = "";

				while (localReadIndex < dataBlock->data[i].size() && dataBlock->data[i][localReadIndex] != '/r')
				{
					name += dataBlock->data[i][localReadIndex];
					localReadIndex++;
				}

				gameObject->name = name;
			}

			// Check variable name "m_TagString" match
			if (FindDataBlockHeader(dataBlock->data[i], "m_TagString: ", localReadIndex))
			{
				std::string tag = "";

				while (localReadIndex < dataBlock->data[i].size() && dataBlock->data[i][localReadIndex] != '/r')
				{
					tag += dataBlock->data[i][localReadIndex];
					localReadIndex++;
				}

				gameObject->tag = tag;
			}

			// Check variable name "m_IsActive" match
			if (FindDataBlockHeader(dataBlock->data[i], "m_IsActive: ", localReadIndex))
			{
				std::string isActive = "";

				while (localReadIndex < dataBlock->data[i].size() && dataBlock->data[i][localReadIndex] != '/r')
				{
					isActive += dataBlock->data[i][localReadIndex];
					localReadIndex++;
				}

				gameObject->SetActive(std::stoi(isActive));
			}
		}

		localGameObject[dataBlock->local_uniqueID] = gameObject;
	}
	else if (classID == 4) //Transform
	{
		GameObject* gameObject = nullptr;
		for (size_t i = 0; i < dataBlock->data.size(); i++)
		{
			if (gameObject != nullptr && gameObject->IsDontDestroyOnLoad())
				return;
			int localReadIndex;
			// Check variable name "m_GameObject" match
			if (FindDataBlockHeader(dataBlock->data[i], "m_GameObject: {fileID: ", localReadIndex))
			{
				std::string fileID = "";

				while (localReadIndex < dataBlock->data[i].size() && dataBlock->data[i][localReadIndex] != '}')
				{
					fileID += dataBlock->data[i][localReadIndex];
					localReadIndex++;
				}

				gameObject = localGameObject[fileID];
			}

			// Check variable name "m_LocalEulerAngles" match
			if (FindDataBlockHeader(dataBlock->data[i], "m_LocalEulerAngles: {v: ", localReadIndex))
			{
				std::string value = "";

				while (localReadIndex < dataBlock->data[i].size() && dataBlock->data[i][localReadIndex] != '}')
				{
					value += dataBlock->data[i][localReadIndex];
					localReadIndex++;
				}

				gameObject->transform->localEulerAngles = std::stof(value);
			}

			// Check variable name "m_LocalPosition" match
			if (FindDataBlockHeader(dataBlock->data[i], "m_LocalPosition: {x: ", localReadIndex))
			{
				Vector2 position;
				std::string value = "";

				// Get x position data
				while (localReadIndex < dataBlock->data[i].size() && dataBlock->data[i][localReadIndex] != ',')
				{
					value += dataBlock->data[i][localReadIndex];
					localReadIndex++;
				}
				position.x = std::stof(value);

				value = "";
				localReadIndex += 4; // Skip ", y:"

				// Get y position data
				while (localReadIndex < dataBlock->data[i].size() && dataBlock->data[i][localReadIndex] != '}')
				{
					value += dataBlock->data[i][localReadIndex];
					localReadIndex++;
				}
				position.y = std::stof(value);

				gameObject->transform->localPosition = position;
			}

			// Check variable name "m_LocalScale" match
			if (FindDataBlockHeader(dataBlock->data[i], "m_LocalScale: {x: ", localReadIndex))
			{
				Vector2 localScale;
				std::string value = "";

				// Get x position data
				while (localReadIndex < dataBlock->data[i].size() && dataBlock->data[i][localReadIndex] != ',')
				{
					value += dataBlock->data[i][localReadIndex];
					localReadIndex++;
				}
				localScale.x = std::stof(value);

				value = "";
				localReadIndex += 4; // Skip ", y:"

				// Get y position data
				while (localReadIndex < dataBlock->data[i].size() && dataBlock->data[i][localReadIndex] != '}')
				{
					value += dataBlock->data[i][localReadIndex];
					localReadIndex++;
				}
				localScale.y = std::stof(value);

				gameObject->transform->localScale = localScale;
			}

			// Check variable name "m_Parent" match
			if (FindDataBlockHeader(dataBlock->data[i], "m_Parent: {fileID: ", localReadIndex))
			{
				std::string fileID = "";

				while (localReadIndex < dataBlock->data[i].size() && dataBlock->data[i][localReadIndex] != '}')
				{
					fileID += dataBlock->data[i][localReadIndex];
					localReadIndex++;
				}

				Vector2 localPosition = gameObject->transform->localPosition;

				gameObject->transform->SetParent(localGameObject[fileID]->transform);
				
				gameObject->transform->localPosition = localPosition;
			}
		}
	}
	else if (classID == 20) // Camera
	{
		Camera* camera = nullptr;
		for (size_t i = 0; i < dataBlock->data.size(); i++)
		{
			if (camera != nullptr && camera->gameObject->IsDontDestroyOnLoad())
				return;
			int localReadIndex;
			// Check variable name "m_GameObject" match
			if (FindDataBlockHeader(dataBlock->data[i], "m_GameObject: {fileID: ", localReadIndex))
			{
				std::string fileID = "";

				while (localReadIndex < dataBlock->data[i].size() && dataBlock->data[i][localReadIndex] != '}')
				{
					fileID += dataBlock->data[i][localReadIndex];
					localReadIndex++;
				}

				if (localGameObject[fileID]->HasComponent<Camera>() == false)
					camera = &localGameObject[fileID]->AddComponent<Camera>();
				else
					camera = &localGameObject[fileID]->GetComponent<Camera>();
			}

			// Check variable name "m_Enabled" match
			if (FindDataBlockHeader(dataBlock->data[i], "m_Enabled: ", localReadIndex))
			{
				std::string value = "";

				while (localReadIndex < dataBlock->data[i].size() && dataBlock->data[i][localReadIndex] != '}')
				{
					value += dataBlock->data[i][localReadIndex];
					localReadIndex++;
				}

				camera->enabled = std::stoi(value);
			}

			// Check variable name "m_Rect" match
			if (FindDataBlockHeader(dataBlock->data[i], "m_Rect: {x: ", localReadIndex))
			{
				Vector2 rect;
				std::string value = "";

				// Get x position data
				while (localReadIndex < dataBlock->data[i].size() && dataBlock->data[i][localReadIndex] != ',')
				{
					value += dataBlock->data[i][localReadIndex];
					localReadIndex++;
				}
				rect.x = std::stof(value);

				value = "";
				localReadIndex += 4; // Skip ", y:"

				// Get y position data
				while (localReadIndex < dataBlock->data[i].size() && dataBlock->data[i][localReadIndex] != '}')
				{
					value += dataBlock->data[i][localReadIndex];
					localReadIndex++;
				}
				rect.y = std::stof(value);

				camera->rect = rect;
			}

			// Check variable name "m_Rect" match
			if (FindDataBlockHeader(dataBlock->data[i], "m_BackgroundColor: {r: ", localReadIndex))
			{
				Color backgroundCol;
				std::string value = "";

				// Get r data
				while (localReadIndex < dataBlock->data[i].size() && dataBlock->data[i][localReadIndex] != ',')
				{
					value += dataBlock->data[i][localReadIndex];
					localReadIndex++;
				}
				backgroundCol.r = std::stof(value);

				value = "";
				localReadIndex += 4; // Skip ", g:"

				// Get g data
				while (localReadIndex < dataBlock->data[i].size() && dataBlock->data[i][localReadIndex] != ',')
				{
					value += dataBlock->data[i][localReadIndex];
					localReadIndex++;
				}
				backgroundCol.g = std::stof(value);

				value = "";
				localReadIndex += 4; // Skip ", b:"

				// Get b data
				while (localReadIndex < dataBlock->data[i].size() && dataBlock->data[i][localReadIndex] != ',')
				{
					value += dataBlock->data[i][localReadIndex];
					localReadIndex++;
				}
				backgroundCol.b = std::stof(value);

				value = "";
				localReadIndex += 4; // Skip ", a:"

				// Get a data
				while (localReadIndex < dataBlock->data[i].size() && dataBlock->data[i][localReadIndex] != '}')
				{
					value += dataBlock->data[i][localReadIndex];
					localReadIndex++;
				}
				backgroundCol.a = std::stof(value);

				camera->backgroundColor = backgroundCol;
			}
		}
	}
	else if (classID == 54) // Rigidbody
	{
		Rigidbody* rigidBody = nullptr;
		for (size_t i = 0; i < dataBlock->data.size(); i++)
		{
			if (rigidBody != nullptr && rigidBody->gameObject->IsDontDestroyOnLoad())
				return;
			int localReadIndex;
			// Check variable name "m_GameObject" match
			if (FindDataBlockHeader(dataBlock->data[i], "m_GameObject: {fileID: ", localReadIndex))
			{
				std::string fileID = "";

				while (localReadIndex < dataBlock->data[i].size() && dataBlock->data[i][localReadIndex] != '}')
				{
					fileID += dataBlock->data[i][localReadIndex];
					localReadIndex++;
				}

				if (localGameObject[fileID]->HasComponent<Rigidbody>() == false)
					rigidBody = &localGameObject[fileID]->AddComponent<Rigidbody>();
				else
					rigidBody = &localGameObject[fileID]->GetComponent<Rigidbody>();
			}

			// Check variable name "m_Mass" match
			if (FindDataBlockHeader(dataBlock->data[i], "m_Mass: ", localReadIndex))
			{
				std::string value = "";

				while (localReadIndex < dataBlock->data[i].size() && dataBlock->data[i][localReadIndex] != '/r')
				{
					value += dataBlock->data[i][localReadIndex];
					localReadIndex++;
				}

				rigidBody->mass = std::stof(value);
			}

			// Check variable name "m_Drag" match
			if (FindDataBlockHeader(dataBlock->data[i], "m_Drag: ", localReadIndex))
			{
				std::string value = "";

				while (localReadIndex < dataBlock->data[i].size() && dataBlock->data[i][localReadIndex] != '/r')
				{
					value += dataBlock->data[i][localReadIndex];
					localReadIndex++;
				}

				rigidBody->drag = std::stof(value);
			}

			// Check variable name "m_AngularDrag" match
			if (FindDataBlockHeader(dataBlock->data[i], "m_AngularDrag: ", localReadIndex))
			{
				std::string value = "";

				while (localReadIndex < dataBlock->data[i].size() && dataBlock->data[i][localReadIndex] != '/r')
				{
					value += dataBlock->data[i][localReadIndex];
					localReadIndex++;
				}

				rigidBody->angularDrag = std::stof(value);
			}

			// Check variable name "m_GravityScale" match
			if (FindDataBlockHeader(dataBlock->data[i], "m_GravityScale: ", localReadIndex))
			{
				std::string value = "";

				while (localReadIndex < dataBlock->data[i].size() && dataBlock->data[i][localReadIndex] != '/r')
				{
					value += dataBlock->data[i][localReadIndex];
					localReadIndex++;
				}

				rigidBody->gravityScale = std::stof(value);
			}

			// Check variable name "m_Simulated" 
			if (FindDataBlockHeader(dataBlock->data[i], "m_Simulated: ", localReadIndex))
			{
				std::string value = "";

				while (localReadIndex < dataBlock->data[i].size() && dataBlock->data[i][localReadIndex] != '/r')
				{
					value += dataBlock->data[i][localReadIndex];
					localReadIndex++;
				}

				rigidBody->simulated = std::stoi(value);
			}
		}
	}
	else if (classID == 58) // Circle Collider
	{
		CircleCollider* circleCollider = nullptr;
		for (size_t i = 0; i < dataBlock->data.size(); i++) 
		{
			if (circleCollider != nullptr && circleCollider->gameObject->IsDontDestroyOnLoad())
				return;
			  int localReadIndex;

			  // Check variable name "m_GameObject" match
			  if (FindDataBlockHeader(dataBlock->data[i], "m_GameObject: {fileID: ", localReadIndex))
			  {
				  std::string fileID = "";

				  while (localReadIndex < dataBlock->data[i].size() && dataBlock->data[i][localReadIndex] != '}')
				  {
					  fileID += dataBlock->data[i][localReadIndex];
					  localReadIndex++;
				  }

				  if (localGameObject[fileID]->HasComponent<CircleCollider>() == false)
					  circleCollider = &localGameObject[fileID]->AddComponent<CircleCollider>();
				  else
					  circleCollider = &localGameObject[fileID]->GetComponent<CircleCollider>();
			  }

			  // Check variable name "m_Enabled" 
			  if (FindDataBlockHeader(dataBlock->data[i], "m_Enabled: ", localReadIndex)) 
			  {
				  std::string value = "";

				  while (localReadIndex < dataBlock->data[i].size() && dataBlock->data[i][localReadIndex] != '/r')
				  {
					  value += dataBlock->data[i][localReadIndex];
					  localReadIndex++;
				  }

				  circleCollider->enabled = std::stoi(value);
			  }

			  // Check variable name "m_Density" 
			  if (FindDataBlockHeader(dataBlock->data[i], "m_Density: ", localReadIndex))
			  {
				  std::string value = "";

				  while (localReadIndex < dataBlock->data[i].size() && dataBlock->data[i][localReadIndex] != '/r')
				  {
					  value += dataBlock->data[i][localReadIndex];
					  localReadIndex++;
				  }

				  circleCollider->density = std::stoi(value);
			  }

			  // Check variable name "m_IsTrigger"
			  if (FindDataBlockHeader(dataBlock->data[i], "m_IsTrigger: ", localReadIndex))
			  {
				  std::string value;
				  
				  while (localReadIndex < dataBlock->data[i].size() && dataBlock->data[i][localReadIndex] != '/r')
				  {
					  value += dataBlock->data[i][localReadIndex];
					  localReadIndex++;
				  }

				  circleCollider->isTrigger = std::stoi(value);
			  }

			  // Check variable name "m_Offset"
			  if (FindDataBlockHeader(dataBlock->data[i], "m_Offset: {x: ", localReadIndex))
			  {
				  std::string value;

				  while (localReadIndex < dataBlock->data[i].size() && dataBlock->data[i][localReadIndex] != ',')
				  {
					  value += dataBlock->data[i][localReadIndex];
					  localReadIndex++;
				  }

				  circleCollider->offset.x = std::stoi(value);

				  value = "";
				  localReadIndex += 4; //Skip " y: "
				  while (localReadIndex < dataBlock->data[i].size() && dataBlock->data[i][localReadIndex] != '}')
				  {
					  value += dataBlock->data[i][localReadIndex];
					  localReadIndex++;
				  }

				  circleCollider->offset.y = std::stoi(value);
			  }

			  //Check variable name " m_Radius"
			  if (FindDataBlockHeader(dataBlock->data[i], "m_Radius: ", localReadIndex))
			  {
				  std::string radius;

				  while (localReadIndex < dataBlock->data[i].size() && dataBlock->data[i][localReadIndex] != '/r')
				  {
					  radius += dataBlock->data[i][localReadIndex];
					  localReadIndex++;
				  }

				  circleCollider->radius = std::stoi(radius);
			  }
		 }
		
	}
	else if (classID == 61) { // Box collider
		BoxCollider* boxCollider = nullptr;

		for (size_t i = 0; i < dataBlock->data.size(); i++) 
		{
			if (boxCollider != nullptr && boxCollider->gameObject->IsDontDestroyOnLoad())
				return;
			int localReadIndex;
			// Check variable name "m_GameObject" match
			if (FindDataBlockHeader(dataBlock->data[i], "m_GameObject: {fileID: ", localReadIndex))
			{
				std::string fileID = "";

				while (localReadIndex < dataBlock->data[i].size() && dataBlock->data[i][localReadIndex] != '}')
				{
					fileID += dataBlock->data[i][localReadIndex];
					localReadIndex++;
				}

				if (localGameObject[fileID]->HasComponent<BoxCollider>() == false)
					boxCollider = &localGameObject[fileID]->AddComponent<BoxCollider>();
				else
					boxCollider = &localGameObject[fileID]->GetComponent<BoxCollider>();
			}

			// Check variable name "m_Enabled" 
			if (FindDataBlockHeader(dataBlock->data[i], "m_Enabled: ", localReadIndex))
			{
				std::string value = "";

				while (localReadIndex < dataBlock->data[i].size() && dataBlock->data[i][localReadIndex] != '/r')
				{
					value += dataBlock->data[i][localReadIndex];
					localReadIndex++;
				}

				boxCollider->enabled = std::stoi(value);
			}

			// Check variable name "m_Density" 
			if (FindDataBlockHeader(dataBlock->data[i], "m_Density: ", localReadIndex))
			{
				std::string value = "";

				while (localReadIndex < dataBlock->data[i].size() && dataBlock->data[i][localReadIndex] != '/r')
				{
					value += dataBlock->data[i][localReadIndex];
					localReadIndex++;
				}

				boxCollider->density = std::stoi(value);
			}

			// Check variable name "m_IsTrigger"
			if (FindDataBlockHeader(dataBlock->data[i], "m_IsTrigger: ", localReadIndex))
			{
				std::string value;

				while (localReadIndex < dataBlock->data[i].size() && dataBlock->data[i][localReadIndex] != '/r')
				{
					value += dataBlock->data[i][localReadIndex];
					localReadIndex++;
				}

				boxCollider->isTrigger = std::stoi(value);
			}

			// Check variable name "m_Offset"
			if (FindDataBlockHeader(dataBlock->data[i], "m_Offset: {x: ", localReadIndex))
			{
				std::string value;

				while (localReadIndex < dataBlock->data[i].size() && dataBlock->data[i][localReadIndex] != ',')
				{
					value += dataBlock->data[i][localReadIndex];
					localReadIndex++;
				}

				boxCollider->offset.x = std::stoi(value);

				value = "";
				localReadIndex += 4; //Skip " y: "
				while (localReadIndex < dataBlock->data[i].size() && dataBlock->data[i][localReadIndex] != '}')
				{
					value += dataBlock->data[i][localReadIndex];
					localReadIndex++;
				}

				boxCollider->offset.y = std::stoi(value);
			}

			//Check variable name " m_Size"
			if (FindDataBlockHeader(dataBlock->data[i], "m_Size: {x: ", localReadIndex))
			{
				std::string size;

				while (localReadIndex < dataBlock->data[i].size() && dataBlock->data[i][localReadIndex] != ',')
				{
					size += dataBlock->data[i][localReadIndex];
					localReadIndex++;
				}

				boxCollider->size.x = std::stoi(size);

				size = "";
				localReadIndex += 4; //Skip " y: "
				while (localReadIndex < dataBlock->data[i].size() && dataBlock->data[i][localReadIndex] != '}')
				{
					size += dataBlock->data[i][localReadIndex];
					localReadIndex++;
				}

				boxCollider->size.y = std::stoi(size);
			}
		}

	}
	else if (classID == 95) { // Animator
		Animator* animator = nullptr;
		for (size_t i = 0; i < dataBlock->data.size(); i++) {
			if (animator != nullptr && animator->gameObject->IsDontDestroyOnLoad())
				return;
			int localReadIndex;
			// Check variable name "m_GameObject" match
			if (FindDataBlockHeader(dataBlock->data[i], "m_GameObject: {fileID: ", localReadIndex))
			{
				std::string fileID = "";

				while (localReadIndex < dataBlock->data[i].size() && dataBlock->data[i][localReadIndex] != '}')
				{
					fileID += dataBlock->data[i][localReadIndex];
					localReadIndex++;
				}

				if (localGameObject[fileID]->HasComponent<Animator>() == false)
					animator = &localGameObject[fileID]->AddComponent<Animator>();
				else
					animator = &localGameObject[fileID]->GetComponent<Animator>();
			}

			// Check variable name "m_Enabled" 
			if (FindDataBlockHeader(dataBlock->data[i], "m_Enabled: ", localReadIndex))
			{
				std::string value = "";

				while (localReadIndex < dataBlock->data[i].size() && dataBlock->data[i][localReadIndex] != '/r')
				{
					value += dataBlock->data[i][localReadIndex];
					localReadIndex++;
				}

				animator->enabled = std::stoi(value);
			}
		}
	}
	else if (classID == 102) // TextRenderer
	{
		TextRenderer* renderer = nullptr;
		for (size_t i = 0; i < dataBlock->data.size(); i++)
		{
			if (renderer != nullptr && renderer->gameObject->IsDontDestroyOnLoad())
				return;
			int localReadIndex;
			// Check variable name "m_GameObject" match
			if (FindDataBlockHeader(dataBlock->data[i], "m_GameObject: {fileID: ", localReadIndex))
			{
				std::string fileID = "";

				while (localReadIndex < dataBlock->data[i].size() && dataBlock->data[i][localReadIndex] != '}')
				{
					fileID += dataBlock->data[i][localReadIndex];
					localReadIndex++;
				}

				if (localGameObject[fileID]->HasComponent<TextRenderer>() == false)
					renderer = &localGameObject[fileID]->AddComponent<TextRenderer>();
				else
					renderer = &localGameObject[fileID]->GetComponent<TextRenderer>();
			}

			// Check variable name "m_Enabled" match
			if (FindDataBlockHeader(dataBlock->data[i], "m_Enabled: ", localReadIndex))
			{
				std::string value = "";

				while (localReadIndex < dataBlock->data[i].size() && dataBlock->data[i][localReadIndex] != '}')
				{
					value += dataBlock->data[i][localReadIndex];
					localReadIndex++;
				}

				renderer->enabled = std::stoi(value);
			}

			// Check variable name "m_Text" match
			if (FindDataBlockHeader(dataBlock->data[i], "m_Text: {textData: ", localReadIndex))
			{
				std::string textData = "";

				while (localReadIndex < dataBlock->data[i].size() && dataBlock->data[i][localReadIndex] != '}')
				{
					textData += dataBlock->data[i][localReadIndex];
					localReadIndex++;
				}

				renderer->text = textData;
			}

			// Check variable name "m_Font" match
			if (FindDataBlockHeader(dataBlock->data[i], "m_Font: {fileName: ", localReadIndex))
			{
				std::string fileName = "";

				while (localReadIndex < dataBlock->data[i].size() && dataBlock->data[i][localReadIndex] != '}')
				{
					fileName += dataBlock->data[i][localReadIndex];
					localReadIndex++;
				}

				renderer->font = fileName;
			}

			// Check variable name "m_FontStyle" 
			if (FindDataBlockHeader(dataBlock->data[i], "m_FontStyle: ", localReadIndex))
			{
				std::string value = "";

				while (localReadIndex < dataBlock->data[i].size() && dataBlock->data[i][localReadIndex] != '/r')
				{
					value += dataBlock->data[i][localReadIndex];
					localReadIndex++;
				}

				renderer->fontStyle = std::stoi(value);
			}

			// Check variable name "m_FontSize" 
			if (FindDataBlockHeader(dataBlock->data[i], "m_FontSize: ", localReadIndex))
			{
				std::string value = "";

				while (localReadIndex < dataBlock->data[i].size() && dataBlock->data[i][localReadIndex] != '/r')
				{
					value += dataBlock->data[i][localReadIndex];
					localReadIndex++;
				}

				renderer->fontSize = std::stoi(value);
			}

			// Check variable name "m_Color" match
			if (FindDataBlockHeader(dataBlock->data[i], "m_FontColor: {r: ", localReadIndex))
			{
				Color color;
				std::string value = "";

				// Get r data
				while (localReadIndex < dataBlock->data[i].size() && dataBlock->data[i][localReadIndex] != ',')
				{
					value += dataBlock->data[i][localReadIndex];
					localReadIndex++;
				}
				color.r = std::stof(value);

				value = "";
				localReadIndex += 4; // Skip ", g:"

				// Get g data
				while (localReadIndex < dataBlock->data[i].size() && dataBlock->data[i][localReadIndex] != ',')
				{
					value += dataBlock->data[i][localReadIndex];
					localReadIndex++;
				}
				color.g = std::stof(value);

				value = "";
				localReadIndex += 4; // Skip ", b:"

				// Get b data
				while (localReadIndex < dataBlock->data[i].size() && dataBlock->data[i][localReadIndex] != ',')
				{
					value += dataBlock->data[i][localReadIndex];
					localReadIndex++;
				}
				color.b = std::stof(value);

				value = "";
				localReadIndex += 4; // Skip ", a:"

				// Get a data
				while (localReadIndex < dataBlock->data[i].size() && dataBlock->data[i][localReadIndex] != '}')
				{
					value += dataBlock->data[i][localReadIndex];
					localReadIndex++;
				}
				color.a = std::stof(value);

				renderer->fontColor = color;
			}

			// Check variable name "m_Order" match
			if (FindDataBlockHeader(dataBlock->data[i], "m_Order: ", localReadIndex))
			{
				std::string value = "";

				while (localReadIndex < dataBlock->data[i].size() && dataBlock->data[i][localReadIndex] != '}')
				{
					value += dataBlock->data[i][localReadIndex];
					localReadIndex++;
				}

				renderer->order = std::stoi(value);
			}
		}
		renderer->RefreshText();
	}
	else if (classID == 212) // SpriteRenderer
	{
		SpriteRenderer* renderer = nullptr;
		for (size_t i = 0; i < dataBlock->data.size(); i++)
		{
			if (renderer != nullptr && renderer->gameObject->IsDontDestroyOnLoad())
				return;
			int localReadIndex;
			// Check variable name "m_GameObject" match
			if (FindDataBlockHeader(dataBlock->data[i], "m_GameObject: {fileID: ", localReadIndex))
			{
				std::string fileID = "";

				while (localReadIndex < dataBlock->data[i].size() && dataBlock->data[i][localReadIndex] != '}')
				{
					fileID += dataBlock->data[i][localReadIndex];
					localReadIndex++;
				}

				if (localGameObject[fileID]->HasComponent<SpriteRenderer>() == false)
					renderer = &localGameObject[fileID]->AddComponent<SpriteRenderer>();
				else
					renderer = &localGameObject[fileID]->GetComponent<SpriteRenderer>();
			}

			// Check variable name "m_Enabled" match
			if (FindDataBlockHeader(dataBlock->data[i], "m_Enabled: ", localReadIndex))
			{
				std::string value = "";

				while (localReadIndex < dataBlock->data[i].size() && dataBlock->data[i][localReadIndex] != '}')
				{
					value += dataBlock->data[i][localReadIndex];
					localReadIndex++;
				}

				renderer->enabled = std::stoi(value);
			}

			// Check variable name "m_Sprite" match
			if (FindDataBlockHeader(dataBlock->data[i], "m_Sprite: {fileName: ", localReadIndex))
			{
				std::string fileName = "";

				while (localReadIndex < dataBlock->data[i].size() && dataBlock->data[i][localReadIndex] != '}')
				{
					fileName += dataBlock->data[i][localReadIndex];
					localReadIndex++;
				}

				if ((renderer->sprite == nullptr || renderer->sprite->textureID != fileName) && fileName != "")
				{
					SpriteManager::GetInstance().AddSprite(new Sprite(fileName));
					renderer->sprite = SpriteManager::GetInstance().GetSprite(fileName);
				}
			}

			// Check variable name "m_Color" match
			if (FindDataBlockHeader(dataBlock->data[i], "m_Color: {r: ", localReadIndex))
			{
				Color color;
				std::string value = "";

				// Get r data
				while (localReadIndex < dataBlock->data[i].size() && dataBlock->data[i][localReadIndex] != ',')
				{
					value += dataBlock->data[i][localReadIndex];
					localReadIndex++;
				}
				color.r = std::stof(value);

				value = "";
				localReadIndex += 4; // Skip ", g:"

				// Get g data
				while (localReadIndex < dataBlock->data[i].size() && dataBlock->data[i][localReadIndex] != ',')
				{
					value += dataBlock->data[i][localReadIndex];
					localReadIndex++;
				}
				color.g = std::stof(value);

				value = "";
				localReadIndex += 4; // Skip ", b:"

				// Get b data
				while (localReadIndex < dataBlock->data[i].size() && dataBlock->data[i][localReadIndex] != ',')
				{
					value += dataBlock->data[i][localReadIndex];
					localReadIndex++;
				}
				color.b = std::stof(value);

				value = "";
				localReadIndex += 4; // Skip ", a:"

				// Get a data
				while (localReadIndex < dataBlock->data[i].size() && dataBlock->data[i][localReadIndex] != '}')
				{
					value += dataBlock->data[i][localReadIndex];
					localReadIndex++;
				}
				color.a = std::stof(value);

				renderer->color = color;
			}

			// Check variable name "m_RenderType" match
			if (FindDataBlockHeader(dataBlock->data[i], "m_RenderType: {type: ", localReadIndex))
			{
				std::string renderType = "";

				while (localReadIndex < dataBlock->data[i].size() && dataBlock->data[i][localReadIndex] != '}')
				{
					renderType += dataBlock->data[i][localReadIndex];
					localReadIndex++;
				}

				renderer->renderType = renderType;
			}

			//Check variable name " m_Slice"
			if (FindDataBlockHeader(dataBlock->data[i], "m_Slice: {x: ", localReadIndex))
			{
				std::string slice;

				while (localReadIndex < dataBlock->data[i].size() && dataBlock->data[i][localReadIndex] != ',')
				{
					slice += dataBlock->data[i][localReadIndex];
					localReadIndex++;
				}

				renderer->slice.x = std::stoi(slice);

				slice = "";
				localReadIndex += 4; //Skip " y: "
				while (localReadIndex < dataBlock->data[i].size() && dataBlock->data[i][localReadIndex] != ',')
				{
					slice += dataBlock->data[i][localReadIndex];
					localReadIndex++;
				}

				renderer->slice.y = std::stoi(slice);

				slice = "";
				localReadIndex += 4; //Skip " z: "
				while (localReadIndex < dataBlock->data[i].size() && dataBlock->data[i][localReadIndex] != ',')
				{
					slice += dataBlock->data[i][localReadIndex];
					localReadIndex++;
				}

				renderer->slice.z = std::stoi(slice);

				slice = "";
				localReadIndex += 4; //Skip " w: "
				while (localReadIndex < dataBlock->data[i].size() && dataBlock->data[i][localReadIndex] != '}')
				{
					slice += dataBlock->data[i][localReadIndex];
					localReadIndex++;
				}

				renderer->slice.w = std::stoi(slice);
			}

			// Check variable name "m_Order" match
			if (FindDataBlockHeader(dataBlock->data[i], "m_Order: ", localReadIndex))
			{
				std::string value = "";

				while (localReadIndex < dataBlock->data[i].size() && dataBlock->data[i][localReadIndex] != '}')
				{
					value += dataBlock->data[i][localReadIndex];
					localReadIndex++;
				}

				renderer->order = std::stoi(value);			
			}
		}
	}
	else if (classID == 1045) // EditorBuildSettings
	{
		for (size_t i = 0; i < dataBlock->data.size(); i++)
		{
			// Check variable name match
			if (dataBlock->data[i].find("m_Scenes") != std::string::npos)
			{
				// Check if scene is not empty
				if (dataBlock->data[i].find("[]") == std::string::npos)
				{
					for (size_t j = i + 1; j < dataBlock->data.size(); j++)
					{
						// Check for enabled
						if (dataBlock->data[j].find("enabled") != std::string::npos)
						{
							int localReadIndex = 0;

							// Skip to enabled value
							while (localReadIndex < dataBlock->data[j].size() && dataBlock->data[j][localReadIndex] != ':')
							{
								localReadIndex++;
							}
							localReadIndex++;

							std::string enabled_value = "";

							// Get enabled value
							while (localReadIndex < dataBlock->data[j].size() && dataBlock->data[j][localReadIndex] != '/r')
							{
								enabled_value += dataBlock->data[j][localReadIndex];
								localReadIndex++;
							}

							// If scene is enabled
							if (std::stoi(enabled_value) == 1)
							{
								j++;
								std::string scene_path = "";

								localReadIndex = 0;
								// Skip to path value
								while (localReadIndex < dataBlock->data[j].size() && dataBlock->data[j][localReadIndex] != ':')
								{
									localReadIndex++;
								}
								localReadIndex += 2; // Skip ": " data

								// Get scene path
								while (localReadIndex < dataBlock->data[j].size() && dataBlock->data[j][localReadIndex] != '/r')
								{
									scene_path += dataBlock->data[j][localReadIndex];
									localReadIndex++;
								}

								std::string sceneName = "";

								localReadIndex = dataBlock->data[j].find(".scene");
								localReadIndex--;
								while (localReadIndex >= 0 && dataBlock->data[j][localReadIndex] != '/')
								{
									sceneName += dataBlock->data[j][localReadIndex];
									localReadIndex--;
								}
								std::reverse(sceneName.begin(), sceneName.end());
								currentScene = sceneName;
								// Load scene data
								LoadYAMLFile(scene_path);
							}
						}
					}

					break;
				}
			}
		}
	}
}

void FileManager::WriteYAMLData(std::string filePath, std::string sceneName)
{
	if (sceneName != "")
		currentScene = sceneName;

	std::string blockHeader = "--- !u!";

	std::ofstream file(filePath);
	if (!file)
	{
		std::cout << "Error Writing File: " << filePath << std::endl;
		throw std::exception();
	}
	else
	{
		std::vector<GameObject*> entitys = Engine::GetInstance().GetManager()->GetEntitysInScene(currentScene);

		if (entitys.size() <= 0)
			return;

		int localID = entitys.size();

		localID++;

		file << "%YAML 1.1" << std::endl;
		file << "%TAG !u! tag:ducklingsengine:" << std::endl;

		for (size_t i = 0; i < entitys.size(); i++)
		{
			if (entitys[i]->tag == EDITOR_IGNORE_TAG)
				continue;

			std::vector<Component*> components = entitys[i]->GetComponents();
			
			GameObject* gameObject = entitys[i];
			file << blockHeader << gameObject->GetComponentClassID() << " &" << gameObject->GetLocalUniqueID() << std::endl;
			file << gameObject->GetComponentClassData();

			for (size_t j = 0; j < components.size(); j++)
			{
				std::string classData = components[j]->GetComponentClassData();
				if (classData != "")
				{
					file << blockHeader << components[j]->GetComponentClassID() << " &" << localID << std::endl;
					file << classData;
					localID++;
				}
			}
		}

		file.close();
	}
}
