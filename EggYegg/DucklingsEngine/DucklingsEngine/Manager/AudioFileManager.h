#pragma once

#include <string>
#include <fstream>
#include <iostream>
#include <vector>
#include <map>

#include "../Core/AudioController.h"

namespace KHORA
{
	struct DataBlock
	{
		std::string uniqueID = "";
		std::string local_uniqueID = "";
		std::vector<std::string> data;
	};

	class FileManager
	{
	private:
		static void ReadYAMLData(DataBlock* dataBlock, AudioController* controller);
		static std::string GetYAMLData(std::string findingStr, std::string data);

	public:
		static void LoadYAMLFile(AudioController* controller);
		static void WriteYAMLData(AudioController* controller);
	};
}


