#pragma once

#include <vector>
#include <memory>

#include "GameObject.h"
#include "SceneManager.h"

namespace DucklingsEngine
{

	class EntityManager
	{
	private:
		std::vector<std::shared_ptr<GameObject>> m_Entities;
		std::vector<GameObject*> m_EntitiesInitial;
		std::vector<GameObject*> m_EntitiesUnload;
		void EraseEntity(unsigned int objectID);

	public:
		EntityManager() = default;
		virtual ~EntityManager() = default;

		void Start();
		void Update();
		void FixedUpdate();

		void AddEntity(GameObject* entity);
		GameObject* GetEntity(unsigned int objectID);
		GameObject* GetEntityInScene(unsigned int objectID, std::string scene);
		std::vector<GameObject*> GetEntitys();
		std::vector<GameObject*> GetEntitysInScene(std::string scene);
		void DestoryAllEntity(std::string scene = "");
		void AddToUnloadEntity(unsigned int objectID);
		void UnloadEntity();
	};
}