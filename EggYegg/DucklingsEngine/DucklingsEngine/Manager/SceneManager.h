#pragma once

#include <vector>

#include "Singleton.h"

#include "Scene.h"

namespace DucklingsEngine
{
	class SceneManager : public Singleton<SceneManager>
	{
	private:
		//
		// Summary:
		//     Number of Scenes in Build Settings.
		static int sceneCountInBuildSettings;
		//
		// Summary:
		//     The total number of currently loaded Scenes.
		static int sceneCount;

		std::vector<std::shared_ptr<Scene>> buildSettingScenes;
		std::vector<Scene*> loadedScenes;

	public:
		SceneManager() = default;
		~SceneManager();

		inline void AddSceneToBuildSettings(std::shared_ptr<Scene> scene)
		{
			buildSettingScenes.push_back(scene);
		}

		void Init();

		//
		// Summary:
		//     Return the number of Scenes in Build Settings.
		inline int GetSceneCountInBuildSettings() { return sceneCountInBuildSettings; }
		//
		// Summary:
		//     The total number of currently loaded Scenes.
		inline int GetSceneCount() { return sceneCount; }

		//
		// Summary:
		//     Gets the first active Scene.
		//
		// Returns:
		//     The active Scene.
		Scene* GetFirstActiveScene();
		//
		// Summary:
		//     Gets the latest active Scene.
		//
		// Returns:
		//     The active Scene.
		Scene* GetLatestActiveScene();

		//
		// Summary:
		//     Return scenes in build settings.
		//
		// Returns:
		//     The active Scene.
		std::vector<Scene*> GetScenesInBuildSettings();
		//
		// Summary:
		//     Return loaded scene.
		//
		// Returns:
		//     The active Scene.
		std::vector<Scene*> GetLoadedScenes();

		//
		// Summary:
		//     Close all active scene and load the Scene by its name or index in Build Settings.
		//
		// Parameters:
		//   sceneName:
		//     Name or path of the Scene to load.
		//
		//   sceneBuildIndex:
		//     Index of the Scene in the Build Settings to load.
		//
		void OpenScene(std::string sceneName);

		//
		// Summary:
		//     Close all active scene and load the Scene by its name or index in Build Settings.
		//
		// Parameters:
		//   sceneName:
		//     Name or path of the Scene to load.
		//
		//   sceneBuildIndex:
		//     Index of the Scene in the Build Settings to load.
		//
		void OpenScene(int sceneBuildIndex);

		//
		// Summary:
		//     Loads the Scene by its name or index in Build Settings.
		//
		// Parameters:
		//   sceneName:
		//     Name or path of the Scene to load.
		//
		//   sceneBuildIndex:
		//     Index of the Scene in the Build Settings to load.
		//
		void LoadScene(std::string sceneName);
		//
		// Summary:
		//     Loads the Scene by its name or index in Build Settings.
		//
		// Parameters:
		//   sceneName:
		//     Name or path of the Scene to load.
		//
		//   sceneBuildIndex:
		//     Index of the Scene in the Build Settings to load.
		void LoadScene(int sceneBuildIndex);
		//
		// Summary:
		//     Destroys all GameObjects associated with the given Scene and removes the Scene
		//     from the loadedScenes.
		//
		// Parameters:
		//   sceneBuildIndex:
		//     Index of the Scene in the Build Settings to unload.
		//
		//   sceneName:
		//     Name or path of the Scene to unload.
		//
		//   scene:
		//     Scene to unload.
		//
		// Returns:
		//     Returns true if the Scene is unloaded.
		bool UnloadScene(std::string sceneName);
		//
		// Summary:
		//     Destroys all GameObjects associated with the given Scene and removes the Scene
		//     from the loadedScenes.
		//
		// Parameters:
		//   sceneBuildIndex:
		//     Index of the Scene in the Build Settings to unload.
		//
		//   sceneName:
		//     Name or path of the Scene to unload.
		//
		//   scene:
		//     Scene to unload.
		//
		// Returns:
		//     Returns true if the Scene is unloaded.
		bool UnloadScene(int sceneBuildIndex);

		//
		// Summary:
		//     Destroys all GameObjects associated with the loaded Scene and removes the Scene
		//     from the loadedScenes.
		//
		void UnloadAllScene();

		void Clean();
	};
}

