#pragma once

#include <iostream>
#include <vector>
#include <map>

#include "Singleton.h"
#include "Collider.h"

constexpr int PHYSIC_SUBDIVISION = 140;

namespace DucklingsEngine
{
	class ColliderManager : public Singleton<ColliderManager>
	{
	private:
		std::vector<Collider*> colliders;

		std::map<std::string, std::vector<Collider*>> segmentedColliders;

		std::vector<std::pair<Collider*, Collider*>> colliderPairs;

		bool OverlapCollider(Collider* source, Collider* target);
		void StaticResolution(Collider* source, Collider* target);
		void DynamicResolution(Collider* source, Collider* target);
		void EraseCollider(Collider* collider);

	public:
		ColliderManager() = default;

		void AddCollider(Collider* collider);
		bool OverlapPoint(std::vector<Collider*> *colliders, Vector2 point);

		void ClearCollider() { colliders.clear(); }

		void FixedUpdate();
		void EndFrameUpdate();

		void OnDrawGizmos(Vector2 center, Vector2 rect);
	};
}
