#include "SceneManager.h"
#include "Editor.h"
#include "ToolBar.h"
#include "Object.h"
#include "Engine.h"

namespace DucklingsEngine
{
	SceneManager::~SceneManager()
	{
		buildSettingScenes.clear();
		loadedScenes.clear();
	}

	void SceneManager::Init()
	{
		// Set build index
		for (unsigned int i = 0; i < buildSettingScenes.size(); i++)
		{
			std::cout << "Build Index: " << i << " " << buildSettingScenes[i]->name << std::endl;
			buildSettingScenes[i]->SetBuildIndex(i);
		}

		if(buildSettingScenes.size() > 0)
			LoadScene(0);
		else
		{
			std::cout << "There is no scene in the build settings to be loaded" << std::endl;
		}
	}

	Scene* SceneManager::GetFirstActiveScene()
	{
		if (loadedScenes.size() > 0)
			return loadedScenes[0];

		return nullptr;
	}

	Scene* SceneManager::GetLatestActiveScene()
	{
		if (loadedScenes.size() > 0)
			return loadedScenes[loadedScenes.size() - 1];

		return nullptr;
	}

	std::vector<Scene*> SceneManager::GetScenesInBuildSettings()
	{
		std::vector<Scene*> scenes;

		for (size_t i = 0; i < buildSettingScenes.size(); i++)
		{
			scenes.push_back(buildSettingScenes[i].get());
		}

		return scenes;
	}

	std::vector<Scene*> SceneManager::GetLoadedScenes()
	{
		return loadedScenes;
	}

	void SceneManager::OpenScene(std::string sceneName)
	{
		SceneManager::GetInstance().UnloadAllScene();
		Engine::GetInstance().GetManager()->UnloadEntity();
#ifdef _DEBUG
		ToolBar::GetInstance().ClearTools();
#endif
		Object::ResetLocalUniqueIDSeed(sceneName);
		SceneManager::GetInstance().LoadScene(sceneName);
		Engine::GetInstance().GetManager()->Start();
	}

	void SceneManager::OpenScene(int sceneBuildIndex)
	{
		SceneManager::GetInstance().UnloadAllScene();
		Engine::GetInstance().GetManager()->UnloadEntity();
#ifdef _DEBUG
		ToolBar::GetInstance().ClearTools();
#endif
		Object::ResetLocalUniqueIDSeed(buildSettingScenes[sceneBuildIndex]->name);
		SceneManager::GetInstance().LoadScene(buildSettingScenes[sceneBuildIndex]->name);
		Engine::GetInstance().GetManager()->Start();
	}

	void SceneManager::LoadScene(std::string sceneName)
	{
		for (auto& scene : buildSettingScenes)
		{
			if (scene->name == sceneName && scene->isLoaded == false)
			{
				loadedScenes.push_back(scene.get());
				scene->LoadScene();
				break;
			}
		}

		return;
	}

	void SceneManager::LoadScene(int sceneBuildIndex)
	{
		for (auto scene : buildSettingScenes)
		{
			if (scene->GetBuildIndex() == sceneBuildIndex && scene->isLoaded == false)
			{
				loadedScenes.push_back(scene.get());
				scene->LoadScene();
				break;
			}
		}

		return;
	}

	bool SceneManager::UnloadScene(std::string sceneName)
	{
		for (unsigned int i = 0; i < loadedScenes.size(); i++)
		{
			if (loadedScenes[i]->name == sceneName && loadedScenes[i]->isLoaded == true)
			{
				if (loadedScenes[i]->UnloadScene() == true)
				{
					loadedScenes.erase(loadedScenes.begin() + i);
					return true;
				}
				else
				{
					return false;
				}
			}
		}

		return false;
	}

	bool SceneManager::UnloadScene(int sceneBuildIndex)
	{
		for (unsigned i = 0; i < loadedScenes.size(); i++)
		{
			if (loadedScenes[i]->GetBuildIndex() == sceneBuildIndex && loadedScenes[i]->isLoaded == true)
			{
				if (loadedScenes[i]->UnloadScene() == true)
				{
					loadedScenes.erase(loadedScenes.begin() + i);
					return true;
				}
				else
				{
					return false;
				}
			}
		}

		return false;
	}

	void SceneManager::UnloadAllScene()
	{
		for (unsigned int i = 0; i < loadedScenes.size(); i++)
		{
			loadedScenes[i]->UnloadScene();
		}

		loadedScenes.clear();
	}

	void SceneManager::Clean()
	{
		// Unload All Scene
		for (unsigned i = 0; i < loadedScenes.size(); i++)
		{
			if (loadedScenes[i]->isLoaded == true)
			{
				if (loadedScenes[i]->UnloadScene() == true)
				{
					loadedScenes.erase(loadedScenes.begin() + i);
				}
			}
		}
		
		Singleton<SceneManager>::Clean();
	}



}