#include "AudioFileManager.h"

using namespace KHORA;

std::string m_AssetControllerPath = "../Assets/Controller/";

void FileManager::LoadYAMLFile(AudioController* controller)
{

	std::ifstream file(m_AssetControllerPath + controller->name + ControllerTypeName);
	if (!file)
	{
		std::cout << "Error Reading File:" << m_AssetControllerPath + controller->name + ControllerTypeName << std::endl;
		throw std::exception();
	}
	else
	{
		std::vector<DataBlock*> dataBlocks;

		std::string data;
		std::getline(file, data);

		// Setup all data blocks
		while (file.eof() == false)
		{
			std::string uniqueID = "";
			std::string local_uniqueID = "";

			// Find data block header
			std::size_t headerIndex = data.find("--- !u!");
			if (headerIndex != std::string::npos)
			{
				// Get unique id
				int localReadIndex = headerIndex + 7;
				while (data[localReadIndex] != ' ')
				{
					uniqueID += data[localReadIndex];
					localReadIndex++;
				}

				// Get local id
				std::size_t local_ID_Index = data.find("&");
				localReadIndex = local_ID_Index + 1;
				while (localReadIndex < data.size() && data[localReadIndex] != ' ' && data[localReadIndex] != '/r')
				{
					local_uniqueID += data[localReadIndex];
					localReadIndex++;
				}

				// Setup data block
				DataBlock* dataBlock = new DataBlock();
				dataBlock->uniqueID = uniqueID;
				dataBlock->local_uniqueID = local_uniqueID;
				while (file.eof() == false)
				{
					std::getline(file, data);

					if (data.find("--- !u!") == std::string::npos)
						dataBlock->data.push_back(data);
					else
						break;
				}

				dataBlocks.push_back(dataBlock);
			}
			else
			{
				std::getline(file, data);
			}
		}

		//Load all AudioSource data
		for (DataBlock* dataBlock : dataBlocks)
		{
			// Check if it's AudioSource data
			if (std::stoi(dataBlock->uniqueID) == AUDIO_SOURCE_CLASS_ID)
			{
				ReadYAMLData(dataBlock, controller);
			}
		}

		//Load the rest
		//Load all parameter data
		for (DataBlock* dataBlock : dataBlocks)
		{
			// Check if it's parameter data
			if (std::stoi(dataBlock->uniqueID) == PARAMETER_TRANSITION_CLASS_ID || std::stoi(dataBlock->uniqueID) == PARAMETER_GLOBAL_CLASS_ID)
			{
				ReadYAMLData(dataBlock, controller);
			}
		}


		for (DataBlock* dataBlock : dataBlocks)
		{
			delete dataBlock;
		}

		dataBlocks.clear();

		file.close();
	}
}

void FileManager::ReadYAMLData(DataBlock* dataBlock, AudioController* controller)
{
	int classID = std::stoi(dataBlock->uniqueID);

	if (classID == AUDIO_SOURCE_CLASS_ID) // AudioSource
	{	
		
		//CREATE AudioSource
		std::shared_ptr<AudioSource> audioNode = std::make_shared<AudioSource>();

		//Loop through audioSource's data
		for (std::size_t i = 0; i < dataBlock->data.size(); i++) {

			//=====================================================================================================

			//Finding name
			std::string name = GetYAMLData("name: ", dataBlock->data[i]);
			if (name != "") {
				//ASSIGN name VALUE 
				audioNode->name = name;
			}

			//=====================================================================================================

			//Finding volume
			std::string volume = GetYAMLData("volume: ", dataBlock->data[i]);
			if (volume != "") {
				//ASSIGN volume VALUE 
				audioNode->volume = std::stof(volume);
			}

			//=====================================================================================================

			//Finding isLoopable
			std::string isLoopable = GetYAMLData("isLoopable: ", dataBlock->data[i]);
			if (isLoopable != "") {
				//ASSIGN isLoopable VALUE 
				audioNode->isLoopable = isLoopable == "true";
			}

			//=====================================================================================================

			//Finding filePath
			std::string filePath = GetYAMLData("filePath: ", dataBlock->data[i]);
			if (filePath != "") {
				//ASSIGN filePath VALUE 
				audioNode->filePath = filePath.c_str();
			}

			//=====================================================================================================

		}

		//ADD THIS audioSource TO THE ENGINE
		if (audioNode->name != "Entry")
			controller->CreateAudioNode(audioNode);

	}

	else if (classID == PARAMETER_TRANSITION_CLASS_ID || classID == PARAMETER_GLOBAL_CLASS_ID) // Parameter
	{

		//CREATE parameter
		Parameter parameter("");

		//CREATE variable for transition parameter
		std::string startingNode = "";
		std::string destinationNode = "";

		//Loop through parameter's data
		for (std::size_t i = 0; i < dataBlock->data.size(); i++) {

			//=====================================================================================================

			//Finding name
			std::string name = GetYAMLData("name: ", dataBlock->data[i]);
			if (name != "") {
				//ASSIGN name VALUE 
				parameter.name = name;
			}

			//=====================================================================================================

			//Finding type
			std::string type = GetYAMLData("type: ", dataBlock->data[i]);
			if (type != "") {
				//ASSIGN type VALUE 
				if (type == "Float") {
					parameter.type = ParameterType::Float;
				}
				else if (type == "Integer") {
					parameter.type = ParameterType::Integer;
				}
				else if (type == "Boolean") {
					parameter.type = ParameterType::Boolean;
				}
			}

			//=====================================================================================================

			//Finding isGreaterThan
			std::string isGreaterThan = GetYAMLData("isGreaterThan: ", dataBlock->data[i]);
			if (isGreaterThan != "") {
				//ASSIGN isGreaterThan VALUE 
				parameter.isGreaterThan = isGreaterThan == "true";
			}

			//=====================================================================================================

			//Finding checkValueBool
			std::string checkValueBool = GetYAMLData("checkValueBool: ", dataBlock->data[i]);
			if (checkValueBool != "") {
				//ASSIGN checkValueBool VALUE 
				parameter.checkValueBool = checkValueBool == "true";
			}

			//=====================================================================================================

			//Finding checkValueInt
			std::string checkValueInt = GetYAMLData("checkValueInt: ", dataBlock->data[i]);
			if (checkValueInt != "") {
				//ASSIGN checkValueInt VALUE 
				parameter.checkValueInt = std::stoi(checkValueInt);
			}

			//=====================================================================================================

			//Finding checkValueFloat
			std::string checkValueFloat = GetYAMLData("checkValueFloat: ", dataBlock->data[i]);
			if (checkValueFloat != "") {
				//ASSIGN checkValueFloat VALUE 
				parameter.checkValueFloat = std::stof(checkValueFloat);
			}

			//=====================================================================================================

			//Finding currentValueBool
			std::string currentValueBool = GetYAMLData("currentValueBool: ", dataBlock->data[i]);
			if (currentValueBool != "") {
				//ASSIGN currentValueBool VALUE 
				parameter.currentValueBool = currentValueBool == "true";
			}

			//=====================================================================================================

			//Finding currentValueInt
			std::string currentValueInt = GetYAMLData("currentValueInt: ", dataBlock->data[i]);
			if (currentValueInt != "") {
				//ASSIGN currentValueInt VALUE 
				parameter.currentValueInt = std::stoi(currentValueInt);
			}

			//=====================================================================================================

			//Finding currentValueFloat
			std::string currentValueFloat = GetYAMLData("currentValueFloat: ", dataBlock->data[i]);
			if (currentValueFloat != "") {
				//ASSIGN currentValueInt VALUE 
				parameter.currentValueFloat = std::stof(currentValueFloat);
			}

			//=====================================================================================================


			if (classID == PARAMETER_TRANSITION_CLASS_ID) {

				//Finding starting audio source
				std::string starting = GetYAMLData("startingNode: ", dataBlock->data[i]);
				if (starting != "") {
					//ASSIGN startingNode value
					startingNode = starting;
				}

				//=====================================================================================================

				//Finding destination's audio source's name
				std::string destination = GetYAMLData("destinationNode: ", dataBlock->data[i]);
				if (destination != "") {
					//ASSIGN destinationNode value
					destinationNode = destination;
				}

				//=====================================================================================================
			}

		}

		//Checking what type of parameter
		if (classID == PARAMETER_GLOBAL_CLASS_ID) { //global parameter
			controller->AddGlobalParameter(parameter);
		}
		else { //transition parameter
			controller->AddTransitionParameter(parameter, startingNode, destinationNode);
		}

	}
}

void FileManager::WriteYAMLData(AudioController* controller)
{
	std::string blockHeader = "--- !u!";

	std::ofstream file(m_AssetControllerPath + controller->name + ControllerTypeName);
	if (!file)
	{
		std::cout << "Error Writing File: " << m_AssetControllerPath + controller->name + ControllerTypeName << std::endl;
		throw std::exception();
	}
	else
	{	

		if (controller->audioNodes.size() <= 0){
			return;
		}

		int localID = controller->audioNodes.size() + 1;
		int uniqueID = 1;

		file << "%YAML 1.1" << std::endl;
		file << "%TAG !u! tag:SoundController:" << std::endl;

		//Loop through all local audio sources
		for (std::vector<std::pair<std::string, std::shared_ptr<AudioSource>>>::iterator audioSource = controller->audioNodes.begin(); audioSource != controller->audioNodes.end(); audioSource++) {

			//Get audio source's data
			file << blockHeader << audioSource->second->GetClassID() << " &" << uniqueID << std::endl;
			uniqueID++;
			file << audioSource->second->GetAudioSourceData();

			for (std::map<std::string, std::vector<Parameter>>::iterator destination = audioSource->second->destinations.begin(); destination != audioSource->second->destinations.end(); destination++) {

				for (int i = 0; i < destination->second.size(); i++) {
					
					//Get transition Parameter's data
					file << blockHeader << destination->second[i].GetTransitionID() << " &" << localID << std::endl;
					file << audioSource->second->GetTransitionData(destination->first);
					file << destination->second[i].GetParameterData();
					localID++;

				}
			}
		}

		//Loop through all global parameters
		for (int i = 0; i < controller->globalParameters.size(); i++) {
			
			//Get global Parameter's data
			file << blockHeader << controller->globalParameters[i].GetGlobalID() << " &" << localID << std::endl;
			file << controller->globalParameters[i].GetParameterData();
			localID++;
		}
		

		file.close();
	}
}

std::string KHORA::FileManager::GetYAMLData(std::string findingStr , std::string data)
{
	std::size_t findingPos = data.find(findingStr);
	if (findingPos != std::string::npos) {
		std::string val = "";
		int localReadIndex = findingPos + findingStr.size();

		while (localReadIndex < data.size() && data[localReadIndex] != '/r') {
			val += data[localReadIndex];
			localReadIndex++;
		}

		return val;
	}
	return "";
}
