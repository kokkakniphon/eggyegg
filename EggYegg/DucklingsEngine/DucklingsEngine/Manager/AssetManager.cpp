#include <iostream>
#include "Engine.h"

namespace DucklingsEngine
{
	std::string AssetManager::defaultPath = "../Assets/Textures/";

	AssetManager::AssetManager()
	{
		if (IMG_Init(IMG_INIT_PNG | IMG_INIT_JPG) != 0)
			std::cerr << IMG_GetError() << std::endl;
	}

	void AssetManager::Clean()
	{
		for (auto it = m_Textures.begin(); it != m_Textures.end(); it++)
		{
			SDL_DestroyTexture(it->second);
		}

		m_Textures.clear();

		std::cout << "assets cleared!" << std::endl;

		Singleton<AssetManager>::Clean();
	}

	SDL_Texture* AssetManager::GetTexture(std::string id)
	{
		if (m_Textures.count(id) > 0)
		{
			return m_Textures[id];
		}
		else
		{
			return (LoadTexture(id) == true) ? GetTexture(id) : nullptr;
		}
	}

	bool AssetManager::LoadTexture(std::string id, std::string path)
	{
		if (m_Textures.count(id) <= 0)
		{
			if (path == "")
			{
				std::string tempPath = "";
				for (int i = 0; i < 2; i++)
				{
					if (i == 0)
						tempPath = defaultPath + id + ((id.find(".png") != std::string::npos) ? "" : ".png");
					else
						tempPath = defaultPath + id + ((id.find(".jpg") != std::string::npos) ? "" : ".jpg");

					SDL_Texture* texture = IMG_LoadTexture(Engine::GetInstance().GetRenderer(), tempPath.c_str());
					if (texture)
					{
						m_Textures[id] = texture;
						std::cout << "texture: [" << tempPath << "] loaded!" << std::endl;
						return true;
					}
					else if (i == 1)
					{
						std::cerr << SDL_GetError() << std::endl;
						return false;
					}
				}
			}
			else
			{
				std::string tempPath = "";
				for (int i = 0; i < 2; i++)
				{
					if (i == 0)
						tempPath = path + id + ((id.find(".png") != std::string::npos) ? "" : ".png");
					else
						tempPath = path + id + ((id.find(".jpg") != std::string::npos) ? "" : ".jpg");

					SDL_Texture* texture = IMG_LoadTexture(Engine::GetInstance().GetRenderer(), tempPath.c_str());
					if (texture)
					{
						m_Textures[id] = texture;
						std::cout << "texture: [" << tempPath << "] loaded!" << std::endl;
						return true;
					}
					else if (i == 1)
					{
						std::cerr << SDL_GetError() << std::endl;
						return false;
					}
				}
			}
			
		}
	}
}


