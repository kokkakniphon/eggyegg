#include "EntityManager.h"
#include <algorithm>
#include <iostream>

namespace DucklingsEngine
{

	void EntityManager::Start()
	{
		for (unsigned int i = 0; i < m_EntitiesInitial.size(); i++)
		{
			m_EntitiesInitial[i]->Start();
		}

		m_EntitiesInitial.clear();
	}

	void EntityManager::Update()
	{
		for (unsigned int i = 0; i < m_Entities.size(); i++)
		{
			if (m_Entities[i]->GetActiveInHierarchy() == true)
			{
				m_Entities[i]->Update();
			}
		}
	}

	void EntityManager::FixedUpdate()
	{
		for (unsigned int i = 0; i < m_Entities.size(); i++)
		{
			if (m_Entities[i]->GetActiveInHierarchy() == true)
			{
				m_Entities[i]->FixedUpdate();
			}
		}
	}

	void EntityManager::AddEntity(GameObject * entity)
	{
		std::shared_ptr<GameObject> sharedPtr{ entity };

		Scene* scene = SceneManager::GetInstance().GetLatestActiveScene();
		if (scene != nullptr)
		{
			scene->GetRootsGameObject()->push_back(sharedPtr);
		}

		m_Entities.push_back(sharedPtr);

		m_EntitiesInitial.push_back(sharedPtr.get());
	}

	GameObject * EntityManager::GetEntity(unsigned int objectID)
	{
		for (unsigned int j = 0; j < m_Entities.size(); j++)
		{
			if (m_Entities[j]->GetUniqueID() == objectID)
			{
				return m_Entities[j].get();
			}
		}
		return nullptr;
	}

	GameObject * EntityManager::GetEntityInScene(unsigned int objectID, std::string scene)
	{
		for (unsigned int j = 0; j < m_Entities.size(); j++)
		{
			if (m_Entities[j]->scene == scene && m_Entities[j]->GetLocalUniqueID() == objectID)
			{
				return m_Entities[j].get();
			}
		}
		return nullptr;
	}

	std::vector<GameObject*> EntityManager::GetEntitys()
	{
		std::vector<GameObject*> allEntity;
		for (size_t i = 0; i < m_Entities.size(); i++)
		{
			allEntity.push_back(m_Entities[i].get());
		}
		return allEntity;
	}

	std::vector<GameObject*> EntityManager::GetEntitysInScene(std::string scene)
	{
		std::vector<GameObject*> allEntity;
		for (size_t i = 0; i < m_Entities.size(); i++)
		{
			if (m_Entities[i]->scene == scene)
				allEntity.push_back(m_Entities[i].get());
		}
		return allEntity;
	}

	void EntityManager::DestoryAllEntity(std::string scene)
	{
		for (size_t i = 0; i < m_Entities.size(); i++)
		{
			if (m_Entities[i]->IsDontDestroyOnLoad() == true)
			{
				std::cout << "Dont destroy: " << m_Entities[i]->name << std::endl;
				continue;
			}
			if (scene == "" || m_Entities[i]->scene == scene)
				m_Entities[i]->Destroy();
		}
	}

	void EntityManager::EraseEntity(unsigned int objectID)
	{
		for (unsigned int j = 0; j < m_Entities.size(); j++)
		{
			if (m_Entities[j]->GetUniqueID() == objectID)
			{
				m_Entities.erase(m_Entities.begin() + j);
			}
		}
	}

	void EntityManager::AddToUnloadEntity(unsigned int objectID)
	{
		for (unsigned int i = 0; i < m_Entities.size(); i++)
		{
			if (m_Entities[i]->GetUniqueID() == objectID)
			{
				m_EntitiesUnload.push_back(m_Entities[i].get());
			}
		}
	}

	void EntityManager::UnloadEntity()
	{
		for (unsigned int i = 0; i < m_Entities.size(); i++)
		{
			if (m_Entities[i]->IsDestoryed() == true)
			{
				m_EntitiesUnload.push_back(m_Entities[i].get());
			}
		}

		for (unsigned int i = 0; i < m_EntitiesUnload.size(); i++)
		{
			EraseEntity(m_EntitiesUnload[i]->GetUniqueID());
		}
		m_EntitiesUnload.clear();
	}
}