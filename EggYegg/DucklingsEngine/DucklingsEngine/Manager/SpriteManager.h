#pragma once

#include <map>
#include <string>
#include <vector>
#include <algorithm>

#include "Singleton.h"

#include "Sprite.h"

namespace DucklingsEngine
{
	class SpriteManager : public Singleton<SpriteManager>
	{
	private:
		std::map<std::string, std::unique_ptr<Sprite>> m_Sprites;

	public:
		SpriteManager() = default;
		~SpriteManager() = default;

		void Clean();

		Sprite* GetSprite(std::string id);
		Sprite* AddSprite(Sprite * sprite, std::string name = "");
	};

}

