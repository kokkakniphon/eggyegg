#pragma once

#include <vector>
#include <algorithm>

#include "Singleton.h"

#include "Renderer.h";

namespace DucklingsEngine
{
	class RendererManager : public Singleton<RendererManager>
	{
	private:
		static RendererManager* s_Instance;

		std::vector<Renderer*> m_Renderers;

	public:

		void AddRenderer(Renderer* renderer);
		void EraseRenderer(Renderer* renderer);

		void Draw(const Vector2 &center, const Vector2 &rect);

	};
}


