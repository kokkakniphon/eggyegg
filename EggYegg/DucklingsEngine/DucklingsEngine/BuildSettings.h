#pragma once

#include "SceneManager.h"

//
// Summary:
//     Include a scene here.
//     Example: #include "{scene_name}.h";

#include "Scenes/SplashScreenScene.h"
#include "Scenes/Tutorial.h"
#include "Scenes/FirstLevel.h"
#include "Scenes/NewTitleScene.h"
#include "Scenes/StoryCutscene.h"
#include "Scenes/EndingScene.h"

namespace DucklingsEngine
{
	class Settings
	{
	public:
		static void SetupBuildSettings()
		{
			//
			// Summary:
			//     Add scene to build settings list of scene.
			//     Example: SceneManager::GetInstance().AddSceneToBuildSettings(std::make_shared<{scene_name}>());
			SceneManager::GetInstance().AddSceneToBuildSettings(std::make_shared <SplashScreenScene>());
			SceneManager::GetInstance().AddSceneToBuildSettings(std::make_shared <NewTitleScene>());
			SceneManager::GetInstance().AddSceneToBuildSettings(std::make_shared <StoryCutscene>());
			SceneManager::GetInstance().AddSceneToBuildSettings(std::make_shared <Tutorial>());
			SceneManager::GetInstance().AddSceneToBuildSettings(std::make_shared <FirstLevel>());
			SceneManager::GetInstance().AddSceneToBuildSettings(std::make_shared <EndingScene>());

			SceneManager::GetInstance().Init();
		}
	};
}

