#include "GameWindow.h"

#include "imgui_internal.h"

#include "Engine.h"
#include "Editor.h"

#include "Camera.h"
#include "Input.h"

namespace DucklingsEngine
{
	void GameWindow::SetupSceneRect()
	{
		ImGuiWindow *window = ImGui::GetCurrentWindow();

		m_SceneView.x = window->ContentRegionRect.ToVec4().x;
		m_SceneView.y = window->ContentRegionRect.ToVec4().y;
		m_SceneView.w = window->Size.x - window->WindowPadding.x * 2;
		m_SceneView.h = window->Size.y - window->WindowPadding.y * 4;

		Camera::mainCamera->SetViewOffset(Vector2(m_SceneView.x, m_SceneView.y));

		m_WindowActive = true;
	}
	void GameWindow::ShowWindow()
	{
		if (ImGui::Begin("Game"))
		{
			SetupSceneRect();
		}
		ImGui::End();
	}
	void GameWindow::Render()
	{
		if (m_WindowActive == false)
			return;

		SDL_RenderSetViewport(Engine::GetInstance().GetRenderer(), &m_SceneView);

		Camera::mainCamera->Render(Vector2(m_SceneView.w, m_SceneView.h));
		ColliderManager::GetInstance().EndFrameUpdate();

		m_WindowActive = false;
	}
}