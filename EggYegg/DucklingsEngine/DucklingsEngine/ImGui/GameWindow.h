#pragma once

#include "Singleton.h"

#include "Vector2D.h"

#include "imgui.h"
#include "imgui_sdl.h"

#include "SDL.h"
#include "SDL_image.h"


namespace DucklingsEngine
{
	class GameWindow : public Singleton<GameWindow>
	{
	private:

		bool m_WindowActive;

		SDL_Rect m_SceneView;

		float m_ZoomSensitivity = 25.0f;

		float m_LastWheelValue;
		Vector2 m_InitialViewCenter;
		Vector2 m_MouseDownPos;

		void SetupSceneRect();

	public:

		void ShowWindow();
		void Render();

	};
}

