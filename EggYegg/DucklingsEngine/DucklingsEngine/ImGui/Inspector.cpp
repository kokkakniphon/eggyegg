#include "Inspector.h"

#include "Hierarchy.h"
#include "History.h"

#include "Component.h"

#include "GameObject.h"
#include "Rigidbody.h"
#include "Animator.h"
#include "CircleCollider.h"
#include "BoxCollider.h"
#include "SpriteRenderer.h"
#include "Camera.h"
#include "TextRenderer.h"

#include "Editor.h"

namespace DucklingsEngine
{

	static std::string s_FontPath = "../Assets/Fonts/";

	void Inspector::LoadFont()
	{
		//Load all file in GUI folder.
		std::string pattern(s_FontPath);
		pattern.append("\\*");
		WIN32_FIND_DATA data;
		HANDLE hFind;
		if ((hFind = FindFirstFile(pattern.c_str(), &data)) != INVALID_HANDLE_VALUE) {
			do {
				std::string fileName = data.cFileName;
				if (fileName == "." || fileName == "..") continue;
				m_FontList.push_back(data.cFileName);
			} while (FindNextFile(hFind, &data) != 0);
			FindClose(hFind);
		}
	}

	int GetCurrentComboIndexNode(std::string name, std::vector<std::string> &selectedList)
	{
		for (size_t i = 0; i < selectedList.size(); i++)
		{
			if (selectedList[i] == name)
			{
				return i;
			}
		}
		return 0;
	}

	void ShowComponent(Component* component, std::vector<std::string> &fontList, std::vector<std::string> &renderTypeList)
	{
		if (component->GetComponentClassID() == 4) // Transform
		{
			Transform* transform = dynamic_cast<Transform*>(component);

			if (ImGui::CollapsingHeader("Transform", ImGuiTreeNodeFlags_DefaultOpen))
			{
				ImGui::Columns(2, NULL, false);
				ImGui::SetColumnWidth(0, ImGui::GetWindowWidth() * 0.35f);

				ImGui::Text("Position"); ImGui::Spacing();
				ImGui::Text("Rotation"); ImGui::Spacing();
				ImGui::Text("Scale"); ImGui::Spacing();

				ImGui::NextColumn();

				//Position value (X, Y)
				ImGui::Text("X"); ImGui::SameLine();
				ImGui::SetNextItemWidth(ImGui::GetContentRegionAvail().x * 0.5f);
				float posx_Before = transform->localPosition.x;
				if (ImGui::DragFloat("##transformPosX", &transform->localPosition.x, 1))
				{
					History::GetInstance().AddHistory<float>(&transform->localPosition.x, posx_Before, transform->localPosition.x);
				}ImGui::SameLine();

				ImGui::Text("Y"); ImGui::SameLine();
				ImGui::SetNextItemWidth(ImGui::GetContentRegionAvail().x);
				float posy_Before = transform->localPosition.y;
				if (ImGui::DragFloat("##transformPosY", &transform->localPosition.y, 1))
				{
					History::GetInstance().AddHistory<float>(&transform->localPosition.y, posy_Before, transform->localPosition.y);
				}

				//Rotation value
				ImGui::SetNextItemWidth(ImGui::GetContentRegionAvail().x);
				float angle_Before = transform->localPosition.x;
				if (ImGui::DragFloat("##transformEulerAngle", &transform->localEulerAngles, 1))
				{
					History::GetInstance().AddHistory<float>(&transform->localEulerAngles, angle_Before, transform->localEulerAngles);
				}

				//Scale value (X, Y)
				ImGui::Text("X"); ImGui::SameLine();
				ImGui::SetNextItemWidth(ImGui::GetContentRegionAvail().x * 0.5f);
				float scalex_Before = transform->localScale.x;
				if (ImGui::DragFloat("##transformScaleX", &transform->localScale.x, 0.02f))
				{
					History::GetInstance().AddHistory<float>(&transform->localScale.x, scalex_Before, transform->localScale.x);
				}ImGui::SameLine();

				ImGui::Text("Y"); ImGui::SameLine();
				ImGui::SetNextItemWidth(ImGui::GetContentRegionAvail().x);
				float scaley_Before = transform->localScale.y;
				if (ImGui::DragFloat("##transformScaleY", &transform->localScale.y, 0.02f))
				{
					History::GetInstance().AddHistory<float>(&transform->localScale.y, scaley_Before, transform->localScale.y);
				}

				ImGui::Columns(1);
			}
		}
		else if (component->GetComponentClassID() == 20) // Camera
		{

			Camera* camera = dynamic_cast<Camera*>(component);

			if (ImGui::CollapsingHeader("Camera", ImGuiTreeNodeFlags_DefaultOpen))
			{
				ImGui::Columns(2, NULL, false);
				ImGui::SetColumnWidth(0, ImGui::GetWindowWidth() * 0.35f);

				ImGui::Text("Active"); ImGui::Spacing();
				ImGui::Text("Rect"); ImGui::Spacing();
				ImGui::Text("Background Color"); ImGui::Spacing();

				ImGui::NextColumn();

				bool enable_Before = camera->enabled;
				if (ImGui::Checkbox("##cameraActive", &camera->enabled))
				{
					History::GetInstance().AddHistory<bool>(&camera->enabled, enable_Before, camera->enabled);
				}

				//Rect value (W, H)
				ImGui::Text("W"); ImGui::SameLine();
				ImGui::SetNextItemWidth(ImGui::GetContentRegionAvail().x * 0.5f);
				float rectx_Before = camera->rect.x;
				if (ImGui::DragFloat("##cameraRectX", &camera->rect.x, 1))
				{
					History::GetInstance().AddHistory<float>(&camera->rect.x, rectx_Before, camera->rect.x);
				}ImGui::SameLine();

				ImGui::Text("H"); ImGui::SameLine();
				ImGui::SetNextItemWidth(ImGui::GetContentRegionAvail().x);
				float recty_Before = camera->rect.y;
				if (ImGui::DragFloat("##cameraRectY", &camera->rect.y, 1))
				{
					History::GetInstance().AddHistory<float>(&camera->rect.y, recty_Before, camera->rect.y);
				}

				////Background color value (Color picker)
				ImVec4 color(camera->backgroundColor.r * 255, camera->backgroundColor.g * 255, camera->backgroundColor.b * 255, camera->backgroundColor.a * 255);
				if (ImGui::ColorEdit4("##cameraBackgroundColor", (float*)&color, ImGuiColorEditFlags_NoInputs | ImGuiColorEditFlags_NoLabel))
				{
					Color color_Before = camera->backgroundColor;
					camera->backgroundColor = Color(color.x / 255.0f, color.y / 255.0f, color.z / 255.0f, color.w / 255.0f);
					History::GetInstance().AddHistory<Color>(&camera->backgroundColor, color_Before, camera->backgroundColor);
				}

				ImGui::Columns(1);
			}
		}
		else if (component->GetComponentClassID() == 54) // Rigidbody
		{
			if (ImGui::CollapsingHeader("Rigidbody", ImGuiTreeNodeFlags_DefaultOpen))
			{

				Rigidbody* rigidbody = dynamic_cast<Rigidbody*>(component);

				ImGui::Columns(2, NULL, false);

				ImGui::Text("Active"); ImGui::Spacing();
				ImGui::Text("Mass: "); ImGui::Spacing();
				ImGui::Text("Drag: "); ImGui::Spacing();
				ImGui::Text("Angular: "); ImGui::Spacing();
				ImGui::Text("Gravity Scale: "); ImGui::Spacing();
				ImGui::Text("Simulated: "); ImGui::Spacing();

				ImGui::NextColumn();
				ImGui::PushItemWidth(-1);

				bool enable_Before = rigidbody->enabled;
				if (ImGui::Checkbox("##rigidBodyActive", &rigidbody->enabled))
				{
					History::GetInstance().AddHistory<bool>(&rigidbody->enabled, enable_Before, rigidbody->enabled);
				}

				float mass_Before = rigidbody->mass;
				if (ImGui::DragFloat("##rigidBodyMass", &rigidbody->mass, 0.05f, 0, +FLT_MAX, "%.3f"))
				{
					History::GetInstance().AddHistory<float>(&rigidbody->mass, mass_Before, rigidbody->mass);
				}

				float drag_Before = rigidbody->drag;
				if (ImGui::DragFloat("##rigidBodyDrag", &rigidbody->drag, 0.05f, 0, +FLT_MAX, "%.3f"))
				{
					History::GetInstance().AddHistory<float>(&rigidbody->drag, drag_Before, rigidbody->drag);
				}

				float angular_Before = rigidbody->angularDrag;
				if (ImGui::DragFloat("##rigidBodyAngular", &rigidbody->angularDrag, 0.05f, -FLT_MAX, +FLT_MAX, "%.3f"))
				{
					History::GetInstance().AddHistory<float>(&rigidbody->drag, angular_Before, rigidbody->drag);
				}

				float gravityScale_Before = rigidbody->gravityScale;
				if (ImGui::DragFloat("##rigidBodyGravityScale", &rigidbody->gravityScale, 0.05f, -FLT_MAX, +FLT_MAX, "%.3f"))
				{
					History::GetInstance().AddHistory<float>(&rigidbody->gravityScale, gravityScale_Before, rigidbody->gravityScale);
				}

				bool simulated_Before = rigidbody->simulated;
				if (ImGui::Checkbox("##rigidBodySimulated", &rigidbody->simulated))
				{
					History::GetInstance().AddHistory<bool>(&rigidbody->simulated, simulated_Before, rigidbody->simulated);
				}

				ImGui::PopItemWidth();
				ImGui::Columns(1);
			}
		}
		else if (component->GetComponentClassID() == 58) // Circle Collider
		{
			if (ImGui::CollapsingHeader("Circle Collider", ImGuiTreeNodeFlags_DefaultOpen))
			{

				CircleCollider* circleCollider = dynamic_cast<CircleCollider*>(component);

				ImGui::Columns(2, NULL, false);

				ImGui::Text("Active"); ImGui::Spacing();
				ImGui::Text("Is Trigger: "); ImGui::Spacing();
				ImGui::Text("Offset: "); ImGui::Spacing();
				ImGui::Text("Radius: "); ImGui::Spacing();

				ImGui::NextColumn();
				ImGui::PushItemWidth(-1);

				bool enable_Before = circleCollider->enabled;
				if (ImGui::Checkbox("##circleActive", &circleCollider->enabled))
				{
					History::GetInstance().AddHistory<bool>(&circleCollider->enabled, enable_Before, circleCollider->enabled);
				}

				bool isTrigger_Before = circleCollider->isTrigger;
				if (ImGui::Checkbox("##circleIsTrigger", &circleCollider->isTrigger))
				{
					History::GetInstance().AddHistory<bool>(&circleCollider->isTrigger, isTrigger_Before, circleCollider->isTrigger);
				}

				//Offset (X, Y)
				ImGui::Text("X"); ImGui::SameLine();
				ImGui::SetNextItemWidth(ImGui::GetContentRegionAvail().x * 0.5f);
				float offsetx_Before = circleCollider->offset.x;
				if (ImGui::DragFloat("##circleOffsetX", &circleCollider->offset.x, 1))
				{
					History::GetInstance().AddHistory<float>(&circleCollider->offset.x, offsetx_Before, circleCollider->offset.x);
				}ImGui::SameLine();

				ImGui::Text("Y"); ImGui::SameLine();
				ImGui::SetNextItemWidth(ImGui::GetContentRegionAvail().x);
				float offsety_Before = circleCollider->offset.y;
				if (ImGui::DragFloat("##circleOffsetY", &circleCollider->offset.y, 1))
				{
					History::GetInstance().AddHistory<float>(&circleCollider->offset.y, offsetx_Before, circleCollider->offset.y);
				}

				float radius_Before = circleCollider->radius;
				if (ImGui::DragFloat("##circleRadius", &circleCollider->radius, 0.05f, 0, +FLT_MAX, "%.3f"))
				{
					History::GetInstance().AddHistory<float>(&circleCollider->radius, radius_Before, circleCollider->radius);
				}

				ImGui::PopItemWidth();
				ImGui::Columns(1);
			}
		}
		else if (component->GetComponentClassID() == 61) // Box Collider
		{
			if (ImGui::CollapsingHeader("Box Collider", ImGuiTreeNodeFlags_DefaultOpen))
			{
				BoxCollider* boxCollider = dynamic_cast<BoxCollider*>(component);

				ImGui::Columns(2, NULL, false);

				ImGui::Text("Active"); ImGui::Spacing();
				ImGui::Text("Is Trigger: "); ImGui::Spacing();
				ImGui::Text("Offset: "); ImGui::Spacing();
				ImGui::Text("Size: "); ImGui::Spacing();

				ImGui::NextColumn();
				ImGui::PushItemWidth(-1);

				bool enable_Before = boxCollider->enabled;
				if (ImGui::Checkbox("##boxActive", &boxCollider->enabled))
				{
					History::GetInstance().AddHistory<bool>(&boxCollider->enabled, enable_Before, boxCollider->enabled);
				}

				bool isTrigger_Before = boxCollider->isTrigger;
				if (ImGui::Checkbox("##boxIsTrigger", &boxCollider->isTrigger))
				{
					History::GetInstance().AddHistory<bool>(&boxCollider->isTrigger, isTrigger_Before, boxCollider->isTrigger);
				}

				//Offset (X, Y)
				ImGui::Text("X"); ImGui::SameLine();
				ImGui::SetNextItemWidth(ImGui::GetContentRegionAvail().x * 0.5f);
				float offsetx_Before = boxCollider->offset.x;
				if (ImGui::DragFloat("##boxOffsetX", &boxCollider->offset.x, 1))
				{
					History::GetInstance().AddHistory<float>(&boxCollider->offset.x, offsetx_Before, boxCollider->offset.x);
				}ImGui::SameLine();

				ImGui::Text("Y"); ImGui::SameLine();
				ImGui::SetNextItemWidth(ImGui::GetContentRegionAvail().x);
				float offsety_Before = boxCollider->offset.y;
				if (ImGui::DragFloat("##boxOffsetY", &boxCollider->offset.y, 1))
				{
					History::GetInstance().AddHistory<float>(&boxCollider->offset.y, offsety_Before, boxCollider->offset.y);
				}

				//Size (X, Y)
				ImGui::Text("X"); ImGui::SameLine();
				ImGui::SetNextItemWidth(ImGui::GetContentRegionAvail().x * 0.5f);
				float sizex_Before = boxCollider->size.x;
				if (ImGui::DragFloat("##boxSizeX", &boxCollider->size.x, 1))
				{
					History::GetInstance().AddHistory<float>(&boxCollider->size.x, sizex_Before, boxCollider->size.x);
				}ImGui::SameLine();

				ImGui::Text("Y"); ImGui::SameLine();
				ImGui::SetNextItemWidth(ImGui::GetContentRegionAvail().x);
				float sizey_Before = boxCollider->size.y;
				if (ImGui::DragFloat("##boxSizeY", &boxCollider->size.y, 1))
				{
					History::GetInstance().AddHistory<float>(&boxCollider->size.y, sizey_Before, boxCollider->size.y);
				}

				ImGui::PopItemWidth();
				ImGui::Columns(1);
			}
		}
		else if (component->GetComponentClassID() == 95) // Animator
		{
			if (ImGui::CollapsingHeader("Animator", ImGuiTreeNodeFlags_DefaultOpen))
			{
				Animator* animator = dynamic_cast<Animator*>(component);

				ImGui::Columns(2, NULL, false);

				ImGui::Text("Active"); ImGui::Spacing();

				ImGui::NextColumn();
				ImGui::PushItemWidth(-1);

				bool enable_Before = animator->enabled;
				if (ImGui::Checkbox("##animatorActive", &animator->enabled))
				{
					History::GetInstance().AddHistory<bool>(&animator->enabled, enable_Before, animator->enabled);
				}

				ImGui::PopItemWidth();
				ImGui::Columns(1);
			}
		}
		else if (component->GetComponentClassID() == 102) // Text Renderer
		{
			if (ImGui::CollapsingHeader("Text Renderer", ImGuiTreeNodeFlags_DefaultOpen))
			{

				TextRenderer* renderer = dynamic_cast<TextRenderer*>(component);

				ImGui::Text("Text"); ImGui::Spacing();
				std::string text_Before = renderer->text;
				if (ImGui::InputTextMultiline("##textRendererText", &renderer->text, ImVec2(-FLT_MIN, ImGui::GetTextLineHeight() * 10)))
				{
					renderer->RefreshText();
					History::GetInstance().AddHistory<std::string>(&renderer->text, text_Before, renderer->text);
				}
				ImGui::Spacing();

				ImGui::Columns(2, NULL, false);

				ImGui::Text("Active"); ImGui::Spacing();
				ImGui::Text("Font"); ImGui::Spacing();
				ImGui::Text("FontStyle"); ImGui::Spacing(); ImGui::Spacing();
				ImGui::Text("FontSize"); ImGui::Spacing();
				ImGui::Text("Color"); ImGui::Spacing();
				ImGui::Text("Order"); ImGui::Spacing();

				ImGui::NextColumn();
				ImGui::PushItemWidth(-1);

				bool enable_Before = renderer->enabled;
				if (ImGui::Checkbox("##textRendererActive", &renderer->enabled))
				{
					History::GetInstance().AddHistory<bool>(&renderer->enabled, enable_Before, renderer->enabled);
				}

				// Font
				int item_current_idx = GetCurrentComboIndexNode(renderer->font, fontList);
				const char* combo_label = fontList[item_current_idx].c_str();
				std::string font_Before = fontList[item_current_idx];
				if (ImGui::BeginCombo("##textRendererFont", combo_label))
				{
					for (int n = 0; n < fontList.size(); n++)
					{
						const bool is_selected = (item_current_idx == n);
						if (ImGui::Selectable(fontList[n].c_str(), is_selected))
						{
							renderer->font = fontList[n];
							item_current_idx = n;
							renderer->RefreshText();
							History::GetInstance().AddHistory<std::string>(&renderer->font, font_Before, renderer->font);
						}

						if (is_selected)
							ImGui::SetItemDefaultFocus();
					}
					ImGui::EndCombo();
				}

				// Font style
				const char* textStyle[4] = {"B", "I", "U", "S"};
				for (int i = 0; i < 4; i++)
				{
					if (i > 0)
						ImGui::SameLine();
					ImGui::PushID(i);
					bool isSelected = false;
					if (i == 0) isSelected = renderer->IsBold();
					if (i == 1) isSelected = renderer->IsItalic();
					if (i == 2) isSelected = renderer->IsUnderline();
					if (i == 3) isSelected = renderer->IsStrikethrough();
					if (isSelected)
					{
						ImGui::PushStyleColor(ImGuiCol_Button, (ImVec4)ImColor::HSV(i / 7.0f, 0.6f, 0.6f));
						ImGui::PushStyleColor(ImGuiCol_ButtonHovered, (ImVec4)ImColor::HSV(i / 7.0f, 0.7f, 0.7f));
						ImGui::PushStyleColor(ImGuiCol_ButtonActive, (ImVec4)ImColor::HSV(i / 7.0f, 0.8f, 0.8f));
					}
					else
					{
						ImGui::PushStyleColor(ImGuiCol_Button, (ImVec4)ImColor::HSV(0.6f, 0.6f, 0.6f));
						ImGui::PushStyleColor(ImGuiCol_ButtonHovered, (ImVec4)ImColor::HSV(0.6f, 0.6f, 0.6f));
						ImGui::PushStyleColor(ImGuiCol_ButtonActive, (ImVec4)ImColor::HSV(0.6f, 0.6f, 0.6f));
					}
					if (ImGui::Button(textStyle[i], ImVec2(40, 20)))
					{
						if (i == 0) renderer->SetBold(!isSelected);
						if (i == 1) renderer->SetItalic(!isSelected);
						if (i == 2) renderer->SetUnderline(!isSelected);
						if (i == 3) renderer->SetStrikethrough(!isSelected);
					}
					ImGui::PopStyleColor(3);
					ImGui::PopID();
				}

				float size_Before = renderer->fontSize;
				if (ImGui::DragFloat("##textRendererFontSize", &renderer->fontSize, 1))
				{
					renderer->RefreshText();
					History::GetInstance().AddHistory<float>(&renderer->fontSize, size_Before, renderer->fontSize);
				}

				// Color value (Color picker)
				ImVec4 color(renderer->fontColor.r, renderer->fontColor.g, renderer->fontColor.b, renderer->fontColor.a);
				Color color_Before = renderer->fontColor;
				if (ImGui::ColorEdit4("##textRendererColor", (float*)&color, ImGuiColorEditFlags_NoInputs | ImGuiColorEditFlags_NoLabel))
				{
					renderer->fontColor = Color(color.x, color.y, color.z, color.w);
					renderer->RefreshText();
					History::GetInstance().AddHistory<Color>(&renderer->fontColor, color_Before, renderer->fontColor);
				}

				// Render order
				int order_Before = renderer->order;
				if (ImGui::InputInt("##textRendererOrder", &renderer->order))
				{
					History::GetInstance().AddHistory<int>(&renderer->order, order_Before, renderer->order);
				}

				ImGui::PopItemWidth();
				ImGui::Columns(1);
			}
		}
		else if (component->GetComponentClassID() == 212) // Sprite Renderer
		{
			if (ImGui::CollapsingHeader("Sprite Renderer", ImGuiTreeNodeFlags_DefaultOpen))
			{
				SpriteRenderer* renderer = dynamic_cast<SpriteRenderer*>(component);

				ImGui::Columns(2, NULL, false);

				ImGui::Text("Active"); ImGui::Spacing();
				ImGui::Text("Color"); ImGui::Spacing();
				ImGui::Text("Render Type"); ImGui::Spacing();
				ImGui::Text("Order"); ImGui::Spacing();

				ImGui::NextColumn();
				ImGui::PushItemWidth(-1);

				bool enable_Before = renderer->enabled;
				if (ImGui::Checkbox("##spriteRendererActive", &renderer->enabled))
				{
					History::GetInstance().AddHistory<bool>(&renderer->enabled, enable_Before, renderer->enabled);
				}

				// Color value (Color picker)
				ImVec4 color(renderer->color.r, renderer->color.g, renderer->color.b, renderer->color.a);
				if (ImGui::ColorEdit4("##spriteRendererColor", (float*)&color, ImGuiColorEditFlags_NoInputs | ImGuiColorEditFlags_NoLabel))
				{
					Color color_Before = renderer->color;
					renderer->color = Color(color.x, color.y, color.z, color.w);
					History::GetInstance().AddHistory<Color>(&renderer->color, color_Before, renderer->color);
				}

				// Render type
				int item_current_idx = GetCurrentComboIndexNode(renderer->renderType, renderTypeList);
				const char* combo_label = renderTypeList[item_current_idx].c_str();
				std::string renderType_Before = renderTypeList[item_current_idx];
				if (ImGui::BeginCombo("##spriteRendererRenderType", combo_label))
				{
					for (int n = 0; n < renderTypeList.size(); n++)
					{
						const bool is_selected = (item_current_idx == n);
						if (ImGui::Selectable(renderTypeList[n].c_str(), is_selected))
						{
							renderer->renderType = renderTypeList[n];
							item_current_idx = n;
							History::GetInstance().AddHistory<std::string>(&renderer->renderType, renderType_Before, renderer->renderType);
						}

						if (is_selected)
							ImGui::SetItemDefaultFocus();
					}
					ImGui::EndCombo();
				}

				// Render order
				int order_Before = renderer->order;
				if (ImGui::InputInt("##spriteRendererOrder", &renderer->order))
				{
					History::GetInstance().AddHistory<int>(&renderer->order, order_Before, renderer->order);
				}

				ImGui::PopItemWidth();
				ImGui::Columns(1);

				if (ImGui::TreeNode("Sprite Editor"))
				{
					ImGui::Text("Slice"); ImGui::Spacing();

					ImGui::Text("L"); ImGui::SameLine();
					ImGui::SetNextItemWidth(ImGui::GetContentRegionAvail().x * 0.5f);
					int slicex_Before = renderer->slice.x;
					if (ImGui::DragFloat("##spriteRendererSliceX", &renderer->slice.x, 1, 0))
					{
						History::GetInstance().AddHistory<float>(&renderer->slice.x, slicex_Before, renderer->slice.x);
					}ImGui::SameLine();

					ImGui::Text("R"); ImGui::SameLine();
					ImGui::SetNextItemWidth(ImGui::GetContentRegionAvail().x);
					float slicey_Before = renderer->slice.y;
					if (ImGui::DragFloat("##spriteRendererSliceY", &renderer->slice.y, 1, 0))
					{
						History::GetInstance().AddHistory<float>(&renderer->slice.y, slicey_Before, renderer->slice.y);
					}

					ImGui::Text("T"); ImGui::SameLine();
					ImGui::SetNextItemWidth(ImGui::GetContentRegionAvail().x * 0.5f);
					int slicez_Before = renderer->slice.z;
					if (ImGui::DragFloat("##spriteRendererSliceZ", &renderer->slice.z, 1, 0))
					{
						History::GetInstance().AddHistory<float>(&renderer->slice.z, slicez_Before, renderer->slice.z);
					}ImGui::SameLine();

					ImGui::Text("B"); ImGui::SameLine();
					ImGui::SetNextItemWidth(ImGui::GetContentRegionAvail().x);
					float slicew_Before = renderer->slice.w;
					if (ImGui::DragFloat("##spriteRendererSliceW", &renderer->slice.w, 1, 0))
					{
						History::GetInstance().AddHistory<float>(&renderer->slice.w, slicew_Before, renderer->slice.w);
					}


					int w, h;
					SDL_QueryTexture(AssetWindow::GetInstance().GetSDLTex(), NULL, NULL, &w, &h);
					//width cannot be more than 380 px wide
					//compute display width and height
					float my_tex_w, my_tex_h;
					if (w > ImGui::GetContentRegionAvail().x)
					{
						my_tex_w = ImGui::GetContentRegionAvail().x;
						my_tex_h = (float)h / ((float)w / ImGui::GetContentRegionAvail().x);
					}
					else if (w < 320)
					{
						my_tex_w = 320;
						my_tex_h = round((float)h / ((float)w / 320));
					}
					else
					{
						my_tex_w = w;
						my_tex_h = h;
					}

					ImVec2 uv_min = ImVec2(0.0f, 0.0f);                 // Top-left
					ImVec2 uv_max = ImVec2(1.0f, 1.0f);                 // Lower-right
					ImVec4 tint_col = ImVec4(1.0f, 1.0f, 1.0f, 1.0f);   // No tint
					ImVec4 border_col = ImVec4(1.0f, 1.0f, 1.0f, 0.5f); // 50% opaque white
					ImGui::Image(renderer->sprite->texture, ImVec2(my_tex_w, my_tex_h), uv_min, uv_max, tint_col, border_col);

					ImVec2 p = ImGui::GetCursorScreenPos();
					float sliceSize = 1.0f;
					// Render slice L
					ImGui::GetWindowDrawList()->AddLine(ImVec2(p.x + renderer->slice.x * my_tex_w / renderer->sprite->rect.w, p.y - my_tex_h - 5), ImVec2(p.x + renderer->slice.x * my_tex_w / renderer->sprite->rect.w, p.y - 5), IM_COL32(0, 255, 0, 255), sliceSize);
					// Render slice R
					ImGui::GetWindowDrawList()->AddLine(ImVec2(p.x + my_tex_w - renderer->slice.y *  my_tex_w / renderer->sprite->rect.w, p.y - my_tex_h - 5), ImVec2(p.x + my_tex_w - renderer->slice.y *  my_tex_w / renderer->sprite->rect.w, p.y - 5), IM_COL32(0, 255, 0, 255), sliceSize);
					// Render slice T
					ImGui::GetWindowDrawList()->AddLine(ImVec2(p.x, p.y - my_tex_h - 5 + renderer->slice.z * my_tex_h / renderer->sprite->rect.h), ImVec2(p.x + my_tex_w, p.y - my_tex_h - 5 + renderer->slice.z  * my_tex_h / renderer->sprite->rect.h), IM_COL32(0, 255, 0, 255), sliceSize);
					// Render slice B
					ImGui::GetWindowDrawList()->AddLine(ImVec2(p.x, p.y - 5 - renderer->slice.w * my_tex_h / renderer->sprite->rect.h), ImVec2(p.x + my_tex_w, p.y - 5 - renderer->slice.w * my_tex_h / renderer->sprite->rect.h), IM_COL32(0, 255, 0, 255), sliceSize);

					ImGui::TreePop();
				}
				
			}
		}
	}

	void ShowSpritePreview()
	{
		ImGuiIO& io = ImGui::GetIO();
		ImTextureID my_tex_id = AssetWindow::GetInstance().GetTexture();
		if (my_tex_id != nullptr)
		{
			int w, h;
			SDL_QueryTexture(AssetWindow::GetInstance().GetSDLTex(), NULL, NULL, &w, &h);
			//width cannot be more than 380 px wide
			//compute display width and height
			float my_tex_w, my_tex_h;
			if (w > ImGui::GetContentRegionAvail().x)
			{
				my_tex_w = ImGui::GetContentRegionAvail().x;
				my_tex_h = (float)h / ((float)w / ImGui::GetContentRegionAvail().x);
			}
			else if (w < 320)
			{
				my_tex_w = 320;
				my_tex_h = round((float)h / ((float)w / 320));
			}
			else
			{
				my_tex_w = w;
				my_tex_h = h;
			}
	
			ImGui::Text("Image Preview");
			ImGui::Text("Image size: %ix%i", w, h);
			ImGui::Text("File name: "); ImGui::SameLine();
			ImGui::Text((AssetWindow::GetInstance().GetFilename()).c_str());
			ImVec2 pos = ImGui::GetCursorScreenPos();
			ImVec2 uv_min = ImVec2(0.0f, 0.0f);                 // Top-left
			ImVec2 uv_max = ImVec2(1.0f, 1.0f);                 // Lower-right
			ImVec4 tint_col = ImVec4(1.0f, 1.0f, 1.0f, 1.0f);   // No tint
			ImVec4 border_col = ImVec4(1.0f, 1.0f, 1.0f, 0.5f); // 50% opaque white
			ImGui::Image(my_tex_id, ImVec2(my_tex_w, my_tex_h), uv_min, uv_max, tint_col, border_col);
			if (ImGui::IsItemHovered())
			{
				ImGui::BeginTooltip();
				float region_sz = 32.0f;
				float region_x = io.MousePos.x - pos.x - region_sz * 0.5f;
				float region_y = io.MousePos.y - pos.y - region_sz * 0.5f;
				float zoom = 4.0f;
				if (region_x < 0.0f) { region_x = 0.0f; }
				else if (region_x > my_tex_w - region_sz) { region_x = my_tex_w - region_sz; }
				if (region_y < 0.0f) { region_y = 0.0f; }
				else if (region_y > my_tex_h - region_sz) { region_y = my_tex_h - region_sz; }
				ImGui::Text("Min: (%.2f, %.2f)", region_x, region_y);
				ImGui::Text("Max: (%.2f, %.2f)", region_x + region_sz, region_y + region_sz);
				ImVec2 uv0 = ImVec2((region_x) / my_tex_w, (region_y) / my_tex_h);
				ImVec2 uv1 = ImVec2((region_x + region_sz) / my_tex_w, (region_y + region_sz) / my_tex_h);
				ImGui::Image(my_tex_id, ImVec2(region_sz * zoom, region_sz * zoom), uv0, uv1, tint_col, border_col);
				ImGui::EndTooltip();
			}
			
		}
	}

	void Inspector::ShowWindow()
	{
		if (ImGui::Begin("Inspector"))
		{
			GameObject* selectedObj = Hierarchy::GetInstance().GetSelectedObject();

			if (selectedObj != nullptr && Editor::GetInstance().GetCurrentItemType() == SelectableItemType::Item_GameObject)
			{
				//Game object properties
				{
					bool objEnable = selectedObj->GetActive();
					if (ImGui::Checkbox("##objectActive", &objEnable))
					{
						selectedObj->SetActive(objEnable);
					}

					//Object's name
					ImGui::SameLine();
					ImGui::PushItemWidth(-1);
					std::string name_Before = selectedObj->name;
					if (ImGui::InputTextWithHint("##name", "Object's name", &selectedObj->name))
					{
						History::GetInstance().AddHistory<std::string>(&selectedObj->name, name_Before, selectedObj->name);
					}
					ImGui::PopItemWidth();

					static int item_current = 0;
					ImGui::Text("Tag"); ImGui::SetNextItemWidth(ImGui::GetWindowWidth() * 0.4f); ImGui::SameLine();
					std::string tag_Before = selectedObj->tag;
					if (ImGui::InputTextWithHint("##tag", "Tag", &selectedObj->tag))
					{
						History::GetInstance().AddHistory<std::string>(&selectedObj->tag, tag_Before, selectedObj->tag);
					}

					ImGui::SameLine();

					ImGui::Text("Layer"); ImGui::SetNextItemWidth(ImGui::GetWindowWidth() * 0.4f); ImGui::SameLine();
					ImGui::PushItemWidth(-1);
					std::string layer_Before = selectedObj->layer;
					if (ImGui::InputTextWithHint("##layer", "Layer name", &selectedObj->layer))
					{
						History::GetInstance().AddHistory<std::string>(&selectedObj->layer, layer_Before, selectedObj->layer);
					}
					ImGui::PopItemWidth();
				}
				//End game object's properties

				ImGui::Separator();

				std::vector<Component*> components = selectedObj->GetComponents();
				for (size_t i = 0; i < components.size(); i++)
				{
					ShowComponent(components[i], m_FontList, m_SpriteRenderType);
				}

				ImGui::Separator();
			}
			else if (Editor::GetInstance().GetCurrentItemType() == SelectableItemType::Item_SDL_Texture)
			{
				ShowSpritePreview();
			}
		}

		ImGui::End();

	}
}