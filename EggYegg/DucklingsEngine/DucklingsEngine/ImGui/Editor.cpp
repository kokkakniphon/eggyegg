#include "Editor.h"

#include "Input.h"
#include "BuildSettings.h"

#include "GameObject.h"
#include "Camera.h"

#include "imgui_internal.h"

#include <windows.h>

namespace DucklingsEngine
{
	Editor* Editor::s_Instance = nullptr;
	EditorCustomStyleData Editor::s_CustomStyleData = EditorCustomStyleData();
	ImFont* Editor::s_PFont = nullptr;
	ImGuiID Editor::s_DockspaceID = 0;

	static std::string s_GuiPath = "../Assets/GUI/";

	static ImVec4 menubar_c = ImVec4(1.0f, 1.0f, 1.0f, 1.0f);
	static ImVec4 window_tool_c = ImVec4(0.54f, 0.54f, 0.54f, 1.0f);

	// Return the keycode according to the given value.
	// I know this look ungly but it work!!
	char ScancodeToKeycode(SDL_Scancode scancode, bool shifted)
	{
		char value = SDL_GetKeyFromScancode(scancode);
		if (shifted == true) // Convert the lower case to upper cause according to QWERTY keyboard layout.
		{
			if (value == 34)
				value += 5;
			else if (value == 45)
				value += 50;
			else if (value == 48)
				value -= 7;
			else if (value == 49 || (value >= 51 && value <= 53))
				value -= 16;
			else if (value == 50)
				value += 14;
			else if (value == 55 || value == 57)
				value -= 17;
			else if (value == 54)
				value += 40;
			else if (value == 56)
				value -= 14;
			else if (value == 58)
				value += 1;
			else if (value == 60 || value == 62 || value == 63)
				value -= 16;
			else if (value == 61)
				value -= 18;
			else if (value == 96)
				value += 30;
			else if (value >= 97 && value <= 125)
				value -= 32;
		}
		return value;
	}

	void LoadGUI()
	{
		//Load all file in GUI folder.
		std::string pattern(s_GuiPath);
		pattern.append("\\*");
		WIN32_FIND_DATA data;
		HANDLE hFind;
		if ((hFind = FindFirstFile(pattern.c_str(), &data)) != INVALID_HANDLE_VALUE) {
			do {
				AssetManager::GetInstance().LoadTexture(data.cFileName, s_GuiPath);
			} while (FindNextFile(hFind, &data) != 0);
			FindClose(hFind);
		}
	}

	void Editor::SetCustomStyleColor()
	{
		ImGuiStyle* style = &ImGui::GetStyle();
		ImVec4* colors = style->Colors;

		colors[ImGuiCol_Text] = ImVec4(1.00f, 1.00f, 1.00f, 1.00f);
		colors[ImGuiCol_TextDisabled] = ImVec4(0.50f, 0.50f, 0.50f, 1.00f);
		colors[ImGuiCol_WindowBg] = ImVec4(0.76f, 0.76f, 0.76f, 1.00f);
		colors[ImGuiCol_ChildBg] = ImVec4(0.00f, 0.00f, 0.00f, 0.00f);
		colors[ImGuiCol_PopupBg] = ImVec4(0.76f, 0.76f, 0.76f, 1.00f);
		colors[ImGuiCol_Border] = ImVec4(0.43f, 0.43f, 0.50f, 0.50f);
		colors[ImGuiCol_BorderShadow] = ImVec4(0.00f, 0.00f, 0.00f, 0.00f);
		colors[ImGuiCol_FrameBg] = ImVec4(0.65f, 0.65f, 0.65f, 1.00f);
		colors[ImGuiCol_FrameBgHovered] = ImVec4(0.80f, 0.80f, 0.80f, 0.40f);
		colors[ImGuiCol_FrameBgActive] = ImVec4(0.80f, 0.80f, 0.80f, 0.67f);
		colors[ImGuiCol_TitleBg] = ImVec4(0.65f, 0.65f, 0.65f, 1.00f);
		colors[ImGuiCol_TitleBgActive] = ImVec4(0.65f, 0.65f, 0.65f, 1.00f);
		colors[ImGuiCol_TitleBgCollapsed] = ImVec4(0.00f, 0.00f, 0.00f, 0.51f);
		colors[ImGuiCol_MenuBarBg] = ImVec4(0.14f, 0.14f, 0.14f, 1.00f);
		colors[ImGuiCol_ScrollbarBg] = ImVec4(0.02f, 0.02f, 0.02f, 0.53f);
		colors[ImGuiCol_ScrollbarGrab] = ImVec4(0.31f, 0.31f, 0.31f, 1.00f);
		colors[ImGuiCol_ScrollbarGrabHovered] = ImVec4(0.41f, 0.41f, 0.41f, 1.00f);
		colors[ImGuiCol_ScrollbarGrabActive] = ImVec4(0.51f, 0.51f, 0.51f, 1.00f);
		colors[ImGuiCol_CheckMark] = ImVec4(0.26f, 0.59f, 0.98f, 1.00f);
		colors[ImGuiCol_SliderGrab] = ImVec4(0.24f, 0.52f, 0.88f, 1.00f);
		colors[ImGuiCol_SliderGrabActive] = ImVec4(0.26f, 0.59f, 0.98f, 1.00f);
		colors[ImGuiCol_Button] = ImVec4(0.26f, 0.59f, 0.98f, 0.40f);
		colors[ImGuiCol_ButtonHovered] = ImVec4(0.26f, 0.59f, 0.98f, 1.00f);
		colors[ImGuiCol_ButtonActive] = ImVec4(0.06f, 0.53f, 0.98f, 1.00f);
		colors[ImGuiCol_Header] = ImVec4(0.26f, 0.59f, 0.98f, 0.31f);
		colors[ImGuiCol_HeaderHovered] = ImVec4(0.26f, 0.59f, 0.98f, 0.80f);
		colors[ImGuiCol_HeaderActive] = ImVec4(0.26f, 0.59f, 0.98f, 1.00f);
		colors[ImGuiCol_Separator] = colors[ImGuiCol_Border];
		colors[ImGuiCol_SeparatorHovered] = ImVec4(0.10f, 0.40f, 0.75f, 0.78f);
		colors[ImGuiCol_SeparatorActive] = ImVec4(0.10f, 0.40f, 0.75f, 1.00f);
		colors[ImGuiCol_ResizeGrip] = ImVec4(0.26f, 0.59f, 0.98f, 0.25f);
		colors[ImGuiCol_ResizeGripHovered] = ImVec4(0.26f, 0.59f, 0.98f, 0.67f);
		colors[ImGuiCol_ResizeGripActive] = ImVec4(0.26f, 0.59f, 0.98f, 0.95f);
		colors[ImGuiCol_Tab] = ImLerp(colors[ImGuiCol_Header], colors[ImGuiCol_TitleBgActive], 0.80f);
		colors[ImGuiCol_TabHovered] = colors[ImGuiCol_HeaderHovered];
		colors[ImGuiCol_TabActive] = ImLerp(colors[ImGuiCol_HeaderActive], colors[ImGuiCol_TitleBgActive], 0.60f);
		colors[ImGuiCol_TabUnfocused] = ImLerp(colors[ImGuiCol_Tab], colors[ImGuiCol_TitleBg], 0.80f);
		colors[ImGuiCol_TabUnfocusedActive] = ImLerp(colors[ImGuiCol_TabActive], colors[ImGuiCol_TitleBg], 0.40f);
		ImVec4 dockingPreview = colors[ImGuiCol_HeaderActive];
		dockingPreview.w = 0.7f;
		colors[ImGuiCol_DockingPreview] = dockingPreview;
		colors[ImGuiCol_DockingEmptyBg] = ImVec4(0.20f, 0.20f, 0.20f, 1.00f);
		colors[ImGuiCol_PlotLines] = ImVec4(0.61f, 0.61f, 0.61f, 1.00f);
		colors[ImGuiCol_PlotLinesHovered] = ImVec4(1.00f, 0.43f, 0.35f, 1.00f);
		colors[ImGuiCol_PlotHistogram] = ImVec4(0.90f, 0.70f, 0.00f, 1.00f);
		colors[ImGuiCol_PlotHistogramHovered] = ImVec4(1.00f, 0.60f, 0.00f, 1.00f);
		colors[ImGuiCol_TextSelectedBg] = ImVec4(0.26f, 0.59f, 0.98f, 0.35f);
		colors[ImGuiCol_DragDropTarget] = ImVec4(1.00f, 1.00f, 0.00f, 0.90f);
		colors[ImGuiCol_NavHighlight] = ImVec4(0.26f, 0.59f, 0.98f, 1.00f);
		colors[ImGuiCol_NavWindowingHighlight] = ImVec4(1.00f, 1.00f, 1.00f, 0.70f);
		colors[ImGuiCol_NavWindowingDimBg] = ImVec4(0.80f, 0.80f, 0.80f, 0.20f);
		colors[ImGuiCol_ModalWindowDimBg] = ImVec4(0.80f, 0.80f, 0.80f, 0.35f);
	}


	void Editor::SetupDockSpace()
	{
		bool active = true;

		static ImGuiDockNodeFlags dockspace_flags = ImGuiDockNodeFlags_None;
		ImGuiWindowFlags window_flags = ImGuiWindowFlags_MenuBar | ImGuiWindowFlags_NoDocking;

		window_flags |= ImGuiWindowFlags_NoTitleBar | ImGuiWindowFlags_NoCollapse | ImGuiWindowFlags_NoResize | ImGuiWindowFlags_NoMove;
		window_flags |= ImGuiWindowFlags_NoBringToFrontOnFocus | ImGuiWindowFlags_NoNavFocus;

		ImGui::PushStyleVar(ImGuiStyleVar_WindowBorderSize, 0.0f);

		ImGui::Begin("Master Window", &active, window_flags);

		// Show top tools bar.
		ToolBar::GetInstance().ShowWindow();

		// Declare Central dockspace
		s_DockspaceID = ImGui::GetID("HUB_DockSpace");
		ImGui::DockSpace(s_DockspaceID, ImVec2(0.0f, 0.0f), dockspace_flags);

		// Show main menu bar.
		MainMenuBar::GetInstance().ShowWindow();

		ImGui::End(); // Master Window

		ImGui::PopStyleVar();
	}

	void Editor::SetupKeyMap()
	{
		m_IO->KeyMap[ImGuiKey_Tab] = Input::GetKey(SDL_SCANCODE_TAB) ? SDL_SCANCODE_TAB : -1;
		m_IO->KeyMap[ImGuiKey_LeftArrow] = Input::GetKey(SDL_SCANCODE_LEFT) ? SDL_SCANCODE_LEFT : -1;
		m_IO->KeyMap[ImGuiKey_RightArrow] = Input::GetKey(SDL_SCANCODE_RIGHT) ? SDL_SCANCODE_RIGHT : -1;
		m_IO->KeyMap[ImGuiKey_UpArrow] = Input::GetKey(SDL_SCANCODE_UP) ? SDL_SCANCODE_UP : -1;
		m_IO->KeyMap[ImGuiKey_DownArrow] = Input::GetKey(SDL_SCANCODE_DOWN) ? SDL_SCANCODE_DOWN : -1;
		m_IO->KeyMap[ImGuiKey_PageUp] = Input::GetKey(SDL_SCANCODE_PAGEUP) ? SDL_SCANCODE_PAGEUP : -1;
		m_IO->KeyMap[ImGuiKey_PageDown] = Input::GetKey(SDL_SCANCODE_PAGEDOWN) ? SDL_SCANCODE_PAGEDOWN : -1;
		m_IO->KeyMap[ImGuiKey_Home] = Input::GetKey(SDL_SCANCODE_HOME) ? SDL_SCANCODE_HOME : -1;
		m_IO->KeyMap[ImGuiKey_End] = Input::GetKey(SDL_SCANCODE_END) ? SDL_SCANCODE_END : -1;
		m_IO->KeyMap[ImGuiKey_Insert] = Input::GetKey(SDL_SCANCODE_INSERT) ? SDL_SCANCODE_INSERT : -1;
		m_IO->KeyMap[ImGuiKey_Delete] = Input::GetKey(SDL_SCANCODE_DELETE) ? SDL_SCANCODE_DELETE : -1;
		m_IO->KeyMap[ImGuiKey_Backspace] = Input::GetKey(SDL_SCANCODE_BACKSPACE) ? SDL_SCANCODE_BACKSPACE : -1;
		m_IO->KeyMap[ImGuiKey_Space] = Input::GetKey(SDL_SCANCODE_SPACE) ? SDL_SCANCODE_SPACE : -1;
		m_IO->KeyMap[ImGuiKey_Enter] = Input::GetKey(SDL_SCANCODE_RETURN) ? SDL_SCANCODE_RETURN : -1;
		m_IO->KeyMap[ImGuiKey_Escape] = Input::GetKey(SDL_SCANCODE_ESCAPE) ? SDL_SCANCODE_ESCAPE : -1;
		m_IO->KeyMap[ImGuiKey_KeyPadEnter] = Input::GetKey(SDL_SCANCODE_KP_ENTER) ? SDL_SCANCODE_KP_ENTER : -1;
		m_IO->KeyMap[ImGuiKey_A] = Input::GetKey(SDL_SCANCODE_A) ? SDL_SCANCODE_A : -1;
		m_IO->KeyMap[ImGuiKey_C] = Input::GetKey(SDL_SCANCODE_C) ? SDL_SCANCODE_C : -1;
		m_IO->KeyMap[ImGuiKey_V] = Input::GetKey(SDL_SCANCODE_V) ? SDL_SCANCODE_V : -1;
		m_IO->KeyMap[ImGuiKey_X] = Input::GetKey(SDL_SCANCODE_X) ? SDL_SCANCODE_X : -1;
		m_IO->KeyMap[ImGuiKey_Y] = Input::GetKey(SDL_SCANCODE_Y) ? SDL_SCANCODE_Y : -1;
		m_IO->KeyMap[ImGuiKey_Z] = Input::GetKey(SDL_SCANCODE_Z) ? SDL_SCANCODE_Z : -1;

		m_IO->ConfigFlags |= ImGuiConfigFlags_NavEnableKeyboard;
	}

	void Editor::SetPlayMode(bool play_mode)
	{
		m_Played = play_mode;

		LoadScene(SceneManager::GetInstance().GetFirstActiveScene());
	}

	void Editor::OpenScene(Scene * scene)
	{
		Engine::GetInstance().GetManager()->DestoryAllEntity();
		SceneManager::GetInstance().OpenScene(scene->name);
	}

	void Editor::LoadScene(Scene *scene)
	{
		Scene* currentScene = scene;

		if (m_Played == true)
		{
			SaveScene();
		}

		if (currentScene != nullptr)
		{
			SceneManager::GetInstance().UnloadScene(currentScene->name);
			Engine::GetInstance().GetManager()->UnloadEntity();
			ToolBar::GetInstance().ClearTools();
			Object::ResetLocalUniqueIDSeed(currentScene->name);
			SceneManager::GetInstance().LoadScene(currentScene->name);
			Engine::GetInstance().GetManager()->Start();
		}
	}

	void Editor::SaveScene()
	{
		Scene* currentScene = SceneManager::GetInstance().GetFirstActiveScene();

		if (currentScene != nullptr)
		{
			std::cout << "Current Scene" << std::endl;
			FileManager::WriteYAMLData(currentScene->path, currentScene->name);
		}

		m_UnsavedChange = false;
	}

	bool Editor::Init()
	{
		Engine::GetInstance().Init();
		Engine::GetInstance().SetGizmos(true);

		m_Window = Engine::GetInstance().GetWindow();
		m_Renderer = Engine::GetInstance().GetRenderer();

		SDL_Surface*image;
		image = SDL_LoadBMP_RW(SDL_RWFromFile((s_GuiPath + "DucklingEngine_logo_small.bmp").c_str(), "rb"), 1);
		SDL_SetWindowIcon(m_Window, image);

		m_ClearColor = DARK;

		SDL_Init(SDL_INIT_EVERYTHING);

		Inspector::GetInstance().LoadFont();

		ImGui::CreateContext();
		ImGuiSDL::Initialize(m_Renderer, SCREEN_WIDTH, SCREEN_HEIGHT);

		m_IO = &ImGui::GetIO();
		m_IO->KeyRepeatDelay = 0.5f;
		m_IO->ConfigFlags |= ImGuiConfigFlags_DockingEnable;
		m_IO->ConfigFlags |= ImGuiConfigFlags_ViewportsEnable;
		m_IO->MouseDoubleClickTime = 1.0f;

		return m_IsRunning = true;
	}

	void Editor::Update()
	{
		int wheel = 0;

		if (Engine::event.type == SDL_WINDOWEVENT)
		{
			if (Engine::event.window.event == SDL_WINDOWEVENT_SIZE_CHANGED)
			{
				m_IO->DisplaySize.x = static_cast<float>(Engine::event.window.data1);
				m_IO->DisplaySize.y = static_cast<float>(Engine::event.window.data2);
			}
		}
		else if (Engine::event.type == SDL_MOUSEWHEEL)
		{
			wheel = Engine::event.wheel.y;
		}
				
		Vector2 mousePos = Input::GetMousePosition();

		m_IO->MousePos = ImVec2(mousePos.x, mousePos.y);
		m_IO->MouseDown[0] = Input::GetMouseButton(SDL_BUTTON_LEFT);
		m_IO->MouseDown[1] = Input::GetMouseButton(SDL_BUTTON_RIGHT);
		m_IO->MouseWheel = static_cast<float>(wheel);


		if (DuckTime::GetDeltaTime() > 0)
			m_IO->DeltaTime = DuckTime::GetDeltaTime();
		else
			m_IO->DeltaTime = 1.0f / 60.0f;

		SetupKeyMap();

		ImGui::NewFrame();

		//SetCustomStyleColor();

		// Show all imgui.
		SetupDockSpace();

		ImGui::ShowDemoWindow();

		Inspector::GetInstance().ShowWindow();
		Hierarchy::GetInstance().ShowWindow();
		AssetWindow::GetInstance().ShowWindow();
		SceneWindow::GetInstance().ShowWindow();
		GameWindow::GetInstance().ShowWindow();

		ImGui::EndFrame();

		// Engine Update
		if (m_Played && m_Paused == false)
		{	
			Engine::GetInstance().GetManager()->Update();
		}
	}

	void Editor::Render()
	{
		// Reset view port.
		SDL_Rect editor_view;
		editor_view.x = 0.0f;
		editor_view.y = 0.0f;
		editor_view.w = SCREEN_WIDTH;
		editor_view.h = SCREEN_HEIGHT;

		SDL_RenderSetViewport(m_Renderer, &editor_view);
		SDL_RenderSetScale(m_Renderer, 1.0f, 1.0f);

		SDL_SetRenderDrawColor(m_Renderer, m_ClearColor.r, m_ClearColor.g, m_ClearColor.b, m_ClearColor.a);
		SDL_RenderClear(m_Renderer);

		ImGui::Render();
		ImGuiSDL::Render(ImGui::GetDrawData());

		SceneWindow::GetInstance().Render();
		GameWindow::GetInstance().Render();

		ToolBar::GetInstance().ShowOverlayTool();

		SDL_RenderSetScale(m_Renderer, 1.0f, 1.0f);
		SDL_RenderPresent(m_Renderer);
	}

	void Editor::Events()
	{
		switch (Engine::event.type)
		{
			case SDL_KEYDOWN:
			{
				for (size_t i = 0; i < 512; i++)
				{
					m_IO->KeysDown[i] = false;
				}

				int key = Engine::event.key.keysym.scancode;
				if (Input::GetKey(Engine::event.key.keysym.scancode))
				{
					if (SDL_GetKeyFromScancode(Engine::event.key.keysym.scancode) <= SDL_NUM_SCANCODES)
					{
						m_IO->KeysDown[key] = true;
						m_IO->AddInputCharacter(ScancodeToKeycode(Engine::event.key.keysym.scancode, Input::GetKey(SDL_SCANCODE_LSHIFT) || Input::GetKey(SDL_SCANCODE_RSHIFT)));
					}
				}

				m_IO->KeySuper = false;
				break;
			}
			case SDL_KEYUP:
			{
				m_IO->KeyShift = ((SDL_GetModState() & KMOD_SHIFT) != 0);
				m_IO->KeyCtrl = ((SDL_GetModState() & KMOD_CTRL) != 0);
				m_IO->KeyAlt = ((SDL_GetModState() & KMOD_ALT) != 0);
				m_IO->KeySuper = false;
				break;
			}
			case SDL_QUIT:
			{
				Quit();
				break;
			}
		}
	}

	void Editor::Clean()
	{
		ImGuiSDL::Deinitialize();
		ImGui::DestroyContext();

		MainMenuBar::GetInstance().Clean();
		Inspector::GetInstance().Clean();
		Hierarchy::GetInstance().Clean();
		ToolBar::GetInstance().Clean();
		AssetWindow::GetInstance().Clean();
		SceneWindow::GetInstance().Clean();
		GameWindow::GetInstance().Clean();

		History::GetInstance().ClearHistory();
		History::GetInstance().Clean();

		Singleton<Editor>::Clean();

		Engine::GetInstance().Clean();
	}

	Editor::Editor()
	{
		m_IO = nullptr;
		m_IsRunning = false;
		m_Window = nullptr;
		m_Renderer = nullptr;

		DuckTime::GetInstance().timeScale = 1.0f;
	}

	Editor::~Editor() {}

	void Editor::Run()
	{
		Init();
	
		Settings::SetupBuildSettings();

		Input::Init();

		Engine::GetInstance().GetManager()->Start();

		LoadGUI();

		while (m_IsRunning)
		{
			if (m_Played && m_Paused == false)
			{
				Engine::GetInstance().GetManager()->Start();
				DuckTime::GetInstance().Tick(PHYSIC_LOOP_CALULATION);
				for (unsigned int i = 0; i < PHYSIC_LOOP_CALULATION; i++)
				{
					ColliderManager::GetInstance().FixedUpdate();
					Engine::GetInstance().GetManager()->FixedUpdate();
				}
			}
			if (SDL_PollEvent(&Engine::event) != 0)
				Input::Listen();
			Input::Update();
			Events();
			Update();
			Render();
			Input::Clear();
			Engine::GetInstance().GetManager()->UnloadEntity();
		}

		Clean();
	}

	void Editor::Quit()
	{
		m_IsRunning = false;
	}

}

