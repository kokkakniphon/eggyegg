#pragma once

#include <iostream>

#include "Singleton.h"
#include "imgui.h"
#include "imgui_sdl.h"
#include "MainMenuBar.h"
#include "Inspector.h"
#include "Hierarchy.h"
#include "ToolBar.h"
#include "AssetWindow.h"
#include "SceneWindow.h"
#include "GameWindow.h"
#include "History.h"

#include "SDL.h"
#include "SDL_image.h"
#include "SDL_ttf.h"

#include "DuckTime.h"
#include "SceneManager.h"
#include "AssetManager.h"

#define EDITOR_IGNORE_TAG "editor_ignore"

namespace DucklingsEngine
{

	struct EditorCustomStyleData
	{
		const float FontScale = 1.3f;

		ImVec2 MainMenuPos;
		ImVec2 MainMenuSize;

		// Colors
		ImVec4 Playmode_tint_c = ImVec4(204, 204, 204, 255);
		ImVec4 Background_c = ImVec4(71, 71, 71, 255);
		ImVec4 Background_prefabs_c = ImVec4(34, 95, 84, 255);
		ImVec4 Center_axis_c = ImVec4(204, 204, 204, 237);
		ImVec4 Grid_c = ImVec4(128, 128, 128, 102);
		ImVec4 Grid_component_c = ImVec4(255, 255, 255, 26);
		ImVec4 Guide_line_c = ImVec4(128, 128, 128, 51);
		ImVec4 Material_validator_pure_metal_c = ImVec4(255, 255, 0, 255);
		ImVec4 Material_validator_value_too_high_c = ImVec4(0, 0, 255, 255);
		ImVec4 Material_validator_value_too_low_c = ImVec4(255, 0, 0, 255);
		ImVec4 Preselection_highlight_c = ImVec4(201, 200, 144, 227);
		ImVec4 Selected_axis_c = ImVec4(246, 242, 50, 227);
		ImVec4 Selected_children_outline_c = ImVec4(94, 119, 155, 255);
		ImVec4 Selected_outline_c = ImVec4(255, 102, 0, 255);
		ImVec4 Wireframe_c = ImVec4(0, 0, 0, 128);
		ImVec4 Wireframe_overlay_c = ImVec4(0, 0, 0, 64);
		ImVec4 Wireframe_selected_c = ImVec4(94, 119, 155, 64);
		ImVec4 X_axis_c = ImVec4(219, 62, 29, 237);
		ImVec4 Y_axis_c = ImVec4(154, 243, 72, 237);
		ImVec4 Z_axis_c = ImVec4(58, 122, 248, 237);
	};

	enum SelectableItemType
	{
		Item_GameObject,
		Item_SDL_Texture
	};

	class Editor : public Singleton<Editor>
	{
	private:
	
		ImGuiIO* m_IO;

		bool m_IsRunning;

		static EditorCustomStyleData s_CustomStyleData;
		static ImFont* s_PFont;
		static ImGuiID s_DockspaceID;

		SDL_Color m_ClearColor;
		SDL_Window* m_Window;
		SDL_Renderer* m_Renderer;
		
		SelectableItemType m_CurrentType;

		const char* m_WindowTitle = "DucklingsEngine - Editor";

		bool m_UnsavedChange = false;

		bool m_Played = false;
		bool m_Paused = false;

		// Windows settings
		bool m_SceneWindow = true;
		bool m_SpriteEditorWindow = false;

		void SetupDockSpace();
		void SetupKeyMap();

		void SetCustomStyleColor();

		bool Init();
		void Update();
		void Render();
		void Events();
		void Clean();

	public:
		Editor();
		~Editor();

		void Run();
		void Quit();
	
		void SetPlayMode(bool play_mode);
		bool GetPlayMode() { return m_Played; }

		void SetPauseMode(bool pause_mode) { m_Paused = pause_mode; }
		bool GetPauseMode() { return m_Paused; }

		void SetSpriteEditorWindow(bool enable) { m_SpriteEditorWindow = enable; }
		bool GetSpriteEditorWindowActive() { return m_SpriteEditorWindow; }

		void SetCurrentItemType(SelectableItemType type) { m_CurrentType = type; }
		SelectableItemType GetCurrentItemType() { return m_CurrentType; }

		// 
		// Summary: 
		//     Unload all scene, and load the given scene.
		//
		void OpenScene(Scene* scene);
		// 
		// Summary: 
		//     Load the given scene.
		//
		void LoadScene(Scene* scene);
		//
		// Summary:
		//     Save the loaded scene data to yaml data
		void SaveScene();

		void TriggerSceneChange() { m_UnsavedChange = true; }
		bool IsSceneUnsaved() { return m_UnsavedChange; }
	};
}


