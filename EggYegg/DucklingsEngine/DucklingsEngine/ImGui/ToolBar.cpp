#include "ToolBar.h"

#include "Engine.h"
#include "Editor.h"
#include "SceneWindow.h"
#include "History.h"

#include "SpriteRenderer.h"
#include "BoxCollider.h"
#include "CircleCollider.h"
#include "Input.h"

namespace DucklingsEngine
{
	ToolBar* ToolBar::s_Instance = nullptr;

	void ToolBar::ShowToolButton(std::string toolName, ImVec2 size, ToolMode toolMode)
	{
		bool isSelected = (toolMode == m_ToolMode);
		if (isSelected)
		{
			ImGui::PushStyleColor(ImGuiCol_Button, m_SelectedCol);
			ImGui::PushStyleColor(ImGuiCol_ButtonActive, m_SelectedCol);
			ImGui::PushStyleColor(ImGuiCol_ButtonHovered, m_SelectedCol);
		}
		if (ImGui::ImageButton(AssetManager::GetInstance().GetTexture(toolName), size, ImVec2(0, 0), ImVec2(1, 1), -1, ImVec4(0, 0, 0, 0), ImVec4(1, 1, 1, 1)))
		{
			m_ToolMode = toolMode;
		}
		
		if (isSelected)
			ImGui::PopStyleColor(3);
	}

	void ToolBar::ShowMoveTool()
	{
		if (m_MoveToolObj == nullptr)
		{
			m_MoveToolObj = new GameObject("MoveTool");
			m_MoveToolObj->tag = EDITOR_IGNORE_TAG;
			m_MoveToolObj->layer = "Editor_MoveTool";

			GameObject* xyPlane = new GameObject("MoveTool_XYPlane");
			xyPlane->transform->SetParent(m_MoveToolObj->transform);
			xyPlane->tag = EDITOR_IGNORE_TAG;
			xyPlane->layer = "Editor_MoveTool";
			xyPlane->transform->localEulerAngles = 90;
			xyPlane->transform->localScale = Vector2(0.5f, 0.5f);
			xyPlane->transform->localPosition = Vector2(12, -12);
			xyPlane->AddComponent<SpriteRenderer>("gui_blue_plane.png").order = INT_MAX;
			xyPlane->AddComponent<BoxCollider>();
			BoxCollider* planeCol = &xyPlane->GetComponent<BoxCollider>();
			planeCol->size = Vector2(25, 25);
			planeCol->isTrigger = true;
			planeCol->showOutline = false;
			xyPlane->Start();

			GameObject* yArrow = new GameObject("MoveTool_YArrow");
			yArrow->transform->SetParent(m_MoveToolObj->transform);
			yArrow->transform->localPosition = Vector2(0, -25);
			yArrow->tag = EDITOR_IGNORE_TAG;
			yArrow->layer = "Editor_MoveTool";
			yArrow->AddComponent<SpriteRenderer>("gui_green_arrow.png").order = INT_MAX;
			yArrow->AddComponent<BoxCollider>();
			BoxCollider* yArrowCol = &yArrow->GetComponent<BoxCollider>();
			yArrowCol->size = Vector2(10, 25);
			yArrowCol->isTrigger = true;
			yArrowCol->showOutline = false;
			yArrowCol->Start();

			GameObject* xArrow = new GameObject("MoveTool_XArrow");
			xArrow->transform->SetParent(m_MoveToolObj->transform);
			xArrow->transform->localPosition = Vector2(25, 0);
			xArrow->tag = EDITOR_IGNORE_TAG;
			xArrow->layer = "Editor_MoveTool";
			xArrow->transform->localEulerAngles = 90;
			xArrow->AddComponent<SpriteRenderer>("gui_red_arrow.png").order = INT_MAX;
			xArrow->AddComponent<BoxCollider>();
			BoxCollider* xArrowCol = &xArrow->GetComponent<BoxCollider>();
			xArrowCol->size = Vector2(10, 25);
			xArrowCol->isTrigger = true;
			xArrowCol->showOutline = false;
			xArrowCol->Start();
		}

		m_MoveToolObj->transform->SetPosition(m_CurrentSelectObj->transform->GetPosition());
	}

	void ToolBar::ShowRotateTool()
	{
		if (m_RotateToolObj == nullptr)
		{
			m_RotateToolObj = new GameObject("RotateTool");
			m_RotateToolObj->tag = EDITOR_IGNORE_TAG;
			m_RotateToolObj->layer = "Editor_RotateTool";

			GameObject* wheel = new GameObject("RotateTool_Wheel");
			wheel->transform->SetParent(m_RotateToolObj->transform);
			wheel->tag = EDITOR_IGNORE_TAG;
			wheel->layer = "Editor_RotateTool";
			wheel->transform->localScale = Vector2(0.25f, 0.25f);
			wheel->AddComponent<SpriteRenderer>("gui_rotate_wheel.png").order = INT_MAX;
			CircleCollider* wheelCol = &wheel->AddComponent<CircleCollider>();
			wheelCol->radius = 55;
			wheelCol->isTrigger = true;
			wheelCol->showOutline = false;
			wheel->Start();
		}

		m_RotateToolObj->transform->SetPosition(m_CurrentSelectObj->transform->GetPosition());
	}

	void ToolBar::ShowScaleTool()
	{
		if (m_ScaleToolObj == nullptr)
		{
			m_ScaleToolObj = new GameObject("ScaleTool");
			m_ScaleToolObj->tag = EDITOR_IGNORE_TAG;
			m_ScaleToolObj->layer = "Editor_ScaleTool";

			GameObject* yArrow = new GameObject("ScaleTool_YArrow");
			yArrow->transform->SetParent(m_ScaleToolObj->transform);
			yArrow->transform->localPosition = Vector2(0, -25);
			yArrow->tag = EDITOR_IGNORE_TAG;
			yArrow->layer = "Editor_ScaleTool";
			yArrow->AddComponent<SpriteRenderer>("gui_green_box_arrow.png").order = INT_MAX;
			yArrow->AddComponent<BoxCollider>();
			BoxCollider* yArrowCol = &yArrow->GetComponent<BoxCollider>();
			yArrowCol->size = Vector2(10, 20);
			yArrowCol->offset = Vector2(0, -10);
			yArrowCol->isTrigger = true;
			yArrowCol->showOutline = false;
			yArrowCol->Start();

			GameObject* xArrow = new GameObject("ScaleTool_XArrow");
			xArrow->transform->SetParent(m_ScaleToolObj->transform);
			xArrow->transform->localPosition = Vector2(25, 0);
			xArrow->tag = EDITOR_IGNORE_TAG;
			xArrow->layer = "Editor_ScaleTool";
			xArrow->transform->localEulerAngles = 90;
			xArrow->AddComponent<SpriteRenderer>("gui_red_box_arrow.png").order = INT_MAX;
			xArrow->AddComponent<BoxCollider>();
			BoxCollider* xArrowCol = &xArrow->GetComponent<BoxCollider>();
			xArrowCol->size = Vector2(10, 20);
			xArrowCol->offset = Vector2(0, -10);
			xArrowCol->isTrigger = true;
			xArrowCol->showOutline = false;
			xArrowCol->Start();

			GameObject* xyPlane = new GameObject("ScaleTool_XYPlane");
			xyPlane->transform->SetParent(m_ScaleToolObj->transform);
			xyPlane->tag = EDITOR_IGNORE_TAG;
			xyPlane->layer = "Editor_ScaleTool";
			xyPlane->transform->localEulerAngles = 90;
			xyPlane->transform->localScale = Vector2(0.5f, 0.5f);
			xyPlane->AddComponent<SpriteRenderer>("gui_white_plane.png").order = INT_MAX;
			xyPlane->AddComponent<BoxCollider>();
			BoxCollider* planeCol = &xyPlane->GetComponent<BoxCollider>();
			planeCol->size = Vector2(25, 25);
			planeCol->isTrigger = true;
			planeCol->showOutline = false;
			xyPlane->Start();
		}

		m_ScaleToolObj->transform->SetPosition(m_CurrentSelectObj->transform->GetPosition());
		m_ScaleToolObj->transform->localEulerAngles = m_CurrentSelectObj->transform->localEulerAngles;
	}

	void ToolBar::FetchInput()
	{
		// Change tools according to user input.
		if (Input::GetKeyDown(SDL_SCANCODE_Q))
			m_ToolMode = ToolMode::Hand;
		if (Input::GetKeyDown(SDL_SCANCODE_W))
			m_ToolMode = ToolMode::Move;
		if (Input::GetKeyDown(SDL_SCANCODE_E))
			m_ToolMode = ToolMode::Rotate;
		if (Input::GetKeyDown(SDL_SCANCODE_R))
			m_ToolMode = ToolMode::Scale;

		Vector2 mousePos = Input::GetMousePosition();
		SDL_Rect scene_rect = *SceneWindow::GetInstance().GetSceneView();

		Vector2 mousePosOffset = Vector2(mousePos.x - scene_rect.x + m_MouseDownOffsetFromObj.x, mousePos.y - scene_rect.y + m_MouseDownOffsetFromObj.y);
		mousePosOffset = SceneWindow::GetInstance().WorldToScreenPoint(mousePosOffset);


		if (Input::GetMouseButtonDown(SDL_BUTTON_LEFT))
		{

			std::vector<Collider*> allOverlaps;
			if (ColliderManager::GetInstance().OverlapPoint(&allOverlaps, mousePosOffset))
			{
				for (size_t i = 0; i < allOverlaps.size(); i++)
				{
					GameObject* gameObject = dynamic_cast<GameObject*>(allOverlaps[i]->gameObject);
					if (gameObject->tag == EDITOR_IGNORE_TAG)
					{
						m_CurrentSelectToolObj = gameObject;
						m_MouseDownPos = mousePosOffset;
						m_MouseDownOffsetFromObj = m_CurrentSelectObj->transform->GetPosition() - mousePosOffset;
						break;
					}
				}
			}

			if (m_CurrentSelectToolObj != nullptr)
			{
				// Move tool
				if (m_CurrentSelectToolObj->name == "MoveTool_XYPlane" || m_CurrentSelectToolObj->name == "MoveTool_YArrow" || m_CurrentSelectToolObj->name == "MoveTool_XArrow")
				{
					m_InitialPosVal = m_CurrentSelectObj->transform->localPosition;
				}
				// Rotate Tool
				else if (m_CurrentSelectToolObj->name == "RotateTool_Wheel")
				{
					Vector2 selectObjPos = m_CurrentSelectObj->transform->GetPosition();
					Vector2 aim_dir = Vector2(selectObjPos.x + scene_rect.x - mousePos.x, selectObjPos.y + scene_rect.y - mousePos.y);
					aim_dir.Normalize();
					m_MouseDownAngleToObject = (atan2(aim_dir.y, aim_dir.x) * 180.0f / M_PI) + 90;
					m_InitialAngleVal = m_CurrentSelectObj->transform->localEulerAngles;
				}
				// Scale Tool
				else if (m_CurrentSelectToolObj->layer == "Editor_ScaleTool")
				{
					m_InitialScaleVal = m_CurrentSelectObj->transform->localScale;
				}
			}
		}
		else if (Input::GetMouseButton(SDL_BUTTON_LEFT) && m_CurrentSelectToolObj != nullptr)
		{
			// Move tool
			if (m_CurrentSelectToolObj->name == "MoveTool_XYPlane")
			{
				m_CurrentSelectObj->transform->SetPosition(mousePosOffset);
			}
			else if (m_CurrentSelectToolObj->name == "MoveTool_YArrow")
			{
				m_CurrentSelectObj->transform->SetPosition(Vector2(m_CurrentSelectObj->transform->GetPosition().x, mousePosOffset.y));
			}
			else if (m_CurrentSelectToolObj->name == "MoveTool_XArrow")
			{
				m_CurrentSelectObj->transform->SetPosition(Vector2(mousePosOffset.x, m_CurrentSelectObj->transform->GetPosition().y));
			}
			// Rotate Tool
			else if (m_CurrentSelectToolObj->name == "RotateTool_Wheel")
			{
				Vector2 selectObjPos = m_CurrentSelectObj->transform->GetPosition();
				Vector2 aim_dir = Vector2(selectObjPos.x - mousePosOffset.x, selectObjPos.y - mousePosOffset.y);
				aim_dir.Normalize();
				float rotate_value = (atan2(aim_dir.y, aim_dir.x) * 180.0f / M_PI) + 90 - m_MouseDownAngleToObject;
				m_RotateToolObj->transform->localEulerAngles = rotate_value;
				m_CurrentSelectObj->transform->localEulerAngles = rotate_value + m_InitialAngleVal;
			}
			// Scale Tool
			else if (m_CurrentSelectToolObj->layer == "Editor_ScaleTool")
			{
				Vector2 tunedValue = ((mousePosOffset - m_MouseDownPos - m_MouseDownOffsetFromObj) * m_ScaleToolSensitivity);
				if (m_CurrentSelectToolObj->name == "ScaleTool_XYPlane")
				{
					float dst = (tunedValue.x + tunedValue.y);
					m_CurrentSelectObj->transform->localScale = m_InitialScaleVal + Vector2(dst, dst);
				}
				else if (m_CurrentSelectToolObj->name == "ScaleTool_YArrow")
				{
					m_CurrentSelectObj->transform->localScale.y = m_InitialScaleVal.y - tunedValue.y;
				}
				else if (m_CurrentSelectToolObj->name == "ScaleTool_XArrow")
				{
					m_CurrentSelectObj->transform->localScale.x = m_InitialScaleVal.x + tunedValue.x;
				}
			}
		}
		else if (Input::GetMouseButtonUp(SDL_BUTTON_LEFT))
		{
			if (m_CurrentSelectToolObj != nullptr)
			{
				// Move tool
				if (m_CurrentSelectToolObj->name == "MoveTool_XYPlane" || m_CurrentSelectToolObj->name == "MoveTool_YArrow" || m_CurrentSelectToolObj->name == "MoveTool_XArrow")
				{
					History::GetInstance().AddHistory<Vector2>(&m_CurrentSelectObj->transform->localPosition, m_InitialPosVal, m_CurrentSelectObj->transform->localPosition);
				}
				// Rotate Tool
				else if (m_CurrentSelectToolObj->name == "RotateTool_Wheel")
				{
					History::GetInstance().AddHistory<float>(&m_CurrentSelectObj->transform->localEulerAngles, m_InitialAngleVal, m_CurrentSelectObj->transform->localEulerAngles);
					m_RotateToolObj->transform->localEulerAngles = 0;
				}
				// Scale Tool
				else if (m_CurrentSelectToolObj->layer == "Editor_ScaleTool")
				{
					History::GetInstance().AddHistory<Vector2>(&m_CurrentSelectObj->transform->localScale, m_InitialScaleVal, m_CurrentSelectObj->transform->localScale);
				}

				Editor::GetInstance().TriggerSceneChange();
			}

			m_CurrentSelectToolObj = nullptr;
			m_MouseDownPos = Vector2();
			m_MouseDownOffsetFromObj = Vector2();

		}
	}

	void ToolBar::ClearTools()
	{
		if (m_MoveToolObj != nullptr)
		{
			m_MoveToolObj->Destroy();
			m_MoveToolObj = nullptr;
		}
		if (m_RotateToolObj != nullptr)
		{
			m_RotateToolObj->Destroy();
			m_RotateToolObj = nullptr;
		}
		if (m_ScaleToolObj != nullptr)
		{
			m_ScaleToolObj->Destroy();
			m_ScaleToolObj = nullptr;
		}
	}

	void ToolBar::ShowWindow()
	{
		// Mainmenu tool bar
		ImVec2 size = ImVec2(20.0f, 20.0f);
		ImGui::Spacing();
		ImGui::SameLine(0, 10);
		ShowToolButton("0203_square.png", size, ToolMode::Hand);
		ImGui::SameLine();
		ShowToolButton("0150_move.png", size, ToolMode::Move);
		ImGui::SameLine();
		ShowToolButton("0181_rotate-cw.png", size, ToolMode::Rotate);
		ImGui::SameLine();
		ShowToolButton("0134_maximize-2.png", size, ToolMode::Scale);

		// Middle section
		ImGui::SameLine((SCREEN_WIDTH / 2) - (size.x / 2));

		bool played = Editor::GetInstance().GetPlayMode();

		if (played == true)
		{
			ImGui::PushStyleColor(ImGuiCol_Button, m_SelectedCol);
			ImGui::PushStyleColor(ImGuiCol_ButtonActive, m_SelectedCol);
			ImGui::PushStyleColor(ImGuiCol_ButtonHovered, m_SelectedCol);
		}

		if (ImGui::ImageButton(AssetManager::GetInstance().GetTexture(m_TogglePlayPath), size, ImVec2(0, 0), ImVec2(1, 1), -1, ImVec4(0, 0, 0, 0), ImVec4(1, 1, 1, 1)))
		{
			// Toggle play
			Editor::GetInstance().SetPlayMode(!played);

			if (Editor::GetInstance().GetPlayMode() == true)
			{
				m_TogglePlayPath = "0154_octagon.png";
			}
			else
			{
				m_TogglePlayPath = "0169_play.png";
			}
		}
		if (played == true)
		{
			ImGui::PopStyleColor(3);
		}

		ImGui::SameLine();

		bool paused = Editor::GetInstance().GetPauseMode();

		if (paused == true)
		{
			ImGui::PushStyleColor(ImGuiCol_Button, m_SelectedCol);
			ImGui::PushStyleColor(ImGuiCol_ButtonActive, m_SelectedCol);
			ImGui::PushStyleColor(ImGuiCol_ButtonHovered, m_SelectedCol);
		}
		if (ImGui::ImageButton(AssetManager::GetInstance().GetTexture("0158_pause.png"), size, ImVec2(0, 0), ImVec2(1, 1), -1, ImVec4(0, 0, 0, 0), ImVec4(1, 1, 1, 1)))
		{
			Editor::GetInstance().SetPauseMode(!paused);
		}
	
		if (paused == true)
		{
			ImGui::PopStyleColor(3);
		}

	}

	void ToolBar::ShowOverlayTool()
	{
		m_CurrentSelectObj = Hierarchy::GetInstance().GetSelectedObject();

		if (m_CurrentSelectObj == nullptr)
			return;

		FetchInput();

		if (m_MoveToolObj != nullptr)
			m_MoveToolObj->SetActive(m_ToolMode == ToolMode::Move);
		if (m_RotateToolObj != nullptr)
			m_RotateToolObj->SetActive(m_ToolMode == ToolMode::Rotate);
		if (m_ScaleToolObj != nullptr)
			m_ScaleToolObj->SetActive(m_ToolMode == ToolMode::Scale);

		if (m_ToolMode == ToolMode::Move)
			ShowMoveTool();
		if (m_ToolMode == ToolMode::Rotate)
			ShowRotateTool();
		if (m_ToolMode == ToolMode::Scale)
			ShowScaleTool();
	}


}

