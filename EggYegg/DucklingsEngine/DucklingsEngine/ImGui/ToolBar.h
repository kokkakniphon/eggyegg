#pragma once

#include <string>

#include "Singleton.h"
#include "imgui.h"

#include "GameObject.h"

namespace DucklingsEngine
{
	enum ToolMode
	{
		Hand,
		Move,
		Rotate,
		Scale
	};

	class ToolBar : public Singleton<ToolBar>
	{
	private:
		ImVec4 m_SelectedCol = ImVec4(0.26f, 0.59f, 0.98f, 1.00f);

		std::string m_TogglePlayPath = "0169_play.png";
		ToolMode m_ToolMode;

		GameObject* m_MoveToolObj;
		GameObject* m_RotateToolObj;
		GameObject* m_ScaleToolObj;
		GameObject* m_CurrentSelectToolObj;

		// For history sake.
		Vector2 m_InitialPosVal;
		float m_InitialAngleVal;
		Vector2 m_InitialScaleVal;

		GameObject* m_CurrentSelectObj;

		float m_ScaleToolSensitivity = 0.02f;

		Vector2 m_MouseDownPos;
		Vector2 m_MouseDownOffsetFromObj;
		float m_MouseDownAngleToObject;

		void ShowToolButton(std::string toolName, ImVec2 size, ToolMode toolMode);

		void ShowMoveTool();
		void ShowRotateTool();
		void ShowScaleTool();

		void FetchInput();

	public:

		void ClearTools();

		void ShowWindow();

		void ShowOverlayTool();
		
		void SetToolMode(ToolMode toolMode) { m_ToolMode = toolMode; }
		ToolMode GetToolMode() { return m_ToolMode; }
	};
}

