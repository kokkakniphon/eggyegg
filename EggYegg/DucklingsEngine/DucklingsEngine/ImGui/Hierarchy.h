#pragma once

#include "Singleton.h"
#include "imgui.h"

#include "GameObject.h"

namespace DucklingsEngine
{
	class Hierarchy : public Singleton<Hierarchy>
	{
	private:
		int m_SelectionMask;
		GameObject* m_SelectedObj;

		void DisplayGameObjectNameUsingTreeNode(GameObject& source, std::vector<GameObject*>& objects);

	public:

		void ShowWindow();
		GameObject* GetSelectedObject() { return m_SelectedObj; }
	};
}

