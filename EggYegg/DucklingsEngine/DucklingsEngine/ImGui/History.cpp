#include "History.h"

namespace DucklingsEngine
{
	struct Undo
	{
		void operator()(HistoryBlock<bool>& val) { val.Undo(); }
		void operator()(HistoryBlock<int>& val) { val.Undo(); }
		void operator()(HistoryBlock<float>& val) { val.Undo(); }
		void operator()(HistoryBlock<std::string>& val) { val.Undo(); }
		void operator()(HistoryBlock<Vector2>& val) { val.Undo(); }
		void operator()(HistoryBlock<Color>& val) { val.Undo(); }
	};

	struct Redo
	{
		void operator()(HistoryBlock<bool>& val) { val.Redo(); }
		void operator()(HistoryBlock<int>& val) { val.Redo(); }
		void operator()(HistoryBlock<float>& val) { val.Redo(); }
		void operator()(HistoryBlock<std::string>& val) { val.Redo(); }
		void operator()(HistoryBlock<Vector2>& val) { val.Redo(); }
		void operator()(HistoryBlock<Color>& val) { val.Redo(); }
	};

	void History::ClearHistory()
	{
		m_HistoryBlocks.clear();
	}

	void History::UndoHistory()
	{
		if (m_CurrentHistoryHead >= 0)
			std::visit(Undo(), m_HistoryBlocks[m_CurrentHistoryHead]);

		if (m_CurrentHistoryHead > 0)
			m_CurrentHistoryHead--;
	}

	void History::RedoHistory()
	{
		if (m_CurrentHistoryHead < m_HistoryBlocks.size())
			std::visit(Redo(), m_HistoryBlocks[m_CurrentHistoryHead]);

		if (m_CurrentHistoryHead < m_HistoryBlocks.size() - 1)
			m_CurrentHistoryHead++;
	}
}