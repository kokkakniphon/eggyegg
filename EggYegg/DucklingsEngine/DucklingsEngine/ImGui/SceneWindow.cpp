#include "SceneWindow.h"

#include "imgui_internal.h"

#include "Engine.h"
#include "Editor.h"

#include "Camera.h"
#include "Input.h"

namespace DucklingsEngine
{
	void SceneWindow::FetchInput()
	{
		if (Input::GetMouseButtonDown(SDL_BUTTON_MIDDLE))
		{
			m_InitialViewCenter = m_ViewCenter;
			m_MouseDownPos = Input::GetMousePosition();
		}
		else if (Input::GetMouseButton(SDL_BUTTON_MIDDLE))
		{
			Vector2 mousePos;
			mousePos = Input::GetMousePosition();
			
			m_ViewCenter = m_InitialViewCenter - (mousePos - m_MouseDownPos);
		}

		if (Engine::event.wheel.y == 1 || Engine::event.wheel.y == -1)
		{
			m_ViewRect.x += -Engine::event.wheel.y * m_ZoomSensitivity;
			m_ViewRect.y += -Engine::event.wheel.y * m_ZoomSensitivity;
		}
		Engine::event.wheel.y = 0;
	}

	void SceneWindow::SetupSceneRect()
	{
		ImGuiWindow *window = ImGui::GetCurrentWindow();

		m_SceneView.x = window->ContentRegionRect.ToVec4().x;
		m_SceneView.y = window->ContentRegionRect.ToVec4().y;
		m_SceneView.w = window->Size.x - window->WindowPadding.x * 2;
		m_SceneView.h = window->Size.y - window->WindowPadding.y * 4;

		m_WindowActive = true;
	}
	void SceneWindow::ShowWindow()
	{
		if (ImGui::Begin("Scene"))
		{
			FetchInput();

			SetupSceneRect();
		}
		ImGui::End();
	}

	void SceneWindow::Render()
	{
		if (m_WindowActive == false)
			return;

		SDL_RenderSetViewport(Engine::GetInstance().GetRenderer(), &m_SceneView);
		float getScale = GetAspectRatio();
		SDL_RenderSetScale(Engine::GetInstance().GetRenderer(), getScale, getScale);

		RendererManager::GetInstance().Draw(m_ViewCenter, m_ViewRect);
		ColliderManager::GetInstance().OnDrawGizmos(m_ViewCenter, m_ViewRect);
		ColliderManager::GetInstance().EndFrameUpdate();
		Camera::mainCamera->OnDrawGizmos(m_ViewCenter, m_ViewRect);

		m_WindowActive = false;
	}

	float SceneWindow::GetAspectRatio(bool invert)
	{
		if (invert)
			return m_ViewRect.x / (float)SCREEN_WIDTH;
		else
			return (float)SCREEN_WIDTH / m_ViewRect.x;
	}

	Vector2 SceneWindow::WorldToScreenPoint(Vector2 position, bool invert)
	{
		if (invert)
		{
			position = position * GetAspectRatio();
			position += (m_ViewRect / 2) - m_ViewCenter;
		}
		else
		{
			position = position * GetAspectRatio(true);
			position -= (m_ViewRect / 2) - m_ViewCenter;
		}

		return position;
	}
}
