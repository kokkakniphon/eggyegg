#pragma once

#include <string>
#include <vector>

#include "Singleton.h"

#include "imgui.h"
#include "imgui_stdlib.h"

class Component;

namespace DucklingsEngine 
{
	class Inspector : public Singleton<Inspector>
	{
	private:
		std::vector<std::string> m_FontList;
		std::vector<std::string> m_SpriteRenderType = {"Stretch", "Slice"};

	public:
		void LoadFont();
		void ShowWindow();

	};
}

