#include "AssetWindow.h"
#include "AssetManager.h"
#include <iostream>
#include <direct.h>

#include "Editor.h"

namespace DucklingsEngine
{
	AssetWindow* AssetWindow::s_Instance = nullptr;

	enum fileTypes {
		image = 0,
		folder,
		etc
	};

	void AssetWindow::ShowFileBrowser(bool* p_open)
	{
		ImGui::PushItemWidth(ImGui::GetFontSize() * -12);
		if (!ImGui::Begin("Asset", p_open, ImGuiWindowFlags_NoCollapse))
		{
			ImGui::End();
			return;
		}

		//Load all file in GUI folder.
		std::string pattern(s_AssetPath);
		pattern.append("\\*");
		WIN32_FIND_DATA data;
		HANDLE hFind;

		static std::vector<std::string> fileNames;
		static std::vector<fileTypes> fileType;
		static std::vector<ImTextureID> icons;

		int frame_padding = -1;

		ImGuiStyle& style = ImGui::GetStyle();
		ImVec2 button_sz(50, 50);
		ImVec2 size = ImVec2(50.0f, 50.0f);
		float window_visible_x2 = ImGui::GetWindowPos().x + ImGui::GetWindowContentRegionMax().x;

		if (ImGui::Button("Refresh") || refresh == true)
		{
			std::cout << "Refresh" << std::endl;
			refresh = false;
			int i = 0;
			fileNames.clear();
			fileType.clear();
			icons.clear();

			//Reload images to create icon textures
			if ((hFind = FindFirstFile(pattern.c_str(), &data)) != INVALID_HANDLE_VALUE) {
				do {
					std::string fileName = data.cFileName;
					//if it's jpeg or png, load texture
					if (fileName == ".") continue;
					else if (fileName.substr(fileName.find_last_of(".") + 1) == "png"
						|| fileName.substr(fileName.find_last_of(".") + 1) == "jpeg"
						|| fileName.substr(fileName.find_last_of(".") + 1) == "jpg")
					{
						//remove file extension
						size_t lastindex = fileName.find_last_of(".");
						std::string rawname = fileName.substr(0, lastindex);
						//load texture and assign rawname as ID
						AssetManager::GetInstance().LoadTexture(rawname, s_AssetPath);
						icons.push_back(AssetManager::GetInstance().GetTexture(rawname));
						fileNames.push_back(fileName);
						fileType.push_back(image);
					}
					else if ((data.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) != 0) //is a directory, display folder icon
					{
						icons.push_back(AssetManager::GetInstance().GetTexture("Folder.png"));
						fileType.push_back(folder);
						fileNames.push_back(fileName);
					}
					i++;
				} while (FindNextFile(hFind, &data) != 0);
				FindClose(hFind);
			}
		}

		//Display images
		for (int i = 0; i < fileNames.size(); i++)
		{
			//ImGui::GetForegroundDrawList()->AddRect(ImGui::GetItemRectMin(), ImGui::GetItemRectMax(), IM_COL32(255, 255, 0, 255));
			ImGui::PushID(i);

			if (ImGui::ImageButton(icons[i], size))
			{
				if (fileType[i] == folder) //update display path
				{
					//add folder name at the back
					if (fileNames[i] == "..")
					{
						//prevent user from going back outside texture folder
						if (s_AssetPath != startingPath)
						{
							s_AssetPath.pop_back();
							size_t lastindex = s_AssetPath.find_last_of("/");
							std::string backPath = s_AssetPath.substr(0, lastindex + 1);
							s_AssetPath = backPath;

							refresh = true;
						}
					}
					else
					{
						std::cout << "filename: " << fileNames[i] << std::endl;
						s_AssetPath.append(fileNames[i] + "/");
						refresh = true;
					}
				}
				else if (fileType[i] == image)
				{
					size_t lastindex = fileNames[i].find_last_of(".");
					std::string rawname = fileNames[i].substr(0, lastindex);
					imageInfo = AssetManager::GetInstance().GetTexture(rawname);

					name = fileNames[i];

					clickedImage = icons[i];

					Editor::GetInstance().SetCurrentItemType(SelectableItemType::Item_SDL_Texture);
				}
			}
			if (ImGui::IsItemHovered())
			{
				ImGui::SetTooltip(fileNames[i].c_str());
			}
			float last_button_x2 = ImGui::GetItemRectMax().x;
			float next_button_x2 = last_button_x2 + style.ItemSpacing.x + button_sz.x; // Expected position if next button was on same line
			if (i + 1 < fileNames.size() && next_button_x2 < window_visible_x2)
			{
				ImGui::SameLine();
			}
			else {
				//NewLine (ImGui defualt) and the display file name under the icon
				ImGui::Spacing();
			}
			ImGui::PopID();
		}

		ImGui::End();
	}

	void AssetWindow::ShowWindow()
	{
		ShowFileBrowser();
	}

	ImTextureID AssetWindow::GetTexture()
	{
		return clickedImage;
	}

	SDL_Texture* AssetWindow::GetSDLTex()
	{
		return imageInfo;
	}

	std::string AssetWindow::GetFilename()
	{
		return name;
	}
}