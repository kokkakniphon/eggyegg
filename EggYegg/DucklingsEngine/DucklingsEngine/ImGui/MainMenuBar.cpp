#include "MainMenuBar.h"

#include "Input.h"
#include "SceneManager.h"
#include "Editor.h"

namespace DucklingsEngine
{
	void MainMenuBar::ShowMenuFile()
	{
		if (ImGui::BeginMenu("	Open Scene		"))
		{
			std::vector<Scene*> allScene = SceneManager::GetInstance().GetScenesInBuildSettings();
			for (size_t i = 0; i < allScene.size(); i++)
			{
				if (allScene[i] == nullptr)
					continue;

				if (ImGui::MenuItem(("	" + allScene[i]->name + "	").c_str()))
				{
					Editor::GetInstance().OpenScene(allScene[i]);
				}
			}
			ImGui::EndMenu();
		}
		ImGui::Separator();
		if (ImGui::MenuItem("	Save		Ctrl+ S"))
		{
			Editor::GetInstance().SaveScene();
		}
		ImGui::Separator();
		if (ImGui::MenuItem("	Exit		ESC"))
		{
			Editor::GetInstance().Quit();
		}
	}

	void MainMenuBar::ShowMenuEdit()
	{
		if (ImGui::MenuItem("	Undo		Ctrl+ Z"))
		{
			History::GetInstance().UndoHistory();
		}

		if (ImGui::MenuItem("	Redo		Ctrl+ Shift+ Z"))
		{
			History::GetInstance().RedoHistory();
		}
	}

	void MainMenuBar::FetchInput()
	{
		// Save Scane
		if (Input::GetKey(SDL_SCANCODE_LCTRL) && Input::GetKeyDown(SDL_SCANCODE_S))
		{
			Editor::GetInstance().SaveScene();
		}

		// Redo
		else if (Input::GetKey(SDL_SCANCODE_LCTRL) && Input::GetKey(SDL_SCANCODE_LSHIFT) && Input::GetKeyDown(SDL_SCANCODE_Z))
		{
			History::GetInstance().RedoHistory();
		}

		// Undo
		else if (Input::GetKey(SDL_SCANCODE_LCTRL) && Input::GetKeyDown(SDL_SCANCODE_Z))
		{
			History::GetInstance().UndoHistory();
		}

		// Save Scane
		else if (Input::GetKeyDown(SDL_SCANCODE_ESCAPE))
		{
			Editor::GetInstance().Quit();
		}

	}

	void MainMenuBar::ShowWindow()
	{
		if (ImGui::BeginMenuBar())
		{
			FetchInput();

			if (ImGui::BeginMenu("File"))
			{
				ShowMenuFile();
				ImGui::EndMenu(); // File
			}

			if (ImGui::BeginMenu("Edit"))
			{
				ShowMenuEdit();
				ImGui::EndMenu(); // Edit
			}

			ImGui::EndMenuBar();
		}
	}
}