#include "Hierarchy.h"

#include "Editor.h"
#include "Input.h"

namespace DucklingsEngine
{
	Hierarchy* Hierarchy::s_Instance = nullptr;

	void Hierarchy::DisplayGameObjectNameUsingTreeNode(GameObject& source, std::vector<GameObject*>& objects)
	{
		ImGuiTreeNodeFlags base_flags = ImGuiTreeNodeFlags_OpenOnArrow | ImGuiTreeNodeFlags_OpenOnDoubleClick | ImGuiTreeNodeFlags_SpanAvailWidth;
		int node_clicked = -1;
		ImGuiTreeNodeFlags node_flags = base_flags;

		std::vector<GameObject*> childs;
		for (size_t i = 0; i < objects.size(); i++)
		{
			if (source.GetUniqueID() == objects[i]->GetUniqueID())
				continue;

			GameObject* gameObject = dynamic_cast<GameObject*>(objects[i]);
			if (gameObject->transform->GetParent()->gameObject->GetUniqueID() == source.GetUniqueID())
			{
				childs.push_back(objects[i]);
			}
		}

		if (childs.size() == 0)
		{
			node_flags |= ImGuiTreeNodeFlags_Leaf | ImGuiTreeNodeFlags_NoTreePushOnOpen;
		}

		const bool is_selected = (m_SelectionMask == source.GetUniqueID());
		if (is_selected)
		{
			node_flags |= ImGuiTreeNodeFlags_Selected;
			m_SelectedObj = &source;
		}

		bool node_open = ImGui::TreeNodeEx(source.name.c_str(), node_flags);

		if (ImGui::IsItemClicked())
		{
			Editor::GetInstance().SetCurrentItemType(SelectableItemType::Item_GameObject);

			m_SelectionMask = source.GetUniqueID();
		}

		if (node_open)
		{
			for (size_t i = 0; i < childs.size(); i++)
			{
				DisplayGameObjectNameUsingTreeNode(*childs[i], objects);
			}
			if (childs.size() > 0)
				ImGui::TreePop();
		}
	}

	void Hierarchy::ShowWindow()
	{
		m_SelectedObj = nullptr;

		ImGuiTreeNodeFlags base_flags = ImGuiTreeNodeFlags_OpenOnArrow | ImGuiTreeNodeFlags_OpenOnDoubleClick | ImGuiTreeNodeFlags_SpanAvailWidth;
		int node_clicked = -1;

		if (ImGui::Begin("Hierarchy"))
		{
			std::vector<Scene*> allScenes = SceneManager::GetInstance().GetLoadedScenes();
			for (size_t i = 0; i < allScenes.size(); i++)
			{
				if (allScenes[i] != nullptr)
				{
					if (ImGui::CollapsingHeader((((Editor::GetInstance().IsSceneUnsaved()) ? "*" : "") + allScenes[i]->name).c_str(), ImGuiTreeNodeFlags_DefaultOpen))
					{
						std::vector<GameObject*> objects = Engine::GetInstance().GetManager()->GetEntitysInScene(allScenes[i]->name);

						// Loop thought all root gameobject with no parent
						for (size_t i = 0; i < objects.size(); i++)
						{
							if (objects[i]->tag == EDITOR_IGNORE_TAG)
								continue;

							GameObject* gameObject = objects[i];
							if (gameObject->transform->GetParent()->gameObject->GetUniqueID() == gameObject->GetUniqueID())
							{
								DisplayGameObjectNameUsingTreeNode(*objects[i], objects);
							}
						}
					}
				}
			}
			
		}
		ImGui::End();
	}
}
