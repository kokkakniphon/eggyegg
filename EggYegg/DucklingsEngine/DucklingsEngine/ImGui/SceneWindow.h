#pragma once

#include "Singleton.h"

#include "Vector2D.h"

#include "imgui.h"
#include "imgui_sdl.h"

#include "SDL.h"
#include "SDL_image.h"

namespace DucklingsEngine
{
	class SceneWindow : public Singleton<SceneWindow>
	{
	private:

		bool m_WindowActive;

		SDL_Rect m_SceneView;

		Vector2 m_ViewCenter = Vector2(960, 540);
		Vector2 m_ViewRect = Vector2(1920, 1080);

		float m_ZoomSensitivity = 25.0f;

		float m_LastWheelValue;
		Vector2 m_InitialViewCenter;
		Vector2 m_MouseDownPos;

		void FetchInput();
		void SetupSceneRect();

	public:

		void ShowWindow();
		void Render();

		float GetAspectRatio(bool invert = false);
		Vector2 GetViewRect() { return m_ViewRect; }
		SDL_Rect* GetSceneView() { return &m_SceneView; }

		//
		// Summary:
		//     Transforms position from world space into screen space.
		//
		// Parameters:
		//   position:
		Vector2 WorldToScreenPoint(Vector2 position, bool invert = false);

	};
}
