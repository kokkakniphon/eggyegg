#pragma once

#include <string>
#include <vector>
#include <variant>
#include <type_traits>

#include "Singleton.h"
#include "Vector2D.h"
#include "Color.h"

namespace DucklingsEngine
{
	constexpr int MAX_HISTORY_SIZE = 30;

	template <typename T>
	struct HistoryBlock
	{
		T *m_Ptr;
		T m_Before, m_After;

		HistoryBlock(T* ptr, T before, T after) : m_Ptr(ptr), m_Before(before), m_After(after) {}

		void Undo()
		{
			if (m_Ptr != nullptr)
				*m_Ptr = m_Before;
		}

		void Redo()
		{
			if (m_Ptr != nullptr)
				*m_Ptr = m_After;
		}
	};

	class History : public Singleton<History>
	{
	private:
		std::vector<std::variant<HistoryBlock<bool>, HistoryBlock<int>, HistoryBlock<float>, HistoryBlock<std::string>, HistoryBlock<Vector2>, HistoryBlock<Color>>> m_HistoryBlocks;
		

		int m_CurrentHistoryHead;

	public:

		template <typename T>
		void AddHistory(T* ptr, T before, T after)
		{
			unsigned int historySize = m_HistoryBlocks.size();
			if (historySize > 0 && m_CurrentHistoryHead != historySize - 1)
			{
				for (size_t i = 0; i < historySize - m_CurrentHistoryHead; i++)
				{
					m_HistoryBlocks.pop_back();
				}
			}

			historySize = m_HistoryBlocks.size();
			if (MAX_HISTORY_SIZE < historySize)
			{
				for (size_t i = 0; i < historySize - MAX_HISTORY_SIZE; i++)
				{
					m_HistoryBlocks.erase(m_HistoryBlocks.begin());
				}
			}

			m_HistoryBlocks.push_back(HistoryBlock(ptr, before, after));
			m_CurrentHistoryHead = m_HistoryBlocks.size() - 1;
		}
		void ClearHistory();

		void UndoHistory();
		void RedoHistory();
	};
}
