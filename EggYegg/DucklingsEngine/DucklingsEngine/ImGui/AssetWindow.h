#pragma once

#define NOMINMAX
#include <windows.h>
#include <vector>
#include <string>

#include "Singleton.h"
#include "imgui.h"
#include "imgui_stdlib.h"
#include "AssetManager.h"

static std::string s_AssetPath = "../Assets/Textures/";

namespace DucklingsEngine
{
	class AssetWindow : public Singleton<AssetWindow>
	{
	private:

		void ShowFileBrowser(bool* p_open = NULL);
		const std::string startingPath = "../Assets/Textures/";
		const std::string s_GuiPath = "../Assets/GUI/";

		std::vector<std::string> fileNames;
		std::vector<unsigned int> fileType;
		std::vector<ImTextureID> icons;

		bool refresh = true;

		ImTextureID clickedImage = NULL;
		SDL_Texture* imageInfo = nullptr;
		std::string name = "";

	public:

		void ShowWindow();

		ImTextureID GetTexture();

		SDL_Texture* GetSDLTex();

		std::string GetFilename();
	};
}

