#pragma once

#include <vector>

#include "Singleton.h"
#include "imgui.h"

#include "History.h"

namespace DucklingsEngine
{
	class MainMenuBar : public Singleton<MainMenuBar>
	{
	private:
		void ShowMenuFile();
		void ShowMenuEdit();

		void FetchInput();

	public:
		void ShowWindow();
	};

}

