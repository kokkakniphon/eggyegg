#pragma once

#include "AssetManager.h"
#include <SDL.h>
#include "Vector2D.h"
#include <iostream>

namespace DucklingsEngine
{
	struct Sprite
	{
	private:
		//
		// Summary:
		//     Current cell of sprites in sprite sheet.
		int currentSpriteCell = 0;

	public:
		//
		// Summary:
		//     Location of the Sprite's center point in the Rect on the original Texture, specified
		//     in pixels.
		Vector2 pivot = Vector2(0.5f, 0.5f);
		//
		// Summary:
		//     Location of the Sprite on the original, specified in pixels.
		SDL_Rect rect = { 0, 0, 0, 0 };
		//
		// Summary:
		//     Is it a sprite sheet.
		bool isSpriteSheet = false;
		//
		// Summary:
		//     Number of colume in sprite sheet.
		int spriteSheetColume = 0;
		//
		// Summary:
		//     Number of sprites in sprite sheet.
		int spriteSheetCellCount = 0;
		
		//
		// Summary:
		//     Set current cell of sprites in sprite sheet.
		void SetCurrentSpriteCell(int value) { if (isSpriteSheet == true) { this->currentSpriteCell = value % spriteSheetCellCount; } }
		//
		// Summary:
		//     Return current cell of sprites in sprite sheet.
		int GetCurrentSpriteCell() { return currentSpriteCell; }

		//
		// Summary:
		//     The number of pixels in the sprite that correspond to one unit in world space.
		float pixelsPerUnit;
		//
		// Summary:
		//     The Unique sprite id that can be loaded from SpriteManager. 
		std::string spriteID = "";
		//
		// Summary:
		//     The Unique texture id that can be loaded from AssetManager. 
		std::string textureID = "";
		//
		// Summary:
		//     Get the reference to the used texture. Point to the source sprite.
		SDL_Texture* texture = nullptr;

		Sprite() {}
		//
		// Summary:
		//     Create a new Sprite object, Load texture and assign to texture.
		Sprite(std::string textureID, bool isSpriteSheet = false, int spriteSheetColume = 0, int spriteSheetCellCount = 0)
		{
			this->textureID = textureID;
			this->isSpriteSheet = isSpriteSheet;
			this->spriteSheetColume = spriteSheetColume;
			this->spriteSheetCellCount = spriteSheetCellCount;
			this->currentSpriteCell = 0;

			texture = AssetManager::GetInstance().GetTexture(this->textureID);
			SDL_QueryTexture(texture, nullptr, nullptr, &rect.w, &rect.h);

			rect.x = 0;
			rect.y = 0;

			if (isSpriteSheet == true)
			{
				if (spriteSheetColume != 0)
				{
					rect.w /= spriteSheetColume;
					rect.h /= std::ceil((float)spriteSheetCellCount / (float)spriteSheetColume);
				}
				else
				{
					std::cout << "ERROR <SPRITE>: Trying to create a sprite sheet with ZERO colume." << std::endl;
				}
			}
		}
	};
}