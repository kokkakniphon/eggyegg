#pragma once

#include "Scene.h"
#include "Engine.h"

namespace DucklingsEngine
{
	class DefaultScene : public Scene
	{
	private:
		std::string sceneData = "";

	public:
		DefaultScene() { name = "DefaultScene"; }
		~DefaultScene() = default;

		void Start();

		inline void LoadScene() 
		{ 
			Scene::LoadScene(); 
			std::cout << "Loading Scene: " << name << std::endl;
			Start();
			Engine::GetInstance().GetManager()->Start();
		}

		inline bool UnloadScene()
		{
			std::cout << "Unload Scene: " << name << std::endl;

			std::vector<GameObject*> gameObjects = Engine::GetInstance().GetManager()->GetEntitysInScene(name);

			for (auto object : gameObjects)
			{
				object->Destroy();
			}

			return Scene::UnloadScene();
		}
	};
}

