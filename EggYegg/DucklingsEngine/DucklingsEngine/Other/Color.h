#pragma once

#include <sstream>
namespace DucklingsEngine
{
	struct Color
	{
		//
		// Summary:
		//     Red component of the color.
		float r;
		//
		// Summary:
		//     Green component of the color.
		float g;
		//
		// Summary:
		//     Blue component of the color.
		float b;
		//
		// Summary:
		//     Alpha component of the color (0 is transparent, 1 is opaque).
		float a;

		Color()
		{
			this->r = 1.0f;
			this->g = 1.0f;
			this->b = 1.0f;
			this->a = 1.0f;
		}

		//
		// Summary:
		//     Constructs a new Color with given r,g,b components and sets a to 1.
		//
		// Parameters:
		//   r:
		//     Red component.
		//
		//   g:
		//     Green component.
		//
		//   b:
		//     Blue component.
		Color(float r, float g, float b) 
		{
			this->r = r;
			this->g = g;
			this->b = b;
			this->a = 1.0f;
		}

		//
		// Summary:
		//     Constructs a new Color with given r,g,b,a components.
		//
		// Parameters:
		//   r:
		//     Red component.
		//
		//   g:
		//     Green component.
		//
		//   b:
		//     Blue component.
		//
		//   a:
		//     Alpha component.
		Color(float r, float g, float b, float a)
		{
			this->r = r;
			this->g = g;
			this->b = b;
			this->a = a;
		}

		//
		// Summary:
		//     Completely transparent. RGBA is (0, 0, 0, 0).
		inline static Color clear() { return Color(0,0,0,0); }
		//
		// Summary:
		//     Solid red. RGBA is (1, 0, 0, 1).
		inline static Color red() { return Color(1, 0, 0, 1); }
		//
		// Summary:
		//     Yellow. RGBA is (1, 0.92, 0.016, 1), but the color is nice to look at!
		inline static Color yellow() { return Color(1, 0.92f, 0.016f, 1); }
		//
		// Summary:
		//     Solid black. RGBA is (0, 0, 0, 1).
		inline static Color black() { return Color(0, 0, 0, 1); }
		//
		// Summary:
		//     Solid green. RGBA is (0, 1, 0, 1).
		inline static Color green() { return Color(0, 1, 0, 1); }
		//
		// Summary:
		//     Solid blue. RGBA is (0, 0, 1, 1).
		inline static Color blue() { return Color(0, 0, 1, 1); }

		//
		// Summary:
		//     Linearly interpolates between colors a and b by t.
		//
		// Parameters:
		//   a:
		//     Color a.
		//
		//   b:
		//     Color b.
		//
		//   t:
		//     Float for combining a and b.
		inline static Color Lerp(Color a, Color b, float t)
		{
			Color value = a + (b - a) * t;
			return value;
		}

		//
		// Summary:
		//     Returns a nicely formatted string of this color.
		//
		// Parameters:
		//   format:
		inline std::string ToString()
		{
			std::ostringstream value;
			value << "(" << this->r << ", " << this->g << ", " << this->b << ", " << this->a << ")";
			return value.str();
		}

		// + operator
		inline Color operator+(const Color& c) const
		{
			return Color(r + c.r, g + c.g, b + c.b, a + c.a);
		}

		// - operator
		inline Color operator-(const Color& c) const
		{
			return Color(r - c.r, g - c.g, b - c.b, a - c.a);
		}

		// * operator
		inline Color operator*(const float scalar) const
		{
			return Color(r * scalar, g * scalar, b * scalar, a * scalar);
		}
	};
}