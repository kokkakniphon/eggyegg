#pragma once

#include <sstream>
#include "Vector2D.h"

namespace DucklingsEngine
{
	template <typename T>
	struct Vector4D
	{
		//
		// Summary:
		//     X component of the vector.
		T x;
		//
		// Summary:
		//     Y component of the vector.
		T y;
		//
		// Summary:
		//     Z component of the vector.
		T z;
		//
		// Summary:
		//     W component of the vector.
		T w;

		//
		// Summary:
		//     Constructs a new vector with zero value on x, y, z, w components.
		//
		Vector4D() : x(0), y(0), z(0), w(0) {}
		//
		// Summary:
		//     Creates a new vector with given x, y, z, w components.
		//
		// Parameters:
		//   x:
		//
		//   y:
		//
		//   z:
		//
		//   w:
		Vector4D(T x, T y, T z, T w) { this->x = x; this->y = y; this->z = z; this->w = w; }
		
		//
		// Summary:
		//     Shorthand for writing Vector4(0,0,0,0).
		inline static Vector4D<T> zero()
		{
			return Vector4D(0.0f, 0.0f, 0.0f, 0.0f);
		}
		//
		// Summary:
		//     Shorthand for writing Vector4(1,1,1,1).
		inline static Vector4D<T> ones()
		{
			return Vector4D(1.0f, 1.0f, 1.0f, 1.0f);
		}

		//
		// Summary:
		//     Returns the squared length of this vector (Read Only).
		inline T sqrMagnitude()
		{
			return pow(this->x, 2) + pow(this->y, 2) + pow(this->z, 2) + pow(this->w, 2);
		}
		//
		// Summary:
		//     Returns the length of this vector (Read Only).
		inline T magnitude()
		{
			return sqrt(this->sqrMagnitude());
		}
		//
		// Summary:
		//     Returns this vector with a magnitude of 1 (Read Only).
		inline Vector4D<T>& normalized()
		{
			T mangitude = this->magnitude();
			this /= magnitude();
			return *this;
		}

		//
		// Summary:
		//     Returns the distance between a and b.
		//
		// Parameters:
		//   a:
		//
		//   b:
		inline static T Distance(Vector4D<T>& a, Vector4D<T>& b)
		{
			return (b - a)->magnitude();
		}
		//
		// Summary:
		//     Dot Product of two vectors.
		//
		// Parameters:
		//   a:
		//
		//   b:
		inline static T Dot(Vector4D<T>& a, Vector4D<T>& b)
		{
			return a.x * b.x + a.y * b.y + a.z * b.z + a.w * b.w;
		}

		//
		// Summary:
		//     Returns a nicely formatted string for this vector.
		//
		// Parameters:
		//   format:
		inline std::string ToString()
		{
			std::ostringstream value;
			value << "(" << this->x << "," << this->y << "," << this->z << "," << this->w << ")";
			return value.str();
		}

		// + operator
		inline Vector4D<T> operator+(const Vector4D<T>& v2) const
		{
			return Vector4D<T>(x + v2.x, y + v2.y, z + v2.z, w + v2.w);
		}
		// += operator
		inline friend Vector4D<T> operator+=(Vector4D<T>& v1, const Vector4D<T>& v2)
		{
			v1.x += v2.x;
			v1.y += v2.y;
			v1.z += v2.z;
			v1.w += v2.w;
			return v1;
		}
		// - operator
		inline Vector4D<T> operator-(const Vector4D<T>& v2) const
		{
			return Vector4D<T>(x - v2.x, y - v2.y, z - v2.z, w - v2.w);
		}
		// -= operator
		inline friend Vector4D<T> operator-=(Vector2D<T>& v1, const Vector4D<T>& v2)
		{
			v1.x -= v2.x;
			v1.y -= v2.y;
			v1.z -= v2.z;
			v1.w -= v2.w;
			return v1;
		}
		// * operator
		inline Vector4D<T> operator*(const T scalar) const
		{
			return Vector4D<T>(x*scalar, y*scalar, z*scalar, w*scalar);
		}
		// / operator
		inline Vector4D<T> operator/(const T divisor) const
		{
			return (divisor != 0 ? Vector4D<T>(x / divisor, y / divisor, z / divisor, w / divisor) : Vector4D<T>());
		}
	};

	using Vector4Int = Vector4D<int>;
	using Vector4UInt = Vector4D<unsigned int>;

	using Vector4 = Vector4D<float>;
	using Vector4Double = Vector4D<double>;

}