#pragma once

#include <sstream>
namespace DucklingsEngine
{
	template <typename T>
	struct Vector2D
	{
		//
		// Summary:
		//     X component of the vector.
		T x;
		//
		// Summary:
		//     Y component of the vector.
		T y;

		//
		// Summary:
		//     Constructs a new vector with zero value on x, y components.
		Vector2D() : x(0), y(0) {}
		//
		// Summary:
		//     Constructs a new vector with given x, y components.
		//
		// Parameters:
		//   x:
		//
		//   y:
		Vector2D(T x, T y) { this->x = x; this->y = y; }

		//
		// Summary:
		//     Shorthand for writing Vector2(1, 0).
		inline static Vector2D<T> right()
		{
			return Vector2D(1.0f, 0.0f);
		}
		//
		// Summary:
		//     Shorthand for writing Vector2(-1, 0).
		inline static Vector2D<T> left()
		{
			return Vector2D(-1.0f, 0.0f);
		}
		//
		// Summary:
		//     Shorthand for writing Vector2(0, -1).
		inline static Vector2D<T> down()
		{
			return Vector2D(0.0f, -1.0f);
		}
		//
		// Summary:
		//     Shorthand for writing Vector2(0, 1).
		inline static Vector2D<T> up()
		{
			return Vector2D(0.0f, 1.0f);
		}
		//
		// Summary:
		//     Shorthand for writing Vector2(0, 0).
		inline static Vector2D<T> zero()
		{
			return Vector2D(0.0f, 0.0f);
		}
		//
		// Summary:
		//     Shorthand for writing Vector2(1, 1).
		inline static Vector2D<T> ones()
		{
			return Vector2D(1.0f, 1.0f);
		}

		//
		// Summary:
		//     Returns the squared length of this vector (Read Only).
		inline T sqrMagnitude()
		{
			return pow(this->x, 2) + pow(this->y, 2);
		}
		//
		// Summary:
		//     Returns the length of this vector (Read Only).
		inline T magnitude()
		{
			return sqrt(this->sqrMagnitude());
		}
		//
		// Summary:
		//     Returns this vector with a magnitude of 1 (Read Only).
		inline Vector2D<T> normalized()
		{
			T mangitude_value = this->magnitude();
			return Vector2D<T>(this->x / mangitude_value, this->y / mangitude_value);
		}

		//
		// Summary:
		//     Set this vector with a magnitude of 1 (Read Only).
		inline void Normalize()
		{
			T mangitude_value = this->magnitude();
			this->x = this->x / mangitude_value;
			this->y = this->y / mangitude_value;
		}

		//
		// Summary:
		//     Returns the distance between a and b.
		//
		// Parameters:
		//   a:
		//
		//   b:
		inline static T Distance(const Vector2D<T>& a, const Vector2D<T>& b)
		{
			return (b - a).magnitude();
		}
		//
		// Summary:
		//     Dot Product of two vectors.
		//
		// Parameters:
		//   a:
		//
		//   b:
		inline static T Dot(Vector2D<T>& a, Vector2D<T>& b)
		{
			return a.x * b.x + a.y * b.y;
		}

		//
		// Summary:
		//     Returns a nicely formatted string for this vector.
		//
		// Parameters:
		//   format:
		inline std::string ToString()
		{
			std::ostringstream value;
			value << "(" << this->x << "," << this->y << ")";
			return value.str();
		}

		//
		// Summary:
		//     Linearly interpolates between vectors a and b by t.
		//
		// Parameters:
		//   a:
		//
		//   b:
		//
		//   t:
		inline static Vector2D<T> Lerp(const Vector2D<T>& a, const Vector2D<T>& b, float t)
		{
			return Vector2D<T>(a + (b - a) * t);
		}

		// + operator
		inline Vector2D<T> operator+(const Vector2D<T>& v2) const
		{
			return Vector2D<T>(x + v2.x, y + v2.y);
		}
		// += operator
		inline friend Vector2D<T> operator+=(Vector2D<T>& v1, const Vector2D<T>& v2)
		{
			v1.x += v2.x;
			v1.y += v2.y;
			return v1;
		}
		// - operator
		inline Vector2D<T> operator-(const Vector2D<T>& v2) const
		{
			return Vector2D<T>(x - v2.x, y - v2.y);
		}
		// -= operator
		inline friend Vector2D<T> operator-=(Vector2D<T>& v1, const Vector2D<T>& v2)
		{
			v1.x -= v2.x;
			v1.y -= v2.y;
			return v1;
		}
		// * operator
		inline Vector2D<T> operator*(const T scalar) const
		{
			return Vector2D<T>(x*scalar, y*scalar);
		}
		// / operator
		inline Vector2D<T> operator/(const T divisor) const
		{
			return (divisor != 0 ? Vector2D<T>(x / divisor, y / divisor) : Vector2D<T>());
		}

		// != operator
		inline bool operator!=(const Vector2D<T>& v2)
		{
			return (this->x != v2.x || this->y != v2.y);
		}
	};

	using Vector2Int = Vector2D<int>;
	using Vector2UInt = Vector2D<unsigned int>;

	using Vector2 = Vector2D<float>;
	using Vector2Double = Vector2D<double>;

}