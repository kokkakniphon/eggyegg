#pragma once

namespace DucklingsEngine
{
	namespace Mathf
	{
		template<class T>
		const T& min(const T& a, const T& b)
		{
			return (b < a) ? b : a;
		}

		template<class T>
		const T& max(const T& a, const T& b)
		{
			return (b > a) ? b : a;
		}

		template<class T>
		const T& clamp(const T& x, const T& min, const T& max)
		{
			return (x > max) ? max : ((x < min) ? min : x);
		}
	}
}

