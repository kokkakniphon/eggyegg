#pragma once

#include "AnimationController.h"

namespace DucklingsEngine
{
	class Controller : public AnimationController
	{
	public:
		Controller() { name = "Controller"; }
		~Controller() = default;

		void Initialize();
		Sprite* GetCurrentSprite();

		//
		// Summary:
		//     Sets the value of the given boolean parameter.
		//
		// Parameters:
		//   name:
		//     The parameter name.
		//
		//   id:
		//     The parameter ID.
		//
		//   value:
		//     The new parameter value.
		inline void SetBool(std::string name, bool value) { BoolKeys.find(name)->second = value; }
		//
		// Summary:
		//     Returns the value of the given boolean parameter.
		//
		// Parameters:
		//   name:
		//     The parameter name.
		//
		//   id:
		//     The parameter ID.
		//
		// Returns:
		//     The value of the parameter.
		inline bool GetBool(std::string name) { return BoolKeys.find(name)->second; }

		//
		// Summary:
		//     Sets the value of the given integer parameter.
		//
		// Parameters:
		//   name:
		//     The parameter name.
		//
		//   id:
		//     The parameter ID.
		//
		//   value:
		//     The new parameter value.
		inline void SetInteger(std::string name, int value) { IntKeys.find(name)->second = value; }
		//
		// Summary:
		//     Returns the value of the given integer parameter.
		//
		// Parameters:
		//   name:
		//     The parameter name.
		//
		//   id:
		//     The parameter ID.
		//
		// Returns:
		//     The value of the parameter.
		inline int GetInteger(std::string name) { return IntKeys.find(name)->second; }

	};
}


