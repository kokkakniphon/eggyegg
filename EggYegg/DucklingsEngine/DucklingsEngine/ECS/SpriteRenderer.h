#pragma once

#include "SDL.h"
#include "Sprite.h"
#include "Renderer.h"
#include "Object.h"
#include "Color.h"

#include "Vector4D.h"

namespace DucklingsEngine
{
	class SpriteRenderer : public Renderer
	{
	private:
		SDL_Rect m_DstRect = { 0, 0, 0, 0 };
		SDL_RendererFlip m_Flip = SDL_FLIP_NONE;

		std::unique_ptr<Sprite> localSprite;

	public:
		//
		// Summary:
		//     The Sprite to render.
		Sprite* sprite = nullptr;

		Color color = Color(1.0f, 1.0f, 1.0f, 1.0f);
		std::string renderType = "Stretch";

		//
		// Summary:
		//     Position of the slice according to the pixel unit of the image.
		Vector4 slice;

		SpriteRenderer() = default;
		SpriteRenderer(std::string textureID);
		SpriteRenderer(Sprite* sprite);
		void SetSprite(std::string textureID);
		~SpriteRenderer() = default;

		int GetComponentClassID() override { return 212; }
		std::string GetComponentClassData() override;

		void Start() {}
		void Update() {}
		void FixedUpdate() {}

		void Draw(const Vector2 &center, const Vector2 &rect);

		
	};

}