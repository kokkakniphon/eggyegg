#pragma once

#include "Collider.h"

namespace DucklingsEngine
{
	//
	// Summary:
	//     Collider for 2D physics representing an circle.
	class CircleCollider : public Collider
	{
	public:		
		//
		// Summary:
		//     Radius of the circle.
		float radius = 100.0f;
		//
		// Summary:
		//     Returns a point on the perimeter of this Collider that is closest to the specified
		//     position.
		//
		// Parameters:
		//   position:
		//     The position from which to find the closest point on this Collider.
		//
		// Returns:
		//     A point on the perimeter of this Collider that is closest to the specified position.
		Vector2 ClosestPoint(Vector2 position) override;
		//
		// Summary:
		//     Check if a collider overlaps a point in space.
		//
		// Parameters:
		//   point:
		//     A point in world space.
		//
		// Returns:
		//     Does point overlap the collider?
		bool OverlapPoint(Vector2 point) override;
		//
		// Summary:
		//     Get a list of all colliders that overlap this collider.
		//
		// Parameters:
		//
		//   collider:
		//     The target collider to calculate the overlaping.
		//
		// Returns:
		//     Does this collider overlap the target collider?
		bool OverlapCollider(Collider* collider);

		void StaticResolution(Collider* collider);
		void DynamicResolution(Collider* collider);

		int GetComponentClassID() override { return 58; }
		std::string GetComponentClassData() override
		{
			std::string data = "";
			data += "CircleCollider:\n";
			data += "  m_GameObject: {fileID: ";
			data += std::to_string(uniqueID);
			data += "}\n";
			data += "  m_Enable: ";
			data += std::to_string(enabled);
			data += "\n";
			data += "  m_Density: ";
			data += std::to_string(density);
			data += "\n";
			data += "  m_IsTrigger: ";
			data += std::to_string(isTrigger);
			data += "\n";
			data += "  m_Offset: {x: ";
			data += std::to_string(offset.x);
			data += ", y: ";
			data += std::to_string(offset.y);
			data += "}\n";
			data += "  m_Radius: ";
			data += std::to_string(radius);
			data += "\n";
			return data;
		}

		void Start() {}
		void Update() {}
		void OnDrawGizmos(const Vector2 &center, const Vector2 &rect) override ;
	};
}

