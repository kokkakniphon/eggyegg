#pragma once

#include "Collider.h"

namespace DucklingsEngine
{
	class EdgeCollider : public Collider
	{
	public:
		//
		// Summary:
		//	   Is the points a close end?
		bool closed = false;

		//
		// Summary:
		//     Get or set the points defining multiple continuous edges.
		std::vector<Vector2> points;
		//
		// Summary:
		//     Returns a point on the perimeter of this Collider that is closest to the specified
		//     position.
		//
		// Parameters:
		//   position:
		//     The position from which to find the closest point on this Collider.
		//
		// Returns:
		//     A point on the perimeter of this Collider that is closest to the specified position.
		Vector2 ClosestPoint(Vector2 position) override;
		//
		// Summary:
		//     Check if a collider overlaps a point in space.
		//
		// Parameters:
		//   point:
		//     A point in world space.
		//
		// Returns:
		//     Does point overlap the collider?
		bool OverlapPoint(Vector2 point) override { return false; }
		//
		// Summary:
		//     Get a list of all colliders that overlap this collider.
		//
		// Parameters:
		//
		//   collider:
		//     The target collider to calculate the overlaping.
		//
		// Returns:
		//     Does this collider overlap the target collider?
		bool OverlapCollider(Collider* collider) override { return false; }

		void StaticResolution(Collider* collider) override {}
		void DynamicResolution(Collider* collider) override {}

		void Start() {}
		void Update() {}
		void OnDrawGizmos(const Vector2 &center, const Vector2 &rect) override;
	};

}

