#include "BoxCollider.h"

#include "CircleCollider.h"

namespace DucklingsEngine
{
	void BoxCollider::UpdateRect()
	{
		m_Calculated_Rect = true;

		Vector2 pos = this->transform->GetPosition();
		Vector2 scale = Vector2(this->transform->localScale.x * size.x, this->transform->localScale.y * size.y);
		Vector2 point;
		float rot = this->transform->GetEulerAngles() * deg2rad;

		point = Vector2(-scale.x, scale.y) + offset;
		points[0] = (Vector2(point.x * cos(rot) - point.y * sin(rot), point.x * sin(rot) + point.y * cos(rot)));
		point = Vector2(-scale.x, -scale.y) + offset;
		points[1] = (Vector2(point.x * cos(rot) - point.y * sin(rot), point.x * sin(rot) + point.y * cos(rot)));
		point = Vector2(scale.x, -scale.y) + offset;
		points[2] = (Vector2(point.x * cos(rot) - point.y * sin(rot), point.x * sin(rot) + point.y * cos(rot)));
		point = Vector2(scale.x, scale.y) + offset;
		points[3] = (Vector2(point.x * cos(rot) - point.y * sin(rot), point.x * sin(rot) + point.y * cos(rot)));
	}

	bool BoxCollider::OverlapPoint(Vector2 point)
	{
		Vector2 closestPoint = ClosestPoint(point);

		Vector2 origin = this->transform->GetPosition();

		float originToClose = sqrtf(pow(origin.x - closestPoint.x, 2) + pow(origin.y - closestPoint.y, 2));
		float originToTarget = sqrtf(pow(origin.x - point.x, 2) + pow(origin.y - point.y, 2));

		return (originToTarget < originToClose);
	}
	bool BoxCollider::OverlapCollider(Collider * collider)
	{
		EdgeCollider *edgeCollider = dynamic_cast<EdgeCollider*>(collider);

		if (edgeCollider != nullptr)
		{
			Vector2 sourcePos;
			Vector2 targetPos;

			EdgeCollider *shape1 = this;
			EdgeCollider *shape2 = edgeCollider;

			overlap = INFINITY;

			for (unsigned int i = 0; i < 2; i++)
			{
				if (i == 1)
				{
					shape1 = edgeCollider;
					shape2 = this;
				}

				sourcePos = shape1->transform->GetPosition();
				targetPos = shape2->transform->GetPosition();

				for (unsigned int a = 0; a < shape1->points.size(); a++)
				{
					int b = (a + 1) % shape1->points.size();
					Vector2 axisProj = { -(shape1->points[b].y - shape1->points[a].y), (shape1->points[b].x - shape1->points[a].x) };
					float d = sqrtf(axisProj.x * axisProj.x + axisProj.y * axisProj.y);
					axisProj = { axisProj.x / d, axisProj.y / d };

					float min_r1 = INFINITY, max_r1 = -INFINITY;
					for (unsigned int p = 0; p < shape1->points.size(); p++)
					{
						float q = (((shape1->points[p].x + sourcePos.x) * axisProj.x) + ((shape1->points[p].y + sourcePos.y) * axisProj.y));
						min_r1 = std::min(min_r1, q);
						max_r1 = std::max(max_r1, q);
					}

					float min_r2 = INFINITY, max_r2 = -INFINITY;
					for (unsigned int p = 0; p < shape2->points.size(); p++)
					{
						float q = (((shape2->points[p].x + targetPos.x) * axisProj.x) + ((shape2->points[p].y + targetPos.y) * axisProj.y));
						min_r2 = std::min(min_r2, q);
						max_r2 = std::max(max_r2, q);
					}

					overlap = std::min(std::min(max_r1, max_r2) - std::max(min_r1, min_r2), overlap);

					if (!(max_r2 >= min_r1 && max_r1 >= min_r2))
					{
						return false;
					}
				}
			}
			
			gizmosColor = SDL_Color{ 255,100,0,255 };
			collider->gizmosColor = SDL_Color{ 255,100,0,255 };

			return true;
		}

		CircleCollider *circleCollider = dynamic_cast<CircleCollider*>(collider);

		if (circleCollider != nullptr)
		{
			// This is not perfect just work for rough checking overlapping.
			Vector2 sourcePos = this->transform->GetPosition();
			Vector2 targetPos = collider->ClosestPoint(sourcePos);

			if (OverlapPoint(targetPos) == false)
			{
				return false;
			}

			gizmosColor = SDL_Color{ 255,100,0,255 };
			collider->gizmosColor = SDL_Color{ 255,100,0,255 };

			return true;
		}
	}
	void BoxCollider::StaticResolution(Collider * collider)
	{
		if (attachedRigidbody != nullptr)
		{
			Vector2 sourcePos = this->transform->GetPosition();
			Vector2 targetPos = collider->ClosestPoint(sourcePos);

			float fDistance = sqrtf(pow(targetPos.x - sourcePos.x, 2) + pow(targetPos.y - sourcePos.y, 2));

			float fOverlap = 0.5f * overlap;

			Vector2 displaceSource = (sourcePos - targetPos);

			if (collider->attachedRigidbody != nullptr)
			{
				this->transform->Translate((fOverlap * displaceSource.x / fDistance), (fOverlap * displaceSource.y / fDistance));

				Vector2 displaceTarget = (targetPos - sourcePos);
				collider->transform->Translate((fOverlap * displaceTarget.x / fDistance), (fOverlap * displaceTarget.y / fDistance));
			}
			else
			{
				this->transform->Translate((2.0f * fOverlap * displaceSource.x / fDistance), (2.0f * fOverlap * displaceSource.y / fDistance));
			}
		}
		overlap = 0;
	}
	void BoxCollider::DynamicResolution(Collider * collider)
	{
		if (attachedRigidbody != nullptr)
		{
			Vector2 sourcePos = transform->GetPosition();
			Vector2 targetPos = collider->ClosestPoint(sourcePos);

			float fDistance = sqrtf(pow(sourcePos.x - targetPos.x, 2) + pow(sourcePos.y - targetPos.y, 2));

			Vector2 normal = (sourcePos - targetPos) / fDistance;

			Vector2 tangent = Vector2(-normal.y, normal.x);

			float dotTangentSource = Vector2::Dot(attachedRigidbody->velocity, tangent);
			float dotNormalSource = Vector2::Dot(attachedRigidbody->velocity, normal);


			if (collider->attachedRigidbody != nullptr)
			{

				float dotTangentTarget = Vector2::Dot(collider->attachedRigidbody->velocity, tangent);
				float dotNormalTarget = Vector2::Dot(collider->attachedRigidbody->velocity, normal);

				float momentumSource = (dotNormalSource * (this->attachedRigidbody->mass - collider->attachedRigidbody->mass) + 2.0f * collider->attachedRigidbody->mass * dotNormalTarget) / (this->attachedRigidbody->mass + collider->attachedRigidbody->mass);
				float momentumTarget = (dotNormalTarget * (collider->attachedRigidbody->mass - this->attachedRigidbody->mass) + 2.0f * this->attachedRigidbody->mass * dotNormalSource) / (this->attachedRigidbody->mass + collider->attachedRigidbody->mass);


				this->attachedRigidbody->velocity = tangent * dotTangentSource + normal * momentumSource;
				this->attachedRigidbody->angularVelocity = dotTangentSource;

				collider->attachedRigidbody->velocity = tangent * dotTangentTarget + normal * momentumTarget;
				collider->attachedRigidbody->angularVelocity = -dotTangentSource;
			}
			else
			{
				Vector2 targetVelocity = Vector2(-attachedRigidbody->velocity.x, -attachedRigidbody->velocity.y);
				float dotTangentTarget = Vector2::Dot(targetVelocity, tangent);
				float dotNormalTarget = Vector2::Dot(targetVelocity, normal);
				float momentumSource = (2.0f * this->attachedRigidbody->mass * dotNormalTarget) / (this->attachedRigidbody->mass * 2.0f);

				this->attachedRigidbody->velocity = tangent * dotTangentSource + normal * momentumSource;
				this->attachedRigidbody->angularVelocity = dotTangentSource;
			}
		}
	}

	void BoxCollider::Init()
	{
		Collider::Init();

		closed = true;

		Vector2 pos = this->transform->GetPosition();
		Vector2 scale = Vector2(this->transform->localScale.x * size.x, this->transform->localScale.y * size.y);
		Vector2 point;
		float rot = this->transform->GetEulerAngles() * deg2rad;

		point = Vector2(-scale.x, scale.y);
		points.push_back(Vector2(point.x * cos(rot) - point.y * sin(rot), point.x * sin(rot) + point.y * cos(rot)));
		point = Vector2(-scale.x, -scale.y);
		points.push_back(Vector2(point.x * cos(rot) - point.y * sin(rot), point.x * sin(rot) + point.y * cos(rot)));
		point = Vector2(scale.x, -scale.y);
		points.push_back(Vector2(point.x * cos(rot) - point.y * sin(rot), point.x * sin(rot) + point.y * cos(rot)));
		point = Vector2(scale.x, scale.y);
		points.push_back(Vector2(point.x * cos(rot) - point.y * sin(rot), point.x * sin(rot) + point.y * cos(rot)));
	}

	void BoxCollider::FixedUpdate()
	{
		UpdateRect();
	}
	
	void BoxCollider::OnDrawGizmos(const Vector2 &center, const Vector2 &rect)
	{
		if (m_Calculated_Rect == false)
			UpdateRect();

		EdgeCollider::OnDrawGizmos(center, rect);

		m_Calculated_Rect = false;
	}
}