#include "GameObject.h"
#include "Engine.h"
#include "SceneManager.h"

namespace DucklingsEngine
{
	GameObject::GameObject() : Object(SceneManager::GetInstance().GetLatestActiveScene()->name)
	{
		this->name = "GameObject";
		this->layer = "";
		this->tag = "Untagged";
		this->transform = &this->GetComponent<Transform>();
		this->transform->name = this->name;

		Engine::GetInstance().GetManager()->AddEntity(this);
	}

	GameObject::GameObject(std::string name) : Object(SceneManager::GetInstance().GetLatestActiveScene()->name)
	{
		this->name = name;
		this->layer = "";
		this->tag = "Untagged";
		this->transform = &this->GetComponent<Transform>();
		this->transform->name = this->name;

		Engine::GetInstance().GetManager()->AddEntity(this);
	}
}