#include "CircleCollider.h"

#include "Engine.h"
#include "Camera.h"

namespace DucklingsEngine
{
	Vector2 CircleCollider::ClosestPoint(Vector2 position)
	{
		Vector2 sourcePos = transform->GetPosition() + offset;
		Vector2 closetPoint = (position - sourcePos) / Vector2::Distance(sourcePos, position) * radius;
		return sourcePos + closetPoint;
	}
	bool CircleCollider::OverlapPoint(Vector2 point)
	{
		Vector2 sourcePos = transform->GetPosition() + offset;
		return fabs((sourcePos.x - point.x) * (sourcePos.x - point.x) + (sourcePos.y - point.y) * (sourcePos.y - point.y)) <= radius * radius;
	}
	bool CircleCollider::OverlapCollider(Collider * collider)
	{
		Vector2 sourcePos = transform->GetPosition() + offset;
		Vector2 targetClosestPoint = collider->ClosestPoint(sourcePos);
		bool isOverlap = OverlapPoint(targetClosestPoint);
		if (isOverlap == true)
		{
			gizmosColor = SDL_Color{ 255,100,0,255 };
			collider->gizmosColor = SDL_Color{ 255,100,0,255 };
		}
		return isOverlap;
	}
	void CircleCollider::StaticResolution(Collider * collider)
	{
		if (attachedRigidbody != nullptr)
		{
			Vector2 sourcePos = transform->GetPosition() + offset;
			Vector2 targetPos = collider->ClosestPoint(sourcePos);

			float fDistance = sqrtf((targetPos.x - sourcePos.x) * (targetPos.x - sourcePos.x) + (targetPos.y - sourcePos.y) * (targetPos.y - sourcePos.y));

			float fOverlap = 0.5f * (fDistance - radius);

			Vector2 displaceSource = (sourcePos - targetPos);

			if (collider->attachedRigidbody != nullptr)
			{
				this->transform->Translate(-(fOverlap * displaceSource.x / fDistance), -(fOverlap * displaceSource.y / fDistance));

				Vector2 displaceTarget = (targetPos - sourcePos);
				collider->transform->Translate(-(fOverlap * displaceTarget.x / fDistance), -(fOverlap * displaceTarget.y / fDistance));
			}
			else
			{
				this->transform->Translate(-(2.0f * fOverlap * displaceSource.x / fDistance), -(2.0f * fOverlap * displaceSource.y / fDistance));
			}
		}
	}
	void CircleCollider::DynamicResolution(Collider * collider)
	{
		if (attachedRigidbody != nullptr)
		{
			Vector2 sourcePos = transform->GetPosition() + offset;
			Vector2 targetPos = collider->ClosestPoint(sourcePos);

			float fDistance = sqrtf((sourcePos.x - targetPos.x) * (sourcePos.x - targetPos.x) + (sourcePos.y - targetPos.y) * (sourcePos.y - targetPos.y));

			Vector2 normal = (sourcePos - targetPos) / fDistance;

			Vector2 tangent = Vector2(-normal.y, normal.x);

			float dotTangentSource = Vector2::Dot(attachedRigidbody->velocity, tangent);
			float dotNormalSource = Vector2::Dot(attachedRigidbody->velocity, normal);

			if (collider->attachedRigidbody != nullptr)
			{
				float dotTangentTarget = Vector2::Dot(collider->attachedRigidbody->velocity, tangent);
				float dotNormalTarget = Vector2::Dot(collider->attachedRigidbody->velocity, normal);

				float momentumSource = (dotNormalSource * (this->attachedRigidbody->mass - collider->attachedRigidbody->mass) + 2.0f * collider->attachedRigidbody->mass * dotNormalTarget) / (this->attachedRigidbody->mass + collider->attachedRigidbody->mass);
				float momentumTarget = (dotNormalTarget * (collider->attachedRigidbody->mass - this->attachedRigidbody->mass) + 2.0f * this->attachedRigidbody->mass * dotNormalSource) / (this->attachedRigidbody->mass + collider->attachedRigidbody->mass);


				this->attachedRigidbody->velocity = tangent * dotTangentSource + normal * momentumSource;
				this->attachedRigidbody->angularVelocity = dotTangentSource;

				collider->attachedRigidbody->velocity = tangent * dotTangentTarget + normal * momentumTarget;
				collider->attachedRigidbody->angularVelocity = -dotTangentSource;
			}
			else
			{
				Vector2 targetVelocity = Vector2(-attachedRigidbody->velocity.x, -attachedRigidbody->velocity.y);
				float dotTangentTarget = Vector2::Dot(targetVelocity, tangent);
				float dotNormalTarget = Vector2::Dot(targetVelocity, normal);
				float momentumSource = (2.0f * this->attachedRigidbody->mass * dotNormalTarget) / (this->attachedRigidbody->mass * 2.0f);

				this->attachedRigidbody->velocity = tangent * dotTangentSource + normal * momentumSource;
				this->attachedRigidbody->angularVelocity = dotTangentSource;
			}
		}
	}
	void CircleCollider::OnDrawGizmos(const Vector2 &center, const Vector2 &rect)
	{
		if (showOutline == true)
		{
			int gizmosResolution = 15 * 2;
			for (unsigned int i = 0; i < gizmosResolution; i++)
			{
				Vector2 drawPoint;
				Vector2 sourcePos = transform->GetPosition() + offset;
				drawPoint.x = sourcePos.x + radius * cos(i * (360.0f / gizmosResolution) * 3.14f/ 180.0f);
				drawPoint.y = sourcePos.y + radius * sin(i * (360.0f / gizmosResolution) * 3.14f / 180.0f);
				drawPoint = drawPoint + rect / 2 - center;
				SDL_SetRenderDrawColor(Engine::GetInstance().GetRenderer(), gizmosColor.r, gizmosColor.g, gizmosColor.b, gizmosColor.a);
				SDL_RenderDrawPoint(Engine::GetInstance().GetRenderer(), drawPoint.x, drawPoint.y);
			}

			gizmosColor = SDL_Color{ 0,255,0,255 };
		}
	}
}
