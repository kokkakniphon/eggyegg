#include "SpriteRenderer.h"

#include "Camera.h"
#include "Engine.h"

namespace DucklingsEngine
{
	SpriteRenderer::SpriteRenderer(std::string textureID)
	{
		localSprite = std::make_unique<Sprite>(textureID);
		sprite = localSprite.get();
	}

	SpriteRenderer::SpriteRenderer(Sprite * sprite)
	{
		this->sprite = sprite;
	}

	void SpriteRenderer::SetSprite(std::string textureID)
	{
		localSprite = std::make_unique<Sprite>(textureID);
		sprite = localSprite.get();
	}

	std::string SpriteRenderer::GetComponentClassData()
	{
		std::string data = "";
		data += "SpriteRenderer:\n";
		data += "  m_GameObject: {fileID: ";
		data += std::to_string(uniqueID);
		data += "}\n";
		data += "  m_Enable: ";
		data += std::to_string(enabled);
		data += "\n";
		data += "  m_Sprite: {fileName: ";
		data += sprite->textureID;
		data += "}\n";
		data += "  m_Flip: ";
		data += std::to_string(m_Flip);
		data += "\n";
		data += "  m_Color: {r: ";
		data += std::to_string(color.r);
		data += ", g: ";
		data += std::to_string(color.g);
		data += ", b: ";
		data += std::to_string(color.b);
		data += ", a: ";
		data += std::to_string(color.a);
		data += "}\n";
		data += "  m_RenderType: {type: ";
		data += renderType;
		data += "}\n";
		data += "  m_Slice: {x: ";
		data += std::to_string(slice.x);
		data += ", y: ";
		data += std::to_string(slice.y);
		data += ", w: ";
		data += std::to_string(slice.z);
		data += ", h: ";
		data += std::to_string(slice.w);
		data += "}\n";
		data += "  m_Order: ";
		data += std::to_string(order);
		data += "\n";
		return data;
	}

	void SpriteRenderer::Draw(const Vector2 &center, const Vector2 &rect)
	{
		if (sprite == nullptr)
			return;

		if (enabled == true)
		{
			m_DstRect.w = static_cast<int>(sprite->rect.w * transform->GetScale().x);
			m_DstRect.h = static_cast<int>(sprite->rect.h * transform->GetScale().y);

			Vector2 thisPos = transform->GetPosition();
			thisPos.x = thisPos.x - (m_DstRect.w * sprite->pivot.x);
			thisPos.y = thisPos.y - (m_DstRect.h * sprite->pivot.y);
			thisPos = thisPos + rect / 2 - center;
			m_DstRect.x = static_cast<int>(thisPos.x);
			m_DstRect.y = static_cast<int>(thisPos.y);

			if (sprite->isSpriteSheet == true)
			{
				sprite->rect.x = (sprite->GetCurrentSpriteCell() % sprite->spriteSheetColume) * sprite->rect.w;
				sprite->rect.y = (std::ceil((float)(sprite->GetCurrentSpriteCell() + 1) / (float)sprite->spriteSheetColume) - 1) * sprite->rect.h;
			}

			SDL_SetTextureColorMod(sprite->texture, (Uint8)(color.r * 255.0f), (Uint8)(color.g * 255.0f), (Uint8)(color.b * 255.0f));
			SDL_SetTextureAlphaMod(sprite->texture, color.a * 255.0f);

			SDL_Rect oriRect = sprite->rect;
			SDL_Rect desRect = m_DstRect;


			if (renderType == "Stretch")
			{
				SDL_RenderCopyEx(Engine::GetInstance().GetRenderer(), sprite->texture, &sprite->rect, &m_DstRect, transform->GetEulerAngles(), nullptr, m_Flip);
			}
			else if (renderType == "Slice")
			{
				// Top Left
				oriRect.x = 0;
				oriRect.y = 0;
				oriRect.w = slice.x;
				oriRect.h = slice.z;

				desRect.x = m_DstRect.x;
				desRect.y = m_DstRect.y;
				desRect.w = static_cast<int>(oriRect.w);
				desRect.h = static_cast<int>(oriRect.h);
				SDL_RenderCopyEx(Engine::GetInstance().GetRenderer(), sprite->texture, &oriRect, &desRect, transform->GetEulerAngles(), nullptr, m_Flip);

				// Top
				oriRect.x = slice.x;
				oriRect.y = 0;
				oriRect.w = sprite->rect.w - slice.y - slice.x;
				oriRect.h = slice.z;

				desRect.x = m_DstRect.x + oriRect.x;
				desRect.y = m_DstRect.y;
				desRect.w = static_cast<int>(m_DstRect.w - slice.y - slice.x);
				desRect.h = static_cast<int>(oriRect.h);
				SDL_RenderCopyEx(Engine::GetInstance().GetRenderer(), sprite->texture, &oriRect, &desRect, transform->GetEulerAngles(), nullptr, m_Flip);

				// Top Right
				oriRect.x = sprite->rect.w - slice.y;
				oriRect.y = 0;
				oriRect.w = slice.y;
				oriRect.h = slice.z;

				desRect.x = m_DstRect.x + m_DstRect.w - oriRect.w;
				desRect.y = m_DstRect.y;
				desRect.w = static_cast<int>(oriRect.w);
				desRect.h = static_cast<int>(oriRect.h);
				SDL_RenderCopyEx(Engine::GetInstance().GetRenderer(), sprite->texture, &oriRect, &desRect, transform->GetEulerAngles(), nullptr, m_Flip);

				// Left
				oriRect.x = 0;
				oriRect.y = slice.z;
				oriRect.w = slice.x;
				oriRect.h = sprite->rect.h - slice.z - slice.w;

				desRect.x = m_DstRect.x;
				desRect.y = m_DstRect.y + oriRect.y;
				desRect.w = static_cast<int>(oriRect.w);
				desRect.h = static_cast<int>(m_DstRect.h - slice.z - slice.w);
				SDL_RenderCopyEx(Engine::GetInstance().GetRenderer(), sprite->texture, &oriRect, &desRect, transform->GetEulerAngles(), nullptr, m_Flip);

				// Middle
				oriRect.x = slice.x;
				oriRect.y = slice.z;
				oriRect.w = sprite->rect.w - slice.y - slice.x;
				oriRect.h = sprite->rect.h - slice.z - slice.w;

				desRect.x = m_DstRect.x + oriRect.x;
				desRect.y = m_DstRect.y + oriRect.y;
				desRect.w = static_cast<int>(m_DstRect.w - slice.y - slice.x);
				desRect.h = static_cast<int>(m_DstRect.h - slice.z - slice.w);
				SDL_RenderCopyEx(Engine::GetInstance().GetRenderer(), sprite->texture, &oriRect, &desRect, transform->GetEulerAngles(), nullptr, m_Flip);

				// Right
				oriRect.x = sprite->rect.w - slice.y;
				oriRect.y = slice.z;
				oriRect.w = slice.y;
				oriRect.h = sprite->rect.h - slice.z - slice.w;

				desRect.x = m_DstRect.x + m_DstRect.w - oriRect.w;
				desRect.y = m_DstRect.y + oriRect.y;
				desRect.w = static_cast<int>(oriRect.w);
				desRect.h = static_cast<int>(m_DstRect.h - slice.z - slice.w);
				SDL_RenderCopyEx(Engine::GetInstance().GetRenderer(), sprite->texture, &oriRect, &desRect, transform->GetEulerAngles(), nullptr, m_Flip);

				// Bottom Left
				oriRect.x = 0;
				oriRect.y = sprite->rect.h - slice.w;
				oriRect.w = slice.x;
				oriRect.h = slice.w;

				desRect.x = m_DstRect.x;
				desRect.y = m_DstRect.y + m_DstRect.h - oriRect.h;
				desRect.w = static_cast<int>(oriRect.w);
				desRect.h = static_cast<int>(oriRect.h);
				SDL_RenderCopyEx(Engine::GetInstance().GetRenderer(), sprite->texture, &oriRect, &desRect, transform->GetEulerAngles(), nullptr, m_Flip);

				// Bottom
				oriRect.x = slice.x;
				oriRect.y = sprite->rect.h - slice.w;
				oriRect.w = sprite->rect.w - slice.y - slice.x;
				oriRect.h = slice.w;

				desRect.x = m_DstRect.x + oriRect.x;
				desRect.y = m_DstRect.y + m_DstRect.h - oriRect.h;
				desRect.w = static_cast<int>(m_DstRect.w - slice.y - slice.x);
				desRect.h = static_cast<int>(oriRect.h);
				SDL_RenderCopyEx(Engine::GetInstance().GetRenderer(), sprite->texture, &oriRect, &desRect, transform->GetEulerAngles(), nullptr, m_Flip);

				// Bottom Right
				oriRect.x = sprite->rect.w - slice.y;
				oriRect.y = sprite->rect.h - slice.w;
				oriRect.w = slice.y;
				oriRect.h = slice.w;

				desRect.x = m_DstRect.x + m_DstRect.w - oriRect.w;
				desRect.y = m_DstRect.y + m_DstRect.h - oriRect.h;
				desRect.w = static_cast<int>(oriRect.w);
				desRect.h = static_cast<int>(oriRect.h);
				SDL_RenderCopyEx(Engine::GetInstance().GetRenderer(), sprite->texture, &oriRect, &desRect, transform->GetEulerAngles(), nullptr, m_Flip);

			}

			

			SDL_SetTextureColorMod(sprite->texture, 255, 255, 255);
			SDL_SetTextureAlphaMod(sprite->texture, 255);
		}
	}
}