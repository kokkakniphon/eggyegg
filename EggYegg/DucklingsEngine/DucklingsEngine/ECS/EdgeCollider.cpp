#include "EdgeCollider.h"

#include "Mathf.h"
#include "Engine.h"
#include "Camera.h"

namespace DucklingsEngine
{
	Vector2 EdgeCollider::ClosestPoint(Vector2 position)
	{
		Vector2 sourcePos = this->transform->GetPosition();

		Vector2 closestPoint;

		if(points.size() > 0)
			closestPoint = points[0] + sourcePos;

		float closestDis;
		
		for (unsigned int i = 0; i < points.size() - 1; i++)
		{
			Vector2 p1 = points[i] + sourcePos;
			Vector2 p2 = points[i + 1] + sourcePos;

			Vector2 u = position - p1;
			Vector2 v = p2 - p1;

			float lineDis = Vector2::Distance(p1, p2);

			float t = Mathf::clamp((float)(Vector2::Dot(u, v) / (lineDis * lineDis)), 0.0f, 1.0f);

			Vector2 l_closestPoint = p1 + v * t;
			float dis = Vector2::Distance(l_closestPoint, position);

			if (i == 0)
			{
				closestPoint = l_closestPoint;
				closestDis = dis;
			}
			else if (dis < closestDis)
			{
				closestPoint = l_closestPoint;
				closestDis = dis;
			}
		}

		if (closed == true && points.size() > 1)
		{
			Vector2 p1 = points[points.size()-1] + sourcePos;
			Vector2 p2 = points[0] + sourcePos;

			Vector2 u = position - p1;
			Vector2 v = p2 - p1;

			float lineDis = sqrtf(pow(p1.x - p2.x, 2) + pow(p1.y - p2.y, 2));

			float t = Mathf::clamp((float)(Vector2::Dot(u, v) / (lineDis * lineDis)), 0.0f, 1.0f);

			Vector2 l_closestPoint = p1 + v * t;
			float dis = sqrtf(pow(l_closestPoint.x - position.x, 2) + pow(l_closestPoint.y - position.y, 2));

			if (dis < closestDis)
			{
				closestPoint = l_closestPoint;
				closestDis = dis;
			}
		}
		return closestPoint;
	}

	void EdgeCollider::OnDrawGizmos(const Vector2 &center, const Vector2 &rect)
	{
		if (showOutline == true)
		{
			Vector2 sourcePos = this->transform->GetPosition();

			for (unsigned int i = 0; i < points.size()-1; i++)
			{
				Vector2 screenP1 = Vector2(points[i].x + sourcePos.x, points[i].y + sourcePos.y) + rect / 2 - center;
				Vector2 screenP2 = Vector2(points[i + 1].x + sourcePos.x, points[i + 1].y + sourcePos.y) + rect / 2 - center;
				SDL_SetRenderDrawColor(Engine::GetInstance().GetRenderer(), gizmosColor.r, gizmosColor.g, gizmosColor.b, gizmosColor.a);
				SDL_RenderDrawLine(Engine::GetInstance().GetRenderer(), screenP1.x, screenP1.y, screenP2.x, screenP2.y);
			}

			if (closed == true && points.size() > 1)
			{
				Vector2 screenP1 = Vector2(points[points.size() - 1].x + sourcePos.x, points[points.size() - 1].y + sourcePos.y) + rect / 2 - center;
				Vector2 screenP2 = Vector2(points[0].x + sourcePos.x, points[0].y + sourcePos.y) + rect / 2 - center;
				SDL_SetRenderDrawColor(Engine::GetInstance().GetRenderer(), gizmosColor.r, gizmosColor.g, gizmosColor.b, gizmosColor.a);
				SDL_RenderDrawLine(Engine::GetInstance().GetRenderer(), screenP1.x, screenP1.y, screenP2.x, screenP2.y);
			}

			gizmosColor = SDL_Color{ 0,255,0,255 };
		}
	}
}