#pragma once

#include "Object.h"

#include "AnimationController.h"
#include "SpriteRenderer.h"
#include "DuckTime.h"

namespace DucklingsEngine
{
	class Animator : public Component
	{
	private:
		//
		// Summary:
		//      This is the target output render.
		SpriteRenderer *renderer = nullptr;

		std::string currentAnimationStateName = "";
		Animation *currentAnimation = nullptr;

		bool play = true;

	public:

		Object* gameObject;

		//
		// Summary:
		//     The Animation Controller to animate.
		std::shared_ptr<AnimationController> controller;

		Animator() = default;
		
		Animator(AnimationController* controller)
		{
			this->controller.reset(controller);
		}

		~Animator() = default;

		void SetAnimationController(std::shared_ptr<AnimationController> controller)
		{
			this->controller = controller;
			Start();
		}

		//
		// Summary:
		//     Returns the value of the given boolean parameter.
		//
		// Parameters:
		//   name:
		//     The parameter name.
		//
		//   id:
		//     The parameter ID.
		//
		// Returns:
		//     The value of the parameter.
		inline bool GetBool(std::string name)
		{
			return controller->GetBool(name);
		}

		//
		// Summary:
		//     Returns the value of the given integer parameter.
		//
		// Parameters:
		//   name:
		//     The parameter name.
		//
		//   id:
		//     The parameter ID.
		//
		// Returns:
		//     The value of the parameter.
		inline int GetInteger(std::string name)
		{
			return controller->GetInteger(name);
		}

		//
		// Summary:
		//     Sets the value of the given boolean parameter.
		//
		// Parameters:
		//   name:
		//     The parameter name.
		//
		//   id:
		//     The parameter ID.
		//
		//   value:
		//     The new parameter value.
		inline void SetBool(std::string name, bool value)
		{
			return controller->SetBool(name, value);
		}

		//
		// Summary:
		//     Sets the value of the given integer parameter.
		//
		// Parameters:
		//   name:
		//     The parameter name.
		//
		//   id:
		//     The parameter ID.
		//
		//   value:
		//     The new parameter value.
		inline void SetInteger(std::string name, int value)
		{
			return controller->SetInteger(name, value);
		}

		inline void SetPlay(bool play)
		{
			this->play = play;
		}

		int GetComponentClassID() override { return 95; }
		std::string GetComponentClassData() override 
		{ 
			std::string data = "";
			data += "Animator:\n";
			data += "  m_GameObject: {fileID: ";
			data += std::to_string(uniqueID);
			data += "}\n";
			data += "  m_Enable: ";
			data += this->enabled;
			data += "\n";
			return data;
		}

		void Init() override 
		{
			
		}
		void Start() 
		{
			if (controller == nullptr)
				return;

			renderer = &gameObject->GetComponent<SpriteRenderer>();

			currentAnimationStateName = controller->animations.begin()->first;
			currentAnimation = controller->animations[currentAnimationStateName].get();
			currentAnimation->animationStartTime = DuckTime::GetTime();
			renderer->sprite = (controller->GetAnimationSprite(currentAnimationStateName));
			renderer->sprite->SetCurrentSpriteCell(0);

			if (renderer == nullptr)
				std::cout << "ERROR <Animator>: Can't find renderer attached to the gameObject." << std::endl;
		}

		void Update() 
		{
			if (controller == nullptr || play == false)
				return;

			if (renderer != nullptr)
			{
				std::string desAnimation = controller->GetDestinationAnimation(currentAnimationStateName);
				if (currentAnimationStateName != desAnimation && desAnimation != "")
				{
					currentAnimationStateName = desAnimation;
					currentAnimation = controller->animations[currentAnimationStateName].get();
					currentAnimation->animationStartTime = DuckTime::GetTime();
					renderer->sprite = controller->GetAnimationSprite(currentAnimationStateName);
					renderer->sprite->SetCurrentSpriteCell(0);
				}
				else
				{
					int targetFrame = (int)((DuckTime::GetTime() - currentAnimation->animationStartTime) * currentAnimation->framePerSecond);
					renderer->sprite->SetCurrentSpriteCell(targetFrame);
				}
			}
		}

		void FixedUpdate() {}
	};
}

