#include "Transform.h"

#define M_PI    3.14159265358979323846264338327950288   /**< pi */

#include <iostream>
namespace DucklingsEngine
{
	void Transform::SetParent(Transform * parent)
	{
		if (parent == nullptr)
		{
			this->parent = this;
			this->root = this;
		}
		else
		{
			this->parent = parent;
			this->root = parent->parent;
		}

		this->localPosition -= parent->GetPosition();
	}

	Transform* Transform::GetParent()
	{
		return this->parent;
	}

	void Transform::SetPosition(const Vector2& v) 
	{
		Vector2 newPos;
		if (this != nullptr)
		{
			newPos = (this != this->parent) ? v - parent->GetPosition() : v;
			if (this != this->parent)
			{
				float thisEulerAngle = parent->GetEulerAngles();
				newPos = Vector2(cos(thisEulerAngle * M_PI / 180) * newPos.x + sin(thisEulerAngle * M_PI / 180) * newPos.y, cos(thisEulerAngle * M_PI / 180) * newPos.y - sin(thisEulerAngle * M_PI / 180) * newPos.x);
			}
			localPosition = newPos;
		}
	}

	Vector2 Transform::GetPosition()
	{
		position = Vector2();
		if (this != nullptr)
		{
			if (this != this->parent)
			{
				position = parent->GetPosition();
				float thisEulerAngle = parent->GetEulerAngles();
				position.x += cos(thisEulerAngle * M_PI / 180) * localPosition.x - sin(thisEulerAngle * M_PI / 180) * localPosition.y;
				position.y += sin(thisEulerAngle * M_PI / 180) * localPosition.x + cos(thisEulerAngle * M_PI / 180) * localPosition.y;
			}
			else
			{
				position = localPosition;
			}
		}
		return this->position;
	}

	Vector2 Transform::GetScale()
	{
		Vector2 scale;
		if (this != this->parent)
		{
			Vector2 parentScale = parent->GetScale();
			scale = Vector2(localScale.x * parentScale.x, localScale.y * parentScale.y);
		}
		else
		{
			scale = localScale;
		}
		return scale;
	}

	float Transform::GetEulerAngles()
	{
		float rotate;
		if (this != nullptr)
			rotate = (this != this->parent) ? localEulerAngles + parent->GetEulerAngles() : localEulerAngles;
		return rotate;
	}

	Transform::Transform()
	{
		parent = this;
		localPosition.zero();
		position.zero();
		localScale.ones();
		eulerAngles = 0.0f;
	}

	Transform::Transform(Vector2 v)
	{
		parent = this;
		localPosition = v;
		position.zero();
		localScale.ones();
		eulerAngles = 0.0f;
	}

	Transform::Transform(Vector2 v, Vector2 s)
	{
		parent = this;
		localPosition = v;
		position.zero();
		localScale = s;
		eulerAngles = 0.0f;
	}

	Transform::Transform(Vector2 v, Vector2 s, float r)
	{
		parent = this;
		localPosition = v;
		position.zero();
		localScale = s;
		eulerAngles = r;
	}

	void Transform::Translate(float x, float y)
	{
		Translate(Vector2(x, y));
	}

	void Transform::Translate(Vector2 v)
	{
		localPosition += v;
	}
}