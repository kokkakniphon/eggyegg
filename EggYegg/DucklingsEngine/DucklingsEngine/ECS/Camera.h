#pragma once

#include "Object.h"
#include "Vector4D.h"
#include "Color.h"
#include "Transform.h"

namespace DucklingsEngine
{
	class Camera : public Component
	{
	private:
		Transform* m_Transform;
		Vector2 m_ViewOffset;

		float m_ScreenScale;

	public:
		Object* gameObject;

		//
		// Summary:
		//     The color with which the screen will be cleared.
		Color backgroundColor;

		Vector2 rect = Vector2(1920, 1080);

		//
		// Summary:
		//     The first enabled camera tagged "MainCamera" (Read Only).
		static Camera* mainCamera;

		//
		// Summary:
		//     Transforms position from world space into screen space.
		//
		// Parameters:
		//   position:
		Vector2 WorldToScreenPoint(Vector2 position);
		Vector2 ScreenPointToWorld(Vector2 screenPoint);

		void Render(const Vector2& screenSize);

		Camera();
		~Camera();

		int GetComponentClassID() override { return 20; }
		std::string GetComponentClassData() override
		{
			std::string data = "";
			data += "Camera:\n";
			data += "  m_GameObject: {fileID: ";
			data += std::to_string(uniqueID);
			data += "}\n";
			data += "  m_Enable: ";
			data += std::to_string(enabled);
			data += "\n";
			data += "  m_Rect: {x: ";
			data += std::to_string(rect.x);
			data += ", y: ";
			data += std::to_string(rect.y);
			data += "}\n";
			data += "  m_BackgroundColor: {r: ";
			data += std::to_string(backgroundColor.r);
			data += ", g: ";
			data += std::to_string(backgroundColor.g);
			data += ", b: ";
			data += std::to_string(backgroundColor.b);
			data += ", a: ";
			data += std::to_string(backgroundColor.a);
			data += "}\n";
			return data;
		}

		void SetViewOffset(const Vector2 &offset) { m_ViewOffset = offset; }

		//
		// Summary: !!Only call this on Editor.
		//     Draw camera boundary
		//
		void OnDrawGizmos(const Vector2 &center, const Vector2 &rect);

		void Init();
		void Start() {}
		void Update() {}
		void FixedUpdate() {}

		Transform* GetTransform() { return m_Transform; }
	};
}
