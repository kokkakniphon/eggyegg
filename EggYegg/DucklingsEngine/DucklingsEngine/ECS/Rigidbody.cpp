#include "Rigidbody.h"

namespace DucklingsEngine
{
	void Rigidbody::Init()
	{
		mass = UNI_MASS;
		gravityScale = GRAVITY;

		m_Transform = &this->gameObject->GetComponent<Transform>();
	}

	void Rigidbody::FixedUpdate()
	{
		if (simulated == false)
			return;

		float fixedDeltaTime = DuckTime::GetFixedDeltaTime();

		m_Accelaration = Vector2(-velocity.x * drag, -velocity.y * drag) / mass;
		m_Accelaration += Vector2(0, gravityScale * GRAVITY) / mass;
		velocity += m_Accelaration * fixedDeltaTime;
		velocity.y = std::min(velocity.y, MAX_TERMINAL_VELOCITY);
		m_Transform->Translate(velocity * fixedDeltaTime);

		inertia = -angularVelocity * angularDrag;
		angularVelocity += inertia * fixedDeltaTime;
		m_Transform->localEulerAngles += angularVelocity * fixedDeltaTime;
	}

	void Rigidbody::AddTorque(float torque) // NEEDED to be implemented
	{
		
	}

	void Rigidbody::SetRotation(float angle)  // NEEDED to be implemented
	{

	}
}