#pragma once

#include "Renderer.h"
#include "Color.h"

#include <string>
#include <bitset>

#include "SDL.h"
#include "SDL_image.h"
#include "SDL_ttf.h"

namespace DucklingsEngine
{
	//
	// Summary:
	//     Render text to screen.
	class TextRenderer : public Renderer
	{
	private:
		SDL_Rect m_DstRect = { 0, 0, 0, 0 };
		SDL_RendererFlip m_Flip = SDL_FLIP_NONE;

		TTF_Font* m_LoadedFont;
		SDL_Surface* m_Surface;
		SDL_Texture* m_Texture;

	public:
		//
		// Summary:
		//     The string data of the mesh.
		std::string text = "";

		//
		// Summary:
		//     Font .ttf file.
		std::string font;

		//
		// Summary:
		//     Style of the font, (Bold, Italic, Underline, Strikethrough).
		int fontStyle = 0x00;

		//
		// Summary:
		//     Size of font mesh.
		float fontSize = 35;

		//
		// Summary:
		//     Displaying color.
		Color fontColor;

		int GetComponentClassID() override { return 102; }
		std::string GetComponentClassData() override;

		TextRenderer() = default;
		TextRenderer(std::string text, std::string font);
		~TextRenderer() = default;

		bool IsNoStyle();
		bool IsBold();
		bool IsItalic();
		bool IsUnderline();
		bool IsStrikethrough();

		void SetBold(bool val);
		void SetItalic(bool val);
		void SetUnderline(bool val);
		void SetStrikethrough(bool val);

		void ClearText();
		void RefreshText();

		void Start() {}
		void Update() {}
		void FixedUpdate() {}

		void Draw(const Vector2 &center, const Vector2 &rect);
	};

}

