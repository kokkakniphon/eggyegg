#pragma once

#include "EdgeCollider.h"

constexpr float deg2rad = M_PI / 180;
constexpr float rad2deg = 180 / M_PI;

namespace DucklingsEngine
{
	class BoxCollider : public EdgeCollider
	{
	private:
		bool m_Calculated_Rect;

		void UpdateRect();

	public:

		//
		// Summary:
		//     The width and height of the rectangle.
		Vector2 size = Vector2::ones() * 50;

		//
		// Summary:
		//     Check if a collider overlaps a point in space.
		//
		// Parameters:
		//   point:
		//     A point in world space.
		//
		// Returns:
		//     Does point overlap the collider?
		bool OverlapPoint(Vector2 point) override;
		//
		// Summary:
		//     Get a list of all colliders that overlap this collider.
		//
		// Parameters:
		//
		//   collider:
		//     The target collider to calculate the overlaping.
		//
		// Returns:
		//     Does this collider overlap the target collider?
		bool OverlapCollider(Collider* collider) override;

		void StaticResolution(Collider* collider) override;
		void DynamicResolution(Collider* collider) override;

		int GetComponentClassID() override { return 61; }
		std::string GetComponentClassData() override
		{
			std::string data = "";
			data += "BoxCollider:\n";
			data += "  m_GameObject: {fileID: ";
			data += std::to_string(uniqueID);
			data += "}\n";
			data += "  m_Enable: ";
			data += std::to_string(enabled);
			data += "\n";
			data += "  m_Density: ";
			data += std::to_string(density);
			data += "\n";
			data += "  m_IsTrigger: ";
			data += std::to_string(isTrigger);
			data += "\n";
			data += "  m_Offset: {x: ";
			data += std::to_string(offset.x);
			data += ", y: ";
			data += std::to_string(offset.y);
			data += "}\n";
			data += "  m_Size: {x: ";
			data += std::to_string(size.x);
			data += ", y: ";
			data += std::to_string(size.y);
			data += "}\n";
			return data;
		}

		void Init() override;
		void Start() {}
		void Update() {}
		void FixedUpdate();
		void OnDrawGizmos(const Vector2 &center, const Vector2 &rect) override;
	};
}

