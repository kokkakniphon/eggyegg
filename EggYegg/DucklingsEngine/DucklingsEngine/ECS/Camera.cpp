#include "Camera.h"

#include "Engine.h"
#include "SceneWindow.h"

namespace DucklingsEngine
{
	Camera* Camera::mainCamera = nullptr;

	Vector2 Camera::WorldToScreenPoint(Vector2 position)
	{
		position = position * m_ScreenScale;
		position -= (rect / 2) - m_Transform->GetPosition() + m_ViewOffset;
		return position;
	}

	Vector2 Camera::ScreenPointToWorld(Vector2 screenPoint)
	{
		screenPoint += (rect / 2) - m_ViewOffset;
		screenPoint = Vector2(screenPoint.x / m_ScreenScale, screenPoint.y / m_ScreenScale);
		screenPoint += m_Transform->GetPosition();

		return screenPoint;
	}

	void Camera::Render(const Vector2& screenSize)
	{
		float scaleX = (float)screenSize.x / rect.x;
		float scaleY = (float)screenSize.y / rect.y;
		m_ScreenScale = (scaleX < scaleY) ? scaleX : scaleY;

		SDL_RenderSetScale(Engine::GetInstance().GetRenderer(), m_ScreenScale, m_ScreenScale);
		RendererManager::GetInstance().Draw(m_Transform->GetPosition(), rect);
	}

	void Camera::OnDrawGizmos(const Vector2 &center, const Vector2 &view_rect)
	{
		// Render camera boundary
		Vector2 sourcePos = this->m_Transform->GetPosition();

		for (unsigned int i = 0; i < 4; i++)
		{
			Vector2 screenP1 = sourcePos, screenP2 = sourcePos;
			if (i == 0) { screenP1 -= rect / 2; screenP2 += Vector2(-rect.x, rect.y) / 2; }
			else if (i == 1) { screenP1 += Vector2(-rect.x, rect.y) / 2; screenP2 += rect / 2; }
			else if (i == 2) { screenP1 += rect / 2; screenP2 += Vector2(rect.x, -rect.y) / 2; }
			else { screenP1 += Vector2(rect.x, -rect.y) / 2; screenP2 -= rect / 2; }
				
			screenP1 += view_rect / 2 - center;
			screenP2 += view_rect / 2 - center;
			SDL_SetRenderDrawColor(Engine::GetInstance().GetRenderer(), 255, 255, 255, 255);
			SDL_RenderDrawLine(Engine::GetInstance().GetRenderer(), screenP1.x, screenP1.y, screenP2.x, screenP2.y);
		}
	}

	Camera::Camera()
	{
		Camera::mainCamera = this;
	}

	Camera::~Camera()
	{
		if (Camera::mainCamera == this)
		{
			Camera::mainCamera = nullptr;
		}
	}

	void Camera::Init()
	{
		m_Transform = &this->gameObject->GetComponent<Transform>();

		if (Camera::mainCamera == nullptr)
			Camera::mainCamera = this;
	}
}