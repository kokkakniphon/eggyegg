#include "TextRenderer.h"

#include "Engine.h"

#include <iostream>

namespace DucklingsEngine
{

	static std::string s_FontPath = "../Assets/Fonts/";

	TextRenderer::TextRenderer(std::string text, std::string font)
	{
		this->text = text;
		this->font = font;
		RefreshText();
	}

	// Get font style

	bool TextRenderer::IsNoStyle()
	{
		std::bitset<4> b(fontStyle);
		return b.none();
	}

	bool TextRenderer::IsBold()
	{
		std::bitset<4> b(fontStyle);
		return (b[0]);
	}

	bool TextRenderer::IsItalic()
	{
		std::bitset<4> b(fontStyle);
		return (b[1]);
	}

	bool TextRenderer::IsUnderline()
	{
		std::bitset<4> b(fontStyle);
		return (b[2]);
	}

	bool TextRenderer::IsStrikethrough()
	{
		std::bitset<4> b(fontStyle);
		return (b[3]);
	}

	// Set font style.

	void TextRenderer::SetBold(bool val)
	{
		std::bitset<4> b(fontStyle);
		b.set(0, val);
		fontStyle = b.to_ulong();
		RefreshText();
	}

	void TextRenderer::SetItalic(bool val)
	{
		std::bitset<4> b(fontStyle);
		b.set(1, val);
		fontStyle = b.to_ulong();
		RefreshText();
	}

	void TextRenderer::SetUnderline(bool val)
	{
		std::bitset<4> b(fontStyle);
		b.set(2, val);
		fontStyle = b.to_ulong();
		RefreshText();
	}

	void TextRenderer::SetStrikethrough(bool val)
	{
		std::bitset<4> b(fontStyle);
		b.set(3, val);
		fontStyle = b.to_ulong();
		RefreshText();
	}

	void TextRenderer::ClearText()
	{
		if (m_LoadedFont != nullptr)
			TTF_CloseFont(m_LoadedFont);
		if (m_Surface != nullptr)
			SDL_FreeSurface(m_Surface);
		if (m_Texture != nullptr)
			SDL_DestroyTexture(m_Texture);
	}

	void TextRenderer::RefreshText()
	{
		ClearText();

		m_LoadedFont = TTF_OpenFont((s_FontPath + font).c_str(), fontSize);
		if (m_LoadedFont == nullptr)
		{
			std::cout << "TextRenderer: Fail to load font <" << font << ">" << std::endl;
			return;
		}

		
		TTF_SetFontStyle(m_LoadedFont, fontStyle);
		SDL_Color color = { fontColor.r * 255.0f, fontColor.g * 255.0f, fontColor.b * 255.0f, fontColor.a * 255.0f };
		if (this->transform == nullptr)
			m_Surface = TTF_RenderText_Solid(m_LoadedFont, text.c_str(), color);
		else
			m_Surface = TTF_RenderText_Blended_Wrapped(m_LoadedFont, text.c_str(), color, this->transform->localScale.x);
		m_Texture = SDL_CreateTextureFromSurface(Engine::GetInstance().GetRenderer(), m_Surface);

	}

	std::string TextRenderer::GetComponentClassData()
	{
		std::string data = "";
		data += "TextRenderer:\n";
		data += "  m_GameObject: {fileID: ";
		data += std::to_string(uniqueID);
		data += "}\n";
		data += "  m_Enable: ";
		data += std::to_string(enabled);
		data += "\n";
		data += "  m_Text: {textData: ";
		data += text;
		data += "}\n";
		data += "  m_Font: {fileName: ";
		data += font;
		data += "}\n";
		data += "  m_FontStyle: ";
		data += std::to_string(fontStyle);
		data += "\n";
		data += "  m_FontSize: ";
		data += std::to_string(fontSize);
		data += "\n";
		data += "  m_FontColor: {r: ";
		data += std::to_string(fontColor.r);
		data += ", g: ";
		data += std::to_string(fontColor.g);
		data += ", b: ";
		data += std::to_string(fontColor.b);
		data += ", a: ";
		data += std::to_string(fontColor.a);
		data += "}\n";
		data += "  m_Order: ";
		data += std::to_string(order);
		data += "\n";
		return data;
	}

	void TextRenderer::Draw(const Vector2 & center, const Vector2 & rect)
	{
		if (text == "")
			return;

		if (enabled == true)
		{
			Vector2 thisPos = transform->GetPosition();
			thisPos = thisPos + rect / 2 - center;
			m_DstRect.x = static_cast<int>(thisPos.x - m_DstRect.w / 2);
			m_DstRect.y = static_cast<int>(thisPos.y - m_DstRect.h / 2);
			m_DstRect.w = m_Surface->w;
			m_DstRect.h = m_Surface->h;

			SDL_RenderCopyEx(Engine::GetInstance().GetRenderer(), m_Texture, NULL, &m_DstRect, transform->GetEulerAngles(), nullptr, m_Flip);
		}
	}
}