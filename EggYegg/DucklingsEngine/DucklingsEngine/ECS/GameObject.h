#pragma once

#include "Object.h"
#include "Transform.h"

namespace DucklingsEngine
{
	class GameObject : public Object
	{
	public:
		
		//
		// Summary:
		//     The Transform attached to this GameObject.
		Transform* transform;

		int GetComponentClassID() { return 1; }
		std::string GetComponentClassData() 
		{
			std::string data = "";
			data += "GameObject:\n";
			data += "  m_Layer: ";
			data += layer;
			data += "\n";
			data += "  m_Name: ";
			data += name;
			data += "\n";
			data += "  m_TagString: ";
			data += tag;
			data += "\n";
			data += "  m_IsActive: ";
			data += std::to_string(GetActive());
			data += "\n";
			return data;
		}

		//
		// Summary:
		//     Creates a new game object, named name.
		//
		// Parameters:
		//   name:
		//     The name that the GameObject is created with.
		//
		//   components:
		//     A list of Components to add to the GameObject on creation.
		GameObject();
		//
		// Summary:
		//     Creates a new game object, named name.
		//
		// Parameters:
		//   name:
		//     The name that the GameObject is created with.
		GameObject(std::string name);
		~GameObject() = default;
	};
}