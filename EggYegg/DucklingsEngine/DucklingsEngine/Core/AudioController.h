
// Use this as a base for the SoundContoller logic.
// the structure shouldn't be too different from the final product that we aim for.
// try to reuse as much as possible to save time.

#pragma once

#include <iostream>
#include <map>
#include <string>
#include <vector>
#include "irrKlang.h"
#include "Unique.h"

#define AUDIO_SOURCE_CLASS_ID 1
#define PARAMETER_TRANSITION_CLASS_ID 2
#define PARAMETER_GLOBAL_CLASS_ID 3

static std::string ControllerTypeName = ".SoundController";

//irrklang::ISoundEngine* audioEngine = irrklang::createIrrKlangDevice();

namespace KHORA
{
	enum ParameterType
	{
		Integer,
		Float,
		Boolean
	};

	struct Parameter
	{
	public:

		Parameter(std::string name, ParameterType type = Boolean) {
			this->name = name;
			this->type = type;

			if (type == Boolean) {
				SetConditionBool(true);
			}
			else if (type == Integer) {
				SetConditionInt(true, 0);
			}
			else if (type == Float) {
				SetConditionFloat(true, 0.0f);
			}
		}

		/*Parameter(std::string name, ParameterType type = Float, bool conditionBool = false, bool conditionInt = false, bool conditionFloat = false, bool checkValueBool = false, bool isGreaterThan = false, int checkValueInt = 0, float checkValueFloat = 0.0f)
		{
			this->name = name;
			this->type = type;
			this->conditionBool = conditionBool;
			this->conditionInt = conditionInt;
			this->conditionFloat = conditionFloat;
			this->isGreaterThan = isGreaterThan;
			this->checkValueBool = checkValueBool;
			this->checkValueInt = checkValueInt;
			this->checkValueFloat = checkValueFloat;
			this->currentValueBool = false;
		}*/

		void SetConditionBool(bool checkValueBool)
		{
			this->type = ParameterType::Boolean;
			this->checkValueBool = checkValueBool;
			this->checkValueInt = 0;
			this->currentValueFloat = 0.0f;
		}

		void SetConditionInt(bool isGreaterThan, bool checkValueInt)
		{
			this->type = ParameterType::Integer;
			this->isGreaterThan = isGreaterThan;
			this->checkValueBool = false;
			this->checkValueInt = checkValueInt;
			this->checkValueFloat = 0.0f;
		}

		void SetConditionFloat(bool isGreaterThan, bool checkValueFloat) 
		{
			this->type = ParameterType::Float;
			this->isGreaterThan = isGreaterThan;
			this->checkValueBool = false;
			this->checkValueInt = 0;
			this->checkValueFloat = checkValueFloat;
		}

		bool CheckParameterCondition()
		{
			if (this->type == ParameterType::Boolean)
			{
				return (checkValueBool == currentValueBool);
			}
			else if (this->type == ParameterType::Integer)
			{
				if (isGreaterThan) return (currentValueInt > checkValueInt);
				else return (currentValueInt < checkValueInt);
			}
			else if (this->type == ParameterType::Float)
			{
				if (isGreaterThan) return (currentValueFloat > checkValueFloat);
				else return (currentValueFloat < checkValueFloat);
			}
		}

		std::string name = "";
		ParameterType type;
		bool isGreaterThan = false;
		bool checkValueBool = false;
		int checkValueInt = 0;
		float checkValueFloat = 0.0f;
		bool currentValueBool = false;
		int currentValueInt = 0;
		float currentValueFloat = 0.0f;

		//======================FILE OUTPUT GETTER======================

		int GetTransitionID() {
			return PARAMETER_TRANSITION_CLASS_ID;
		}

		int GetGlobalID() {
			return PARAMETER_GLOBAL_CLASS_ID;
		}

		std::string GetParameterData() {
			std::string data = "";

			data += "Parameter:\n";

			//=============================
			data += "  name: ";
			data += name;
			data += "\n";
			//=============================
			data += "  type: ";
			if (type == Float) {
				data += "Float";
			}
			else if (type == Integer) {
				data += "Integer";
			}
			else if (type == Boolean) {
				data += "Boolean";
			}
			data += "\n";
			//=============================
			data += "  isGreaterThan: ";
			if (isGreaterThan) {
				data += "true";
			}
			else {
				data += "false";
			}
			data += "\n";
			//=============================
			data += "  checkValueBool: ";
			if (checkValueBool) {
				data += "true";
			}
			else {
				data += "false";
			}
			data += "\n";
			//=============================
			data += "  checkValueInt: ";
			data += std::to_string(checkValueInt);
			data += "\n";
			//=============================
			data += "  checkValueFloat: ";
			data += std::to_string(checkValueFloat);
			data += "\n";
			//=============================
			data += "  currentValueBool: ";
			if (currentValueBool) {
				data += "true";
			}
			else {
				data += "false";
			}
			data += "\n";
			//=============================
			data += "  currentValueInt: ";
			data += std::to_string(currentValueInt);
			data += "\n";
			//=============================
			data += "  currentValueFloat: ";
			data += std::to_string(currentValueFloat);
			data += "\n";
			//=============================
			return data;
		}

		//======================FILE OUTPUT GETTER======================
	};

	struct AudioSource
	{
		//float animationStartTime = 0;

		std::string name = "";

		std::string filePath;

		float volume;

		bool isLoopable;

		std::map<std::string, std::vector<Parameter>> destinations;

		//Sprite* sprites; NOTE: Replace this with audio data.

		//int framePerSecond = 1;

		AudioSource() = default;

		AudioSource(std::string name, float volume, bool isLoopable, std::string filePath)
		{
			this->name = name;
			this->filePath = filePath;
			this->volume = volume;
			this->isLoopable = isLoopable;
		}

		//
		// Summary:
		//     Return "" if no paramiters meet destination conditions. else return destination.
		std::string CheckAllParameterCondition()
		{
			for (std::map<std::string, std::vector<Parameter>>::iterator condition = destinations.begin(); condition != destinations.end(); condition++)
			{
				if (condition->second.size() == 0) {
					if (this->name == "Entry")
					{
						return condition->first;
					}
					continue;
				}
				bool isPass = true;
				for (unsigned int i = 0; i < condition->second.size(); i++)
				{
					if (condition->second[i].CheckParameterCondition() == false)
					{
						isPass = false;
						break;
					}
				}

				if (isPass == true)
				{
					return condition->first;
				}
			}

			return "";
		}

		//
		// Summary:
		//     Sets the value of the given boolean parameter.
		//
		// Parameters:
		//   name:
		//     The parameter name.
		//
		//   id:
		//     The parameter ID.
		//
		//   value:
		//     The new parameter value.
		void SetBool(std::string name, bool value)
		{
			for (std::map<std::string, std::vector<Parameter>>::iterator condition = destinations.begin(); condition != destinations.end(); condition++)
			{
				for (unsigned int i = 0; i < condition->second.size(); i++)
				{
					if (condition->second[i].name == name)
					{
						condition->second[i].currentValueBool = value;
					}
				}
			}
		}

		//
		// Summary:
		//     Sets the value of the given integer parameter.
		//
		// Parameters:
		//   name:
		//     The parameter name.
		//
		//   id:
		//     The parameter ID.
		//
		//   value:
		//     The new parameter value.
		void SetInteger(std::string name, int value)
		{
			for (std::map<std::string, std::vector<Parameter>>::iterator condition = destinations.begin(); condition != destinations.end(); condition++)
			{
				for (unsigned int i = 0; i < condition->second.size(); i++)
				{
					if (condition->second[i].name == name)
					{
						condition->second[i].currentValueInt = value;
					}
				}
			}
		}

		//
		// Summary:
		//     Sets the value of the given float value parameter.
		//
		// Parameters:
		//   name:
		//     The parameter name.
		//
		//   id:
		//     The parameter ID.
		//
		//   value:
		//     The new parameter value.
		void SetFloat(std::string name, float value)
		{
			for (std::map<std::string, std::vector<Parameter>>::iterator condition = destinations.begin(); condition != destinations.end(); condition++)
			{
				for (unsigned int i = 0; i < condition->second.size(); i++)
				{
					if (condition->second[i].name == name)
					{
						condition->second[i].currentValueFloat = value;
					}
				}
			}
		}

		//
		// Summary:
		//     Return the value of boolean of the given name parameter.
		//
		// Parameters:
		//   name:
		//     The parameter name.
		//
		//   id:
		//     The parameter ID.
		//
		bool GetBool(std::string name)
		{
			for (std::map<std::string, std::vector<Parameter>>::iterator condition = destinations.begin(); condition != destinations.end(); condition++)
			{
				for (unsigned int i = 0; i < condition->second.size(); i++)
				{
					if (condition->second[i].name == name)
					{
						return condition->second[i].currentValueBool;
					}
				}
			}
		}

		//
		// Summary:
		//     Return the value of integer of the given name parameter.
		//
		// Parameters:
		//   name:
		//     The parameter name.
		//
		//   id:
		//     The parameter ID.
		//
		int GetInt(std::string name)
		{
			for (std::map<std::string, std::vector<Parameter>>::iterator condition = destinations.begin(); condition != destinations.end(); condition++)
			{
				for (unsigned int i = 0; i < condition->second.size(); i++)
				{
					if (condition->second[i].name == name)
					{
						return condition->second[i].currentValueInt;
					}
				}
			}
		}

		//
		// Summary:
		//     Return the value of float of the given name parameter.
		//
		// Parameters:
		//   name:
		//     The parameter name.
		//
		//   id:
		//     The parameter ID.
		//
		float GetFloat(std::string name)
		{
			for (std::map<std::string, std::vector<Parameter>>::iterator condition = destinations.begin(); condition != destinations.end(); condition++)
			{
				for (unsigned int i = 0; i < condition->second.size(); i++)
				{
					if (condition->second[i].name == name)
					{
						return condition->second[i].currentValueFloat;
					}
				}
			}
		}

		//assign new parameter (used when set new type of the parameter from the controller)
		void SetType(Parameter parameter) 
		{
			for (std::map<std::string, std::vector<Parameter>>::iterator condition = destinations.begin(); condition != destinations.end(); condition++)
			{
				for (unsigned int i = 0; i < condition->second.size(); i++)
				{
					if (condition->second[i].name == parameter.name)
					{
						condition->second[i] = parameter;
					}
				}
			}
		}

		//======================FILE OUTPUT GETTER======================

		int GetClassID() {
			return AUDIO_SOURCE_CLASS_ID;
		}

		std::string GetTransitionData(std::string destination) {
			std::string data = "";

			data += "Transition:\n";

			//=============================
			data += "  startingNode: ";
			data += name;
			data += "\n";
			//=============================
			data += "  destinationNode: ";
			data += destination;
			data += "\n";
			//=============================

			return data;
		}

		std::string GetAudioSourceData() {
			std::string data = "";

			data += "AudioSource:\n";

			//=============================
			data += "  name: ";
			data += name;
			data += "\n";
			//=============================
			data += "  volume: ";
			data += std::to_string(volume);
			data += "\n";
			//=============================
			data += "  isLoopable: ";
			if (isLoopable) {
				data += "true";
			}
			else {
				data += "false";
			}
			data += "\n";
			//=============================
			data += "  filePath: ";
			data += filePath;
			data += "\n";
			//=============================

			return data;
		}

		//======================FILE OUTPUT GETTER======================

	};


	//
	// Summary:
	//     This script only contain the data for the Animator to use.
	struct AudioController : DucklingsEngine::Unique
	{
	public:

		AudioController()
		{
			CreateAudioNode("Entry", 0.0f, true, "");
		}

		std::string name;

		std::vector<std::pair<std::string, std::shared_ptr<AudioSource>>> audioNodes;

		std::vector<Parameter> globalParameters;

		//
		// Summary:
		//     Return "" if animations can't be found in animations. else return destination.
		std::string GetDestinationAudioNode(std::string currentAudioNode)
		{
			for (std::vector<std::pair<std::string, std::shared_ptr<AudioSource>>>::iterator audioNode = audioNodes.begin(); audioNode != audioNodes.end(); audioNode++)
			{
				if (audioNode->first == currentAudioNode)
				{
					return audioNode->second->CheckAllParameterCondition();
				}
			}
			return "";
		}

		AudioSource* GetAudioNode(std::string targetAudioNode)
		{
			for (std::vector<std::pair<std::string, std::shared_ptr<AudioSource>>>::iterator audioNode = audioNodes.begin(); audioNode != audioNodes.end(); audioNode++)
			{
				if (audioNode->first == targetAudioNode)
				{
					return audioNode->second.get();
				}
			}
			return nullptr;
		}

		//void /* Sprite* */ GetAnimationSprite(std::string targetAudioNode)
		//{
		//	for (std::map<std::string, std::shared_ptr<AudioSource>>::iterator audioNode = audioNodes.begin(); audioNode != audioNodes.end(); audioNode++)
		//	{
		//		if (audioNode->first == targetAudioNode)
		//		{
		//			//return animation->second->sprites;
		//		}
		//	}
		//	//return nullptr;
		//}

		//
		// Summary:
		//     Returns the value of the given boolean parameter.
		//
		// Parameters:
		//   name:
		//     The parameter name.
		//
		//   id:
		//     The parameter ID.
		//
		// Returns:
		//     The value of the parameter.
		bool GetBool(std::string name)
		{
			for (unsigned int i = 0; i < globalParameters.size(); i++)
			{
				if (globalParameters[i].name == name)
				{
					return globalParameters[i].currentValueBool;
				}
			}
			return false;
		}

		//
		// Summary:
		//     Returns the value of the given integer parameter.
		//
		// Parameters:
		//   name:
		//     The parameter name.
		//
		//   id:
		//     The parameter ID.
		//
		// Returns:
		//     The value of the parameter.
		int GetInteger(std::string name)
		{
			for (unsigned int i = 0; i < globalParameters.size(); i++)
			{
				if (globalParameters[i].name == name)
				{
					return globalParameters[i].currentValueInt;
				}
			}
			return 0;
		}

		//
		// Summary:
		//     Returns the value of the given float parameter.
		//
		// Parameters:
		//   name:
		//     The parameter name.
		//
		//   id:
		//     The parameter ID.
		//
		// Returns:
		//     The value of the parameter.
		float GetFloat(std::string name)
		{
			for (unsigned int i = 0; i < globalParameters.size(); i++)
			{
				if (globalParameters[i].name == name)
				{
					return globalParameters[i].currentValueFloat;
				}
			}
			return 0.0f;
		}

		//
		// Summary:
		//     Sets the value of the given boolean parameter.
		//
		// Parameters:
		//   name:
		//     The parameter name.
		//
		//   id:
		//     The parameter ID.
		//
		//   value:
		//     The new parameter value.
		void SetBool(std::string name, bool value)
		{
			for (unsigned int i = 0; i < globalParameters.size(); i++)
			{
				if (globalParameters[i].name == name)
				{
					globalParameters[i].currentValueBool = value;
				}
			}

			for (std::vector<std::pair<std::string, std::shared_ptr<AudioSource>>>::iterator audioNode = audioNodes.begin(); audioNode != audioNodes.end(); audioNode++)
			{
				audioNode->second->SetBool(name, value);
			}
		}

		//
		// Summary:
		//     Sets the value of the given integer parameter.
		//
		// Parameters:
		//   name:
		//     The parameter name.
		//
		//   id:
		//     The parameter ID.
		//
		//   value:
		//     The new parameter value.
		void SetInteger(std::string name, int value)
		{
			for (unsigned int i = 0; i < globalParameters.size(); i++)
			{
				if (globalParameters[i].name == name)
				{
					globalParameters[i].currentValueInt = value;
				}
			}

			for (std::vector<std::pair<std::string, std::shared_ptr<AudioSource>>>::iterator audioNode = audioNodes.begin(); audioNode != audioNodes.end(); audioNode++)
			{
				audioNode->second->SetInteger(name, value);
			}
		}

		//
		// Summary:
		//     Sets the value of the given float parameter.
		//
		// Parameters:
		//   name:
		//     The parameter name.
		//
		//   id:
		//     The parameter ID.
		//
		//   value:
		//     The new parameter value.
		void SetFloat(std::string name, float value)
		{
			for (unsigned int i = 0; i < globalParameters.size(); i++)
			{
				if (globalParameters[i].name == name)
				{
					globalParameters[i].currentValueFloat = value;
				}
			}

			for (std::vector<std::pair<std::string, std::shared_ptr<AudioSource>>>::iterator audioNode = audioNodes.begin(); audioNode != audioNodes.end(); audioNode++)
			{
				audioNode->second->SetFloat(name, value);
			}
		}

		//Set parameter's type
		void SetType(std::string name, ParameterType type) {

			for (unsigned int i = 0; i < globalParameters.size(); i++)
			{
				if (globalParameters[i].name == name && globalParameters[i].type != type)
				{
					Parameter parameter(name, type);
					globalParameters[i] = parameter;

					for (std::vector<std::pair<std::string, std::shared_ptr<AudioSource>>>::iterator audioNode = audioNodes.begin(); audioNode != audioNodes.end(); audioNode++)
					{
						audioNode->second->SetType(parameter);
					}
				}
			}
		}
		//add a global parameter by passing parameter
		void AddGlobalParameter(Parameter parameter) {
			globalParameters.push_back(parameter);
		}

		//add a new global parameter
		void AddGlobalParameter(std::string parameterName, ParameterType type = Boolean) {
			Parameter parameter(parameterName, type);
			globalParameters.push_back(parameter);
		}

		//remove a global parameter
		void RemoveGlobalParameter(std::string parameterName) {

			//remove a global parameter data inside the global parameter vector
			for (unsigned int i = 0; i < globalParameters.size(); i++) {
				if (globalParameters[i].name == parameterName) {
					globalParameters.erase(globalParameters.begin() + i);
					return;
				}
			}

			//remove the parameter with the same name in all transitions
			//loop through all audio nodes
			for (std::vector<std::pair<std::string, std::shared_ptr<AudioSource>>>::iterator audioNode = audioNodes.begin(); audioNode != audioNodes.end(); audioNode++)
			{
				//loop through all transitions
				for (std::map<std::string, std::vector<Parameter>>::iterator destination = audioNode->second->destinations.begin(); destination != audioNode->second->destinations.end(); destination++) {
					RemoveTransitionParameter(parameterName, audioNode->first, destination->first);
				}
			}
		}

		//does not need to use for other task (used only in AddTransitionParameter function)
		//return a global parameter 
		Parameter GetGlobalParameter(std::string name) {
			for (unsigned int i = 0; i < globalParameters.size(); i++) {
				if (globalParameters[i].name == name) {
					return globalParameters[i];
				}
			}

			// if no global parameter found return a default global parameter
			return Parameter("");
		}

		std::vector<Parameter>* GetGlobalParameters()
		{
			return &globalParameters;
		}

		//add a new transition parameter by passing parameter
		void AddTransitionParameter(Parameter parameter, std::string startingNode, std::string destinationNode) {
			for (size_t i = 0; i < audioNodes.size(); i++)
			{
				if (audioNodes[i].first == startingNode)
				{
					audioNodes[i].second->destinations[destinationNode].push_back(parameter);
					break;
				}
			}
		}

		//add a new transition parameter
		void AddTransitionParameter(std::string parameterName, std::string startingNode, std::string destinationNode) {

			//check whether to global parameter exists
			if (GetGlobalParameter(parameterName).name != "") {
				//add parameter to the specific transition as a condition
				for (size_t i = 0; i < audioNodes.size(); i++)
				{
					if (audioNodes[i].first == startingNode)
					{
						audioNodes[i].second->destinations[destinationNode].push_back(GetGlobalParameter(parameterName));
						break;
					}
				}
			}

		}

		//remove a transition parameter
		void RemoveTransitionParameter(std::string parameterName, std::string startingNode, std::string destinationNode) {

			//loop through all conditions in the specific transition

			for (size_t i = 0; i < audioNodes.size(); i++)
			{
				if (audioNodes[i].first == startingNode)
				{
					for (unsigned int i = 0; i < audioNodes[i].second->destinations[destinationNode].size(); i++) {

						//check for the removing parameter name
						if (parameterName == audioNodes[i].second->destinations[destinationNode][i].name) {

							//erase the parameter
							audioNodes[i].second->destinations[destinationNode].erase(audioNodes[i].second->destinations[destinationNode].begin() + i);

							//check if there's no parameter left in the transition
							/*if (audioNodes[startingNode]->destinations[destinationNode].size() == 0) {
								//delete the transition
								RemoveTransition(startingNode, destinationNode);
							}*/

							//return;
						}
					}
					break;
				}
			}
			
		}

		//set transition parameter's condition
		void SetConditionBool(std::string parameterName, std::string startingNode, std::string destinationNode, bool checkingValue)
		{
			//loop through all conditions in the specific transition
			for (size_t i = 0; i < audioNodes.size(); i++)
			{
				if (audioNodes[i].first == startingNode)
				{
					for (unsigned int i = 0; i < audioNodes[i].second->destinations[destinationNode].size(); i++) {

						//check for the parameter name
						if (parameterName == audioNodes[i].second->destinations[destinationNode][i].name) {

							audioNodes[i].second->destinations[destinationNode][i].SetConditionBool(checkingValue);
						}
					}
					break;
				}
			}
		}

		void SetConditionInt(std::string parameterName, std::string startingNode, std::string destinationNode, bool isGreaterThan, int checkingValue) {
			//loop through all conditions in the specific transition
			for (size_t i = 0; i < audioNodes.size(); i++)
			{
				if (audioNodes[i].first == startingNode)
				{
					for (unsigned int i = 0; i < audioNodes[i].second->destinations[destinationNode].size(); i++) {

						//check for the parameter name
						if (parameterName == audioNodes[i].second->destinations[destinationNode][i].name) {

							audioNodes[i].second->destinations[destinationNode][i].SetConditionInt(isGreaterThan, checkingValue);
						}
					}
					break;
				}
			}
		}

		void SetConditionFloat(std::string parameterName, std::string startingNode, std::string destinationNode, bool isGreaterThan, float checkingValue) {
			//loop through all conditions in the specific transition
			for (size_t i = 0; i < audioNodes.size(); i++)
			{
				if (audioNodes[i].first == startingNode)
				{
					for (unsigned int i = 0; i < audioNodes[i].second->destinations[destinationNode].size(); i++) {

						//check for the parameter name
						if (parameterName == audioNodes[i].second->destinations[destinationNode][i].name) {

							audioNodes[i].second->destinations[destinationNode][i].SetConditionFloat(isGreaterThan, checkingValue);
						}
					}
					break;
				}
			}
		}

		//add an empty transition
		std::vector<Parameter>* AddTransition(std::string startingNode, std::string destinationNode) {
			for (size_t i = 0; i < audioNodes.size(); i++)
			{
				if (audioNodes[i].first == startingNode)
				{
					return &audioNodes[i].second->destinations[destinationNode];
				}
			}
			
		}

		//remove a transition
		void RemoveTransition(std::string startingNode, std::string destinationNode) {
			for (size_t i = 0; i < audioNodes.size(); i++)
			{
				if (audioNodes[i].first == startingNode)
				{
					audioNodes[i].second->destinations.erase(destinationNode);
					break;
				}
			}
		}

		//create new node by passing AudioSource
		void CreateAudioNode(std::shared_ptr<AudioSource> node) {
			audioNodes.push_back(std::make_pair(node->name, node));
		}

		//create new node
		AudioSource* CreateAudioNode(std::string nodeName, float volume, bool isLoopable, std::string filePath) {
			std::shared_ptr<AudioSource> node = std::make_shared<AudioSource>();
			node->name = nodeName;
			node->volume = volume;
			node->isLoopable = isLoopable;
			node->filePath = filePath;
			//add to audioNodes
			audioNodes.push_back(std::make_pair(nodeName, node));
			return node.get();
		}

		//remove an existing node
		void RemoveAudioNode(std::string nodeName) {
			for (size_t i = 0; i < audioNodes.size(); i++)
			{
				if (audioNodes[i].first == nodeName)
				{
					audioNodes.erase(audioNodes.begin() + i);
					break;
				}
			}
		}

	};

}


