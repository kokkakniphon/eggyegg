#include "Engine.h"

#include "FileManager.h"
#include "BuildSettings.h"
#include "Input.h"
#include "Camera.h"

namespace DucklingsEngine
{
	SDL_Event Engine::event;

	Engine::Engine() 
	{
		m_IsRunning = false;
		m_Window = nullptr;
		m_Renderer = nullptr;

		DuckTime::GetInstance().timeScale = 1.0f;

		m_lastTimeScale = DuckTime::GetInstance().timeScale;
	}

	Engine::~Engine() {}


	//
	// Summary:
	//		Order of Execution loop
	//		- Physics
	//		- Input events
	//		- Game logics
	//		- Scene rendering
	//
	void Engine::Run()
	{
		Init();

		Settings::SetupBuildSettings();
		
		Input::Init();

		while (IsRunning())
		{
			Start();
			DuckTime::GetInstance().Tick(PHYSIC_LOOP_CALULATION);
			for (unsigned int i = 0; i < PHYSIC_LOOP_CALULATION; i++)
			{
				ColliderManager::GetInstance().FixedUpdate();
				FixedUpdate();
			}
			if(SDL_PollEvent(&event) != 0)
				Input::Listen();
			Input::Update();
			Events();
			Update();
			Render();
			Input::Clear();
			m_Manager->UnloadEntity();
		}

		Clean();
	}

	bool Engine::Init()
	{
		if (SDL_Init(SDL_INIT_VIDEO) != 0 && IMG_Init(IMG_INIT_JPG | IMG_INIT_PNG) != 0) // If fail initializing SDL return false
		{
			SDL_Log("Failed to initialize SDL: %s", SDL_GetError());
			return false;
		}

		if (TTF_Init() != 0)
		{
			SDL_Log("Failed to initialize TTF: %s", SDL_GetError());
			return false;
		}

		auto wflags = (SDL_WINDOW_RESIZABLE | SDL_WINDOW_ALLOW_HIGHDPI);
		m_Window = SDL_CreateWindow(m_WindowTitle, SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, SCREEN_WIDTH, SCREEN_HEIGHT, wflags);

		if (m_Window == nullptr)
		{
			SDL_Log("Failed to create Window: %s", SDL_GetError());
			return false;
		}


		m_Renderer = SDL_CreateRenderer(m_Window, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
		if (m_Renderer == nullptr)
		{
			SDL_Log("Failed to create Renderer: %s", SDL_GetError());
			return false;
		}

		SDL_Surface*image;
		image = SDL_LoadBMP_RW(SDL_RWFromFile("../Assets/GUI/EggYegg_logo_small.bmp", "rb"), 1);
		SDL_SetWindowIcon(m_Window, image);

		m_Manager = new EntityManager();

		m_ClearColor = DARK;

		return m_IsRunning = true;
	}

	void Engine::Start()
	{
		m_Manager->Start();
	}

	void Engine::Update()
	{
		m_Manager->Update();
	}

	void Engine::FixedUpdate()
	{
		m_Manager->FixedUpdate();
	}

	void Engine::Render()
	{
		SDL_Rect editor_view;
		editor_view.x = 0.0f;
		editor_view.y = 0.0f;
		editor_view.w = SCREEN_WIDTH;
		editor_view.h = SCREEN_HEIGHT;

		SDL_RenderSetViewport(m_Renderer, &editor_view);

		SDL_SetRenderDrawColor(m_Renderer, m_ClearColor.r, m_ClearColor.g, m_ClearColor.b, m_ClearColor.a);
		SDL_RenderClear(m_Renderer);

		Camera::mainCamera->Render(SCREEN_SIZE);
		ColliderManager::GetInstance().EndFrameUpdate();

		SDL_RenderPresent(m_Renderer);
	}

	void Engine::Events()
	{
		switch (event.type)
		{
		case SDL_QUIT:
			Engine::GetInstance().Quit();
			break;
		case SDL_WINDOWEVENT:
			switch (event.window.event)
			{
			case SDL_WINDOWEVENT_ENTER:
				m_WindowEnter = true;
				DuckTime::GetInstance().timeScale = Engine::GetInstance().m_lastTimeScale;
				break;
			case SDL_WINDOWEVENT_LEAVE:
				if (m_WindowEnter == true)
				{
					m_WindowEnter = false;
					Engine::GetInstance().m_lastTimeScale = DuckTime::GetInstance().timeScale;
					//DuckTime::GetInstance().timeScale = 0.0f;
				}
				break;
			}
			break;
		}
	}

	void Engine::Clean()
	{
		AssetManager::GetInstance().Clean();
		SceneManager::GetInstance().Clean();
		DuckTime::GetInstance().Clean();
		ColliderManager::GetInstance().Clean();
		SpriteManager::GetInstance().Clean();
		RendererManager::GetInstance().Clean();

		SDL_DestroyRenderer(m_Renderer);
		SDL_DestroyWindow(m_Window);
		TTF_Quit();
		IMG_Quit();
		SDL_Quit();

		delete m_Manager;

		Singleton<Engine>::Clean();
	}

	void Engine::Quit()
	{
		m_IsRunning = false;
	}
}
