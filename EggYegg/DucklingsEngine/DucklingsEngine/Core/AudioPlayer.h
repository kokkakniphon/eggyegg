
#pragma once

#include "irrKlang.h"
#include "AudioController.h"
#include "../Manager/AudioFileManager.h"

#define ASSET_AUDIO_PATH "../Assets/Audio/"
#define ASSET_CONTROLLER_PATH "../Assets/Controller/"

namespace KHORA 
{
	class AudioPlayer 
	{
	private:
		std::string currentAudioStateName = "";
		AudioSource* currentAudio = nullptr;

		std::string controllerName = "";

		irrklang::ISound* sound = nullptr;

		bool play = false;
		bool pause = false;

		irrklang::ISoundEngine* s_AudioEngineInstance;

		//
		// Summary:
		//     The Audio Controller to play.
		std::shared_ptr<AudioController> controller = nullptr;

		float globalVolume = 100.0f;

	public:


		AudioPlayer();

		//
		// Summary:
		//     Initialize audio player with the audio controller path.
		AudioPlayer(std::string name);

		~AudioPlayer() = default;

		inline std::string GetCurrentAudioState() {
			return currentAudioStateName;
		}

		inline AudioController* GetController() { return controller.get(); }

		void SetupController();

		void SetAudioControllerPath(std::string name);

		void SetAudioController(AudioController* controller);

		//
		// Summary:
		//     Returns the value of the given boolean parameter.
		//
		// Parameters:
		//   name:
		//     The parameter name.
		//
		//   id:
		//     The parameter ID.
		//
		// Returns:
		//     The value of the parameter.
		inline bool GetBool(std::string name)
		{
			if (controller == nullptr) return false;
			return controller->GetBool(name);
		}

		//
		// Summary:
		//     Returns the value of the given integer parameter.
		//
		// Parameters:
		//   name:
		//     The parameter name.
		//
		//   id:
		//     The parameter ID.
		//
		// Returns:
		//     The value of the parameter.
		inline int GetInteger(std::string name)
		{
			if (controller == nullptr) return 0;
			return controller->GetInteger(name);
		}

		//
		// Summary:
		//     Returns the value of the given float parameter.
		//
		// Parameters:
		//   name:
		//     The parameter name.
		//
		//   id:
		//     The parameter ID.
		//
		// Returns:
		//     The value of the parameter.
		inline float GetFloat(std::string name)
		{
			if (controller == nullptr) return 0.0f;
			return controller->GetFloat(name);
		}

		inline unsigned int GetDuration()
		{
			if (sound != nullptr)
			{
				return sound->getPlayLength();
			}
			else
			{
				return 1;
			}
		}

		inline unsigned int GetCurrentProgression()
		{
			if (sound != nullptr)
			{
				return sound->getPlayPosition();
			}
			else
			{
				return 0;
			}
		}

		inline void SetCurrentProgression(unsigned int pos)
		{
			if (sound != nullptr)
			{
				sound->setPlayPosition(pos);
			}
		}

		inline bool isPlaying()
		{
			if (sound != nullptr)
			{
				return !sound->isFinished();
			}
			else
			{
				return false;
			}
		}

		//
		// Summary:
		//     Sets the value of the given boolean parameter.
		//
		// Parameters:
		//   name:
		//     The parameter name.
		//
		//   id:
		//     The parameter ID.
		//
		//   value:
		//     The new parameter value.
		inline void SetBool(std::string name, bool value)
		{
			if (controller == nullptr) return;
			return controller->SetBool(name, value);
		}

		//
		// Summary:
		//     Sets the value of the given integer parameter.
		//
		// Parameters:
		//   name:
		//     The parameter name.
		//
		//   id:
		//     The parameter ID.
		//
		//   value:
		//     The new parameter value.
		inline void SetInteger(std::string name, int value)
		{
			if (controller == nullptr) return;
			return controller->SetInteger(name, value);
		}

		//
		// Summary:
		//     Sets the value of the given float parameter.
		//
		// Parameters:
		//   name:
		//     The parameter name.
		//
		//   id:
		//     The parameter ID.
		//
		//   value:
		//     The new parameter value.
		inline void SetFloat(std::string name, float value)
		{
			if (controller == nullptr) return;
			return controller->SetFloat(name, value);
		}

		/*int GetComponentClassID() override { return 95; }
		std::string GetComponentClassData() override
		{
			std::string data = "";
			data += "Animator:\n";
			data += "  m_GameObject: {fileID: ";
			data += std::to_string(uniqueID);
			data += "}\n";
			data += "  m_Enable: ";
			data += this->enabled;
			data += "\n";
			return data;
		}*/

		/*bool Init() override
		{
			return true;
		}*/

		void Play();

		void Play(AudioSource* audio);

		void Pause();
		void Stop();

		void Update();

		//vakue between 0-100
		void SetVolume(float vol) 
		{
			if (s_AudioEngineInstance == nullptr) return;

			vol = vol > 100 ? 100 : vol < 0 ? 0 : vol;

			s_AudioEngineInstance->setSoundVolume(vol / 100.0f);
			
		}
	};
}