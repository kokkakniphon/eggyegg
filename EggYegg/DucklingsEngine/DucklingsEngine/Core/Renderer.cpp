#include "Renderer.h"

#include "RendererManager.h"

namespace DucklingsEngine
{
	void Renderer::Init()
	{
		transform = &gameObject->GetComponent<Transform>();

		RendererManager::GetInstance().AddRenderer(this);
	}
}