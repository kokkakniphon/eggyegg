#include "Collider.h"

#include "ColliderManager.h"

namespace DucklingsEngine
{

	void Collider::Init()
	{
		transform = &gameObject->GetComponent<Transform>();

		attachedRigidbody = &gameObject->GetComponent<Rigidbody>();

		ColliderManager::GetInstance().AddCollider(this);
	}

}
