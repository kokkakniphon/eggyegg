#pragma once

#include "Vector2D.h"
#include "Rigidbody.h"
#include "Bounds.h"

namespace DucklingsEngine
{
	//
	// Summary:
	//     Parent class for collider types used with 2D gameplay.
	class Collider : public Component
	{
	private:
		//
		// Summary:
		//     Get the bounciness used by the collider.
		float bounciness;
		//
		// Summary:
		//     Get the friction used by the collider.
		float friction;
		//
		// Summary:
		//     The world space bounding area of the collider.
		Bounds bounds;

		

	public:
		Object* gameObject;

		Transform* transform = nullptr;

		std::vector<Collider*> collidedColliders;
		std::vector<Collider*> collidedTriggers;

		float overlap;

		SDL_Color gizmosColor = SDL_Color{ 0,255,0,255 };

		void Init() override;
		void Start() {}
		void Update() {}
		void FixedUpdate() {}

		bool showOutline = true;

		//
		// Summary:
		//     The density of the collider used to calculate its mass (when auto mass is enabled).
		float density;
		//
		// Summary:
		//     Is this collider configured as a trigger?
		bool isTrigger;
		//
		// Summary:
		//     The local offset of the collider geometry.
		Vector2 offset;
		//
		// Summary:
		//     The Rigidbody2D attached to the Collider2D.
		Rigidbody* attachedRigidbody = nullptr;
		//
		// Summary:
		//     Returns a point on the perimeter of this Collider that is closest to the specified
		//     position.
		//
		// Parameters:
		//   position:
		//     The position from which to find the closest point on this Collider.
		//
		// Returns:
		//     A point on the perimeter of this Collider that is closest to the specified position.
		virtual Vector2 ClosestPoint(Vector2 position) = 0;
		//
		// Summary:
		//     Check if a collider overlaps a point in space.
		//
		// Parameters:
		//   point:
		//     A point in world space.
		//
		// Returns:
		//     Does point overlap the collider?
		virtual bool OverlapPoint(Vector2 point) = 0;
		//
		// Summary:
		//     Get a list of all colliders that overlap this collider.
		//
		// Parameters:
		//
		//   collider:
		//     The target collider to calculate the overlaping.
		//
		// Returns:
		//     Does this collider overlap the target collider?
		virtual bool OverlapCollider(Collider* collider) = 0;

		virtual void StaticResolution(Collider* collider) = 0;

		virtual void DynamicResolution(Collider* collider) = 0;

		virtual void OnDrawGizmos(const Vector2 &center, const Vector2 &rect) = 0;
	};
}