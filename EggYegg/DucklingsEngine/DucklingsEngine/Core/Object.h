#pragma once

#include "ECS.h"
#include <string>
#include <vector>
#include <memory>
#include <map>
#include "Component.h"

#include <iostream>

namespace DucklingsEngine
{
	//
	//	Summary:
	//		Base class for all objects DucklingEngine can reference.
	class Object
	{
	private:
		ComponentList m_ComponentList;
		ComponentBitset m_ComponentBitset;
		std::vector<std::unique_ptr<Component>> m_Components;

		unsigned int m_UniqueID;
		unsigned int m_LocalUniqueID;
		bool m_IsDestroyed = false;
		bool m_DontDestoryOnLoad = false;

		static unsigned int s_IDseed;
		static std::map<std::string, unsigned int> s_IDlocalSeed;
		static int GenerateUniqueID(std::string sceneName) 
		{ 
			s_IDseed++;
			return ++s_IDlocalSeed[sceneName];
		}

		//
		// Summary:
		//     The local active state of this GameObject. (Read Only)
		bool m_ActiveSelf = true;
		//
		// Summary:
		//     Defines whether the GameObject is active in the Scene.
		bool m_ActiveInHierarchy = true;

	public:

		//
		// Summary:
		//     The name of the object.
		std::string name;
		//
		// Summary:
		//     The tag of the object.
		std::string tag;
		//
		// Summary:
		//     The layer the game object is in.
		std::string layer;

		//
		// Summary:
		//     The name of the scene that an object is in.
		std::string scene = "";
		
		Object(std::string scene);
		virtual ~Object() = default;

		unsigned int GetUniqueID() { return m_UniqueID; }

		unsigned int GetLocalUniqueID() { return m_LocalUniqueID; }

		static void ResetUniqueIDSeed() { s_IDseed = 0; }

		static void ResetLocalUniqueIDSeed(std::string sceneName) { s_IDlocalSeed[sceneName] = 0; }

		//
		// Summary:
		//     Add the component of Type type to the object.
		//
		// Parameters:
		//   type:
		//     The type of Component to create.
		template<typename T, typename... TArgs>
		inline T& AddComponent(TArgs&&... args)
		{
			T* comp(new T(std::forward<TArgs>(args)...));
			std::unique_ptr<Component> uptr{ comp };
			m_Components.emplace_back(std::move(uptr));

			comp->gameObject = this;
			comp->enabled = true;
			comp->uniqueID = m_LocalUniqueID;

			comp->Init();

			m_ComponentList[getComponentTypeID<T>()] = comp;
			m_ComponentBitset[getComponentTypeID<T>()] = true;
			return *comp;
		}

		//
		// Summary:
		//     Remove the component of Type type to the object.
		//
		// Parameters:
		//   type:
		//     The type of Component to dispatch.
		template<typename T>
		inline void RemoveComponent(Component* comp)
		{
			if (HasComponent<T>() == false)
				return;


			int id = comp->GetComponentClassID();

			for (size_t i = 0; i < m_Components.size(); i++)
			{
				if (m_Components[i]->uniqueID == comp->uniqueID)
				{
					std::cout << (m_Components.begin() + i + 1)->get()->GetComponentClassID() << std::endl;
					m_Components.erase(m_Components.begin() + i + 1);
					break;
				}
			}

			m_ComponentList[getComponentTypeID<T>()] = nullptr;
			m_ComponentBitset[getComponentTypeID<T>()] = false;

			for (size_t i = 0; i < m_Components.size(); i++)
			{
				if (m_Components[i]->GetComponentClassID() == id)
				{
					m_ComponentList[getComponentTypeID<T>()] = m_Components[i].get();
					m_ComponentBitset[getComponentTypeID<T>()] = true;
					break;
				}
			}

			
		}

		//
		// Summary:
		//     Returns the component of Type type if the object has one attached, null
		//     if it doesn't.
		//
		// Parameters:
		//   type:
		//     The type of Component to retrieve.
		template<typename T>
		inline T& GetComponent() const
		{
			auto ptr(m_ComponentList[getComponentTypeID<T>()]);
			return *static_cast<T*>(ptr);
		}

		inline std::vector<Component*> GetComponents()
		{
			std::vector<Component*> allComponent;
			for (size_t i = 0; i < m_Components.size(); i++)
			{
				allComponent.push_back(m_Components[i].get());
			}
			return allComponent;
		}

		//
		// Summary:
		//     Return True if the object has the component of Type type attached, false
		//		if it doesn't
		template<typename T>
		inline bool HasComponent() const { return m_ComponentBitset[getComponentTypeID<T>()]; }

		void SetActive(bool active);
		//
		// Summary:
		//     Return the local active state of this GameObject. (Read Only)
		inline bool GetActive() const { return m_ActiveSelf; }

		//
		// Summary:
		//     Return whether the GameObject is active in the Scene.
		bool GetActiveInHierarchy();

		bool IsDontDestroyOnLoad();

		inline void DontDestroyOnLoad() { m_DontDestoryOnLoad = true; }

		inline bool IsDestoryed() const { return m_IsDestroyed; }

		inline void Destroy() { m_IsDestroyed = true; }

		inline void Start() { for (auto& component : m_Components) { component->Start(); } }

		inline void Update() { for (int i = m_Components.size() - 1; i >= 0; i--) { if (m_Components[i]->enabled == true) { m_Components[i]->Update(); } } }

		inline void FixedUpdate() { for (int i = m_Components.size() - 1; i >= 0; i--) { if (m_Components[i]->enabled == true) { m_Components[i]->FixedUpdate(); } } }
		
	};

}