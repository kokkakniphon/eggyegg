#pragma once

#include <SDL.h>

#include "Singleton.h"
#include "Vector2D.h"

constexpr float MINIMUM_FPS = 30.0f;
constexpr float TARGET_FPS = 60.0f;

namespace DucklingsEngine
{
	class DuckTime : public Singleton<DuckTime>
	{
	protected:
		Uint32 newTime;
		Uint32 oldTime;

	private:

		//
		// Summary:
		//     The timeScale-independant time for this frame (Read Only). This is the time in
		//     seconds since the start of the game.
		float unscaledTime;
		//
		// Summary:
		//     The completion time in seconds since the last frame (Read Only).
		float deltaTime = 0.01f;
		//
		// Summary:
		//     The interval in seconds at which physics and other fixed frame rate updates (like
		//     MonoBehaviour's MonoBehaviour.FixedUpdate) are performed.
		float fixedDeltaTime = 0.01f;
		//
		// Summary:
		//     The time at the beginning of this frame (Read Only). This is the time in seconds
		//     since the start of the game.
		float time;

		static DuckTime* s_Instance;

	public:

		//
		// Summary:
		//     The scale at which time passes. This can be used for slow motion effects.
		float timeScale;

		//
		// Summary:
		//     Return the timeScale-independant time for this frame. This is the time in
		//     seconds since the start of the game.
		inline static float GetUnscaledTime() { return DuckTime::GetInstance().unscaledTime; }
		//
		// Summary:
		//     Return the completion time in seconds since the last frame.
		inline static float GetDeltaTime() { return DuckTime::GetInstance().deltaTime * DuckTime::GetInstance().timeScale; }
		//
		// Summary:
		//     Return the interval in seconds at which physics and other fixed frame rate updates (like
		//     MonoBehaviour's MonoBehaviour.FixedUpdate) are performed.
		inline static float GetFixedDeltaTime() { return DuckTime::GetInstance().fixedDeltaTime * DuckTime::GetInstance().timeScale; }
		//
		// Summary:
		//     Return the time at the beginning of this frame. This is the time in seconds
		//     since the start of the game.
		inline static float GetTime() { return DuckTime::GetInstance().time; }

		DuckTime();

		//
		// Summary:
		//     Update the deltaTime, time, unscaledTime.
		void Tick(int fixedTimeDivision);
	};
}