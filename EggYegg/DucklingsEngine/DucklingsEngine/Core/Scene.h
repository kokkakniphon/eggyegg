#pragma once

#include <string>
#include <vector>
#include <iostream>

#include "Object.h"

namespace DucklingsEngine
{
	struct Scene
	{
	private:
		
		//
		// Summary:
		//     Return the index of the Scene in the Build Settings.
		int buildIndex;

		//
		// Summary:
		//     An array of game objects.
		std::vector<std::shared_ptr<Object>> rootGameObjects;

	public:
		Scene() { isLoaded = false; }
		virtual ~Scene() = default;

		std::string path;

		//
		// Summary:
		//     Returns the name of the Scene that is currently active in the game or app.
		std::string name;
		//
		// Summary:
		//     Returns true if the Scene is loaded.
		bool isLoaded;

		bool isActive = true;

		//
		// Summary:
		//     Returns true if the Scene is loaded.
		inline bool IsLoaded() { return isLoaded; }
		//
		// Summary:
		//     Return the index of the Scene in the Build Settings.
		inline int GetBuildIndex() { return buildIndex; }
		//
		// Summary:
		//     Set the index of the Scene in the Build Settings.
		inline void SetBuildIndex(int buildIndex) { this->buildIndex = buildIndex; }

		//
		// Summary:
		//     Loads the Scene by its name or index in Build Settings.
		virtual void LoadScene() = 0 { isLoaded = true; }
		//
		// Summary:
		//     Destroys all GameObjects associated with the given Scene and removes the Scene
		//     from the SceneManager.
		//
		// Returns:
		//     Returns true if the Scene is unloaded.
		virtual bool UnloadScene() = 0 { isLoaded = false; return true; }

		//
		// Summary:
		//     Returns all the root game objects in the Scene.
		//
		// Returns:
		//     An array of game objects.
		std::vector<std::shared_ptr<Object>>* GetRootsGameObject() { return &rootGameObjects; }

	};
}