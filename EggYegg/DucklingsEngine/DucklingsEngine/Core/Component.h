#pragma once

#include <iostream>

namespace DucklingsEngine
{
	class Component
	{
	public:
		Component() = default;
		virtual ~Component() = default;

		//
		// Summary:
		//     Enabled Component are Updated, disabled Component are not.
		bool enabled = true;

		//
		// Summary:
		//     The unique id of the object that have this transform.
		unsigned int uniqueID;

		virtual int GetComponentClassID() { return 0; }
		virtual std::string GetComponentClassData() { return ""; }

		virtual void Init() {}
		virtual void Start() = 0 {}
		virtual void Update() = 0 {}
		virtual void FixedUpdate() = 0 {}
	};
}