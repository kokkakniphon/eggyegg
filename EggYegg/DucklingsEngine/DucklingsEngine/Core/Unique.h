#pragma once

namespace DucklingsEngine
{
	class Unique
	{
	private:
		unsigned int uniqueID;

		static unsigned int IDseed;
		static int GenerateUniqueID() { IDseed++; return IDseed; }

	public:
		Unique() { uniqueID = GenerateUniqueID(); }
		virtual ~Unique() = default;

		unsigned int GetUniqueID() { return uniqueID; }
	};
}

