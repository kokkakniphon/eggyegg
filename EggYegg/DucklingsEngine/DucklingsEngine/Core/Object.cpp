#include "Object.h"
#include "Transform.h"
namespace DucklingsEngine
{
	unsigned int Object::s_IDseed = 0;
	std::map<std::string, unsigned int> Object::s_IDlocalSeed;

	Object::Object(std::string scene)
	{
		this->scene = scene;
		m_LocalUniqueID = GenerateUniqueID(scene);
		m_UniqueID = s_IDseed;
		m_IsDestroyed = false;
		this->AddComponent<Transform>();
	}

	void Object::SetActive(bool active)
	{
		m_ActiveSelf = active;

		m_ActiveInHierarchy = active;
	}

	bool Object::GetActiveInHierarchy()
	{
		if (m_ActiveSelf && this->GetComponent<Transform>().GetParent() != &this->GetComponent<Transform>())
			m_ActiveInHierarchy = this->GetComponent<Transform>().GetParent()->gameObject->GetActiveInHierarchy();

		return m_ActiveInHierarchy;
	}
	bool Object::IsDontDestroyOnLoad()
	{
		if (m_DontDestoryOnLoad)
			return true;
		if (this->GetComponent<Transform>().GetParent() != &this->GetComponent<Transform>())
			return this->GetComponent<Transform>().GetParent()->gameObject->IsDontDestroyOnLoad();

		return false;
	}
}