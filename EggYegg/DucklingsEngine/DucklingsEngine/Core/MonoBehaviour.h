#pragma once

#include "Component.h"
#include "Transform.h"
#include "Object.h"

namespace DucklingsEngine
{
	class MonoBehaviour : public Component
	{
	public:
		Object* gameObject;
		Transform* transform = nullptr;

		void Init() override { transform = &gameObject->GetComponent<Transform>(); }
		void Draw() {}
		void FixedUpdate() {}
	};
}