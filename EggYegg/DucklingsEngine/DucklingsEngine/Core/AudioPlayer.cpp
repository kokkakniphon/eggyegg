#include "AudioPlayer.h"

namespace KHORA {
	
	AudioPlayer::AudioPlayer()
	{
		s_AudioEngineInstance = irrklang::createIrrKlangDevice();
	}

	AudioPlayer::AudioPlayer(std::string name)
	{
		s_AudioEngineInstance = irrklang::createIrrKlangDevice();
		controllerName = name;
		SetupController();
	}

	void AudioPlayer::SetupController()
	{
		if (controllerName != "")
		{
			controller = std::make_shared<AudioController>();
			controller->name = controllerName;
			std::cout << "Setup Controller: " << controllerName << std::endl;
			FileManager::LoadYAMLFile(controller.get());
		}
	}


	void AudioPlayer::SetAudioControllerPath(std::string name)
	{
		controllerName = name;
		SetupController();
	}

	void AudioPlayer::SetAudioController(AudioController * controller)
	{
		this->controller.reset(controller);
	}

	void AudioPlayer::Play()
	{
		if (controller == nullptr || s_AudioEngineInstance == nullptr || controller->audioNodes.size() <= 0)
			return;

		Stop();

		currentAudioStateName = controller->audioNodes.begin()->first;
		currentAudio = (controller->audioNodes[0].second).get();

		std::string destinationNode = controller->GetDestinationAudioNode(currentAudioStateName);
		if (currentAudioStateName != destinationNode && destinationNode != "")
		{
			currentAudioStateName = destinationNode;
			bool isLoop = controller->GetAudioNode(currentAudioStateName)->isLoopable;
			currentAudio = controller->GetAudioNode(currentAudioStateName);
			sound = s_AudioEngineInstance->play2D((ASSET_AUDIO_PATH + controller->GetAudioNode(currentAudioStateName)->filePath).c_str(), isLoop, false, true);
			sound->setVolume(controller->GetAudioNode(currentAudioStateName)->volume * (globalVolume / 100.0f));
		}

		play = true;
	}

	void AudioPlayer::Play(AudioSource * audio)
	{
		if (s_AudioEngineInstance == nullptr)
			return;

		Stop();

		sound = s_AudioEngineInstance->play2D((ASSET_AUDIO_PATH + audio->filePath).c_str(), audio->isLoopable, false, true);
		sound->setVolume(audio->volume * (globalVolume / 100.0f));

		play = true;
	}

	void AudioPlayer::Pause()
	{
		if (!pause)
		{
			pause = true;
			sound->setIsPaused(pause);
		}
	}

	void AudioPlayer::Stop()
	{
		if (sound == nullptr)
			return;

		sound->stop();
		sound->drop();

		sound = nullptr;

		play = false;
	}

	void AudioPlayer::Update()
	{
		if (controller == nullptr || s_AudioEngineInstance == nullptr || play == false)
			return;

		if (pause)
		{
			pause = false;
			sound->setIsPaused(pause);
		}

		std::string destinationNode = controller->GetDestinationAudioNode(currentAudioStateName);
		if (currentAudioStateName != destinationNode && destinationNode != "")
		{
			currentAudioStateName = destinationNode;
			bool isLoop = controller->GetAudioNode(currentAudioStateName)->isLoopable;
			currentAudio = controller->GetAudioNode(currentAudioStateName);

			Stop();
			play = true;

			sound = s_AudioEngineInstance->play2D((ASSET_AUDIO_PATH + controller->GetAudioNode(currentAudioStateName)->filePath).c_str(), isLoop, false, true);
			sound->setVolume(controller->GetAudioNode(currentAudioStateName)->volume * (globalVolume / 100.0f));
		}
	}

};