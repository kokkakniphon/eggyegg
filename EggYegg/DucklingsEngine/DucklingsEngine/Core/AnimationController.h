#pragma once

#include <map>
#include <string>
#include <vector>
#include "Sprite.h"
#include "Unique.h"

namespace DucklingsEngine
{
	enum ParamiterType
	{
		Interger,
		Boolean
	};

	struct Paramiter
	{
	public:
		Paramiter(std::string name, ParamiterType type = Interger, bool conditionBool = false, bool conditionInt = false, bool checkValueBool = false, bool isGreaterThan = false, int checkValueInt = 0)
		{
			this->name = name;
			this->type = type;
			this->conditionBool = conditionBool;
			this->conditionInt = conditionInt;
			this->isGreaterThan = isGreaterThan;
			this->checkValueBool = checkValueBool;
			this->checkValueInt = checkValueInt;
			this->currentValueBool = false;
		}

		void SetConditionBool(bool checkValueBool)
		{
			this->conditionBool = true;
			this->conditionInt = false;
			this->isGreaterThan = false;
			this->checkValueBool = checkValueBool;
			this->checkValueInt = 0;
		}

		void SetConditionInt(bool isGreaterThan, bool checkValueInt)
		{
			this->conditionBool = false;
			this->conditionInt = true;
			this->isGreaterThan = isGreaterThan;
			this->checkValueBool = false;
			this->checkValueInt = checkValueInt;
		}

		bool CheckParamiterCondition()
		{
			if (conditionBool == true)
			{
				return (checkValueBool == currentValueBool);
			}
			else if (conditionInt == true)
			{
				if (isGreaterThan) return (currentValueInt > checkValueInt);
				else return (currentValueInt < checkValueInt);
			}
		}

		std::string name = "";
		ParamiterType type;
		bool conditionBool = false;
		bool conditionInt = false;
		bool isGreaterThan = false;
		bool checkValueBool = false;
		int checkValueInt = 0;
		bool currentValueBool = false;
		int currentValueInt = 0;
	};

	struct Animation
	{
		float animationStartTime = 0;

		std::string name = "";

		std::map<std::string, std::vector<Paramiter>> destinations;

		Sprite* sprites;

		int framePerSecond = 1;

		Animation(std::string name, Sprite* sprite, int framePerSecond = 1)
		{
			this->name = name;
			this->sprites = sprite;
			this->framePerSecond = framePerSecond;
		}

		//
		// Summary:
		//     Return "" if no paramiters meet destination conditions. else return destination.
		std::string CheckAllParamiterCondition()
		{
			for (std::map<std::string, std::vector<Paramiter>>::iterator condition = destinations.begin(); condition != destinations.end(); condition++)
			{
				bool isPass = true;
				for (unsigned int i = 0; i < condition->second.size(); i++)
				{
					if (condition->second[i].CheckParamiterCondition() == false)
					{
						isPass = false;
						break;
					}
				}

				if (condition->second.size() == 0 && (int)((DuckTime::GetTime() - animationStartTime) * framePerSecond) <= sprites->spriteSheetCellCount)
				{
					isPass = false;
				}

				if (isPass == true)
				{
					return condition->first;
				}
			}

			return "";
		}

		//
		// Summary:
		//     Sets the value of the given boolean parameter.
		//
		// Parameters:
		//   name:
		//     The parameter name.
		//
		//   id:
		//     The parameter ID.
		//
		//   value:
		//     The new parameter value.
		void SetBool(std::string name, bool value)
		{
			for (std::map<std::string, std::vector<Paramiter>>::iterator condition = destinations.begin(); condition != destinations.end(); condition++)
			{
				for (unsigned int i = 0; i < condition->second.size(); i++)
				{
					if (condition->second[i].name == name)
					{
						condition->second[i].currentValueBool = value;
					}
				}
			}
		}

		//
		// Summary:
		//     Sets the value of the given integer parameter.
		//
		// Parameters:
		//   name:
		//     The parameter name.
		//
		//   id:
		//     The parameter ID.
		//
		//   value:
		//     The new parameter value.
		void SetInteger(std::string name, int value)
		{
			for (std::map<std::string, std::vector<Paramiter>>::iterator condition = destinations.begin(); condition != destinations.end(); condition++)
			{
				for (unsigned int i = 0; i < condition->second.size(); i++)
				{
					if (condition->second[i].name == name)
					{
						condition->second[i].currentValueInt = value;
					}
				}
			}
		}
	};


	//
	// Summary:
	//     This script only contain the data for the Animator to use.
	struct AnimationController : Unique
	{
	public:

		std::map<std::string, std::shared_ptr<Animation>> animations;

		std::vector<Paramiter> paramiters;

		//
		// Summary:
		//     Return "" if animations can't be found in animations. else return destination.
		std::string GetDestinationAnimation(std::string currentAnimation)
		{
			for (std::map<std::string, std::shared_ptr<Animation>>::iterator animation = animations.begin(); animation != animations.end(); animation++)
			{
				if (animation->first == currentAnimation)
				{
					return animation->second->CheckAllParamiterCondition();
				}
			}
			return "";
		}

		Animation* GetAnimation(std::string targetAnimation)
		{
			for (std::map<std::string, std::shared_ptr<Animation>>::iterator animation = animations.begin(); animation != animations.end(); animation++)
			{
				if (animation->first == targetAnimation)
				{
					return animation->second.get();
				}
			}
			return nullptr;
		}

		Sprite* GetAnimationSprite(std::string targetAnimation)
		{
			for (std::map<std::string, std::shared_ptr<Animation>>::iterator animation = animations.begin(); animation != animations.end(); animation++)
			{
				if (animation->first == targetAnimation)
				{
					return animation->second->sprites;
				}
			}
			return nullptr;
		}

		//
		// Summary:
		//     Returns the value of the given boolean parameter.
		//
		// Parameters:
		//   name:
		//     The parameter name.
		//
		//   id:
		//     The parameter ID.
		//
		// Returns:
		//     The value of the parameter.
		bool GetBool(std::string name)
		{
			for (unsigned int i = 0; i < paramiters.size(); i++)
			{
				if (paramiters[i].name == name)
				{
					return paramiters[i].currentValueBool;
				}
			}
			return false;
		}

		//
		// Summary:
		//     Returns the value of the given integer parameter.
		//
		// Parameters:
		//   name:
		//     The parameter name.
		//
		//   id:
		//     The parameter ID.
		//
		// Returns:
		//     The value of the parameter.
		int GetInteger(std::string name)
		{
			for (unsigned int i = 0; i < paramiters.size(); i++)
			{
				if (paramiters[i].name == name)
				{
					return paramiters[i].currentValueInt;
				}
			}
			return 0;
		}

		//
		// Summary:
		//     Sets the value of the given boolean parameter.
		//
		// Parameters:
		//   name:
		//     The parameter name.
		//
		//   id:
		//     The parameter ID.
		//
		//   value:
		//     The new parameter value.
		void SetBool(std::string name, bool value)
		{
			for (unsigned int i = 0; i < paramiters.size(); i++)
			{
				if (paramiters[i].name == name)
				{
					paramiters[i].currentValueBool = value;
				}
			}

			for (std::map<std::string, std::shared_ptr<Animation>>::iterator animation = animations.begin(); animation != animations.end(); animation++)
			{
				animation->second->SetBool(name, value);
			}
		}

		//
		// Summary:
		//     Sets the value of the given integer parameter.
		//
		// Parameters:
		//   name:
		//     The parameter name.
		//
		//   id:
		//     The parameter ID.
		//
		//   value:
		//     The new parameter value.
		void SetInteger(std::string name, int value)
		{
			for (unsigned int i = 0; i < paramiters.size(); i++)
			{
				if (paramiters[i].name == name)
				{
					paramiters[i].currentValueInt = value;
				}
			}

			for (std::map<std::string, std::shared_ptr<Animation>>::iterator animation = animations.begin(); animation != animations.end(); animation++)
			{
				animation->second->SetInteger(name, value);
			}
		}

	};

	

	

	
}


