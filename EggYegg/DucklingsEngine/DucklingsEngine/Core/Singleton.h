#pragma once
#include <iostream>
namespace DucklingsEngine
{
	template <typename T>
	class Singleton
	{
	private:
		static T* s_Instance;

	public:

		inline static T& GetInstance()
		{
			if (s_Instance == nullptr)
			{
				s_Instance = new T();
			}
			return *s_Instance;
		}

		inline void Clean() { delete s_Instance; }
	};

	template <typename T>
	T* Singleton<T>::s_Instance = nullptr;
}

