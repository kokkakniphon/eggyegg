#pragma once

#include <string>

#include "Object.h"
#include "Transform.h"

namespace DucklingsEngine
{
	class Renderer : public Component
	{
	public:
		Object* gameObject;

		Transform* transform = nullptr;

		int order = 0;

		void Init() override;
		void Start() {}
		void Update() {}
		void FixedUpdate() {}

		virtual void Draw(const Vector2 &center, const Vector2 &rect) = 0;
	};
}