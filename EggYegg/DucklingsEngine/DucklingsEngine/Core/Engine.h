#pragma once

#include "SDL.h"
#include "SDL_image.h"
#include "SDL_ttf.h"

#include "Singleton.h"

#include "DuckTime.h"
#include "SceneManager.h"
#include "EntityManager.h"
#include "AssetManager.h"
#include "ColliderManager.h"
#include "SpriteManager.h"
#include "RendererManager.h"

#include <iostream>


namespace DucklingsEngine
{
	constexpr float SCREEN_WIDTH = 1920;
	constexpr float SCREEN_HEIGHT = 1080;
	const Vector2 SCREEN_SIZE = { SCREEN_WIDTH, SCREEN_HEIGHT };
	constexpr SDL_Color DARK = { 30, 30, 30, 255 };

	constexpr int PHYSIC_LOOP_CALULATION = 4;

	class Engine : public Singleton<Engine>
	{
	private:

		bool m_IsRunning;

		SDL_Color m_ClearColor;
		SDL_Window* m_Window;
		SDL_Renderer* m_Renderer;
		EntityManager* m_Manager;

		bool m_WindowEnter = true;

		const char* m_WindowTitle = "DucklingsEngine";

		bool m_EditorMode = true;
		bool m_DrawGizmos = false;

		void Start();
		void Update();
		void FixedUpdate();
		void Render();
		void Events();

	public:
		Engine();
		~Engine();

		bool Init();
		void Run();
		void Quit();
		void Clean();
		
		float m_lastTimeScale;
		static SDL_Event event;

		inline bool IsRunning() { return m_IsRunning; }
		inline SDL_Window* GetWindow() { return m_Window; }
		inline SDL_Renderer* GetRenderer() { return m_Renderer; }
		inline EntityManager* GetManager() { return m_Manager; }
		inline void SetGizmos(bool enable) { m_DrawGizmos = enable; }
		inline bool GetGizmos() { return m_DrawGizmos; }
	};
}
