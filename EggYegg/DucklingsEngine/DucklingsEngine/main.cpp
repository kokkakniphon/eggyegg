
#include <iostream>
#include "Engine.h"
#include "Editor.h"
#include <time.h>
#include "Scripts/GM.h"

int main(int argc, char** argv)
{
	_CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF);

	std::srand(time(NULL));

#ifdef _DEBUG
	DucklingsEngine::Editor::GetInstance().Run();
#else
	DucklingsEngine::Engine::GetInstance().Run();
#endif // _DEBUG

	DucklingsEngine::GM::GetInstance().Clean();

	return 0;
}
