This is item template for creating new scene, script. (I create this item-template specifically for DucklingsEngine)

Create new folder called "DucklingsEngine"
in the path: C:\Users\user\Documents\Visual Studio 2017\Templates\ItemTemplates

Then add "NewScene.zip", "NewAnimationController.zip", and "NewScript.zip" to the created folder (DucklingsEngine)

**If you're using other version of Visual Studio, then just change the version number to the one that you're using.**